package com.igrand.ThreeWaySolutions.Client;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


    public static final String BASE_URL1 = "http://www.igranddeveloper.xyz/3way-services/services/";

    //http://igrandit.site/3way-services/services/"

    private static Retrofit retrofit = null;
    public static Retrofit getClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                             .connectTimeout(5, TimeUnit.MINUTES)
                             .readTimeout(5, TimeUnit.MINUTES)
                             .writeTimeout(5, TimeUnit.MINUTES)
                             .addInterceptor(interceptor)
                             .build();

        retrofit = new Retrofit.Builder().baseUrl(BASE_URL1).addConverterFactory(GsonConverterFactory.create()).client(client).build();
        return retrofit;

    }
}
