package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddProjectEstimation;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddSubWorkTypeAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DistrictAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterSubworkWorkList extends RecyclerView.Adapter<RecyclerAdapterSubworkWorkList.ViewHolder>{

Context context;
    List<AdminWorkTypeList.DataBean> dataBeans;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    TextView cityname;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid;
    AddSubWorkTypeAdmin subWorkTypeAdmin;
    List<AdminActiveStateList.DataBean> dataBeans1;




    public RecyclerAdapterSubworkWorkList(AddSubWorkTypeAdmin addSubWorkTypeAdmin, List<AdminWorkTypeList.DataBean> dataBeans, TextView cityname, Dialog dialog, AddSubWorkTypeAdmin subWorkTypeAdmin) {

        this.context=addSubWorkTypeAdmin;
        this.dataBeans=dataBeans;
        this.cityname=cityname;
        this.dialog=dialog;
        this.subWorkTypeAdmin=subWorkTypeAdmin;
    }




    @NonNull
    @Override
    public RecyclerAdapterSubworkWorkList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterSubworkWorkList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        holder.radio.setChecked(position == mSelectedItem);
        holder.radio.setText(dataBeans.get(position).getWork_name());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                cityname.setText(selectedwork);

            }
        });


        subWorkTypeAdmin.getId(selectedworkid);





    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    selectedwork=dataBeans.get(mSelectedItem).getWork_name();
                    selectedworkid=dataBeans.get(mSelectedItem).getId();

                }
            };
            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
