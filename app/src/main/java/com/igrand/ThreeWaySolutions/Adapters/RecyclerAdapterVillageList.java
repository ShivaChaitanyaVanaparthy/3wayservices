package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MandalList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminMandalList;
import com.igrand.ThreeWaySolutions.Response.AdminVillageList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterVillageList extends RecyclerView.Adapter<RecyclerAdapterVillageList.Holder> {
    List<AdminVillageList.DataBean> dataBeans;
    Context context;







    public RecyclerAdapterVillageList(VillageList villageList, List<AdminVillageList.DataBean> dataBeans) {

        this.context=villageList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterVillageList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_vill, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterVillageList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.cityname.setText(dataBeans.get(position).getState_name());
        holder.dstname.setText(dataBeans.get(position).getDistrict_name());
        holder.mandal.setText(dataBeans.get(position).getMandal_name());
        holder.village.setText(dataBeans.get(position).getVillage_name());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }



    }

    @Override
    public int getItemCount() {
        
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,cityname,status,dstname,mandal,village;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            cityname=itemView.findViewById(R.id.cityname);
            status=itemView.findViewById(R.id.status);
            dstname=itemView.findViewById(R.id.dstname);
            mandal=itemView.findViewById(R.id.mandal);
            village=itemView.findViewById(R.id.village);
        }
    }
}
