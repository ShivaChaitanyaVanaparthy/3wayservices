package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.SupplierReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ViewReports;
import com.igrand.ThreeWaySolutions.Activities.MaterialReportListDate;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.MaterialListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementSheetResponse;
import com.igrand.ThreeWaySolutions.Response.SupplierReportResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterMeasurementReport extends RecyclerView.Adapter<RecyclerAdapterMeasurementReport.ViewHolder> {

Context context;
    List<MeasurementListResponse.DataBean> dataBeans;





    public RecyclerAdapterMeasurementReport(MeasurementReportListDate measurementReportListDate, List<MeasurementListResponse.DataBean> dataBeans) {

        this.context=measurementReportListDate;
        this.dataBeans=dataBeans;
    }

    public RecyclerAdapterMeasurementReport(ViewReports viewReports, List<MeasurementListResponse.DataBean> dataBeans) {
        this.context=viewReports;
        this.dataBeans=dataBeans;
    }


    @NonNull
    @Override
    public RecyclerAdapterMeasurementReport.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_measurement, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMeasurementReport.ViewHolder holder, final int position) {

        if(dataBeans!=null){

            holder.work.setText(dataBeans.get(position).getWork_name());
            holder.subwork.setText(dataBeans.get(position).getSubwork_name());
            holder.bill.setText(dataBeans.get(position).getBill_no());
            holder.name.setText(dataBeans.get(position).getPerson_name());
            holder.length.setText(dataBeans.get(position).getLength());
            holder.width.setText(dataBeans.get(position).getWidth());
            holder.depth.setText(dataBeans.get(position).getDepth());
            holder.quantity.setText(dataBeans.get(position).getQuantity());
            holder.uom.setText(dataBeans.get(position).getUom());
            holder.date.setText(dataBeans.get(position).getDate());


        }



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView work,subwork,no,length,width,depth,quantity,uom,bill,name,date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            work=itemView.findViewById(R.id.work);
            subwork=itemView.findViewById(R.id.subwork);
            no=itemView.findViewById(R.id.no);
            length=itemView.findViewById(R.id.length);
            width=itemView.findViewById(R.id.width);
            depth=itemView.findViewById(R.id.depth);
            quantity=itemView.findViewById(R.id.quantity);
            uom=itemView.findViewById(R.id.uom);
            bill=itemView.findViewById(R.id.bill);
            name=itemView.findViewById(R.id.name);
            date=itemView.findViewById(R.id.date);
        }
    }
}
