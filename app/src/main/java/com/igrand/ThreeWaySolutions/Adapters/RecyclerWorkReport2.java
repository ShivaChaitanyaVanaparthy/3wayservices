package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.EngagerReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.EngagerReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkReports;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;
import com.igrand.ThreeWaySolutions.Response.EngagerReportLIst;
import com.igrand.ThreeWaySolutions.Response.WorkReportLIst;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerWorkReport2 extends RecyclerView.Adapter<RecyclerWorkReport2.ViewHolder>{

   Context context;
    List<AdminWorkReportList.DataBean> dataBeans;
    List<List<EngagerReportLIst.DataBean>> dataBeans1;
    List<Integer> amount;



    public RecyclerWorkReport2(EngagerReports engagerReports, List<List<EngagerReportLIst.DataBean>> dataBeans) {

        this.context=engagerReports;
        this.dataBeans1=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerWorkReport2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_workreport1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerWorkReport2.ViewHolder holder, final int position) {



 if(dataBeans1!=null){

            holder.projectname.setText(dataBeans1.get(position).get(0).getProject_name());
            holder.date.setText(dataBeans1.get(position).get(0).getDate());


        }

        holder.amountt.setVisibility(View.GONE);


 holder.viewDetails.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View view) {
         Intent intent=new Intent(context, EngagerReportListDate.class);
         intent.putExtra("ID",dataBeans1.get(position).get(0).getProject_id());
         intent.putExtra("Date",dataBeans1.get(position).get(0).getDate());
         context.startActivity(intent);
     }
 });







    }

    @Override
    public int getItemCount() {

        return dataBeans1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView projectname,date,amount;
        LinearLayout viewDetails,amountt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            projectname=itemView.findViewById(R.id.projectname);
            date=itemView.findViewById(R.id.date);
            amount=itemView.findViewById(R.id.amount);
            viewDetails=itemView.findViewById(R.id.viewDetails);
            amountt=itemView.findViewById(R.id.amountt);

        }
    }
}
