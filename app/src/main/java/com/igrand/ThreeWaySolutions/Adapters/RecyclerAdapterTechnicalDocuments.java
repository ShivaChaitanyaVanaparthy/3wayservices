package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.CheckingDocList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.MarketingDocList;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.TechDocList;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.WebPdfView;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.TechDocResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterTechnicalDocuments extends RecyclerView.Adapter<RecyclerAdapterTechnicalDocuments.ViewHolder>{

Context context;
    String document1,document2;
    String Document;
    private static final String TAG = "user1";
    String pdfFileName;
    Integer pageNumber = 0;
    List<String> strings = new ArrayList<>();
    List<String> strings1 = new ArrayList<>();
    String[] documentx;
    List<String> output;
    LeadDetailsAgent leadDetailsAgent;
    LeadDetailsSubAgent leadDetailsSubAgent;
    String key,keyadmin;
    LeadDetailsAdmin leadDetailsAdmin;
    LeadDetailsProcurementAdmin leadDetailsProcurementAdmin;
    TechDocList techDocList;
    String document;
    List<TechDocResponse.DataBean> dataBeans;
    ArrayList<String> list=new ArrayList<>();
    String marketing;



    public RecyclerAdapterTechnicalDocuments(TechDocList techDocList, List<TechDocResponse.DataBean> dataBeans,String technical) {

        this.context=techDocList;
        this.dataBeans=dataBeans;
        this.marketing=technical;

    }

    public RecyclerAdapterTechnicalDocuments(CheckingDocList checkingDocList, List<TechDocResponse.DataBean> dataBeans, String checking) {
        this.context=checkingDocList;
        this.dataBeans=dataBeans;
        this.marketing=checking;
    }

    public RecyclerAdapterTechnicalDocuments(MarketingDocList marketingDocList, List<TechDocResponse.DataBean> dataBeans, String marketing) {

        this.context=marketingDocList;
        this.dataBeans=dataBeans;
        this.marketing=marketing;
    }


    @NonNull
    @Override
    public RecyclerAdapterTechnicalDocuments.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_techdocument, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterTechnicalDocuments.ViewHolder holder, final int position) {


        if(dataBeans.get(position).getDocument_image().equals("")){

            holder.linearimg.setVisibility(View.GONE);

            if(marketing.equals("Technical")){
                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });


                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){

                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {

                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument();

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

                            /*Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                            /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/


                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);
                            /*Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                        }




                    }
                });



            }

            if(marketing.equals("Checking")){

                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/checking_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });


                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){
                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {
                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/checking_doc/"+ dataBeans.get(position).getDocument();
                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

                            /*Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                          /*  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/

                          /*  Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);

                        }




                    }
                });



            }

            if(marketing.equals("Marketing")){

                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/marketing_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/marketing_doc/"+ dataBeans.get(position).getDocument_image();
                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });





                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){

                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {

                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/marketing_doc/"+ dataBeans.get(position).getDocument();

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/
                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                            /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/

                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/


                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);
                        }




                    }
                });




            }

            if(marketing.equals("technical1")){

                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });





                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){

                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {


                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument();

                            /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/
                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);

                        }




                    }
                });




            }


        }

       else {

            if(marketing.equals("Technical")){
                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });


                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){

                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {

                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument();

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

//                            Intent intent=new Intent(context, WebPdfView.class);
//                            intent.putExtra("PDF",document2);
//                            context.startActivity(intent);

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/

                          /*  Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);
                        }




                    }
                });



            }

            if(marketing.equals("Checking")){

                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/checking_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });


                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){
                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {
                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/checking_doc/"+ dataBeans.get(position).getDocument();
                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/
                            /*Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/

                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);

                        }




                    }
                });



            }

            if(marketing.equals("Marketing")){

                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/marketing_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/marketing_doc/"+ dataBeans.get(position).getDocument_image();
                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });





                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){

                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {

                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/marketing_doc/"+ dataBeans.get(position).getDocument();

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                            /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/

                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/


                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);

                        }




                    }
                });




            }

            if(marketing.equals("technical1")){

                holder.imgtext.setText(dataBeans.get(position).getImage_description());
                holder.pdftext.setText(dataBeans.get(position).getDoc_description());
                holder.pdf.setImageResource(R.drawable.imagepdf);
                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument_image();
                list.add(document1);
                Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument_image();
                        Intent intent = new Intent(context, ProfileImage.class);
                        intent.putExtra("Image",document1);
                        intent.putExtra("List",list);
                        context.startActivity(intent);
                    }
                });





                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(dataBeans.get(position).getDocument().equals("")){

                            Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                        } else {

                            document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/techenical_doc/"+ dataBeans.get(position).getDocument();

                            /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

                            /*Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/

                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                            context.startActivity(browserIntent);*/
                           /* Intent intent=new Intent(context, WebPdfView.class);
                            intent.putExtra("PDF",document2);
                            context.startActivity(intent);*/


                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                            context.startActivity(intent);

                        }




                    }
                });




            }
        }
    }




    @Override
    public int getItemCount() {



        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img,pdf;
        TextView imgtext,pdftext;
        LinearLayout linearimg;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img=itemView.findViewById(R.id.img);
            pdf=itemView.findViewById(R.id.pdf);
            imgtext=itemView.findViewById(R.id.imgtxt);
            pdftext=itemView.findViewById(R.id.pdftxt);
            linearimg=itemView.findViewById(R.id.linearimg);

        }
    }
}
