package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.LeadDetailsLegal;
import com.igrand.ThreeWaySolutions.Activities.LESION.LeadDetailsLesion;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearByProjectsList;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjectsDetails;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Activities.SURVEY.LeadDetailsSurvey;
import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterCheckingDocuments extends RecyclerView.Adapter<RecyclerAdapterCheckingDocuments.ViewHolder>{

Context context;
    String[] document;
    String document1,document2,document3;




    public RecyclerAdapterCheckingDocuments(LeadDetailsMarketing leadDetailsMarketing, String[] document, String document2) {

        this.context=leadDetailsMarketing;
        this.document=document;
    }

    public RecyclerAdapterCheckingDocuments(LeadDetailsChecking leadDetailsChecking, String[] document, String document2) {

        this.context=leadDetailsChecking;
        this.document=document;
    }

    public RecyclerAdapterCheckingDocuments(LeadDetailsProcurement leadDetailsProcurement, String[] document, String document2) {

        this.context=leadDetailsProcurement;
        this.document=document;
    }

    public RecyclerAdapterCheckingDocuments(LeadDetailsLegal leadDetailsLegal, String[] document, String document2) {

        this.context=leadDetailsLegal;
        this.document=document;
    }

    public RecyclerAdapterCheckingDocuments(LeadDetailsSurvey leadDetailsSurvey, String[] document, String document2) {

        this.context=leadDetailsSurvey;
        this.document=document;
    }

    public RecyclerAdapterCheckingDocuments(LeadDetailsLesion leadDetailsLesion, String[] document, String document2) {

        this.context=leadDetailsLesion;
        this.document=document;
    }



    public RecyclerAdapterCheckingDocuments(LeadDetailsAdmin leadDetailsAdmin, String[] document, String document2) {

        this.context=leadDetailsAdmin;
        this.document=document;
    }

    public RecyclerAdapterCheckingDocuments(NearbyProjectsDetails nearbyProjectsDetails, String[] document, String document2) {
        this.context=nearbyProjectsDetails;
        this.document=document;
    }


    @NonNull
    @Override
    public RecyclerAdapterCheckingDocuments.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterCheckingDocuments.ViewHolder holder, int position) {



            document1="http://igrandit.site/3way-services/admin_assets/uploads/leads/documents/"+document[position];
            if(document1!=null){

                Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);
            }

        document2="http://igrandit.site/3way-services/admin_assets/uploads/near_by_projects/brochures/"+document[position];
            if(document2!=null){

                Picasso.get().load(document2).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);

            }

 document3="http://igrandit.site/3way-services/admin_assets/uploads/near_by_projects/images/"+document[position];
            if(document3!=null){

                Picasso.get().load(document3).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);

            }






        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        holder.itemView.buildDrawingCache();
                        Bitmap bitmap = holder.itemView.getDrawingCache();
                        Intent intent = new Intent(context, ProfileImage.class);
                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        context.startActivity(intent);
                    }
                });
            }


    @Override
    public int getItemCount() {
        return document.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            image=itemView.findViewById(R.id.image);



        }
    }
}
