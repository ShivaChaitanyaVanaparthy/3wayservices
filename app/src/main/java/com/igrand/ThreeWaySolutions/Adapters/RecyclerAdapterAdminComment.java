package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NearByProjectsList1;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsProcurementAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterAdminComment extends RecyclerView.Adapter<RecyclerAdapterAdminComment.ViewHolder>{

Context context;
    List<AgentLeadsCommentsResponse.DataBean> dataBeans;



    public RecyclerAdapterAdminComment(LeadDetailsAdmin applicationContext, List<AgentLeadsCommentsResponse.DataBean> dataBeans) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;

    }

    public RecyclerAdapterAdminComment(LeadDetailsProcurementAdmin leadDetailsProcurementAdmin, List<AgentLeadsCommentsResponse.DataBean> dataBeans) {

        this.context=leadDetailsProcurementAdmin;
        this.dataBeans=dataBeans;
    }

    public RecyclerAdapterAdminComment(LeadDetailsProcurementAgent leadDetailsProcurementAgent, List<AgentLeadsCommentsResponse.DataBean> dataBeans) {

        this.context=leadDetailsProcurementAgent;
        this.dataBeans=dataBeans;
    }


    @NonNull
    @Override
    public RecyclerAdapterAdminComment.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterAdminComment.ViewHolder holder, final int position) {


        if(dataBeans.get(position).getUsertype().equals("checking_team")){
            holder.text.setText(dataBeans.get(position).getComments());
            holder.text1.setText(dataBeans.get(position).getDatetime());
        } else {
            Toast.makeText(context, "No Comments for Checking Team...", Toast.LENGTH_SHORT).show();
        }






    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text,text1;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            text=itemView.findViewById(R.id.text);
            text1=itemView.findViewById(R.id.text1);



        }
    }
}
