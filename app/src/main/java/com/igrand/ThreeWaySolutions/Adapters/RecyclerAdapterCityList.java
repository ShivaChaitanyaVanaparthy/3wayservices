package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AdminProjectDetails;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.CityList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminCityList;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterCityList extends RecyclerView.Adapter<RecyclerAdapterCityList.Holder> {
    List<AdminCityList.DataBean> dataBeans;
    Context context;

    public RecyclerAdapterCityList(CityList projectsList, List<AdminCityList.DataBean> dataBeans) {

        this.context=projectsList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterCityList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_city, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterCityList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.cityname.setText(dataBeans.get(position).getCity_name());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,cityname,status;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            cityname=itemView.findViewById(R.id.cityname);
            status=itemView.findViewById(R.id.status);
        }
    }
}
