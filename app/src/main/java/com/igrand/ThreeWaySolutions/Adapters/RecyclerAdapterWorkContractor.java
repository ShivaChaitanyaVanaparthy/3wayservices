package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddEngagerReport;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddSupplierReport;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddWorkReport;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMaterialUsage;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMeasurementSheet;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.VendorListResponse;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterWorkContractor extends RecyclerView.Adapter<RecyclerAdapterWorkContractor.ViewHolder>{

    Context context;
    List<VendorListResponse.StatusBean.VendorsBean>  dataBeans;
    public int mSelectedItem = -1;
    AddWorkReport addWorkReport;
    String Name,workid;
    TextView userType;
    Button add;
    Dialog dialog;
    AddEngagerReport addEngagerReport;
    AddSupplierReport addSupplierReport;
    AddMeasurementSheet addMeasurementSheet;
    AddMaterialUsage addMaterialUsage;



    public RecyclerAdapterWorkContractor(AddWorkReport addWorkReport, List<VendorListResponse.StatusBean.VendorsBean> dataBeans, TextView userType, Dialog dialog, Button add) {
        this.addWorkReport=addWorkReport;
        this.dataBeans=dataBeans;
        this.userType=userType;
        this.add=add;
        this.dialog=dialog;

    }

    public RecyclerAdapterWorkContractor(AddEngagerReport addEngagerReport, List<VendorListResponse.StatusBean.VendorsBean> dataBeans, TextView engagername, Dialog dialog, Button add) {
        this.addEngagerReport=addEngagerReport;
        this.dataBeans=dataBeans;
        this.userType=engagername;
        this.add=add;
        this.dialog=dialog;
    }

    public RecyclerAdapterWorkContractor(AddSupplierReport addSupplierReport, List<VendorListResponse.StatusBean.VendorsBean> dataBeans, TextView suppliername, Dialog dialog, Button add) {
        this.addSupplierReport=addSupplierReport;
        this.dataBeans=dataBeans;
        this.userType=suppliername;
        this.add=add;
        this.dialog=dialog;
    }

    public RecyclerAdapterWorkContractor(AddMeasurementSheet addMeasurementSheet, List<VendorListResponse.StatusBean.VendorsBean> dataBeans, TextView name, Dialog dialog, Button add) {

        this.addMeasurementSheet=addMeasurementSheet;
        this.dataBeans=dataBeans;
        this.userType=name;
        this.add=add;
        this.dialog=dialog;
    }

    public RecyclerAdapterWorkContractor(AddMaterialUsage addMaterialUsage, List<VendorListResponse.StatusBean.VendorsBean> dataBeans, TextView name, Dialog dialog, Button add) {

        this.addMaterialUsage=addMaterialUsage;
        this.dataBeans=dataBeans;
        this.userType=name;
        this.add=add;
        this.dialog=dialog;
    }

    @NonNull
    @Override
    public RecyclerAdapterWorkContractor.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_work, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterWorkContractor.ViewHolder holder, final int position) {



        holder.radio.setChecked(position == mSelectedItem);
        holder.radio.setText(dataBeans.get(position).getPerson_name());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                userType.setText(Name);
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton radio;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener =new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    if(addWorkReport!=null){
                        Name=dataBeans.get(mSelectedItem).getPerson_name();
                        workid=dataBeans.get(mSelectedItem).getId();
                        addWorkReport.getId0(workid);
                    }if(addEngagerReport!=null){
                        Name=dataBeans.get(mSelectedItem).getPerson_name();
                        workid=dataBeans.get(mSelectedItem).getId();
                        addEngagerReport.getId0(workid);
                    }if(addSupplierReport!=null){
                        Name=dataBeans.get(mSelectedItem).getPerson_name();
                        workid=dataBeans.get(mSelectedItem).getId();
                        addSupplierReport.getId0(workid);
                    }if(addMeasurementSheet!=null){
                        Name=dataBeans.get(mSelectedItem).getPerson_name();
                        workid=dataBeans.get(mSelectedItem).getId();
                        addMeasurementSheet.getId0(workid);
                    }if(addMaterialUsage!=null){
                        Name=dataBeans.get(mSelectedItem).getPerson_name();
                        workid=dataBeans.get(mSelectedItem).getId();
                        addMaterialUsage.getId0(workid);
                    }

                }
            };

            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);

        }
    }
}
