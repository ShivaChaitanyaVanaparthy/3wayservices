package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.ApprovedLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PendingLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.ApprovedLeadsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.PendingLeadsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.RejectedLeadsMarketing;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.ApprovedLeadsTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadsListTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.RejectedLeadsTechnical;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentApprovedLeadResponse;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListMarketing;
import com.igrand.ThreeWaySolutions.Response.TechLeadsListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterMarketingLeads extends RecyclerView.Adapter<RecyclerAdapterMarketingLeads.ViewHolder>{

    Context context;
    List<LeadsListMarketing.DataBean> dataBeans;
    String mobileNumber;


    public RecyclerAdapterMarketingLeads(ApprovedLeadsMarketing approvedLeadsMarketing, List<LeadsListMarketing.DataBean> dataBeans, String mobileNumber) {

        this.context=approvedLeadsMarketing;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterMarketingLeads(PendingLeadsMarketing pendingLeadsMarketing, List<LeadsListMarketing.DataBean> dataBeans, String mobileNumber) {

        this.context=pendingLeadsMarketing;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterMarketingLeads(RejectedLeadsMarketing rejectedLeadsMarketing, List<LeadsListMarketing.DataBean> dataBeans, String mobileNumber) {

        this.context=rejectedLeadsMarketing;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    @NonNull
    @Override
    public RecyclerAdapterMarketingLeads.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_agentleads, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMarketingLeads.ViewHolder holder, final int position) {

        final Activity activity = (Activity) context;

        if(dataBeans!=null) {


            holder.id.setText(dataBeans.get(position).getId());
            holder.property.setText(dataBeans.get(position).getProperty_name());
            holder.date.setText(dataBeans.get(position).getDatetime());
            holder.village.setText(dataBeans.get(position).getVillage_name());
            holder.acres.setText(dataBeans.get(position).getAcres());
            holder.survey.setText(dataBeans.get(position).getSurvey_no());

            if(dataBeans.get(position).getMt_status().equals("1")){
                holder.status.setText("APPROVED");
            }else  if(dataBeans.get(position).getMt_status().equals("0")) {
                holder.status.setText("REJECTED");
            }else  if(dataBeans.get(position).getMt_status().equals("2")) {
                holder.status.setText("PENDING");
            }else  if(dataBeans.get(position).getMt_status().equals("3")) {
                holder.status.setText("OPEN");
            }


            holder.viewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, LeadDetailsAgent.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("ID", dataBeans.get(position).getId());
                    intent.putExtra("Property", dataBeans.get(position).getProperty_name());
                    intent.putExtra("Date", dataBeans.get(position).getDatetime());
                    intent.putExtra("Village", dataBeans.get(position).getVillage_name());
                    intent.putExtra("Checking", dataBeans.get(position).getCt_status());
                    intent.putExtra("Document", dataBeans.get(position).getDocument());
                    intent.putExtra("Image", dataBeans.get(position).getImages());
                    intent.putExtra("Status", dataBeans.get(position).getStatus());
                    intent.putExtra("Comments", dataBeans.get(position).getCt_status());

                    intent.putExtra("MarketingStatus", dataBeans.get(position).getMt_status());
                    intent.putExtra("MarketingDate", dataBeans.get(position).getMt_update_dt());
                    intent.putExtra("CheckingDate", dataBeans.get(position).getCt_update_dt());
                    //intent.putExtra("UserType", dataBeans.get(position).());
                    intent.putExtra("MobileNumber", mobileNumber);
                    intent.putExtra("ProcurementDate", dataBeans.get(position).getPt_update_dt());
                    intent.putExtra("ProcurementStatus", dataBeans.get(position).getPt_status());

                    intent.putExtra("Acres", dataBeans.get(position).getAcres());
                    intent.putExtra("Survey", dataBeans.get(position).getSurvey_no());
                    intent.putExtra("Propertydesc", dataBeans.get(position).getProperty_description());



                    intent.putExtra("Latitude", dataBeans.get(position).getLatitude());
                    intent.putExtra("Longitude", dataBeans.get(position).getLongitude());
                    intent.putExtra("Mandal", dataBeans.get(position).getMandal_name());
                    intent.putExtra("District", dataBeans.get(position).getDistrict_name());

                    intent.putExtra("TechnicalDate",dataBeans.get(position).getTt_update_dt());
                    intent.putExtra("TechnicalStatus",dataBeans.get(position).getTt_status());


                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            });

        }



    }

    @Override
    public int getItemCount() {
        

        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView id,property,date,village,status,acres,survey;
        LinearLayout viewDetails;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);

        }
    }
}
