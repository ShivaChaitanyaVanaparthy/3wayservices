package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.EngagerReport;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.MaterialUsage;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.MeasurementSheet;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.SupplierReport;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.WorkReport;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerList;
import com.igrand.ThreeWaySolutions.Response.AdminMachineryList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;
import com.igrand.ThreeWaySolutions.Response.MaterialListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementSheetResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerWorkName extends RecyclerView.Adapter<RecyclerWorkName.ViewHolder> {


    List<AdminWorkReportList.DataBean> dataBeans;
    Dialog dialog;
    TextView userType;
    Button add;
    WorkReport workReport;
    String Name,workid;
    EngagerReport engagerReport;
    List<AdminEngagerList.DataBean> dataBeans1;
    List<AdminMachineryList.DataBean> dataBeans2;
    SupplierReport supplierReport;
    MeasurementSheet measurementSheet;
    List<MeasurementSheetResponse.DataBean> dataBeans3;
    MaterialUsage materialUsage;
    List<MaterialListResponse.DataBean> dataBeans4;

    public int mSelectedItem = -1;


    public RecyclerWorkName(WorkReport workReport, List<AdminWorkReportList.DataBean> dataBeans, TextView contractor, Dialog dialog, Button add) {
        this.workReport=workReport;
        this.dataBeans=dataBeans;
        this.dialog=dialog;
        this.userType=contractor;
        this.add=add;
    }

    public RecyclerWorkName(EngagerReport engagerReport, List<AdminEngagerList.DataBean> dataBeans, TextView contractor, Dialog dialog, Button add) {
        this.engagerReport=engagerReport;
        this.dataBeans1=dataBeans;
        this.dialog=dialog;
        this.userType=contractor;
        this.add=add;
    }

    public RecyclerWorkName(SupplierReport supplierReport, List<AdminMachineryList.DataBean> dataBeans, TextView contractor, Dialog dialog, Button add) {
        this.supplierReport=supplierReport;
        this.dataBeans2=dataBeans;
        this.dialog=dialog;
        this.userType=contractor;
        this.add=add;
    }

    public RecyclerWorkName(MeasurementSheet measurementSheet, List<MeasurementSheetResponse.DataBean> dataBeans, TextView contractor, Dialog dialog, Button add) {
        this.measurementSheet=measurementSheet;
        this.dataBeans3=dataBeans;
        this.dialog=dialog;
        this.userType=contractor;
        this.add=add;

    }

    public RecyclerWorkName(MaterialUsage materialUsage, List<MaterialListResponse.DataBean> dataBeans, TextView contractor, Dialog dialog, Button add) {
        this.materialUsage=materialUsage;
        this.dataBeans4=dataBeans;
        this.dialog=dialog;
        this.userType=contractor;
        this.add=add;
    }


    @NonNull
    @Override
    public RecyclerWorkName.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_work, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerWorkName.ViewHolder holder, final int position) {



        add=dialog.findViewById(R.id.add);

        if(dataBeans!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans.get(position).getPerson_name());
        } if(dataBeans1!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans1.get(position).getPerson_name());
        }if(dataBeans2!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans2.get(position).getPerson_name());
        }if(dataBeans3!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans3.get(position).getPerson_name());
        }if(dataBeans4!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans4.get(position).getPerson_name());
        }


            add.setOnClickListener(view -> {
                dialog.dismiss();
                userType.setText(Name);
            });









    }

    @Override
    public int getItemCount() {

        if(dataBeans!=null){
            return dataBeans.size();
        }else if(dataBeans1!=null){
           return dataBeans1.size();
        }else if(dataBeans2!=null){
           return dataBeans2.size();
        }else if(dataBeans3!=null){
           return dataBeans3.size();
        }else if(dataBeans4!=null){
           return dataBeans4.size();
        }
        return dataBeans.size();
    }

    public void setFilter(ArrayList<AdminWorkReportList.DataBean> filteredList) {
        dataBeans = new ArrayList<>();
        dataBeans.addAll(filteredList);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            radio=itemView.findViewById(R.id.radio);

                View.OnClickListener clickListener= view -> {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    if(workReport!=null){
                        Name=dataBeans.get(mSelectedItem).getPerson_name();
                        workid=dataBeans.get(mSelectedItem).getContractor_id();
                        workReport.getId0(workid);
                    }if(engagerReport!=null){
                        Name=dataBeans1.get(mSelectedItem).getPerson_name();
                        workid=dataBeans1.get(mSelectedItem).getEngagor_id();
                        engagerReport.getId0(workid);
                    }if(supplierReport!=null){
                        Name=dataBeans2.get(mSelectedItem).getPerson_name();
                        workid=dataBeans2.get(mSelectedItem).getSupplier_id();
                        supplierReport.getId0(workid);
                    }if(measurementSheet!=null){
                        Name=dataBeans3.get(mSelectedItem).getPerson_name();
                        workid=dataBeans3.get(mSelectedItem).getContractor_id();
                        measurementSheet.getId0(workid);
                    }if(materialUsage!=null){
                        Name=dataBeans4.get(mSelectedItem).getPerson_name();
                        workid=dataBeans4.get(mSelectedItem).getContractor_id();
                        materialUsage.getId0(workid);
                    }
                };
            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
