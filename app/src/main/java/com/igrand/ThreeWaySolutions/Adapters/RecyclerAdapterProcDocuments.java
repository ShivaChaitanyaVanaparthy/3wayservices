package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage1;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.ProcDocList;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.WebPdfView;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProcDocumentResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterProcDocuments extends RecyclerView.Adapter<RecyclerAdapterProcDocuments.ViewHolder>{

Context context;
    String document1,document2;
    List<ProcDocumentResponse.ProposalDocumentsBean> dataBeans;
    ArrayList<String> list=new ArrayList<>();





    public RecyclerAdapterProcDocuments(ProcDocList procDocList, List<ProcDocumentResponse.ProposalDocumentsBean> dataBeans) {
        this.context=procDocList;
        this.dataBeans=dataBeans;
    }


    @NonNull
    @Override
    public RecyclerAdapterProcDocuments.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_techdocument, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterProcDocuments.ViewHolder holder, final int position) {


        if (dataBeans.get(position).getProposal_document().equals("")) {

            holder.linearimg.setVisibility(View.GONE);

            holder.imgtext.setText(dataBeans.get(position).getProposal_notes());
            //holder.pdftext.setText(dataBeans.get(position).getDoc_description());
            holder.pdf.setImageResource(R.drawable.imagepdf);
            document1 = "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/proposal_documents/" + dataBeans.get(position).getProposal_document();
            list.add(document1);
            Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image", document1);
                    intent.putExtra("List", list);
                    context.startActivity(intent);
                }
            });


            holder.pdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (dataBeans.get(position).getProposal_document_pdf().equals("")) {
                        Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                    } else {
                        document2 = "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/proposal_documents/" + dataBeans.get(position).getProposal_document_pdf();
                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

                       /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url=" + document2));
                        context.startActivity(browserIntent);*/

                       /* Intent intent=new Intent(context, WebPdfView.class);
                        intent.putExtra("PDF",document2);
                        context.startActivity(intent);
*/
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                        context.startActivity(intent);
                    }


                }
            });


        } else {

            holder.imgtext.setText(dataBeans.get(position).getProposal_notes());
           // holder.pdftext.setText(dataBeans.get(position).getDoc_description());
            holder.pdf.setImageResource(R.drawable.imagepdf);
            document1 = "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/proposal_documents/" + dataBeans.get(position).getProposal_document();
            list.add(document1);
            Picasso.get().load(document1).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.img);


            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    document1 = "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/proposal_documents/" + dataBeans.get(position).getProposal_document();
                    Intent intent = new Intent(context, ProfileImage.class);
                    intent.putExtra("Image", document1);
                    intent.putExtra("List", list);
                    context.startActivity(intent);
                }
            });


            holder.pdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (dataBeans.get(position).getProposal_document_pdf().equals("")) {
                        Toast.makeText(context, "No PDF File...", Toast.LENGTH_SHORT).show();

                    } else {
                        document2 = "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/proposal_documents/" + dataBeans.get(position).getProposal_document_pdf();
                           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                            context.startActivity(browserIntent);*/

                       /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url=" + document2));
                        context.startActivity(browserIntent);*/

                     /*   Intent intent=new Intent(context, WebPdfView.class);
                        intent.putExtra("PDF",document2);
                        context.startActivity(intent);*/

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                        context.startActivity(intent);

                    }

                }
            });
        }
    }


    @Override
    public int getItemCount() {

        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img,pdf;
        TextView imgtext,pdftext;
        LinearLayout linearimg,lineardoc;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img=itemView.findViewById(R.id.img);
            pdf=itemView.findViewById(R.id.pdf);
            imgtext=itemView.findViewById(R.id.imgtxt);
            pdftext=itemView.findViewById(R.id.pdftxt);
            linearimg=itemView.findViewById(R.id.linearimg);
            lineardoc=itemView.findViewById(R.id.lineardoc);

        }
    }
}
