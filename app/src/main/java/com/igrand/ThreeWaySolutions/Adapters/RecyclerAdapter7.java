package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PdfActivity;
import com.igrand.ThreeWaySolutions.Activities.AGENT.Pdfvieww;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.PdfActivityProjects;
import com.igrand.ThreeWaySolutions.R;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter7 extends RecyclerView.Adapter<RecyclerAdapter7.ViewHolder> implements OnPageChangeListener, OnLoadCompleteListener {

Context context;

    private int pageNumber = 0;



    ArrayList<String> stringList;
    String path,pdfFileName;
    PDFView pdfView;
    File file;


    public RecyclerAdapter7(PdfActivity pdfActivity, ArrayList<String> stringList) {
        this.context=pdfActivity;
        this.stringList=stringList;

    }

    public RecyclerAdapter7(PdfActivity pdfActivity, ArrayList<String> stringList, String path, File file) {
        this.context=pdfActivity;
        this.stringList=stringList;
        this.path=path;
        this.file=file;
    }

    public RecyclerAdapter7(PdfActivityProjects pdfActivityProjects, ArrayList<String> stringList) {
        this.context=pdfActivityProjects;
        this.stringList=stringList;
    }

    public RecyclerAdapter7(PdfActivityProjects pdfActivityProjects, ArrayList<String> stringList, String path, File file) {
        this.context=pdfActivityProjects;
        this.stringList=stringList;
        this.path=path;
        this.file=file;
    }


    @NonNull
    @Override
    public RecyclerAdapter7.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_pdf1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter7.ViewHolder holder, final int position) {



        holder.txt.setText(stringList.get(position));

        /*holder.txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, Pdfvieww.class);
                intent.putExtra("file",file);
                intent.putExtra("path",path);
                context.startActivity(intent);
            }
        });*/

        Uri uri = Uri.fromFile(new File(file.getAbsolutePath()));
        pdfFileName = getFileName(uri);

        holder.pdfView1.fromFile(file).load();
               /* .fromFile(file)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)*/
                //.scrollHandle(new DefaultScrollHandle(this))
                /*.spacing(10) // in dp
                .onPageError(this)*/
                //.load();
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    @Override
    public void loadComplete(int nbPages) {

        PdfDocument.Meta meta = pdfView.getDocumentMeta();

        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getLastPathSegment();
        }
        return result;
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            //Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt;
        PDFView pdfView1;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
             txt=itemView.findViewById(R.id.txt);
            pdfView1=itemView.findViewById(R.id.pdfView);


        }
    }

}
