package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.EngagerReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.EngagerReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MaterialReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.SupplierReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.SupplierReports;
import com.igrand.ThreeWaySolutions.Activities.MaterialReportListDate;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;
import com.igrand.ThreeWaySolutions.Response.EngagerReportLIst;
import com.igrand.ThreeWaySolutions.Response.SupplierReportLIst;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerWorkReport3 extends RecyclerView.Adapter<RecyclerWorkReport3.ViewHolder>{

   Context context;
    List<AdminWorkReportList.DataBean> dataBeans;
    List<List<SupplierReportLIst.DataBean>> dataBeans1;
    List<Integer> amount;
    String key,key1;





    public RecyclerWorkReport3(SupplierReports supplierReports, List<List<SupplierReportLIst.DataBean>> dataBeans) {

        this.context=supplierReports;
        this.dataBeans1=dataBeans;
    }

    public RecyclerWorkReport3(MeasurementReports measurementReports, List<List<SupplierReportLIst.DataBean>> dataBeans, String key) {
        this.context=measurementReports;
        this.dataBeans1=dataBeans;
        this.key=key;
    }

    public RecyclerWorkReport3(MaterialReports materialReports, List<List<SupplierReportLIst.DataBean>> dataBeans, String key1) {
        this.context=materialReports;
        this.dataBeans1=dataBeans;
        this.key1=key1;
    }

    @NonNull
    @Override
    public RecyclerWorkReport3.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_workreport1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerWorkReport3.ViewHolder holder, final int position) {



 if(dataBeans1!=null){

            holder.projectname.setText(dataBeans1.get(position).get(0).getProject_name());
            holder.date.setText(dataBeans1.get(position).get(0).getDate());


        }

        holder.amountt.setVisibility(View.GONE);


 if(key!=null){
     if(key.equals("key")){
         holder.viewDetails.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent intent=new Intent(context, MeasurementReportListDate.class);
                 intent.putExtra("ID",dataBeans1.get(position).get(0).getProject_id());
                 intent.putExtra("Date",dataBeans1.get(position).get(0).getDate());
                 context.startActivity(intent);
             }
         });
     }
 }else if(key1!=null){
     if(key1.equals("key1")){
         holder.viewDetails.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent intent=new Intent(context, MaterialReportListDate.class);
                 intent.putExtra("ID",dataBeans1.get(position).get(0).getProject_id());
                 intent.putExtra("Date",dataBeans1.get(position).get(0).getDate());
                 context.startActivity(intent);
             }
         });
     }
 } else {
     holder.viewDetails.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Intent intent=new Intent(context, SupplierReportListDate.class);
             intent.putExtra("ID",dataBeans1.get(position).get(0).getProject_id());
             intent.putExtra("Date",dataBeans1.get(position).get(0).getDate());
             context.startActivity(intent);
         }
     });
 }








    }

    @Override
    public int getItemCount() {

        return dataBeans1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView projectname,date,amount;
        LinearLayout viewDetails,amountt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            projectname=itemView.findViewById(R.id.projectname);
            date=itemView.findViewById(R.id.date);
            amount=itemView.findViewById(R.id.amount);
            viewDetails=itemView.findViewById(R.id.viewDetails);
            amountt=itemView.findViewById(R.id.amountt);

        }
    }
}
