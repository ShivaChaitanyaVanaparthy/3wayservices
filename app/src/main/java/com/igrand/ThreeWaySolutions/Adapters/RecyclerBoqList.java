package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.TECHNICAL1.BOQ;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.BoqListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerBoqList extends RecyclerView.Adapter<RecyclerBoqList.ViewHolder>{

   Context context;
    List<BoqListResponse.DataBean> dataBeans;







    public RecyclerBoqList(BOQ context, List<BoqListResponse.DataBean> dataBeans) {
        this.context=context;
        this.dataBeans=dataBeans;
    }


    @NonNull
    @Override
    public RecyclerBoqList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_boq, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerBoqList.ViewHolder holder, final int position) {



        if(dataBeans!=null){

           holder.work.setText(dataBeans.get(position).getWork_name());
           holder.subwork.setText(dataBeans.get(position).getSubwork_name());
           holder.no.setText(dataBeans.get(position).getNo());
           holder.length.setText(dataBeans.get(position).getLength());
           holder.width.setText(dataBeans.get(position).getWidth());
           holder.depth.setText(dataBeans.get(position).getDepth());
           holder.quantity.setText(dataBeans.get(position).getQuantity());
           holder.uom.setText(dataBeans.get(position).getUom());


        }


    }

    @Override
    public int getItemCount() {

        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView work,subwork,no,length,width,depth,quantity,uom;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            work=itemView.findViewById(R.id.work);
            subwork=itemView.findViewById(R.id.subwork);
            no=itemView.findViewById(R.id.no);
            length=itemView.findViewById(R.id.length);
            width=itemView.findViewById(R.id.width);
            depth=itemView.findViewById(R.id.depth);
            quantity=itemView.findViewById(R.id.quantity);
            uom=itemView.findViewById(R.id.uom);


        }
    }
}
