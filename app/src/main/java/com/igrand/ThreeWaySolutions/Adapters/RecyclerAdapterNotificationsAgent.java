package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.NotificationsAgent;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.NotificationsSiteEngineer;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.NotificationsTech;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.NotificationsAdminListResponse;
import com.igrand.ThreeWaySolutions.Response.NotificationsAgentListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Callback;

public class RecyclerAdapterNotificationsAgent extends RecyclerView.Adapter<RecyclerAdapterNotificationsAgent.ViewHolder>{

    Context context;
    List<NotificationsAgentListResponse.DataBean> dataBeans1;

    public RecyclerAdapterNotificationsAgent(NotificationsAgent applicationContext, List<NotificationsAgentListResponse.DataBean> dataBeans1) {
        this.context=applicationContext;
        this.dataBeans1=dataBeans1;
    }

    public RecyclerAdapterNotificationsAgent(NotificationsTech notificationsTech, List<NotificationsAgentListResponse.DataBean> dataBeans1) {

        this.context=notificationsTech;
        this.dataBeans1=dataBeans1;
    }

    public RecyclerAdapterNotificationsAgent(NotificationsSiteEngineer notificationsSiteEngineer, List<NotificationsAgentListResponse.DataBean> dataBeans1) {

        this.context=notificationsSiteEngineer;
        this.dataBeans1=dataBeans1;
    }


    @NonNull
    @Override
    public RecyclerAdapterNotificationsAgent.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications_agent, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterNotificationsAgent.ViewHolder holder, int position) {

        holder.description.setText(dataBeans1.get(position).getNotification());
        holder.date.setText(dataBeans1.get(position).getDate());
        holder.notificationfor.setText(dataBeans1.get(position).getNotification_for());

    }

    @Override
    public int getItemCount() {
        return dataBeans1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView viewDetails,date,description,notificationfor;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            date=itemView.findViewById(R.id.date);
            description=itemView.findViewById(R.id.description);
            notificationfor=itemView.findViewById(R.id.notificationfor);


        }
    }
}
