package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerEngagerReport extends RecyclerView.Adapter<RecyclerEngagerReport.ViewHolder>{

Context context;
    List<AdminEngagerList.DataBean> dataBeans;



    public RecyclerEngagerReport(Context context, List<AdminEngagerList.DataBean> dataBeans) {
        this.context=context;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerEngagerReport.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_engagerreport, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerEngagerReport.ViewHolder holder, final int position) {


        holder.engagername.setText(dataBeans.get(position).getPerson_name());
        holder.workname.setText(dataBeans.get(position).getWork_name());
        holder.subworkname.setText(dataBeans.get(position).getSubwork_name());
        holder.description.setText(dataBeans.get(position).getDescription());
        //holder.amount.setText(dataBeans.get(position).getA());
        holder.material.setText(dataBeans.get(position).getType_name());
        holder.start.setText(dataBeans.get(position).getStart_time());
        holder.end.setText(dataBeans.get(position).getEnd_time());
        holder.end1.setText(dataBeans.get(position).getEnd_time2());
        holder.start1.setText(dataBeans.get(position).getStart_time2());


    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView engagername,workname,subworkname,description,amount,material,start,end,start1,end1;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            engagername=itemView.findViewById(R.id.engagername);
            workname=itemView.findViewById(R.id.workname);
            subworkname=itemView.findViewById(R.id.subworkname);
            description=itemView.findViewById(R.id.description);
            amount=itemView.findViewById(R.id.amount);
            material=itemView.findViewById(R.id.material);
            start=itemView.findViewById(R.id.start);
            end=itemView.findViewById(R.id.end);
            start1=itemView.findViewById(R.id.start1);
            end1=itemView.findViewById(R.id.end1);



        }
    }
}
