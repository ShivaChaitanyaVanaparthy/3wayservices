package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MandalAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ManaVillageRegister;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.PlayersListAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveDistList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveMandalList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveVillageList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterVillageActiveList extends RecyclerView.Adapter<RecyclerAdapterVillageActiveList.ViewHolder>{

Context context;

    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    TextView state;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid;
    List<AdminActiveVillageList.DataBean> dataBeans;
    MandalAdmin mandalAdmin1;
    List<AdminActiveDistList.DataBean> dataBeans2;
    VillageAdmin villageAdmin1;
    ManaVillageRegister manaVillageRegister1;
    PlayersListAdmin playersListAdmin1;








    public RecyclerAdapterVillageActiveList(ManaVillageRegister manaVillageRegister, List<AdminActiveVillageList.DataBean> dataBeans, TextView mandal, Dialog dialog, ManaVillageRegister manaVillageRegister1) {

        this.context=manaVillageRegister;
        this.dataBeans=dataBeans;
        this.state=mandal;
        this.dialog=dialog;
        this.manaVillageRegister1=manaVillageRegister1;
    }

    public RecyclerAdapterVillageActiveList(PlayersListAdmin playersListAdmin, List<AdminActiveVillageList.DataBean> dataBeans, TextView village, Dialog dialog, PlayersListAdmin playersListAdmin1) {

        this.context=playersListAdmin;
        this.dataBeans=dataBeans;
        this.state=village;
        this.dialog=dialog;
        this.playersListAdmin1=playersListAdmin1;
    }


    @NonNull
    @Override
    public RecyclerAdapterVillageActiveList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterVillageActiveList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        holder.radio.setChecked(position == mSelectedItem);
        holder.radio.setText(dataBeans.get(position).getVillage_name());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                state.setText(selectedwork);

            }
        });




        //mandalAdmin1.getId1(selectedworkid);



        if(manaVillageRegister1!=null){
            manaVillageRegister1.getId3(selectedworkid);
        }

       if(playersListAdmin1!=null){
            playersListAdmin1.getId3(selectedworkid);
        }






    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    selectedwork=dataBeans.get(mSelectedItem).getVillage_name();
                    selectedworkid=dataBeans.get(mSelectedItem).getId();

                }
            };
            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
