package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadsListChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.PendingLeads;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadsListCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.PendingLeadsListCheckingResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterChecking extends RecyclerView.Adapter<RecyclerAdapterChecking.ViewHolder>{

Context context;
    List<LeadsListCheckingResponse.DataBean> dataBeans;
    List<String> documents;
    String mobileNumber;
    List<PendingLeadsListCheckingResponse.DataBean> dataBeans1;

    public RecyclerAdapterChecking(DashBoardChecking dashBoardChecking, List<LeadsListCheckingResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=dashBoardChecking;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterChecking(LeadsListChecking leadsListChecking, List<LeadsListCheckingResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=leadsListChecking;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }



    @NonNull
    @Override
    public RecyclerAdapterChecking.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_checking, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterChecking.ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;

        holder.id.setText(dataBeans.get(position).getId());
        holder.property.setText(dataBeans.get(position).getProperty_name());
        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.village.setText(dataBeans.get(position).getVillage_name());
        holder.acres.setText(dataBeans.get(position).getAcres());
        holder.survey.setText(dataBeans.get(position).getSurvey_no());

        if(dataBeans.get(position).getCt_status().equals("1")){
            holder.status.setText("APPROVED");
        }else  if(dataBeans.get(position).getCt_status().equals("0")) {
            holder.status.setText("REJECTED");
        }else  if(dataBeans.get(position).getCt_status().equals("2")) {
            holder.status.setText("PENDING");
        }



        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsChecking.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("MobileNumber",mobileNumber);
                intent.putExtra("ID",dataBeans.get(position).getId());
                intent.putExtra("Property",dataBeans.get(position).getProperty_name());
                intent.putExtra("Date",dataBeans.get(position).getDatetime());
                intent.putExtra("Village",dataBeans.get(position).getVillage_name());
                intent.putExtra("Checking",dataBeans.get(position).getCt_status());
                intent.putExtra("Document",dataBeans.get(position).getDocument());
                intent.putExtra("Image",dataBeans.get(position).getImages());
                intent.putExtra("Comments",dataBeans.get(position).getCt_status());
                intent.putExtra("Latitude",dataBeans.get(position).getLatitude());
                intent.putExtra("Longitude",dataBeans.get(position).getLongitude());
                intent.putExtra("MOBILE",dataBeans.get(position).getUser_mobile());

                intent.putExtra("MarketingStatus",dataBeans.get(position).getMt_status());
                intent.putExtra("MarketingDate",dataBeans.get(position).getMt_update_dt());
                intent.putExtra("CheckingDate",dataBeans.get(position).getCt_update_dt());
                intent.putExtra("ProcurementDate",dataBeans.get(position).getPt_update_dt());
                intent.putExtra("ProcurementStatus",dataBeans.get(position).getPt_status());
                intent.putExtra("Status",dataBeans.get(position).getCt_status());
                intent.putExtra("Acres",dataBeans.get(position).getAcres());
                intent.putExtra("Survey",dataBeans.get(position).getSurvey_no());
                intent.putExtra("Propertydesc",dataBeans.get(position).getProperty_description());

                intent.putExtra("Mandal",dataBeans.get(position).getMandal_name());
                intent.putExtra("District",dataBeans.get(position).getDistrict_name());




                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });



       /* holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsChecking.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });
*/
       /* holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        Intent intent=new Intent(context, EditChecking.class);
                        context.startActivity(intent);

            }
        });*/



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView id,property,date,village,status,acres,survey;
        LinearLayout viewDetails;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);



        }
    }
}
