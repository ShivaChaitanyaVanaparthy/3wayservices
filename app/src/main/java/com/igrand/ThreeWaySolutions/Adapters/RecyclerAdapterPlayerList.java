package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.PlayerDetails;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.PlayersFilter;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.PlayersListAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.PlayersListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterPlayerList extends RecyclerView.Adapter<RecyclerAdapterPlayerList.ViewHolder>  {

Context context;
    List<PlayersListResponse.DataBean> dataBeans;
    String documents;



    public RecyclerAdapterPlayerList(PlayersListAdmin playersListAdmin, List<PlayersListResponse.DataBean> dataBeans) {
        this.context=playersListAdmin;
        this.dataBeans=dataBeans;
    }

    public RecyclerAdapterPlayerList(PlayersFilter playersFilter, List<PlayersListResponse.DataBean> dataBeans) {
        this.context=playersFilter;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterPlayerList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_player_card, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterPlayerList.ViewHolder holder, final int position) {





        holder.fname.setText(dataBeans.get(position).getPlayer_first_name());
        holder.lname.setText(dataBeans.get(position).getPlayer_last_name());
        holder.game.setText(dataBeans.get(position).getGame_type());
        holder.state.setText(dataBeans.get(position).getState_name());
        holder.dst.setText(dataBeans.get(position).getDistrict_name());
        holder.mandal.setText(dataBeans.get(position).getMandal_name());
        holder.village.setText(dataBeans.get(position).getVillage_name());


        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, PlayerDetails.class);
                intent.putExtra("Id",dataBeans.get(position).getId());
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView fname,lname,game,state,dst,mandal,village;
        LinearLayout viewDetails;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            fname=itemView.findViewById(R.id.fname);
            lname=itemView.findViewById(R.id.lname);
            game=itemView.findViewById(R.id.gametype);
            state=itemView.findViewById(R.id.state);
            dst=itemView.findViewById(R.id.district);
            mandal=itemView.findViewById(R.id.mandal);
            village=itemView.findViewById(R.id.village);
            viewDetails=itemView.findViewById(R.id.viewDetails);



        }
    }
}
