package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.FRAGMENTS.ProjectEstimationFragment;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProjectEstimationListResponse;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerProjectEstimation extends RecyclerView.Adapter<RecyclerProjectEstimation.ViewHolder>{

Context context;
    List<ProjectEstimationListResponse.DataBean> dataBeans;

    double sum = 0;
    List<Double> integerList= new ArrayList<Double>();

    public RecyclerProjectEstimation(Context context, List<ProjectEstimationListResponse.DataBean> dataBeans) {
        this.context=context;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerProjectEstimation.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_estimation, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerProjectEstimation.ViewHolder holder, final int position) {


       // holder.projectname.setText(dataBeans.get(position).getProject_name());
        holder.worktype.setText(dataBeans.get(position).getWork_name());
        holder.subworktype.setText(dataBeans.get(position).getSubwork_name());
        holder.measurementunit.setText(dataBeans.get(position).getUom());
        holder.nou.setText(dataBeans.get(position).getNo_quantity());
        holder.rate.setText(dataBeans.get(position).getRate());
        holder.amount.setText(dataBeans.get(position).getAmount());
        //holder.estimation.setText(dataBeans.get(position).get());


        integerList.add(Double.valueOf(dataBeans.get(position).getAmount()));



        /*for(int i = 0; i < integerList.size(); i++)
        {
            sum += integerList.get(i);
            ProjectEstimationFragment fragmentB=new ProjectEstimationFragment();
            Bundle bundle=new Bundle();
            bundle.putDouble("SUM",sum);
            fragmentB.setArguments(bundle);
        }*/




        }



    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView projectname,worktype,subworktype,measurementunit,nou,rate,amount,estimation;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //projectname=itemView.findViewById(R.id.projectname);
            worktype=itemView.findViewById(R.id.worktype);
            subworktype=itemView.findViewById(R.id.subworktype);
            measurementunit=itemView.findViewById(R.id.measurementunit);
            nou=itemView.findViewById(R.id.nou);
            rate=itemView.findViewById(R.id.rate);
            amount=itemView.findViewById(R.id.amount);
            //estimation=itemView.findViewById(R.id.estimation);


        }
    }
}
