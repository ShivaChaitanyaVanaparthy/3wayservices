package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjects1;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter6 extends RecyclerView.Adapter<RecyclerAdapter6.ViewHolder>{

Context context;

    int number;
    ArrayList<String> pdflist;



    public RecyclerAdapter6(AddLeadsAgent applicationContext, int number, ArrayList<String> pdflist) {
        this.context=applicationContext;
        this.number=number;
        this.pdflist=pdflist;
    }

    public RecyclerAdapter6(NearbyProjects1 nearbyProjects1, int number, ArrayList<String> pdflist) {
        this.context=nearbyProjects1;
        this.number=number;
        this.pdflist=pdflist;

    }

    @NonNull
    @Override
    public RecyclerAdapter6.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_pdf, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter6.ViewHolder holder, final int position) {


    holder.img.setImageResource(R.drawable.imagepdf);

    holder.img.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            pdflist.remove(position);
            holder.img.setVisibility(View.GONE);
            return true;
        }
    });



/*
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });*/


    }

    @Override
    public int getItemCount() {
        return number;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            img=itemView.findViewById(R.id.img);


        }
    }
}
