package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.WorkReport;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;


import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerWorkReport extends RecyclerView.Adapter<RecyclerWorkReport.ViewHolder> {

   Context context;
    List<AdminWorkReportList.DataBean> dataBeans;
    Dialog dialog;
    TextView userType;
    Button add;






    public RecyclerWorkReport(Context context, List<AdminWorkReportList.DataBean> dataBeans) {

        this.context=context;
        this.dataBeans=dataBeans;

    }



    @NonNull
    @Override
    public RecyclerWorkReport.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_workreport, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerWorkReport.ViewHolder holder, final int position) {



        if(dataBeans!=null){

            holder.contractorname.setText(dataBeans.get(position).getPerson_name());
            holder.workname.setText(dataBeans.get(position).getWork_name());
            holder.subworkname.setText(dataBeans.get(position).getSubwork_name());
            holder.description.setText(dataBeans.get(position).getWork_description());
            holder.skilled.setText(dataBeans.get(position).getSkilled());
            holder.male.setText(dataBeans.get(position).getMale_no());
            holder.female.setText(dataBeans.get(position).getFemale_no());

        }







    }

    @Override
    public int getItemCount() {

        return dataBeans.size();
    }

    public void setFilter(ArrayList<AdminWorkReportList.DataBean> filteredList) {
        dataBeans = new ArrayList<>();
        dataBeans.addAll(filteredList);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView userType,contractorname,workname,subworkname,description,amount,skilled,malerate,femalerate,male,female,skilledrate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            userType=itemView.findViewById(R.id.userType);
            contractorname=itemView.findViewById(R.id.contractorname);
            workname=itemView.findViewById(R.id.workname);
            subworkname=itemView.findViewById(R.id.subworkname);
            description=itemView.findViewById(R.id.description);
            amount=itemView.findViewById(R.id.amount);
            skilled=itemView.findViewById(R.id.skilled);
            male=itemView.findViewById(R.id.male);
            female=itemView.findViewById(R.id.female);


        }
    }
}
