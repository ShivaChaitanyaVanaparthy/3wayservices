package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurement1;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage1;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjectsDetails;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterAgentImages extends RecyclerView.Adapter<RecyclerAdapterAgentImages.ViewHolder>{

    LeadDetailsSubAgent leadDetailsSubAgent;
    String document1,document2,key;
    String[] document,document3,document4;
    String Document;
    String[] documentx;
    List<String> strings;
    LeadDetailsProcurementAdmin leadDetailsProcurementAdmin;
    LeadDetailsAdmin leadDetailsAdmin;
    LeadDetailsAgent leadDetailsAgent;
    Context context;
    String stringsss;
    ArrayList<String> list=new ArrayList<>();







    public RecyclerAdapterAgentImages(LeadDetailsAgent leadDetailsAgent, String[] document2, List<String> strings, String document,String key3) {

        this.context=leadDetailsAgent;
        this.documentx=document2;
        this.Document=document;
        this.strings=strings;
        this.key=key3;
    }



    public RecyclerAdapterAgentImages(LeadDetailsChecking leadDetailsChecking, String[] imagedoc, List<String> stringsimg, String image,String key2) {

        this.context=leadDetailsChecking;
        this.documentx=imagedoc;
        this.Document=image;
        this.strings=stringsimg;
        this.key=key2;
    }


    public RecyclerAdapterAgentImages(NearbyProjectsDetails nearbyProjectsDetails, String[] imagedoc, List<String> stringsimg, String image, String key) {

        this.context=nearbyProjectsDetails;
        this.documentx=imagedoc;
        this.Document=image;
        this.strings=stringsimg;
        this.key=key;
    }

    public RecyclerAdapterAgentImages(LeadDetailsMarketing leadDetailsMarketing, String[] imagedoc, List<String> stringsimg, String image, String key1) {

        this.context=leadDetailsMarketing;
        this.documentx=imagedoc;
        this.Document=image;
        this.strings=stringsimg;
        this.key=key1;
    }

    public RecyclerAdapterAgentImages(LeadDetailsTech leadDetailsTech, String[] imagedoc, List<String> stringsimg, String image, String key2) {
        this.context=leadDetailsTech;
        this.documentx=imagedoc;
        this.Document=image;
        this.strings=stringsimg;
        this.key=key2;
    }

    public RecyclerAdapterAgentImages(LeadDetailsProcurement leadDetailsProcurement, String[] imagedoc, List<String> stringsimg, String image, String keyp) {
        this.context=leadDetailsProcurement;
        this.documentx=imagedoc;
        this.Document=image;
        this.strings=stringsimg;
        this.key=keyp;
    }

    public RecyclerAdapterAgentImages(LeadDetailsProcurement1 leadDetailsProcurement1, String[] imagedoc, List<String> stringsimg, String image, String keyppp) {
        this.context=leadDetailsProcurement1;
        this.documentx=imagedoc;
        this.Document=image;
        this.strings=stringsimg;
        this.key=keyppp;
    }

    public RecyclerAdapterAgentImages(LeadDetailsAdmin leadDetailsAdmin, String[] imagedoc, List<String> stringsimg, String image, String key2) {
        this.context=leadDetailsAdmin;
        this.documentx=imagedoc;
        this.Document=image;
        this.strings=stringsimg;
        this.key=key2;
    }


    @NonNull
    @Override
    public RecyclerAdapterAgentImages.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_img, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterAgentImages.ViewHolder holder, final int position) {

        if(key.equals("key")){

            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/near_by_projects/images/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);





            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

       if(key.equals("keyadmin")) {
            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

       if(key.equals("keyagent")) {
            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

       if(key.equals("keychecking")) {
            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

       if(key.equals("keymarketing")) {
            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

       if(key.equals("keytechnical")) {
            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

       if(key.equals("keyprocurement")) {
            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

       if(key.equals("keyprocurementadmin")) {
            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


            list.add(document1);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                    Intent intent = new Intent(context, ProfileImage1.class);
                    intent.putExtra("Image",document1);
                    intent.putExtra("List",list);
                    context.startActivity(intent);



                }
            });
        }

    }



    @Override
    public int getItemCount() {


        return strings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        //PDFView pdfView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            image=itemView.findViewById(R.id.img);
            //pdfView= itemView.findViewById(R.id.pdfView);



        }
    }
}
