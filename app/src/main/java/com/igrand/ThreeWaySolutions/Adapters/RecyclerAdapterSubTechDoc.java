package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterSubTechDoc extends RecyclerView.Adapter<RecyclerAdapterSubTechDoc.ViewHolder>{

Context context;
    String document1,document2;
    String[] document,document3,document4;
    String Document;
    private static final String TAG = "user1";
    String pdfFileName;
    Integer pageNumber = 0;
    List<String> strings = new ArrayList<>();
    List<String> strings1 = new ArrayList<>();
    String[] documentx;
    List<String> output;
    LeadDetailsAgent leadDetailsAgent;
    LeadDetailsSubAgent leadDetailsSubAgent;
    String key,keyadmin;
    LeadDetailsAdmin leadDetailsAdmin;
    LeadDetailsProcurementAdmin leadDetailsProcurementAdmin;

    /*List<String> stringList = new ArrayList<String>(Arrays.asList(document));
    String  str = String.join("http://igrandit.site/3way-services/admin_assets/uploads/leads/documents/", stringList);
*/







    public RecyclerAdapterSubTechDoc(LeadDetailsTech leadDetailsTech, String[] document2, String document, List<String> strings, String keyp) {
        this.context=leadDetailsProcurementAdmin;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=keyp;

    }


    @NonNull
    @Override
    public RecyclerAdapterSubTechDoc.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterSubTechDoc.ViewHolder holder, final int position) {



        holder.image.setImageResource(R.drawable.imagepdf);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+ output.get(position);

                if(key.equals("keyp")){

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);
                }

                    }
                });

            }

    @Override
    public int getItemCount() {


        return output.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image=itemView.findViewById(R.id.image);

        }
    }
}
