package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.InvestmentList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LegalTeamDocumentsList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminInvestmentList;
import com.igrand.ThreeWaySolutions.Response.AdminLegalList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterInvestmentList extends RecyclerView.Adapter<RecyclerAdapterInvestmentList.Holder> {
    List<AdminInvestmentList.DataBean> dataBeans;
    Context context;



    public RecyclerAdapterInvestmentList(InvestmentList investmentList, List<AdminInvestmentList.DataBean> dataBeans) {

        this.context=investmentList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterInvestmentList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_investment, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterInvestmentList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.projectname.setText(dataBeans.get(position).getProject_name());
        holder.investorname.setText(dataBeans.get(position).getUsername());
        holder.investoramount.setText(dataBeans.get(position).getInvest_amount());
        holder.investordate.setText(dataBeans.get(position).getInvest_date());
        holder.maturedate.setText(dataBeans.get(position).getMature_date());
        holder.duration.setText(dataBeans.get(position).getDuration());
        holder.matureamount.setText(dataBeans.get(position).getMature_amount());
        holder.date.setText(dataBeans.get(position).getDatetime());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView projectname,investorname,investoramount,investordate,maturedate,duration,matureamount,status,date;
        public Holder(@NonNull View itemView) {
            super(itemView);
            projectname=itemView.findViewById(R.id.projectname);
            investorname=itemView.findViewById(R.id.investorname);
            investoramount=itemView.findViewById(R.id.investoramount);

            investordate=itemView.findViewById(R.id.investordate);
            maturedate=itemView.findViewById(R.id.maturedate);
            duration=itemView.findViewById(R.id.duration);
            matureamount=itemView.findViewById(R.id.matureamount);
            status=itemView.findViewById(R.id.status);
            date=itemView.findViewById(R.id.date);
        }
    }
}
