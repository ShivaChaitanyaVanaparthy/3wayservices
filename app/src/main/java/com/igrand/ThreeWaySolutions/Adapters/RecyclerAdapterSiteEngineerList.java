package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.DashBoardSiteEngineer;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.SiteEngineerProjectDetails;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterSiteEngineerList extends RecyclerView.Adapter<RecyclerAdapterSiteEngineerList.Holder> {
    List<AdminProjectsList.DataBean> dataBeans;
    Context context;
    String key;



    public RecyclerAdapterSiteEngineerList(DashBoardSiteEngineer dashBoardSiteEngineer, List<AdminProjectsList.DataBean> dataBeans, String key) {

        this.context=dashBoardSiteEngineer;
        this.dataBeans=dataBeans;

    }


    @NonNull
    @Override
    public RecyclerAdapterSiteEngineerList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_project, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterSiteEngineerList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.villagename.setText(dataBeans.get(position).getVillage_name());
        holder.projectname.setText(dataBeans.get(position).getProject_name());

        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }


            holder.viewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, SiteEngineerProjectDetails.class);
                    intent.putExtra("Date",dataBeans.get(position).getDatetime());
                    intent.putExtra("Village",dataBeans.get(position).getVillage_name());
                    intent.putExtra("Project",dataBeans.get(position).getProject_name());
                    intent.putExtra("Status",dataBeans.get(position).getStatus());
                    intent.putExtra("ID",dataBeans.get(position).getId());
                    intent.putExtra("keysite","keysite");
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });










    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,villagename,projectname,status;
        LinearLayout viewDetails;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            villagename=itemView.findViewById(R.id.villagename);
            projectname=itemView.findViewById(R.id.projectname);
            status=itemView.findViewById(R.id.status);
            viewDetails=itemView.findViewById(R.id.viewDetails);
        }
    }
}
