package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AdminProjectDetails;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VendorsList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.AdminVendorList;
import com.igrand.ThreeWaySolutions.Response.VendorListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterVendorList extends RecyclerView.Adapter<RecyclerAdapterVendorList.Holder> {

    Context context;
    List<VendorListResponse.StatusBean.VendorsBean> vendorsBeans;




    public RecyclerAdapterVendorList(Context vendorsList, List<VendorListResponse.StatusBean.VendorsBean> vendorsBeans) {
        this.context=vendorsList;
        this.vendorsBeans=vendorsBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterVendorList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_vendor, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterVendorList.Holder holder, final int position) {





        if(vendorsBeans!=null){
            holder.personname.setText(vendorsBeans.get(position).getPerson_name());
            holder.mobile.setText(vendorsBeans.get(position).getPhone_no());
            holder.description.setText(vendorsBeans.get(position).getDescription());
            holder.description.setText(vendorsBeans.get(position).getRemarks());


            if(vendorsBeans.get(position).getVendor_type().equals("1")){
                holder.vendor.setText("Labour Contractor");
            } else if(vendorsBeans.get(position).getVendor_type().equals("2")){
                holder.vendor.setText("Work Contractor");
            } else if(vendorsBeans.get(position).getVendor_type().equals("3")){
                holder.vendor.setText("Material Supplier");
            }else if(vendorsBeans.get(position).getVendor_type().equals("4")){
                holder.vendor.setText("Machinery Engagers");
            }
        }






    }

    @Override
    public int getItemCount() {

        return vendorsBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView personname,mobile,description,vendor;
        LinearLayout viewDetails;
        public Holder(@NonNull View itemView) {
            super(itemView);
            personname=itemView.findViewById(R.id.personname);
            mobile=itemView.findViewById(R.id.mobile);
            description=itemView.findViewById(R.id.description);
            vendor=itemView.findViewById(R.id.vendor);

        }
    }
}
