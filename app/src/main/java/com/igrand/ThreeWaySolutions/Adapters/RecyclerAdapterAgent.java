package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.ApprovedLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadsListAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PendingLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.ApprovedLeadsTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadsListTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.RejectedLeadsTechnical;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentApprovedLeadResponse;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.TechLeadsListResponse;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterAgent extends RecyclerView.Adapter<RecyclerAdapterAgent.ViewHolder>{

    Context context;
    List<AgentLeadsListResponse.DataBean> dataBeans;
    String mobileNumber;
    List<TechLeadsListResponse.DataBean> dataBeans1;
    List<AgentApprovedLeadResponse.DataBean> dataBeans2;

    public RecyclerAdapterAgent(Context applicationContext, List<AgentLeadsListResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;

    }

    public RecyclerAdapterAgent(DashBoardTechnical applicationContext, List<TechLeadsListResponse.DataBean> dataBeans1, String mobileNumber) {

        this.context=applicationContext;
        this.dataBeans1=dataBeans1;
        this.mobileNumber=mobileNumber;
    }


    public RecyclerAdapterAgent(LeadsListTech leadsListTech, List<TechLeadsListResponse.DataBean> dataBeans1, String mobileNumber) {

        this.context=leadsListTech;
        this.dataBeans1=dataBeans1;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterAgent(ApprovedLeadsAgent approvedLeadsAgent, List<AgentApprovedLeadResponse.DataBean> dataBeans2, String mobileNumber) {
        this.context=approvedLeadsAgent;
        this.dataBeans2=dataBeans2;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterAgent(PendingLeadsAgent pendingLeadsAgent, List<AgentApprovedLeadResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=pendingLeadsAgent;
        this.dataBeans2=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterAgent(ApprovedLeadsTechnical approvedLeadsTechnical, List<AgentApprovedLeadResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=approvedLeadsTechnical;
        this.dataBeans2=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterAgent(RejectedLeadsTechnical rejectedLeadsTechnical, List<AgentApprovedLeadResponse.DataBean> dataBeans, String mobileNumber) {

        this.context=rejectedLeadsTechnical;
        this.dataBeans2=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    @NonNull
    @Override
    public RecyclerAdapterAgent.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_agentleads, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterAgent.ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;

        if(dataBeans!=null) {


            holder.id.setText(dataBeans.get(position).getId());
            holder.property.setText(dataBeans.get(position).getProperty_name());
            holder.date.setText(dataBeans.get(position).getDatetime());
            holder.village.setText(dataBeans.get(position).getVillage_name());
            holder.acres.setText(dataBeans.get(position).getAcres());
            holder.survey.setText(dataBeans.get(position).getSurvey_no());

            if (dataBeans.get(position).getStatus().equals("1")) {
                holder.status.setText("ACTIVE");

            } else if (dataBeans.get(position).getStatus().equals("0")) {
                holder.status.setText("IN-ACTIVE");
            }

            holder.viewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, LeadDetailsAgent.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("ID", dataBeans.get(position).getId());
                    intent.putExtra("Property", dataBeans.get(position).getProperty_name());
                    intent.putExtra("Date", dataBeans.get(position).getDatetime());
                    intent.putExtra("Village", dataBeans.get(position).getVillage_name());
                    intent.putExtra("Checking", dataBeans.get(position).getCt_status());
                    intent.putExtra("Document", dataBeans.get(position).getDocument());
                    intent.putExtra("Image", dataBeans.get(position).getImages());
                    intent.putExtra("Status", dataBeans.get(position).getStatus());
                    intent.putExtra("Comments", dataBeans.get(position).getCt_status());

                    intent.putExtra("MarketingStatus", dataBeans.get(position).getMt_status());
                    intent.putExtra("MarketingDate", dataBeans.get(position).getMt_update_dt());
                    intent.putExtra("CheckingDate", dataBeans.get(position).getCt_update_dt());
                    //intent.putExtra("UserType", dataBeans.get(position).());
                    intent.putExtra("MobileNumber", mobileNumber);
                    intent.putExtra("ProcurementDate", dataBeans.get(position).getPt_update_dt());
                    intent.putExtra("ProcurementStatus", dataBeans.get(position).getPt_status());

                    intent.putExtra("Acres", dataBeans.get(position).getAcres());
                    intent.putExtra("Survey", dataBeans.get(position).getSurvey_no());
                    intent.putExtra("Propertydesc", dataBeans.get(position).getProperty_description());



                    intent.putExtra("Latitude", dataBeans.get(position).getLatitude());
                    intent.putExtra("Longitude", dataBeans.get(position).getLongitude());
                    intent.putExtra("Mandal", dataBeans.get(position).getMandal_name());
                    intent.putExtra("District", dataBeans.get(position).getDistrict_name());

                    intent.putExtra("TechnicalDate",dataBeans.get(position).getTt_update_dt());
                    intent.putExtra("TechnicalStatus",dataBeans.get(position).getTt_status());


                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            });

        }

            if(dataBeans1!=null){


                holder.id.setText(dataBeans1.get(position).getId());
                holder.property.setText(dataBeans1.get(position).getProperty_name());
                holder.date.setText(dataBeans1.get(position).getDatetime());
                holder.village.setText(dataBeans1.get(position).getVillage_name());
                holder.acres.setText(dataBeans1.get(position).getAcres());
                holder.survey.setText(dataBeans1.get(position).getSurvey_no());

                if(dataBeans1.get(position).getTt_status().equals("0")){
                    holder.status.setText("REJECTED");
                }else  if(dataBeans1.get(position).getTt_status().equals("1")) {
                    holder.status.setText("APPROVED");
                }else  if(dataBeans1.get(position).getTt_status().equals("2")) {
                    holder.status.setText("OPEN");
                }

                holder.viewDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(context, LeadDetailsTech.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("ID",dataBeans1.get(position).getId());
                        intent.putExtra("Property",dataBeans1.get(position).getProperty_name());
                        intent.putExtra("Date",dataBeans1.get(position).getDatetime());
                        intent.putExtra("Village",dataBeans1.get(position).getVillage_name());
                        intent.putExtra("Checking",dataBeans1.get(position).getCt_status());
                        intent.putExtra("Document",dataBeans1.get(position).getDocument());
                        intent.putExtra("Image",dataBeans1.get(position).getImages());
                        intent.putExtra("Status",dataBeans1.get(position).getStatus());
                        intent.putExtra("Comments",dataBeans1.get(position).getCt_status());

                        intent.putExtra("MarketingStatus",dataBeans1.get(position).getMt_status());
                        intent.putExtra("TechStatus",dataBeans1.get(position).getTt_status());
                        intent.putExtra("TechDate",dataBeans1.get(position).getTt_update_dt());
                        intent.putExtra("MarketingDate",dataBeans1.get(position).getMt_update_dt());
                        intent.putExtra("CheckingDate",dataBeans1.get(position).getCt_update_dt());
                        intent.putExtra("MobileNumber",mobileNumber);

                        intent.putExtra("Acres",dataBeans1.get(position).getAcres());
                        intent.putExtra("Survey",dataBeans1.get(position).getSurvey_no());

                        intent.putExtra("Latitude",dataBeans1.get(position).getLatitude());
                        intent.putExtra("Longitude",dataBeans1.get(position).getLongitude());
                        intent.putExtra("Mandal",dataBeans1.get(position).getMandal_name());
                        intent.putExtra("District",dataBeans1.get(position).getDistrict_name());

                        intent.putExtra("TechnicalDate",dataBeans1.get(position).getTt_update_dt());
                        intent.putExtra("TechnicalStatus",dataBeans1.get(position).getTt_status());



                        context.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                    }
                });
            }

        if(dataBeans2!=null){


            holder.id.setText(dataBeans2.get(position).getId());
            holder.property.setText(dataBeans2.get(position).getProperty_name());
            holder.date.setText(dataBeans2.get(position).getDatetime());
            holder.village.setText(dataBeans2.get(position).getVillage_name());
            holder.acres.setText(dataBeans2.get(position).getAcres());
            holder.survey.setText(dataBeans2.get(position).getSurvey_no());

            if(dataBeans2.get(position).getTt_status().equals("0")){
                holder.status.setText("REJECTED");
            }else  if(dataBeans2.get(position).getTt_status().equals("1")) {
                holder.status.setText("APPROVED");
            }else  if(dataBeans2.get(position).getTt_status().equals("2")) {
                holder.status.setText("OPEN");
            }

            holder.viewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, LeadDetailsAgent.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("ID",dataBeans2.get(position).getId());
                    intent.putExtra("Property",dataBeans2.get(position).getProperty_name());
                    intent.putExtra("Date",dataBeans2.get(position).getDatetime());
                    intent.putExtra("Village",dataBeans2.get(position).getVillage_name());
                    intent.putExtra("Checking",dataBeans2.get(position).getCt_status());
                    intent.putExtra("Document",dataBeans2.get(position).getDocument());
                    intent.putExtra("Image",dataBeans2.get(position).getImages());
                    intent.putExtra("Status",dataBeans2.get(position).getStatus());
                    intent.putExtra("Comments",dataBeans2.get(position).getCt_status());

                    intent.putExtra("MarketingStatus",dataBeans2.get(position).getMt_status());
                    intent.putExtra("TechStatus",dataBeans2.get(position).getTt_status());
                    intent.putExtra("TechDate",dataBeans2.get(position).getTt_update_dt());
                    intent.putExtra("MarketingDate",dataBeans2.get(position).getMt_update_dt());
                    intent.putExtra("CheckingDate",dataBeans2.get(position).getCt_update_dt());
                    intent.putExtra("MobileNumber",mobileNumber);

                    intent.putExtra("Acres",dataBeans2.get(position).getAcres());
                    intent.putExtra("Survey",dataBeans2.get(position).getSurvey_no());

                    intent.putExtra("Latitude",dataBeans2.get(position).getLatitude());
                    intent.putExtra("Longitude",dataBeans2.get(position).getLongitude());
                    intent.putExtra("Mandal",dataBeans2.get(position).getMandal_name());
                    intent.putExtra("District",dataBeans2.get(position).getDistrict_name());

                    intent.putExtra("TechnicalDate",dataBeans2.get(position).getTt_update_dt());
                    intent.putExtra("TechnicalStatus",dataBeans2.get(position).getTt_status());



                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            });
        }


    }

    @Override
    public int getItemCount() {
        
        if(dataBeans1!=null){
            return dataBeans1.size();
        }
        else if(dataBeans!=null){
            return dataBeans.size();
        }
        else if(dataBeans2!=null){
            return dataBeans2.size();
        }
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView id,property,date,village,status,acres,survey;
        LinearLayout viewDetails;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);

        }
    }
}
