package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.EditUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UserDetailsAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.UserListAdminResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterUser1 extends RecyclerView.Adapter<RecyclerAdapterUser1.ViewHolder>{

Context context;
String MobileNumber,UserType;
String key;
    List<UserListAdminResponse.DataBean> dataBeans;
    public RecyclerAdapterUser1(Context applicationContext, String mobileNumber, List<UserListAdminResponse.DataBean> dataBeans, String keyadmin1) {
        this.context=applicationContext;
        this.MobileNumber=mobileNumber;
        this.dataBeans=dataBeans;
        this.key=keyadmin1;

    }

    @NonNull
    @Override
    public RecyclerAdapterUser1.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_user1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterUser1.ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;



       if(dataBeans.get(position).getUsertype().equals("site_engineer")) {


           holder.linear.setVisibility(View.VISIBLE);

            holder.viewUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, UserDetailsAdmin.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("MobileNumber",dataBeans.get(position).getMobile());
                    intent.putExtra("Profile",dataBeans.get(position).getProfile());
                    intent.putExtra("Email",dataBeans.get(position).getEmail());
                    intent.putExtra("Name",dataBeans.get(position).getUsername());


                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            });
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, EditUserAdmin.class);
                    intent.putExtra("MobileNumber",dataBeans.get(position).getMobile());
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });
            holder.phoneNumber.setText(dataBeans.get(position).getMobile());
            holder.name.setText(dataBeans.get(position).getUsername());
            holder.email.setText(dataBeans.get(position).getEmail());
            holder.date1.setText(dataBeans.get(position).getDatetime());
            //holder.date.setText(dataBeans.get(position).getDatetime());
            Picasso.get().load(dataBeans.get(position).getProfile()).placeholder( R.drawable.loading).error(R.drawable.profilepic).into(holder.image);


            UserType=dataBeans.get(position).getUsertype();

            if(UserType.equals("marketing_team")){
                holder.type.setText("Marketing Team");
            }

            if(UserType.equals("agent")){
                holder.type.setText("Agent");
            }

            if(UserType.equals("admin")){
                holder.type.setText("Admin");
            }
            if(UserType.equals("checking_team")){
                holder.type.setText("Checking Team");
            }

            if(UserType.equals("techenical_team")){
                holder.type.setText("Technical Team");
            }

            if(UserType.equals("procurement_team")){
                holder.type.setText("Procurement Team Team");
            }
            if(UserType.equals("site_engineer")){
                holder.type.setText("Site Engineer");
            }if(UserType.equals("legal_team")){
                holder.type.setText("Legal team");
            }

            if(UserType.equals("survey_team")){
                holder.type.setText("Survey Team");
            }
            if(UserType.equals("lesion_team")){
                holder.type.setText("Lesion Team");
            }
            if(UserType.equals("project_contractor")){
                holder.type.setText("Project Contractor");
            }
            if(UserType.equals("labour_contractor")){
                holder.type.setText("Labour Contractor");
            }
            if(UserType.equals("labour")){
                holder.type.setText("Labour");
            }
            if(UserType.equals("marketing_agent")){
                holder.type.setText("Marketing Agent");
            }
            if(UserType.equals("investor")){
                holder.type.setText("Investor");
            }
            if(UserType.equals("sub_agent")){
                holder.type.setText("SUB-AGENT");
            }

        } else {

holder.linear.setVisibility(View.GONE);

       }


    }

    @Override
    public int getItemCount() {


        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        LinearLayout viewUser,linear;
        TextView viewDetails,date1;
        ImageView edit,image;
        TextView type,name,email,phoneNumber,date;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            card=itemView.findViewById(R.id.card);
            viewUser=itemView.findViewById(R.id.viewUser);
            edit=itemView.findViewById(R.id.edit);
            image=itemView.findViewById(R.id.image);
            type=itemView.findViewById(R.id.type);
            name=itemView.findViewById(R.id.name);
            email=itemView.findViewById(R.id.email);
            phoneNumber=itemView.findViewById(R.id.phoneNumber);
            date1=itemView.findViewById(R.id.date);
            linear=itemView.findViewById(R.id.linear);
            //date=itemView.findViewById(R.id.date);


        }
    }
}
