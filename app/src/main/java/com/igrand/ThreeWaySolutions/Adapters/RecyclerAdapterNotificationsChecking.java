package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.NotificationsChecking;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.NotificationsAgentListResponse;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterNotificationsChecking extends RecyclerView.Adapter<RecyclerAdapterNotificationsChecking.Holder> {

    Context context;
    List<NotificationsAgentListResponse.DataBean> dataBeans1;


    public RecyclerAdapterNotificationsChecking(NotificationsChecking notificationsChecking, List<NotificationsAgentListResponse.DataBean> dataBeans1) {
        this.context=notificationsChecking;
        this.dataBeans1=dataBeans1;
    }

    @NonNull
    @Override
    public RecyclerAdapterNotificationsChecking.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications_checking, parent, false);
        return new Holder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterNotificationsChecking.Holder holder, int position) {
        holder.description.setText(dataBeans1.get(position).getNotification());
        holder.date.setText(dataBeans1.get(position).getDate());
        holder.notificationfor.setText(dataBeans1.get(position).getNotification_for());
    }

    @Override
    public int getItemCount() {
        return dataBeans1.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView viewDetails,date,description,notificationfor;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            description=itemView.findViewById(R.id.description);
            notificationfor=itemView.findViewById(R.id.notificationfor);

        }
    }
}
