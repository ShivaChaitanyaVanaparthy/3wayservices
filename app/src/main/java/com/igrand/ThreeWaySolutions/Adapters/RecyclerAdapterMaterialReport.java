package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReportListDate;
import com.igrand.ThreeWaySolutions.Activities.MaterialReportListDate;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.MaterialListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterMaterialReport extends RecyclerView.Adapter<RecyclerAdapterMaterialReport.ViewHolder> {

Context context;
    List<MaterialListResponse.DataBean> dataBeans;





    public RecyclerAdapterMaterialReport(MaterialReportListDate measurementReportListDate, List<MaterialListResponse.DataBean> dataBeans) {

        this.context=measurementReportListDate;
        this.dataBeans=dataBeans;
    }




    @NonNull
    @Override
    public RecyclerAdapterMaterialReport.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_material, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMaterialReport.ViewHolder holder, final int position) {

        if(dataBeans!=null){

            holder.work.setText(dataBeans.get(position).getWork_name());
            holder.subwork.setText(dataBeans.get(position).getSubwork_name());
            holder.name.setText(dataBeans.get(position).getPerson_name());
            holder.uom.setText(dataBeans.get(position).getUom());
            holder.date.setText(dataBeans.get(position).getDate());
            holder.material.setText(dataBeans.get(position).getMaterial_type());
            holder.openingstock.setText(dataBeans.get(position).getOpening_stock());
            holder.received.setText(dataBeans.get(position).getRecived());
            holder.consumption.setText(dataBeans.get(position).getConsumption());



        }



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView work,subwork,no,length,width,depth,quantity,uom,bill,name,date,material,openingstock,received,consumption;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            work=itemView.findViewById(R.id.work);
            subwork=itemView.findViewById(R.id.subwork);
            no=itemView.findViewById(R.id.no);
            length=itemView.findViewById(R.id.length);
            width=itemView.findViewById(R.id.width);
            depth=itemView.findViewById(R.id.depth);
            quantity=itemView.findViewById(R.id.quantity);
            uom=itemView.findViewById(R.id.uom);
            bill=itemView.findViewById(R.id.bill);
            name=itemView.findViewById(R.id.name);
            date=itemView.findViewById(R.id.date);
            material=itemView.findViewById(R.id.material);
            openingstock=itemView.findViewById(R.id.openingstock);
            received=itemView.findViewById(R.id.received);
            consumption=itemView.findViewById(R.id.consumption);

        }
    }
}
