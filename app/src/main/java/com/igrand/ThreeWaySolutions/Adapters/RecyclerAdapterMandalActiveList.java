package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MandalAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ManaVillageRegister;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.PlayersListAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveDistList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveMandalList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterMandalActiveList extends RecyclerView.Adapter<RecyclerAdapterMandalActiveList.ViewHolder>{

Context context;
    List<AdminActiveMandalList.DataBean> dataBeans;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    TextView state;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid;
    List<AdminActiveStateList.DataBean> dataBeans1;
    MandalAdmin mandalAdmin1;
    List<AdminActiveDistList.DataBean> dataBeans2;
    VillageAdmin villageAdmin1;
    ManaVillageRegister manaVillageRegister1;
    PlayersListAdmin playersListAdmin1;








    public RecyclerAdapterMandalActiveList(VillageAdmin villageAdmin, List<AdminActiveMandalList.DataBean> dataBeans, TextView district, Dialog dialog, VillageAdmin villageAdmin1) {

        this.context=villageAdmin;
        this.dataBeans=dataBeans;
        this.state=district;
        this.dialog=dialog;
        this.villageAdmin1=villageAdmin1;
    }

    public RecyclerAdapterMandalActiveList(ManaVillageRegister manaVillageRegister, List<AdminActiveMandalList.DataBean> dataBeans, TextView mandal, Dialog dialog, ManaVillageRegister manaVillageRegister1) {

        this.context=manaVillageRegister;
        this.dataBeans=dataBeans;
        this.state=mandal;
        this.dialog=dialog;
        this.manaVillageRegister1=manaVillageRegister1;
    }

    public RecyclerAdapterMandalActiveList(PlayersListAdmin playersListAdmin, List<AdminActiveMandalList.DataBean> dataBeans, TextView mandal, Dialog dialog, PlayersListAdmin playersListAdmin1) {

        this.context=playersListAdmin;
        this.dataBeans=dataBeans;
        this.state=mandal;
        this.dialog=dialog;
        this.playersListAdmin1=playersListAdmin1;
    }


    @NonNull
    @Override
    public RecyclerAdapterMandalActiveList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMandalActiveList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        holder.radio.setChecked(position == mSelectedItem);
        holder.radio.setText(dataBeans.get(position).getMandal_name());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                state.setText(selectedwork);

            }
        });




        //mandalAdmin1.getId1(selectedworkid);

        if(villageAdmin1!=null){
            villageAdmin1.getId2(selectedworkid);
        }

        if(manaVillageRegister1!=null){
            manaVillageRegister1.getId2(selectedworkid);
        }

        if(playersListAdmin1!=null){
            playersListAdmin1.getId2(selectedworkid);
        }


    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    selectedwork=dataBeans.get(mSelectedItem).getMandal_name();
                    selectedworkid=dataBeans.get(mSelectedItem).getId();

                }
            };
            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
