package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterAgentComment extends RecyclerView.Adapter<RecyclerAdapterAgentComment.ViewHolder>{

Context context;
    List<AgentLeadsCommentsResponse.DataBean> dataBeans;



    public RecyclerAdapterAgentComment(LeadDetailsAgent applicationContext, List<AgentLeadsCommentsResponse.DataBean> dataBeans) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;

    }

    @NonNull
    @Override
    public RecyclerAdapterAgentComment.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterAgentComment.ViewHolder holder, final int position) {


        holder.text.setText(dataBeans.get(position).getComments());




    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            text=itemView.findViewById(R.id.text);



        }
    }
}
