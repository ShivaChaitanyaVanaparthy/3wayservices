package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin1;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterAdminDocuments extends RecyclerView.Adapter<RecyclerAdapterAdminDocuments.ViewHolder>{

    Context context;
    String[] document;
    String document1,document2,document3;


    public RecyclerAdapterAdminDocuments(LeadDetailsAdmin leadDetailsAdmin, String[] document, String document2) {

        this.context=leadDetailsAdmin;
        this.document=document;
    }

    public RecyclerAdapterAdminDocuments(LeadDetailsAdmin1 leadDetailsAdmin1, String[] document, String document2) {
        this.context=leadDetailsAdmin1;
        this.document=document;
    }

    @NonNull
    @Override
    public RecyclerAdapterAdminDocuments.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterAdminDocuments.ViewHolder holder, final int position) {

        document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+document[position];

        if(document1!=null){

            Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);
        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        holder.itemView.buildDrawingCache();
                        Bitmap bitmap = holder.itemView.getDrawingCache();
                        Intent intent = new Intent(context, ProfileImage.class);
                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        context.startActivity(intent);
                    }
                });
            }


    @Override
    public int getItemCount() {
        return document.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            image=itemView.findViewById(R.id.image);


        }
    }
}
