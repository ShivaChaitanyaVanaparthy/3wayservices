package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.CityList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UserDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.UserDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.UsersListAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminCityList;
import com.igrand.ThreeWaySolutions.Response.AgentUserList;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterUserList extends RecyclerView.Adapter<RecyclerAdapterUserList.Holder> {
    List<AgentUserList.DataBean> dataBeans;
    Context context;


    public RecyclerAdapterUserList(UsersListAgent usersListAgent, List<AgentUserList.DataBean> dataBeans) {

        this.context=usersListAgent;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterUserList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_agentuser, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterUserList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.name.setText(dataBeans.get(position).getUsername());
        holder.type.setText(dataBeans.get(position).getUsertype());
        holder.email.setText(dataBeans.get(position).getEmail());
        holder.phoneNumber.setText(dataBeans.get(position).getMobile());

        Picasso.get().load(dataBeans.get(position).getProfile()).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(holder.image);

        if(dataBeans.get(position).getUsertype().equals("sub_agent")){
            holder.type.setText("Sub-Agent");
        }


        holder.viewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, UserDetailsAgent.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("MobileNumber",dataBeans.get(position).getMobile());
                intent.putExtra("Profile",dataBeans.get(position).getProfile());
                intent.putExtra("Email",dataBeans.get(position).getEmail());
                intent.putExtra("Name",dataBeans.get(position).getUsername());


                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });


    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name,date,type,email,phoneNumber;
        ImageView image;
        LinearLayout viewUser;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            name =itemView.findViewById(R.id.name);
            type=itemView.findViewById(R.id.type);
            email=itemView.findViewById(R.id.email);
            phoneNumber=itemView.findViewById(R.id.phoneNumber);
            image=itemView.findViewById(R.id.image);
            viewUser=itemView.findViewById(R.id.viewUser);

        }
    }
}
