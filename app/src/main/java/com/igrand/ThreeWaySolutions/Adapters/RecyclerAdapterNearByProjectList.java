package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AdminProjectDetails;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NearByProjectsList1;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearByProjectsList;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjects;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjectsDetails;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.NearByProjectResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterNearByProjectList extends RecyclerView.Adapter<RecyclerAdapterNearByProjectList.Holder> {
    List<NearByProjectResponse.DataBean> dataBeans;
    Context context;


    public RecyclerAdapterNearByProjectList(NearByProjectsList nearByProjectsList, List<NearByProjectResponse.DataBean> dataBeans) {

        this.context=nearByProjectsList;
        this.dataBeans=dataBeans;
    }

    public RecyclerAdapterNearByProjectList(NearByProjectsList1 nearByProjectsList1, List<NearByProjectResponse.DataBean> dataBeans) {
        this.context=nearByProjectsList1;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterNearByProjectList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_nearbyprojects, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterNearByProjectList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.projectname.setText(dataBeans.get(position).getProject_name());
        holder.acres.setText(dataBeans.get(position).getAcres());
        holder.plotsize.setText(dataBeans.get(position).getPlot_size());




        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, NearbyProjectsDetails.class);
                intent.putExtra("Date",dataBeans.get(position).getDatetime());
                intent.putExtra("Project",dataBeans.get(position).getProject_name());
                intent.putExtra("Acres",dataBeans.get(position).getAcres());
                intent.putExtra("Plotsize",dataBeans.get(position).getPlot_size());
                intent.putExtra("FastMoving",dataBeans.get(position).getFast_moving_plot_size());
                intent.putExtra("Cost",dataBeans.get(position).getCost());
                intent.putExtra("GoogleLocation",dataBeans.get(position).getGoogle_location());
                intent.putExtra("Broucher",dataBeans.get(position).getBrochure());
                intent.putExtra("Image",dataBeans.get(position).getImage());
                intent.putExtra("ID",dataBeans.get(position).getLead_id());
                intent.putExtra("Comment",dataBeans.get(position).getComment());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,acres,projectname,plotsize;
        LinearLayout viewDetails;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            projectname=itemView.findViewById(R.id.projectname);
            acres=itemView.findViewById(R.id.acres);
            plotsize=itemView.findViewById(R.id.plotsize);
            viewDetails=itemView.findViewById(R.id.viewDetails);
        }
    }
}
