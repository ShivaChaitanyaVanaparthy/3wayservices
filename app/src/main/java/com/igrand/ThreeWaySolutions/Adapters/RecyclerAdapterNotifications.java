package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.NotificationsAdminListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterNotifications extends RecyclerView.Adapter<RecyclerAdapterNotifications.ViewHolder>{

Context context;
    List<NotificationsAdminListResponse.StatusBean.DataBean> dataBeans1;
    public RecyclerAdapterNotifications(Context applicationContext, List<NotificationsAdminListResponse.StatusBean.DataBean> dataBeans1) {
        this.context=applicationContext;
        this.dataBeans1=dataBeans1;

    }

    @NonNull
    @Override
    public RecyclerAdapterNotifications.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterNotifications.ViewHolder holder, int position) {


        holder.description.setText(dataBeans1.get(position).getNotification());
        holder.date.setText(dataBeans1.get(position).getDate());
        holder.notificationfor.setText(dataBeans1.get(position).getNotification_for());



     /*   if(dataBeans1.get(position).getNotification_for().equals("agent")){

            holder.notificationfor.setText("AGENT");
        }
*/

    }

    @Override
    public int getItemCount() {
        return dataBeans1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        TextView viewDetails,date,description,notificationfor;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            date=itemView.findViewById(R.id.date);
            description=itemView.findViewById(R.id.description);
            notificationfor=itemView.findViewById(R.id.notificationfor);




        }
    }
}
