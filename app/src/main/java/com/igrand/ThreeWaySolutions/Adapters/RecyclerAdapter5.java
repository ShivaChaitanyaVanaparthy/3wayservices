package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.AddLeadsSubAgent1;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjects1;
import com.igrand.ThreeWaySolutions.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter5 extends RecyclerView.Adapter<RecyclerAdapter5.ViewHolder>{

Context context;

    ArrayList<Bitmap> bitmapArray;
    List<String> stringList;
    AddLeadsAgent addLeadsAgent;
    List<String> selectedvideoImgList;
    List<ArrayList<Bitmap>> stringList1;
    ArrayList<Bitmap> bmp_images1;



    public RecyclerAdapter5(AddLeadsAgent addLeadsAgent, ArrayList<Bitmap> bitmapArray) {

        this.addLeadsAgent=addLeadsAgent;
        this.bitmapArray=bitmapArray;
    }



    @NonNull
    @Override
    public RecyclerAdapter5.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        parent.removeAllViews();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter5.ViewHolder holder, int position) {






          holder.img.setImageBitmap(bitmapArray.get(position));


          ArrayList<Bitmap> bmp_images = new ArrayList<Bitmap>();
          for (int i = 0; i < bitmapArray.size(); i++) {
              bmp_images.add(bitmapArray.get(i));
             // addLeadsAgent.getpics(bmp_images);
          }
      }









    @Override
    public int getItemCount() {

        return bitmapArray.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

       ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img=itemView.findViewById(R.id.img);



        }
    }
}
