package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.AddLeadsSubAgent1;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.AddDocumentChecking;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.AddDocumentMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjects1;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.AddDocumentTech;
import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MultipartBody;

public class RecyclerAdapter4 extends RecyclerView.Adapter<RecyclerAdapter4.ViewHolder>{

Context context;

    ArrayList<Bitmap> bitmapArray;
    List<String> stringList;
    AddLeadsAgent addLeadsAgent;
    List<String> selectedvideoImgList;
    List<ArrayList<Bitmap>> stringList1;
    ArrayList<Bitmap> bmp_images1;
    List<MultipartBody.Part> bmp_images2;
    String imageFilePath;




    public RecyclerAdapter4(ArrayList<Bitmap> bmp_images1, AddLeadsAgent context) {
        this.bitmapArray=bmp_images1;
        this.context=context;
    }

    public RecyclerAdapter4(ArrayList<Bitmap> bitmapArray, AddDocumentTech context) {
        this.bitmapArray=bitmapArray;
        this.context=context;
    }

    public RecyclerAdapter4(ArrayList<Bitmap> bitmapArray, AddDocumentChecking context) {
        this.bitmapArray=bitmapArray;
        this.context=context;
    }

    public RecyclerAdapter4(ArrayList<Bitmap> bitmapArray, AddDocumentMarketing context) {
        this.bitmapArray=bitmapArray;
        this.context=context;
    }


    @NonNull
    @Override
    public RecyclerAdapter4.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        parent.removeAllViews();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter4.ViewHolder holder, int position) {




            holder.img.setImageBitmap(bitmapArray.get(position));


          /*  bmp_images1.add(bitmapArray);

           addLeadsAgent.getimage(bitmapArray);*/

    }




    @Override
    public int getItemCount() {

        return bitmapArray.size();
    }




  /*  public void updateList(ArrayList<Bitmap> bitmapArray) {
        this.bitmapArray = bitmapArray;
        notifyDataSetChanged();
    }*/

    public class ViewHolder extends RecyclerView.ViewHolder {

       ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img=itemView.findViewById(R.id.img);


        }
    }
}
