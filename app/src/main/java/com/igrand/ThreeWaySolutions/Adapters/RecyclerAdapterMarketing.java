package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.MARKETING.EditMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.MarketingLeadsListResponse;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterMarketing extends RecyclerView.Adapter<RecyclerAdapterMarketing.ViewHolder>{

Context context;
    List<MarketingLeadsListResponse.DataBean> dataBeans;
    String mobileNumber;
    public RecyclerAdapterMarketing(Context applicationContext, List<MarketingLeadsListResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;

    }

    @NonNull
    @Override
    public RecyclerAdapterMarketing.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_marketing, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMarketing.ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;

        holder.id.setText(dataBeans.get(position).getId());
        holder.property.setText(dataBeans.get(position).getProperty_name());
        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.village.setText(dataBeans.get(position).getVillage_name());
        holder.acres.setText(dataBeans.get(position).getAcres());
        holder.survey.setText(dataBeans.get(position).getSurvey_no());

        if(dataBeans.get(position).getMt_status().equals("1")){
            holder.status.setText("APPROVED");
        }else  if(dataBeans.get(position).getMt_status().equals("0")) {
            holder.status.setText("REJECTED");
        }else  if(dataBeans.get(position).getMt_status().equals("2")) {
            holder.status.setText("PENDING");
        }else  if(dataBeans.get(position).getMt_status().equals("3")) {
            holder.status.setText("OPEN");
        }

        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsMarketing.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("MobileNumber",mobileNumber);
                intent.putExtra("ID",dataBeans.get(position).getId());
                intent.putExtra("Property",dataBeans.get(position).getProperty_name());
                intent.putExtra("Date",dataBeans.get(position).getDatetime());
                intent.putExtra("Village",dataBeans.get(position).getVillage_name());
                intent.putExtra("CheckingStatus",dataBeans.get(position).getCt_status());
                intent.putExtra("Document",dataBeans.get(position).getDocument());
                intent.putExtra("Image",dataBeans.get(position).getImages());
                intent.putExtra("MarketingStatus",dataBeans.get(position).getMt_status());
                intent.putExtra("Comments",dataBeans.get(position).getCt_status());
                intent.putExtra("Latitude",dataBeans.get(position).getLatitude());
                intent.putExtra("Longitude",dataBeans.get(position).getLongitude());
                intent.putExtra("MOBILE",dataBeans.get(position).getUser_mobile());
                intent.putExtra("MarketingDate",dataBeans.get(position).getMt_update_dt());
                intent.putExtra("CheckingDate",dataBeans.get(position).getCt_update_dt());
                intent.putExtra("Acres",dataBeans.get(position).getAcres());
                intent.putExtra("Survey",dataBeans.get(position).getSurvey_no());
                intent.putExtra("ProcurementDate",dataBeans.get(position).getPt_update_dt());
                intent.putExtra("ProcurementStatus",dataBeans.get(position).getPt_status());
                intent.putExtra("Propertydesc",dataBeans.get(position).getProperty_description());
                intent.putExtra("Mandal",dataBeans.get(position).getMandal_name());
                intent.putExtra("District",dataBeans.get(position).getDistrict_name());

                intent.putExtra("Checking",dataBeans.get(position).getCt_status());
                intent.putExtra("Status",dataBeans.get(position).getCt_status());



                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });

      /*  holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        Intent intent=new Intent(context, EditMarketing.class);
                        context.startActivity(intent);

            }
        });*/



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        LinearLayout viewDetails;
        TextView id,property,date,village,status,acres,survey;
        ImageView edit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);



            viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);


        }
    }
}
