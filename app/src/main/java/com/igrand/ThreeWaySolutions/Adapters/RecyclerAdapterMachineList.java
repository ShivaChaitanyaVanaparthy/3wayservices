package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MachineList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UomList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminMachineList;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterMachineList extends RecyclerView.Adapter<RecyclerAdapterMachineList.Holder> {
    List<AdminMachineList.DataBean> dataBeans;
    Context context;



    public RecyclerAdapterMachineList(MachineList machineList, List<AdminMachineList.DataBean> dataBeans) {

        this.context=machineList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterMachineList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_uom, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMachineList.Holder holder, final int position) {


        holder.cityname.setText(dataBeans.get(position).getType_name());
        holder.date.setText(dataBeans.get(position).getId());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }


    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,cityname,status;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            cityname=itemView.findViewById(R.id.cityname);
            status=itemView.findViewById(R.id.status);
        }
    }
}
