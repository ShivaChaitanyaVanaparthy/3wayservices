package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.LeadDetailsLegal;
import com.igrand.ThreeWaySolutions.Activities.LESION.LeadDetailsLesion;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjectsDetails;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Activities.SURVEY.LeadDetailsSurvey;
import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterCheckingDocuments9 extends RecyclerView.Adapter<RecyclerAdapterCheckingDocuments9.ViewHolder>{

Context context;
    String[] document;
    String document1,document2,document3;






    public RecyclerAdapterCheckingDocuments9(LeadDetailsProcurementAdmin leadDetailsProcurementAdmin, String[] document, String document2) {
        this.context=leadDetailsProcurementAdmin;
        this.document=document;
    }


    @NonNull
    @Override
    public RecyclerAdapterCheckingDocuments9.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterCheckingDocuments9.ViewHolder holder, int position) {



            document1="http://igrandit.site/3way-services/admin_assets/uploads/leads/documents/"+document[position];
            if(document1!=null){

                Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);
            }





        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        holder.itemView.buildDrawingCache();
                        Bitmap bitmap = holder.itemView.getDrawingCache();
                        Intent intent = new Intent(context, ProfileImage.class);
                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        context.startActivity(intent);
                    }
                });
            }


    @Override
    public int getItemCount() {
        return document.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            image=itemView.findViewById(R.id.image);



        }
    }
}
