package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.DashBoardLegal;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.EditLegal;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.LeadDetailsLegal;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.LeadsListLegal;
import com.igrand.ThreeWaySolutions.Activities.LESION.LeadsListLesion;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadsListLegalResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListLesionResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterLegal extends RecyclerView.Adapter<RecyclerAdapterLegal.ViewHolder>{

Context context;
    List<LeadsListLegalResponse.DataBean> dataBeans;
    String mobileNumber;


    public RecyclerAdapterLegal(DashBoardLegal dashBoardLegal, List<LeadsListLegalResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=dashBoardLegal;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    public RecyclerAdapterLegal(LeadsListLegal leadsListLegal, List<LeadsListLegalResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=leadsListLegal;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }



    @NonNull
    @Override
    public RecyclerAdapterLegal.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_legal, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterLegal.ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;

        holder.id.setText(dataBeans.get(position).getId());
        holder.property.setText(dataBeans.get(position).getProperty_name());
        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.village.setText(dataBeans.get(position).getVillage_name());
        holder.acres.setText(dataBeans.get(position).getAcres());
        holder.survey.setText(dataBeans.get(position).getSurvey_no());

        if(dataBeans.get(position).getStatus().equals("1")){
            holder.status.setText("APPROVED");

        }else  if(dataBeans.get(position).getStatus().equals("0")) {
            holder.status.setText("REJECTED");
        }



        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsLegal.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("MobileNumber",mobileNumber);
                intent.putExtra("ID",dataBeans.get(position).getId());
                intent.putExtra("Property",dataBeans.get(position).getProperty_name());
                intent.putExtra("Date",dataBeans.get(position).getDatetime());
                intent.putExtra("Village",dataBeans.get(position).getVillage_name());
                intent.putExtra("Checking",dataBeans.get(position).getCt_status());
                intent.putExtra("Document",dataBeans.get(position).getDocument());
                intent.putExtra("Status",dataBeans.get(position).getCt_status());
                intent.putExtra("Address",dataBeans.get(position).getAddress());
                intent.putExtra("Comments",dataBeans.get(position).getCt_status());
                intent.putExtra("Latitude",dataBeans.get(position).getLatitude());
                intent.putExtra("Longitude",dataBeans.get(position).getLongitude());
                intent.putExtra("Address",dataBeans.get(position).getAddress());
                intent.putExtra("MOBILE",dataBeans.get(position).getUser_mobile());

                intent.putExtra("MarketingStatus",dataBeans.get(position).getMt_status());
                intent.putExtra("MarketingDate",dataBeans.get(position).getMt_update_dt());
                intent.putExtra("CheckingDate",dataBeans.get(position).getCt_update_dt());
                intent.putExtra("ProcurementDate",dataBeans.get(position).getPt_update_dt());
                intent.putExtra("ProcurementStatus",dataBeans.get(position).getPt_status());

                intent.putExtra("LegalStatus",dataBeans.get(position).getLgt_status());
                intent.putExtra("legalDate",dataBeans.get(position).getLgt_update_dt());

                intent.putExtra("SurveyStatus",dataBeans.get(position).getSt_status());
                intent.putExtra("SurveyDate",dataBeans.get(position).getSt_update_dt());
                intent.putExtra("LesionDate",dataBeans.get(position).getLs_update_dt());
                intent.putExtra("LesionStatus",dataBeans.get(position).getLs_status());

                intent.putExtra("Acres",dataBeans.get(position).getAcres());
                intent.putExtra("Survey",dataBeans.get(position).getSurvey_no());
                intent.putExtra("Mandal",dataBeans.get(position).getMandal_name());
                intent.putExtra("District",dataBeans.get(position).getDistrict_name());

                intent.putExtra("COMMENTS",dataBeans.get(position).getAd_comments());
                intent.putExtra("IMAGE",dataBeans.get(position).getAd_image());





                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });



       /* holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsChecking.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });
*/
       /* holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        Intent intent=new Intent(context, EditChecking.class);
                        context.startActivity(intent);

            }
        });*/



    }


    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView id,property,date,village,status,acres,survey;
        LinearLayout viewDetails;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);



        }
    }
}
