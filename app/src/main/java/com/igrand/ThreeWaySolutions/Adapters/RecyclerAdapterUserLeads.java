package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin1;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UserDetailsAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadsDetailResponse;
import com.igrand.ThreeWaySolutions.Response.UserDetailsResponse;
import com.igrand.ThreeWaySolutions.Response.UserListAdminResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterUserLeads extends RecyclerView.Adapter<RecyclerAdapterUserLeads.ViewHolder>{

Context context;
    List<UserDetailsResponse.DataBean> dataBeans;
    List<UserDetailsResponse.DataBean.LeadsBean> leadsBeans;
    Integer sizee;

    String Date;

    public RecyclerAdapterUserLeads(UserDetailsAdmin applicationContext, List<UserDetailsResponse.DataBean> dataBeans, Integer sizee, List<UserDetailsResponse.DataBean.LeadsBean> leadsBeans) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;
        this.sizee=sizee;
        this.leadsBeans=leadsBeans;
    }



    @NonNull
    @Override
    public RecyclerAdapterUserLeads.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_user_leads, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterUserLeads.ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;


       // Date=leadsBeans.get(position).getTime();


        holder.date.setText(leadsBeans.get(position).getTime());
        holder.property.setText(leadsBeans.get(position).getProperty_name());
        holder.village.setText(leadsBeans.get(position).getVillage_name());
        holder.survey.setText(leadsBeans.get(position).getSurvey_no());
        holder.acres.setText(leadsBeans.get(position).getAcres());
       // holder.status.setText(leadsBeans.get(position).getAcres());


        /*if(leadsBeans.get(position).get().equals("1")){
            holder.status.setText("APPROVED");

        }else  if(leadsBeans.get(position).getStatus().equals("0")) {
            holder.status.setText("REJECTED");
        }
*/




       holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsAdmin1.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // intent.putExtra("ID",dataBeans.get(position).ge());
                intent.putExtra("Property",leadsBeans.get(position).getProperty_name());
                intent.putExtra("Date",leadsBeans.get(position).getTime());
                intent.putExtra("Village",leadsBeans.get(position).getVillage_name());
               // intent.putExtra("Checking",leadsBeans.get(position).getCh());
                intent.putExtra("Document",leadsBeans.get(position).getDocument());
                intent.putExtra("Address",leadsBeans.get(position).getAddress());
                intent.putExtra("Latitude",leadsBeans.get(position).getLatitude());
                intent.putExtra("Longitude",leadsBeans.get(position).getLongitude());
                //intent.putExtra("Status",leadsBeans.get(position).g());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });



    }

    @Override
    public int getItemCount() {

        return leadsBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        LinearLayout viewDetails;
        TextView id,date,property,village,status,survey,acres;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            viewDetails=itemView.findViewById(R.id.viewUser);
            id=itemView.findViewById(R.id.id);
            date=itemView.findViewById(R.id.date);
            property=itemView.findViewById(R.id.property);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            survey=itemView.findViewById(R.id.survey);
            acres=itemView.findViewById(R.id.acres);


        }
    }
}
