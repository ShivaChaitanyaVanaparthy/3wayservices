package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.EarnsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ReferralsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ReferralsListIdAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.AddReferralAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.EarnsListAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ReferralsListAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ReferralsListIdAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.EarnsListResponse;
import com.igrand.ThreeWaySolutions.Response.ReferralListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterReferrals extends RecyclerView.Adapter<RecyclerAdapterReferrals.ViewHolder>{


    Context context;
    List<ReferralListResponse.DataBean> dataBeans;
    String id;

    public RecyclerAdapterReferrals(ReferralsListAgent referralsListAgent, List<ReferralListResponse.DataBean> dataBeans,String List) {
        this.context=referralsListAgent;
        this.dataBeans=dataBeans;
        this.id=List;
    }

    public RecyclerAdapterReferrals(ReferralsListIdAgent referralsListIdAgent, List<ReferralListResponse.DataBean> dataBeans, String id) {
        this.context=referralsListIdAgent;
        this.dataBeans=dataBeans;
        this.id=id;
    }

    public RecyclerAdapterReferrals(ReferralsListIdAdmin referralsListIdAdmin, List<ReferralListResponse.DataBean> dataBeans, String admin) {
        this.context=referralsListIdAdmin;
        this.dataBeans=dataBeans;
        this.id=admin;
    }

    public RecyclerAdapterReferrals(ReferralsList referralsList, List<ReferralListResponse.DataBean> dataBeans, String key) {

        this.context=referralsList;
        this.dataBeans=dataBeans;
        this.id=key;
    }


    @NonNull
    @Override
    public RecyclerAdapterReferrals.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_referral, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterReferrals.ViewHolder holder, final int position) {

        holder.title.setText(dataBeans.get(position).getTitle());
        holder.name.setText(dataBeans.get(position).getUsername());
        holder.refname.setText(dataBeans.get(position).getReferal_name());
        holder.refnum.setText(dataBeans.get(position).getReferal_phone_no());



        if(id.equals("ID")){
            holder.add.setVisibility(View.GONE);
        }
         if(id.equals("ADMIN")){
            holder.add.setVisibility(View.GONE);
        }if(id.equals("key")){
            holder.add.setVisibility(View.GONE);
        }


       holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, AddReferralAgent.class);
                intent.putExtra("Id",dataBeans.get(position).getEarn_id());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView title,name,refname,refnum;
        LinearLayout add;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title=itemView.findViewById(R.id.title);
            name=itemView.findViewById(R.id.name);
            refname=itemView.findViewById(R.id.refname);
            refnum=itemView.findViewById(R.id.refnun);
            add=itemView.findViewById(R.id.add);



        }
    }
}
