package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UserDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.UserDetailsAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.UserDetailsAgentResponse;
import com.igrand.ThreeWaySolutions.Response.UserDetailsResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterAgentLeads extends RecyclerView.Adapter<RecyclerAdapterAgentLeads.ViewHolder>{

    UserDetailsAgent context;
    List<UserDetailsAgentResponse.DataBean> dataBeans;

    Integer sizee;

    String Date;




    public RecyclerAdapterAgentLeads(UserDetailsAgent userDetailsAgent, List<UserDetailsAgentResponse.DataBean> dataBeans, Integer  sizee) {

        this.context=userDetailsAgent;
        this.dataBeans=dataBeans;
        this.sizee=sizee;

    }


    @NonNull
    @Override
    public RecyclerAdapterAgentLeads.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_user_leads, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;


       // Date=leadsBeans.get(position).getTime();




        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.property.setText(dataBeans.get(position).getProperty_name());
        holder.village.setText(dataBeans.get(position).getVillage_name());
        holder.survey.setText(dataBeans.get(position).getSurvey_no());
        holder.acres.setText(dataBeans.get(position).getAcres());

       /* if(dataBeans.get(position).getStatus().equals("1")){
            holder.status.setText("APPROVED");

        }else  if(dataBeans.get(position).getStatus().equals("0")) {
            holder.status.setText("REJECTED");
        }*/



       holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsSubAgent.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // intent.putExtra("ID",dataBeans.get(position).ge());
                intent.putExtra("Property",dataBeans.get(position).getProperty_name());
                intent.putExtra("Date",dataBeans.get(position).getDatetime());
                intent.putExtra("Village",dataBeans.get(position).getVillage_name());
               // intent.putExtra("Checking",leadsBeans.get(position).getCh());
                intent.putExtra("Document",dataBeans.get(position).getDocument());
                intent.putExtra("Address",dataBeans.get(position).getAddress());
                intent.putExtra("GoogleLocation",dataBeans.get(position).getGoogle_location());
                //intent.putExtra("Status",leadsBeans.get(position).g());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });
        sizee=dataBeans.size();
        SharedPreferences sharedPreferences = context.getSharedPreferences("Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("b_type", String.valueOf(dataBeans.size()));
        editor.apply();
        context.getSize(sizee,context);


    }

    @Override
    public int getItemCount() {

        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        LinearLayout viewDetails;
        TextView id,date,property,village,status,survey,acres;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            viewDetails=itemView.findViewById(R.id.viewUser);
            id=itemView.findViewById(R.id.id);
            date=itemView.findViewById(R.id.date);
            property=itemView.findViewById(R.id.property);
            village=itemView.findViewById(R.id.village);
           // status=itemView.findViewById(R.id.status);
            survey=itemView.findViewById(R.id.survey);
            acres=itemView.findViewById(R.id.acres);


        }
    }
}
