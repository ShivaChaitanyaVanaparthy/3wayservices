package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterAdmin extends RecyclerView.Adapter<RecyclerAdapterAdmin.ViewHolder>  {

Context context;
    List<LeadsListResponse.DataBean> dataBeans;
    String documents;

    public RecyclerAdapterAdmin(Context applicationContext, List<LeadsListResponse.DataBean> dataBeans, String documents) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;
        this.documents=documents;

    }

    @NonNull
    @Override
    public RecyclerAdapterAdmin.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterAdmin.ViewHolder holder, final int position) {


        final Activity activity = (Activity) context;


        holder.id.setText(dataBeans.get(position).getId());
        holder.property.setText(dataBeans.get(position).getProperty_name());
        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.village.setText(dataBeans.get(position).getVillage_name());
        holder.acres.setText(dataBeans.get(position).getAcres());
        holder.survey.setText(dataBeans.get(position).getSurvey_no());

        if(dataBeans.get(position).getCt_status().equals("1")){
            holder.status.setText("APPROVED");

        }else  if(dataBeans.get(position).getCt_status().equals("0")) {
            holder.status.setText("REJECTED");
        }else  if(dataBeans.get(position).getCt_status().equals("2")) {
            holder.status.setText("PENDING");
        }


        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LeadDetailsAdmin.class);
                String checkingdate=dataBeans.get(position).getCt_update_dt();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("ID",dataBeans.get(position).getId());
                intent.putExtra("Property",dataBeans.get(position).getProperty_name());
                intent.putExtra("Date",dataBeans.get(position).getDatetime());
                intent.putExtra("Village",dataBeans.get(position).getVillage_name());
                intent.putExtra("Checking",dataBeans.get(position).getCt_status());
                intent.putExtra("Acres",dataBeans.get(position).getAcres());
                intent.putExtra("Survey",dataBeans.get(position).getSurvey_no());

                intent.putExtra("Document",dataBeans.get(position).getDocument());
                intent.putExtra("Propertydesc",dataBeans.get(position).getProperty_name());

                intent.putExtra("Status",dataBeans.get(position).getStatus());
                intent.putExtra("Comments",dataBeans.get(position).getCt_status());
                intent.putExtra("Longitude",dataBeans.get(position).getLongitude());
                intent.putExtra("Latitude",dataBeans.get(position).getLatitude());
                intent.putExtra("MarketingStatus",dataBeans.get(position).getMt_status());
                intent.putExtra("MarketingDate",dataBeans.get(position).getMt_update_dt());
                intent.putExtra("CheckingDate",checkingdate);
                intent.putExtra("ProcurementDate",dataBeans.get(position).getPt_update_dt());
                intent.putExtra("ProcurementStatus",dataBeans.get(position).getPt_status());
                intent.putExtra("Image",dataBeans.get(position).getImages());



                intent.putExtra("TechnicalDate",dataBeans.get(position).getTt_update_dt());
                intent.putExtra("TechnicalStatus",dataBeans.get(position).getTt_status());






                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });


    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView id,property,date,village,status,acres,survey;
        LinearLayout viewDetails;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);


        }
    }
}
