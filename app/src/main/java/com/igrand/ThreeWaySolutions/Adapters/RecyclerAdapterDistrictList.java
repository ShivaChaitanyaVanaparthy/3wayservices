package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.DistrictList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.StateList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminDistrictList;
import com.igrand.ThreeWaySolutions.Response.AdminStateList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterDistrictList extends RecyclerView.Adapter<RecyclerAdapterDistrictList.Holder> {
    List<AdminDistrictList.DataBean> dataBeans;
    Context context;





    public RecyclerAdapterDistrictList(DistrictList districtList, List<AdminDistrictList.DataBean> dataBeans) {

        this.context=districtList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterDistrictList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_dst, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterDistrictList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.cityname.setText(dataBeans.get(position).getState_name());
        holder.dstname.setText(dataBeans.get(position).getDistrict_name());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,cityname,status,dstname;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            cityname=itemView.findViewById(R.id.cityname);
            status=itemView.findViewById(R.id.status);
            dstname=itemView.findViewById(R.id.dstname);
        }
    }
}
