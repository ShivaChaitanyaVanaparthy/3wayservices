package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.barteksc.pdfviewer.PDFView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurement1;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjectsDetails;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.WebPdfView;
import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterSubAgentDocuments extends RecyclerView.Adapter<RecyclerAdapterSubAgentDocuments.ViewHolder>{

Context context;
    String document1,document2;
    String[] document,document3,document4;
    String Document;
    private static final String TAG = "user1";
    String pdfFileName;
    Integer pageNumber = 0;
    List<String> strings = new ArrayList<>();
    List<String> strings1 = new ArrayList<>();
    String[] documentx;
    List<String> output;
    LeadDetailsAgent leadDetailsAgent;
    LeadDetailsSubAgent leadDetailsSubAgent;
    String key,keyadmin;
    LeadDetailsAdmin leadDetailsAdmin;
    LeadDetailsProcurementAdmin leadDetailsProcurementAdmin;

    /*List<String> stringList = new ArrayList<String>(Arrays.asList(document));
    String  str = String.join("http://igrandit.site/3way-services/admin_assets/uploads/leads/documents/", stringList);
*/





    public RecyclerAdapterSubAgentDocuments(LeadDetailsSubAgent leadDetailsSubAgent, String[] document2, String document, List<String> strings) {

        this.context=leadDetailsSubAgent;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsAgent leadDetailsAgent, String[] document2, String document, List<String> strings, String key) {

        this.context=leadDetailsAgent;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=key;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsSubAgent leadDetailsSubAgent, String[] document2, String document, List<String> strings, String key1) {
        this.context=leadDetailsSubAgent;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=key1;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsAdmin leadDetailsAdmin,String document, String[] document2,  List<String> strings, String keyadmin) {

        this.context=leadDetailsAdmin;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=keyadmin;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsProcurementAdmin leadDetailsProcurementAdmin, String document, String[] document2, List<String> strings, String keyp) {

        this.context=leadDetailsProcurementAdmin;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=keyp;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsMarketing leadDetailsMarketing, String[] document2, String document, List<String> strings, String key) {

        this.context=leadDetailsMarketing;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=key;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsChecking leadDetailsChecking, String[] document2, String document, List<String> strings, String keymarketing) {
        this.context=leadDetailsChecking;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=keymarketing;
    }

    public RecyclerAdapterSubAgentDocuments(NearbyProjectsDetails nearbyProjectsDetails, String[] document2, String document, List<String> strings, String key) {

        this.context=nearbyProjectsDetails;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=key;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsTech leadDetailsTech, String[] document2, String document, List<String> strings, String key) {

        this.context=leadDetailsTech;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=key;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsProcurement leadDetailsProcurement, String[] document2, String document, List<String> strings, String keyp) {
        this.context=leadDetailsProcurement;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=keyp;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsProcurement1 leadDetailsProcurement1, String[] document2, String document, List<String> strings, String keyppp) {
        this.context=leadDetailsProcurement1;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=keyppp;
    }

    public RecyclerAdapterSubAgentDocuments(LeadDetailsAdmin leadDetailsAdmin, String[] document2, String document, List<String> strings, String key) {
        this.context=leadDetailsAdmin;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=key;
    }


    @NonNull
    @Override
    public RecyclerAdapterSubAgentDocuments.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterSubAgentDocuments.ViewHolder holder, final int position) {



        holder.image.setImageResource(R.drawable.imagepdf);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+ output.get(position);



                if(key.equals("keyAdmin")){

                   /* Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/


                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);
                      /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                }if(key.equals("key")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);
                   /*Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/
                    /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                }
                if(key.equals("key1")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                    /*Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);*/
                }
                if(key.equals("keyadmin")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                  /*  Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);*/
                }
                if(key.equals("keyp")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                   /* Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);*/
                } if(key.equals("keymarketing")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                    /*Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);*/
                }

                if(key.equals("keychecking")){

                  /*  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);
*/
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                  /*  Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);*/
                } if(key.equals("key5")){

                    document2= "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/near_by_projects/brochures/"+ output.get(position);

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                    /*Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);*/
                }if(key.equals("keyt")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                   /* Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                }if(key.equals("keypp")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                   /* Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                }if(key.equals("keyppp")){

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + document2), "text/html");
                    context.startActivity(intent);

                   /* Intent intent=new Intent(context, WebPdfView.class);
                    intent.putExtra("PDF",document2);
                    context.startActivity(intent);*/
                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/viewerng/viewer?url="+document2));
                    context.startActivity(browserIntent);*/
                }

                    }
                });

            }

    @Override
    public int getItemCount() {


        return output.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image=itemView.findViewById(R.id.image);

        }
    }
}
