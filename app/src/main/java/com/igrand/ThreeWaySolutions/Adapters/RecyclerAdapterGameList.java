package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.GamesList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.StateList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ManaVillageRegister;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminGameList;
import com.igrand.ThreeWaySolutions.Response.AdminStateList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterGameList extends RecyclerView.Adapter<RecyclerAdapterGameList.Holder> {
    List<AdminGameList.DataBean> dataBeans;
    Context context;



    public RecyclerAdapterGameList(GamesList gamesList, List<AdminGameList.DataBean> dataBeans) {

        this.context=gamesList;
        this.dataBeans=dataBeans;
    }




    @NonNull
    @Override
    public RecyclerAdapterGameList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_game, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterGameList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.cityname.setText(dataBeans.get(position).getGame_type());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,cityname,status;
        public Holder(@NonNull View itemView) {
            super(itemView);
            cityname=itemView.findViewById(R.id.cityname);
            status=itemView.findViewById(R.id.status);
        }
    }
}
