package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsProcurementAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterSubAgentDocuments1 extends RecyclerView.Adapter<RecyclerAdapterSubAgentDocuments1.ViewHolder>{

Context context;
    String document1,document2;
    String[] document,document3,document4;
    String Document;
    private static final String TAG = "user1";
    String pdfFileName;
    Integer pageNumber = 0;
    List<String> strings = new ArrayList<>();
    List<String> strings1 = new ArrayList<>();
    String[] documentx;
    List<String> output;
    LeadDetailsAgent leadDetailsAgent;
    LeadDetailsSubAgent leadDetailsSubAgent;
    String key,keyadmin;
    LeadDetailsAdmin leadDetailsAdmin;
    LeadDetailsProcurementAdmin leadDetailsProcurementAdmin;



    public RecyclerAdapterSubAgentDocuments1(LeadDetailsProcurementAgent leadDetailsProcurementAgent, String document, String[] document2, List<String> strings, String keyp) {

        this.context=leadDetailsProcurementAgent;
        this.documentx=document2;
        this.Document=document;
        this.output=strings;
        this.key=keyp;
    }


    @NonNull
    @Override
    public RecyclerAdapterSubAgentDocuments1.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterSubAgentDocuments1.ViewHolder holder, final int position) {



        holder.image.setImageResource(R.drawable.imagepdf);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                document2="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+ output.get(position);

                if(key.equals("key")){

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);
                }
                if(key.equals("key1")){

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);
                }
                if(key.equals("keyadmin")){

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);
                }
                if(key.equals("keyp")){

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document2));
                    context.startActivity(browserIntent);
                }

                    }
                });

            }

    @Override
    public int getItemCount() {


        return output.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image=itemView.findViewById(R.id.image);

        }
    }
}
