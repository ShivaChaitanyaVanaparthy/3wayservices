package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.SupplierReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ViewReports;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.SupplierReportResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterSupplierReport extends RecyclerView.Adapter<RecyclerAdapterSupplierReport.ViewHolder> {

Context context;
    List<SupplierReportResponse.DataBean> dataBeans;



    public RecyclerAdapterSupplierReport(ViewReports applicationContext, List<SupplierReportResponse.DataBean> dataBeans) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;

    }

    public RecyclerAdapterSupplierReport(SupplierReportListDate supplierReportListDate, List<SupplierReportResponse.DataBean> dataBeans) {
        this.context=supplierReportListDate;
        this.dataBeans=dataBeans;
    }



    @NonNull
    @Override
    public RecyclerAdapterSupplierReport.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_machineryreport, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterSupplierReport.ViewHolder holder, final int position) {

        holder.engagername.setText(dataBeans.get(position).getPerson_name());
        holder.workname.setText(dataBeans.get(position).getWork_name());
        holder.subworkname.setText(dataBeans.get(position).getSubwork_name());
        holder.description.setText(dataBeans.get(position).getDescription());
        //holder.amount.setText(dataBeans.get(position).getA());
        holder.material.setText(dataBeans.get(position).getMaterial_type());
        holder.uom.setText(dataBeans.get(position).getUom());
        holder.qty.setText(dataBeans.get(position).getQuantity());

    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView engagername,workname,subworkname,description,amount,material,uom,qty;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            engagername=itemView.findViewById(R.id.engagername);
            workname=itemView.findViewById(R.id.workname);
            subworkname=itemView.findViewById(R.id.subworkname);
            description=itemView.findViewById(R.id.description);
            amount=itemView.findViewById(R.id.amount);
            material=itemView.findViewById(R.id.material);
            uom=itemView.findViewById(R.id.uom);
            qty=itemView.findViewById(R.id.qty);

        }
    }
}
