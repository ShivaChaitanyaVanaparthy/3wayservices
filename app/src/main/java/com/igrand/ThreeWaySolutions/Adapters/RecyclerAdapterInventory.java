package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddInvestment;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.InventoryAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryProjectResponse;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterInventory extends RecyclerView.Adapter<RecyclerAdapterInventory.ViewHolder>{

    InventoryAdmin context;
    TextView project;
    String Textt,selection,ProjectID;
    Dialog dialog;
    List<AdminInventoryProjectResponse.DataBean> dataBeans;
    Button submit;
    private RadioButton lastCheckedRB = null;


    public RecyclerAdapterInventory(InventoryAdmin inventoryAdmin, List<AdminInventoryProjectResponse.DataBean> dataBeans) {

        this.context=inventoryAdmin;
        this.dataBeans=dataBeans;


    }


    @NonNull
    @Override
    public RecyclerAdapterInventory.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_inventory, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {




        Textt=dataBeans.get(position).getProject_name();
        ProjectID=dataBeans.get(position).getId();
        holder.radio.setText(Textt);





        holder.rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }
                //store the clicked radiobutton
                lastCheckedRB = holder.radio;
                selection = holder.radio.getText().toString();
                context.TextgetProject(selection,dataBeans.get(position).getId());
                notifyDataSetChanged();
            }
        });

       /* if(holder.radio.isChecked()) {

            selection = holder.radio.getText().toString();
            context.Textget(selection);
            notifyDataSetChanged();
        }*/




       /* submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(holder.radio.isChecked()){

                    selection=holder.radio.getText().toString();
                    dialog.dismiss();
                    //context.Textget(selection);

                }


            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton radio;
        RadioGroup rg;

       /* TextView id,property,date,village,status,acres,survey;
        LinearLayout viewDetails;*/
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);
            rg=itemView.findViewById(R.id.rg);




           /* viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);*/


        }
    }
}
