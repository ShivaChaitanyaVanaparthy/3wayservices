package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddProjectEstimation;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddWorkReport;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ReportsList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminContractorList;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterEstimationProjectsList extends RecyclerView.Adapter<RecyclerAdapterEstimationProjectsList.ViewHolder>{

Context context;
    List<AdminProjectsList.DataBean> dataBeans;
    List<AdminWorkTypeList.DataBean> dataBeans1;
    List<AdminProjectsList.DataBean> dataBeans0;
    List<AdminSubWorkTypeListbyId.DataBean> dataBeans2;
    List<AdminUOMList.DataBean> dataBeans3;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    public int mSelectedItem1 = -1;
    TextView cityname,cityname1,cityname2,cityname3,cityname0;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid,selectedwork1,selectedworkid1,selectedwork2,selectedworkid2,selectedwork3,selectedworkid3,selectedwork0,selectedworkid0;
    AddProjectEstimation addProjectEstimation1;
    AddWorkReport addWorkReport;
    String key,key1,key2,keyreport;
    ReportsList reportsList1;



    public RecyclerAdapterEstimationProjectsList(AddProjectEstimation addProjectEstimation, List<AdminProjectsList.DataBean> dataBeans, AddProjectEstimation addProjectEstimation1, TextView projectname, Dialog dialog) {

        this.context=addProjectEstimation;
        this.dataBeans=dataBeans;
        this.cityname=projectname;
        this.dialog= dialog;
        this.addProjectEstimation1=addProjectEstimation1;
    }

    public RecyclerAdapterEstimationProjectsList(AddProjectEstimation addProjectEstimation, List<AdminWorkTypeList.DataBean> dataBeans, TextView worktype, Dialog dialog, AddProjectEstimation addProjectEstimation1, String key) {
        this.context=addProjectEstimation;
        this.dataBeans1=dataBeans;
        this.cityname1=worktype;
        this.dialog= dialog;
        this.addProjectEstimation1=addProjectEstimation1;
        this.key=key;
    }

    public RecyclerAdapterEstimationProjectsList(AddProjectEstimation addProjectEstimation, List<AdminSubWorkTypeListbyId.DataBean> dataBeans, Dialog dialog, AddProjectEstimation addProjectEstimation1, TextView worktype,String key2) {

        this.context=addProjectEstimation;
        this.dataBeans2=dataBeans;
        this.cityname2=worktype;
        this.dialog= dialog;
        this.addProjectEstimation1=addProjectEstimation1;
        this.key2=key2;
    }

    public RecyclerAdapterEstimationProjectsList(AddProjectEstimation addProjectEstimation, List<AdminUOMList.DataBean> dataBeans, TextView uom, AddProjectEstimation addProjectEstimation1, Dialog dialog, String key) {

        this.context=addProjectEstimation;
        this.dataBeans3=dataBeans;
        this.cityname3=uom;
        this.dialog= dialog;
        this.addProjectEstimation1=addProjectEstimation1;
        this.key1=key;
    }



    public RecyclerAdapterEstimationProjectsList(ReportsList reportsList, List<AdminProjectsList.DataBean> dataBeans, ReportsList reportsList1, TextView projectname, Dialog dialog, String keyreport) {


        this.context=reportsList;
        this.dataBeans0=dataBeans;
        this.cityname0=projectname;
        this.dialog= dialog;
        this.reportsList1=reportsList1;
        this.keyreport=keyreport;
    }


    @NonNull
    @Override
    public RecyclerAdapterEstimationProjectsList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterEstimationProjectsList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        if(dataBeans0!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans0.get(position).getProject_name());
        } if(dataBeans!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans.get(position).getProject_name());
        }

        if(dataBeans1!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans1.get(position).getWork_name());
        }

        if(dataBeans2!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans2.get(position).getSubwork_name());
        }
        if(dataBeans3!=null){
            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans3.get(position).getUom());
        }



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(selectedwork!=null) {

                    cityname.setText(selectedwork);
                    dialog.dismiss();
                }
                    else {
                        //Toast.makeText(context, "Please select the Values", Toast.LENGTH_SHORT).show();
                    }


if(keyreport!=null){
    if(keyreport.equals("keyreport")){
        if(selectedwork0!=null) {
            cityname0.setText(selectedwork0);
            dialog.dismiss();
        } else {
            Toast.makeText(context, "Please select Project", Toast.LENGTH_SHORT).show();
        }
    }
}if(key!=null){
    if(key.equals("key")){
        if(selectedwork1!=null) {
            cityname1.setText(selectedwork1);
            dialog.dismiss();
        } else {
            Toast.makeText(context, "Please select WorkType", Toast.LENGTH_SHORT).show();
        }
    }
}



if(key2!=null){
    if(key2.equals("sub")){
        if(selectedwork2!=null){
            cityname2.setText(selectedwork2);
            dialog.dismiss();
        }else {
            Toast.makeText(context, "Please select SubWorkType", Toast.LENGTH_SHORT).show();
        }
    }
}


                if(key1!=null) {
                    if (key1.equals("uom")) {
                        if (selectedwork3 != null) {
                            cityname3.setText(selectedwork3);
                            dialog.dismiss();
                        } else {
                            Toast.makeText(context, "Please select Units", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                }



        });













    }

    @Override
    public int getItemCount() {
        if(dataBeans0!=null){
            return dataBeans0.size();
        } if(dataBeans!=null){
            return dataBeans.size();
        } else if(dataBeans1!=null){
            return dataBeans1.size();
        }else if(dataBeans2!=null){
            return dataBeans2.size();
        }else if(dataBeans3!=null){
            return dataBeans3.size();
        }

        return dataBeans.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    if(dataBeans0!=null){
                        selectedwork0=dataBeans0.get(mSelectedItem).getProject_name();
                        selectedworkid0=dataBeans0.get(mSelectedItem).getId();

                        reportsList1.getId0(selectedworkid0);
                    }
 if(dataBeans!=null){
                        selectedwork=dataBeans.get(mSelectedItem).getProject_name();
                        selectedworkid=dataBeans.get(mSelectedItem).getId();

                        addProjectEstimation1.getId0(selectedworkid);
                    }


                    if(dataBeans1!=null){

                        selectedwork1=dataBeans1.get(mSelectedItem).getWork_name();
                        selectedworkid1=dataBeans1.get(mSelectedItem).getId();

                        addProjectEstimation1.getId1(selectedworkid1);


                    }
                   if(dataBeans2!=null){

                        selectedwork2=dataBeans2.get(mSelectedItem).getSubwork_name();
                        selectedworkid2=dataBeans2.get(mSelectedItem).getId();

                       addProjectEstimation1.getId2(selectedworkid2);

                    }
                   if(dataBeans3!=null){

                        selectedwork3=dataBeans3.get(mSelectedItem).getUom();
                        selectedworkid3=dataBeans3.get(mSelectedItem).getId();

                       addProjectEstimation1.getId3(selectedworkid3);
                    }

                }
            };




            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
