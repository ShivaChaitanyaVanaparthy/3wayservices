package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.R;
import com.vincent.filepicker.filter.entity.ImageFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterImages extends RecyclerView.Adapter<RecyclerAdapterImages.ViewHolder>{

Context context;
    List<String> dataBeans;
    ArrayList<ImageFile> mResults;


    public RecyclerAdapterImages(Context applicationContext, List<String> dataBeans, ArrayList<ImageFile> mResults) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;
        this.mResults=mResults;
    }



    @NonNull
    @Override
    public RecyclerAdapterImages.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterImages.ViewHolder holder, final int position) {

        //Picasso.get().load(dataBeans.get(position)).into(holder.img);


     /*   File imgFile = new  File(dataBeans.get(position));

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            holder.img.setImageBitmap(myBitmap);*/


        File imgFile1 = new  File(String.valueOf(dataBeans.get(position)));

        Bitmap myBitmap1 = BitmapFactory.decodeFile(imgFile1.getAbsolutePath());
        holder.img.setImageBitmap(myBitmap1);


        holder.img.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    dataBeans.remove(position);
                    holder.img.setVisibility(View.GONE);
                    notifyDataSetChanged();
                    return true;
                }
            });





    }

    @Override
    public int getItemCount() {
        return  dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

       ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img=itemView.findViewById(R.id.img);

        }
    }
}
