package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AdminProjectDetails;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.InventoryList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryList;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterInventoryList extends RecyclerView.Adapter<RecyclerAdapterInventoryList.Holder> {
    List<AdminInventoryList.DataBean> dataBeans;
    Context context;


    public RecyclerAdapterInventoryList(InventoryList projectsList, List<AdminInventoryList.DataBean> dataBeans) {
        this.context=projectsList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterInventoryList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_inventorylist, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterInventoryList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.villagename.setText(dataBeans.get(position).getMaterial_type());
        holder.projectname.setText(dataBeans.get(position).getProject_name());
        holder.description.setText(dataBeans.get(position).getDescriptions());
        holder.qty.setText(dataBeans.get(position).getQuantity());

        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }

    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,villagename,projectname,status,description,qty;
        LinearLayout viewDetails;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            villagename=itemView.findViewById(R.id.villagename);
            projectname=itemView.findViewById(R.id.projectname);
            status=itemView.findViewById(R.id.status);
            description=itemView.findViewById(R.id.description);
            qty=itemView.findViewById(R.id.qty);

        }
    }
}
