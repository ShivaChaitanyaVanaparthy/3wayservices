package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddEngagerReport;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddWorkReport;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VendorsAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddEngager;
import com.igrand.ThreeWaySolutions.Response.AdminContractorList;
import com.igrand.ThreeWaySolutions.Response.AdminMachineList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterEngagerProjectsList extends RecyclerView.Adapter<RecyclerAdapterEngagerProjectsList.ViewHolder>{


    List<AdminAddEngager.DataBean> dataBeans;
    List<AdminWorkTypeList.DataBean> dataBeans1;
    List<AdminSubWorkTypeListbyId.DataBean> dataBeans2;
    List<AdminMachineList.DataBean> dataBeans3;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    public int mSelectedItem1 = -1;
    TextView cityname3;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid,selectedworkid1,selectedwork1,selectedworkid2,selectedwork2,selectedwork3,selectedworkid3;
    AddEngagerReport addEngagerReport;
    VendorsAdmin vendorsAdmin;
    String key,key1,key2,key3,key4;




    public RecyclerAdapterEngagerProjectsList(AddEngagerReport addEngagerReport, List<AdminAddEngager.DataBean> dataBeans, TextView engagername, Dialog dialog,String key) {

        this.addEngagerReport=addEngagerReport;
        this.dataBeans=dataBeans;
        this.cityname3=engagername;
        this.dialog= dialog;
        this.key=key;
    }

    public RecyclerAdapterEngagerProjectsList(AddEngagerReport addEngagerReport, List<AdminWorkTypeList.DataBean> dataBeans1, TextView worktype, Dialog dialog, AddEngagerReport addEngagerReport1,String key1) {

        this.addEngagerReport=addEngagerReport;
        this.dataBeans1=dataBeans1;
        this.cityname3=worktype;
        this.dialog= dialog;
        this.key1=key1;
    }

    public RecyclerAdapterEngagerProjectsList(AddEngagerReport addEngagerReport, List<AdminSubWorkTypeListbyId.DataBean> dataBeans2, Dialog dialog, AddEngagerReport addEngagerReport1, TextView subworktype,String key2) {
        this.addEngagerReport=addEngagerReport;
        this.dataBeans2=dataBeans2;
        this.cityname3=subworktype;
        this.dialog= dialog;
        this.key2=key2;
    }

    public RecyclerAdapterEngagerProjectsList(AddEngagerReport addEngagerReport, List<AdminMachineList.DataBean> dataBeans3,Dialog dialog, TextView machinetype,String key3) {
        this.addEngagerReport=addEngagerReport;
        this.dataBeans3=dataBeans3;
        this.cityname3=machinetype;
        this.dialog= dialog;
        this.key3=key3;
    }

    public RecyclerAdapterEngagerProjectsList(VendorsAdmin vendorsAdmin, List<AdminMachineList.DataBean> dataBeans, Dialog dialog, TextView machinetype, String machinevendor) {

        this.vendorsAdmin=vendorsAdmin;
        this.dataBeans3=dataBeans;
        this.cityname3=machinetype;
        this.dialog= dialog;
        this.key4=machinevendor;
    }


    @NonNull
    @Override
    public RecyclerAdapterEngagerProjectsList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterEngagerProjectsList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        if(dataBeans!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans.get(position).getPerson_name());
        } if(dataBeans1!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans1.get(position).getWork_name());
        }if(dataBeans2!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans2.get(position).getSubwork_name());
        }if(dataBeans3!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans3.get(position).getType_name());
        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {






                if (key != null) {
                    if (key.equals("engager")) {
                        if (selectedwork != null) {
                            cityname3.setText(selectedwork);
                            dialog.dismiss();
                        } else {
                            Toast.makeText(addEngagerReport, "Please select Engager Name", Toast.LENGTH_SHORT).show();
                        }
                    }
                }



                if (key1 != null) {
                    if (key1.equals("work")) {
                        if (selectedwork1 != null) {
                            cityname3.setText(selectedwork1);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addEngagerReport, "Please select Worktype", Toast.LENGTH_SHORT).show();
                        }
                    }
                }


                if (key2!= null) {
                    if (key2.equals("sub")) {
                        if (selectedwork2 != null) {
                            cityname3.setText(selectedwork2);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addEngagerReport, "Please select SubWorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }


                if (key3!= null) {
                    if (key3.equals("machine")) {
                        if (selectedwork3 != null) {
                            cityname3.setText(selectedwork3);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addEngagerReport, "Please select MachineType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key4!= null) {
                    if (key4.equals("machinevendor")) {
                        if (selectedwork3 != null) {
                            cityname3.setText(selectedwork3);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addEngagerReport, "Please select MachineType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });





    }

    @Override
    public int getItemCount() {

        if(dataBeans!=null){
            return dataBeans.size();
        } else if(dataBeans1!=null){
            return dataBeans1.size();
        }else if(dataBeans2!=null){
            return dataBeans2.size();
        }else if(dataBeans3!=null){
            return dataBeans3.size();
        }
            return dataBeans.size();


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    if(dataBeans!=null){
                        selectedwork=dataBeans.get(mSelectedItem).getPerson_name();
                        selectedworkid=dataBeans.get(mSelectedItem).getId();

                        addEngagerReport.getId0(selectedworkid);
                    }
                   if(dataBeans1!=null){
                        selectedwork1=dataBeans1.get(mSelectedItem).getWork_name();
                        selectedworkid1=dataBeans1.get(mSelectedItem).getId();

                        addEngagerReport.getId1(selectedworkid1);
                    }
                   if(dataBeans2!=null){
                        selectedwork2=dataBeans2.get(mSelectedItem).getSubwork_name();
                        selectedworkid2=dataBeans2.get(mSelectedItem).getId();

                        addEngagerReport.getId2(selectedworkid2);
                    } if(dataBeans3!=null){
                        selectedwork3=dataBeans3.get(mSelectedItem).getType_name();
                        selectedworkid3=dataBeans3.get(mSelectedItem).getId();

                        if(addEngagerReport!=null){
                            addEngagerReport.getId3(selectedworkid3);
                        }
                        if(vendorsAdmin!=null){
                            vendorsAdmin.getId5(selectedworkid3);
                        }

                    }

                }
            };

            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
