package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.DistrictAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MandalAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ManaVillageRegister;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.PlayersListAdmin;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveDistList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminGameList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterStateActiveList extends RecyclerView.Adapter<RecyclerAdapterStateActiveList.ViewHolder>{

Context context;
    List<AdminActiveStateList.DataBean> dataBeans;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    TextView state;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid;
    DistrictAdmin districtAdmin1;
    List<AdminActiveStateList.DataBean> dataBeans1;
    MandalAdmin mandalAdmin1;
    List<AdminActiveDistList.DataBean> dataBeans2;
    VillageAdmin villageAdmin1;
    ManaVillageRegister manaVillageRegister1;
    PlayersListAdmin playersListAdmin1;





    public RecyclerAdapterStateActiveList(DistrictAdmin districtAdmin, List<AdminActiveStateList.DataBean> dataBeans, TextView state, Dialog dialog, DistrictAdmin districtAdmin1) {

        this.context=districtAdmin;
        this.dataBeans=dataBeans;
        this.state=state;
        this.dialog=dialog;
        this.districtAdmin1=districtAdmin1;
    }

    public RecyclerAdapterStateActiveList(MandalAdmin mandalAdmin, List<AdminActiveStateList.DataBean> dataBeans, TextView state, Dialog dialog, MandalAdmin mandalAdmin1) {

        this.context=mandalAdmin;
        this.dataBeans=dataBeans;
        this.state=state;
        this.dialog=dialog;
        this.mandalAdmin1=mandalAdmin1;
    }

    public RecyclerAdapterStateActiveList(VillageAdmin villageAdmin, List<AdminActiveStateList.DataBean> dataBeans, TextView state, Dialog dialog, VillageAdmin villageAdmin1) {

        this.context=villageAdmin;
        this.dataBeans=dataBeans;
        this.state=state;
        this.dialog=dialog;
        this.villageAdmin1=villageAdmin1;
    }

    public RecyclerAdapterStateActiveList(ManaVillageRegister manaVillageRegister, List<AdminActiveStateList.DataBean> dataBeans, TextView state, Dialog dialog, ManaVillageRegister manaVillageRegister1) {

        this.context=manaVillageRegister;
        this.dataBeans=dataBeans;
        this.state=state;
        this.dialog=dialog;
        this.manaVillageRegister1=manaVillageRegister1;
    }

    public RecyclerAdapterStateActiveList(PlayersListAdmin playersListAdmin, List<AdminActiveStateList.DataBean> dataBeans, TextView state, Dialog dialog, PlayersListAdmin playersListAdmin1) {
        this.context=playersListAdmin;
        this.dataBeans=dataBeans;
        this.state=state;
        this.dialog=dialog;
        this.playersListAdmin1=playersListAdmin1;

    }


    @NonNull
    @Override
    public RecyclerAdapterStateActiveList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterStateActiveList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        holder.radio.setChecked(position == mSelectedItem);
        holder.radio.setText(dataBeans.get(position).getState_name());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                state.setText(selectedwork);

            }
        });


        if(districtAdmin1!=null){
            districtAdmin1.getId(selectedworkid);
        }



        if(mandalAdmin1!=null){
            mandalAdmin1.getId(selectedworkid);
        }


        if(villageAdmin1!=null){
            villageAdmin1.getId(selectedworkid);
        }

        if(manaVillageRegister1!=null){
            manaVillageRegister1.getId(selectedworkid);
        }

        if(playersListAdmin1!=null){
            playersListAdmin1.getId(selectedworkid);
        }


    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    selectedwork=dataBeans.get(mSelectedItem).getState_name();
                    selectedworkid=dataBeans.get(mSelectedItem).getId();

                }
            };
            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
