package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddWorkReport;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VendorsAdmin;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMaterialUsage;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMeasurementSheet;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL1.AddWorkBOQ;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminContractorList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterContractorProjectsList extends RecyclerView.Adapter<RecyclerAdapterContractorProjectsList.ViewHolder>{


    List<AdminContractorList.DataBean> dataBeans;
    List<AdminWorkTypeList.DataBean> dataBeans1;
    List<AdminSubWorkTypeListbyId.DataBean> dataBeans2;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    public int mSelectedItem1 = -1;
    TextView cityname3;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid,selectedworkid1,selectedwork1,selectedworkid2,selectedwork2;
    AddWorkReport addWorkReport;
    String key,key1,key2,key3,key4,key5,key6,key7,key8,key9,key10;
    AddWorkBOQ addWorkBOQ;
    AddMeasurementSheet addMeasurementSheet;
    AddMaterialUsage addMaterialUsage;
    VendorsAdmin vendorsAdmin;



    public RecyclerAdapterContractorProjectsList(AddWorkReport addWorkReport, List<AdminContractorList.DataBean> dataBeans, TextView userType, Dialog dialog,String key) {

        this.addWorkReport=addWorkReport;
        this.dataBeans=dataBeans;
        this.cityname3=userType;
        this.dialog= dialog;
        this.key=key;
    }

    public RecyclerAdapterContractorProjectsList(AddWorkReport addWorkReport, List<AdminWorkTypeList.DataBean> dataBeans1, TextView worktype, Dialog dialog, AddWorkReport addWorkReport1,String key) {

        this.addWorkReport=addWorkReport;
        this.dataBeans1=dataBeans1;
        this.cityname3=worktype;
        this.dialog= dialog;
        this.key1=key;
    }

    public RecyclerAdapterContractorProjectsList(AddWorkReport addWorkReport, List<AdminSubWorkTypeListbyId.DataBean> dataBeans, Dialog dialog, AddWorkReport addWorkReport1, TextView subworktype,String key) {

        this.addWorkReport=addWorkReport;
        this.dataBeans2=dataBeans;
        this.cityname3=subworktype;
        this.dialog= dialog;
        this.key2=key;
    }

    public RecyclerAdapterContractorProjectsList(AddWorkBOQ addWorkBOQ, List<AdminSubWorkTypeListbyId.DataBean> dataBeans, Dialog dialog, AddWorkBOQ addWorkBOQ1, TextView subworktype, String sub) {

        this.addWorkBOQ=addWorkBOQ;
        this.dataBeans2=dataBeans;
        this.cityname3=subworktype;
        this.dialog= dialog;
        this.key3=sub;
    }

    public RecyclerAdapterContractorProjectsList(AddWorkBOQ addWorkBOQ, List<AdminWorkTypeList.DataBean> dataBeans, TextView worktype, Dialog dialog, AddWorkBOQ addWorkBOQ1, String work1) {

        this.addWorkBOQ=addWorkBOQ;
        this.dataBeans1=dataBeans;
        this.cityname3=worktype;
        this.dialog= dialog;
        this.key4=work1;
    }

    public RecyclerAdapterContractorProjectsList(AddMeasurementSheet addMeasurementSheet, List<AdminWorkTypeList.DataBean> dataBeans, TextView worktype, Dialog dialog, AddMeasurementSheet addMeasurementSheet1, String work2) {

        this.addMeasurementSheet=addMeasurementSheet;
        this.dataBeans1=dataBeans;
        this.cityname3=worktype;
        this.dialog= dialog;
        this.key5=work2;
    }

    public RecyclerAdapterContractorProjectsList(AddMeasurementSheet addMeasurementSheet, List<AdminSubWorkTypeListbyId.DataBean> dataBeans, Dialog dialog, AddMeasurementSheet addMeasurementSheet1, TextView subworktype, String subwork2) {
        this.addMeasurementSheet=addMeasurementSheet;
        this.dataBeans2=dataBeans;
        this.cityname3=subworktype;
        this.dialog= dialog;
        this.key6=subwork2;
    }

    public RecyclerAdapterContractorProjectsList(AddMaterialUsage addMaterialUsage, List<AdminWorkTypeList.DataBean> dataBeans, TextView worktype, Dialog dialog, AddMaterialUsage addMaterialUsage1, String work3) {

        this.addMaterialUsage=addMaterialUsage;
        this.dataBeans1=dataBeans;
        this.cityname3=worktype;
        this.dialog= dialog;
        this.key7=work3;
    }

    public RecyclerAdapterContractorProjectsList(AddMaterialUsage addMaterialUsage, List<AdminSubWorkTypeListbyId.DataBean> dataBeans, Dialog dialog, AddMaterialUsage addMaterialUsage1, TextView subworktype, String subwork3) {

        this.addMaterialUsage=addMaterialUsage;
        this.dataBeans2=dataBeans;
        this.cityname3=subworktype;
        this.dialog= dialog;
        this.key8=subwork3;
    }

    public RecyclerAdapterContractorProjectsList(VendorsAdmin vendorsAdmin, List<AdminWorkTypeList.DataBean> dataBeans, TextView worktype, Dialog dialog, VendorsAdmin vendorsAdmin1, String workvendor) {
        this.vendorsAdmin=vendorsAdmin;
        this.dataBeans1=dataBeans;
        this.cityname3=worktype;
        this.dialog= dialog;
        this.key9=workvendor;
    }

    public RecyclerAdapterContractorProjectsList(VendorsAdmin vendorsAdmin, List<AdminSubWorkTypeListbyId.DataBean> dataBeans, Dialog dialog, VendorsAdmin vendorsAdmin1, TextView subworktype, String subvendor) {
        this.vendorsAdmin=vendorsAdmin;
        this.dataBeans2=dataBeans;
        this.cityname3=subworktype;
        this.dialog= dialog;
        this.key10=subvendor;
    }


    @NonNull
    @Override
    public RecyclerAdapterContractorProjectsList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterContractorProjectsList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        if(dataBeans!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans.get(position).getPerson_name());
        } if(dataBeans1!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans1.get(position).getWork_name());
        }if(dataBeans2!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans2.get(position).getSubwork_name());
        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (key != null) {
                    if (key.equals("contractor")) {
                        if (selectedwork != null) {
                            cityname3.setText(selectedwork);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addWorkReport, "Please Select ContractorName", Toast.LENGTH_SHORT).show();
                        }
                    }
                }


                if (key1 != null) {
                    if (key1.equals("work")) {
                        if (selectedwork1 != null) {
                            cityname3.setText(selectedwork1);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addWorkReport, "Please Select WorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                if (key2 != null) {
                    if (key2.equals("sub")) {
                        if (selectedwork2 != null) {
                            cityname3.setText(selectedwork2);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addWorkReport, "Please Select SubWorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                } if (key3 != null) {
                    if (key3.equals("subwork")) {
                        if (selectedwork2 != null) {
                            cityname3.setText(selectedwork2);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addWorkBOQ, "Please Select SubWorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key6 != null) {
                    if (key6.equals("subwork2")) {
                        if (selectedwork2 != null) {
                            cityname3.setText(selectedwork2);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addMeasurementSheet, "Please Select SubWorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key8 != null) {
                    if (key8.equals("subwork3")) {
                        if (selectedwork2 != null) {
                            cityname3.setText(selectedwork2);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addMaterialUsage, "Please Select SubWorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key10 != null) {
                    if (key10.equals("subvendor")) {
                        if (selectedwork2 != null) {
                            cityname3.setText(selectedwork2);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addMaterialUsage, "Please Select SubWorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key4 != null) {
                    if (key4.equals("work1")) {
                        if (selectedwork1 != null) {
                            cityname3.setText(selectedwork1);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addWorkBOQ, "Please Select WorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key5!= null) {
                    if (key5.equals("work2")) {
                        if (selectedwork1 != null) {
                            cityname3.setText(selectedwork1);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addMeasurementSheet, "Please Select WorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key7!= null) {
                    if (key7.equals("work3")) {
                        if (selectedwork1 != null) {
                            cityname3.setText(selectedwork1);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addMaterialUsage, "Please Select WorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }if (key9!= null) {
                    if (key9.equals("workvendor")) {
                        if (selectedwork1 != null) {
                            cityname3.setText(selectedwork1);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(vendorsAdmin, "Please Select WorkType", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });





    }

    @Override
    public int getItemCount() {

        if(dataBeans!=null){
            return dataBeans.size();
        } else if(dataBeans1!=null){
            return dataBeans1.size();
        }else if(dataBeans2!=null){
            return dataBeans2.size();
        }
            return dataBeans.size();


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    if(dataBeans!=null){
                        selectedwork=dataBeans.get(mSelectedItem).getPerson_name();
                        selectedworkid=dataBeans.get(mSelectedItem).getId();

                        addWorkReport.getId0(selectedworkid);
                    }
                    if(dataBeans1!=null){
                        selectedwork1=dataBeans1.get(mSelectedItem).getWork_name();
                        selectedworkid1=dataBeans1.get(mSelectedItem).getId();

                        if(addWorkReport!=null){
                            addWorkReport.getId1(selectedworkid1);
                        }
                       if(addWorkBOQ!=null){
                           addWorkBOQ.getId1(selectedworkid1);
                       }
                       if(addMeasurementSheet!=null){
                           addMeasurementSheet.getId1(selectedworkid1);
                       } if(addMaterialUsage!=null){
                           addMaterialUsage.getId1(selectedworkid1);
                       }if(vendorsAdmin!=null){
                           vendorsAdmin.getId1(selectedworkid1);
                       }

                    }
if(dataBeans2!=null){
                        selectedwork2=dataBeans2.get(mSelectedItem).getSubwork_name();
                        selectedworkid2=dataBeans2.get(mSelectedItem).getId();
if(addWorkReport!=null){
    addWorkReport.getId2(selectedworkid2);
}

if(addWorkBOQ!=null){
    addWorkBOQ.getId2(selectedworkid2);
}if(addMeasurementSheet!=null){
    addMeasurementSheet.getId2(selectedworkid2);
    }if(addMaterialUsage!=null){
    addMaterialUsage.getId2(selectedworkid2);
    }if(vendorsAdmin!=null){
    vendorsAdmin.getId2(selectedworkid2);
    }
                        //addWorkBOQ.getId2(selectedworkid2);
                    }

                }
            };

            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
