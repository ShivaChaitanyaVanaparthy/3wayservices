package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.DistrictAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MandalAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ManaVillageRegister;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveDistList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminGameList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterGamesActiveList extends RecyclerView.Adapter<RecyclerAdapterGamesActiveList.ViewHolder>{

Context context;
    List<AdminGameList.DataBean> dataBeans;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    TextView state;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid;
    DistrictAdmin districtAdmin1;
    List<AdminActiveStateList.DataBean> dataBeans1;
    MandalAdmin mandalAdmin1;
    List<AdminActiveDistList.DataBean> dataBeans2;
    VillageAdmin villageAdmin1;
    ManaVillageRegister manaVillageRegister1;







    public RecyclerAdapterGamesActiveList(List<AdminGameList.DataBean> dataBeans, ManaVillageRegister manaVillageRegister, TextView games, Dialog dialog, ManaVillageRegister manaVillageRegister1) {

        this.context=manaVillageRegister;
        this.dataBeans=dataBeans;
        this.state=games;
        this.dialog=dialog;
        this.manaVillageRegister1=manaVillageRegister1;
    }




    @NonNull
    @Override
    public RecyclerAdapterGamesActiveList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterGamesActiveList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        holder.radio.setChecked(position == mSelectedItem);
        holder.radio.setText(dataBeans.get(position).getGame_type());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                state.setText(selectedwork);

            }
        });


        if(districtAdmin1!=null){
            districtAdmin1.getId(selectedworkid);
        }



        if(mandalAdmin1!=null){
            mandalAdmin1.getId(selectedworkid);
        }


        if(villageAdmin1!=null){
            villageAdmin1.getId(selectedworkid);
        }

        if(manaVillageRegister1!=null){
            manaVillageRegister1.getId5(selectedworkid);
        }





    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    selectedwork=dataBeans.get(mSelectedItem).getGame_type();
                    selectedworkid=dataBeans.get(mSelectedItem).getId();

                }
            };
            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
