package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.EarnsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ReferralsListIdAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.AddReferralAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.EarnsListAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ReferralsListIdAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.EarnsListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterEarnsAdmin extends RecyclerView.Adapter<RecyclerAdapterEarnsAdmin.ViewHolder>{


    Context context;
    List<EarnsListResponse.DataBean> dataBeans;




    public RecyclerAdapterEarnsAdmin(EarnsList applicationContext, List<EarnsListResponse.DataBean> dataBeans) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;

    }

    public RecyclerAdapterEarnsAdmin(EarnsListAgent earnsListAgent, List<EarnsListResponse.DataBean> dataBeans) {
        this.context=earnsListAgent;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterEarnsAdmin.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_earn1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterEarnsAdmin.ViewHolder holder, final int position) {

        holder.title.setText(dataBeans.get(position).getTitle());
        holder.desc.setText(dataBeans.get(position).getDescription());
        holder.desc.setText(dataBeans.get(position).getDescription());
        holder.date.setText(dataBeans.get(position).getDatetime());

        if(dataBeans.get(position).getStatus().equals("1")){
            holder.status.setText("ACTIVE");

        }else  if(dataBeans.get(position).getStatus().equals("0")) {
            holder.status.setText("INACTIVE");
        }


        holder.myreferral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ReferralsListIdAdmin.class);
                intent.putExtra("Id",dataBeans.get(position).getId());
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView title,desc,status,date;
        LinearLayout myreferral;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title=itemView.findViewById(R.id.title);
            desc=itemView.findViewById(R.id.desc);
            status=itemView.findViewById(R.id.status);
            date=itemView.findViewById(R.id.date);
            myreferral=itemView.findViewById(R.id.myreferral);



        }
    }
}
