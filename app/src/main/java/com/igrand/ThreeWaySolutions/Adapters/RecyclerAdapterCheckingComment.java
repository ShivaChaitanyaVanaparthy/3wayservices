package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterCheckingComment extends RecyclerView.Adapter<RecyclerAdapterCheckingComment.ViewHolder>{

Context context;
    List<AgentLeadsCommentsResponse.DataBean> dataBeans;
    String mobileNumber;





    public RecyclerAdapterCheckingComment(LeadDetailsChecking leadDetailsChecking, List<AgentLeadsCommentsResponse.DataBean> dataBeans, String mobileNumber) {
        this.context=leadDetailsChecking;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    @NonNull
    @Override
    public RecyclerAdapterCheckingComment.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterCheckingComment.ViewHolder holder, final int position) {



        if(mobileNumber.equals(dataBeans.get(position).getUser_mobile())) {


            holder.text.setText(dataBeans.get(position).getComments());
        }





    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            text=itemView.findViewById(R.id.text);



        }
    }
}
