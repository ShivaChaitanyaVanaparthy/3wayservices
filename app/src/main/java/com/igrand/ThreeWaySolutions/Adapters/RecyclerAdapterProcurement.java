package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.DashBoardProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.EditProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadsListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterProcurement extends RecyclerView.Adapter<RecyclerAdapterProcurement.ViewHolder>{

Context context;
    List<ProcurementLeadsListResponse.DataBean> dataBeans;
    String mobileNumber;
    public RecyclerAdapterProcurement(Context applicationContext) {
        this.context=applicationContext;

    }

    public RecyclerAdapterProcurement(DashBoardProcurement dashBoardProcurement, List<ProcurementLeadsListResponse.DataBean> dataBeans, String mobileNumber) {

        this.context=dashBoardProcurement;
        this.dataBeans=dataBeans;
        this.mobileNumber=mobileNumber;
    }

    @NonNull
    @Override
    public RecyclerAdapterProcurement.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_procurement, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterProcurement.ViewHolder holder, final int position) {



        final Activity activity = (Activity) context;

        holder.id.setText(dataBeans.get(position).getId());
        holder.property.setText(dataBeans.get(position).getProperty_name());
        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.village.setText(dataBeans.get(position).getVillage_name());
        holder.acres.setText(dataBeans.get(position).getAcres());
        holder.survey.setText(dataBeans.get(position).getSurvey_no());

        if(dataBeans.get(position).getPt_status().equals("4")){
            holder.status.setText("REJECTED");
        }else  if(dataBeans.get(position).getPt_status().equals("5")) {
            holder.status.setText("APPROVED");
        }else  if(dataBeans.get(position).getPt_status().equals("2")) {
            holder.status.setText("OPEN");
        }


        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String checkingdate=dataBeans.get(position).getCt_update_dt();
                Intent intent=new Intent(context, LeadDetailsProcurement.class);
                intent.putExtra("ID",dataBeans.get(position).getId());
                intent.putExtra("Property",dataBeans.get(position).getProperty_name());
                intent.putExtra("Date",dataBeans.get(position).getDatetime());
                intent.putExtra("Village",dataBeans.get(position).getVillage_name());
                intent.putExtra("Checking",dataBeans.get(position).getCt_status());

                intent.putExtra("Document",dataBeans.get(position).getDocument());
                intent.putExtra("Image",dataBeans.get(position).getImages());

                intent.putExtra("Status",dataBeans.get(position).getStatus());

                intent.putExtra("Comments",dataBeans.get(position).getCt_status());
                intent.putExtra("Latitude",dataBeans.get(position).getLatitude());
                intent.putExtra("Longitude",dataBeans.get(position).getLongitude());

                intent.putExtra("MarketingStatus",dataBeans.get(position).getMt_status());
                intent.putExtra("MarketingDate",dataBeans.get(position).getMt_update_dt());
                intent.putExtra("CheckingDate",checkingdate);
                intent.putExtra("ProcurementDate",dataBeans.get(position).getPt_update_dt());
                intent.putExtra("ProcurementStatus",dataBeans.get(position).getPt_status());
                intent.putExtra("Propertydesc",dataBeans.get(position).getProperty_description());


                //intent.putExtra("ProposalNotes",dataBeans.get(position).getProposal_notes());
                intent.putExtra("Mandal",dataBeans.get(position).getMandal_name());
                intent.putExtra("District",dataBeans.get(position).getDistrict_name());

                intent.putExtra("TechnicalStatus",dataBeans.get(position).getTt_status());
                intent.putExtra("TechnicalDate",dataBeans.get(position).getTt_update_dt());
                intent.putExtra("key","key1");
                intent.putExtra("Status",dataBeans.get(position).getCt_status());
                intent.putExtra("Acres",dataBeans.get(position).getAcres());
                intent.putExtra("Survey",dataBeans.get(position).getSurvey_no());


                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });


        /*holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        Intent intent=new Intent(context, EditProcurement.class);
                        context.startActivity(intent);

            }
        });*/



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        LinearLayout viewDetails;
        TextView id,property,date,village,status,acres,survey;
       // ImageView edit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            //edit=itemView.findViewById(R.id.edit);

            viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);



        }
    }
}
