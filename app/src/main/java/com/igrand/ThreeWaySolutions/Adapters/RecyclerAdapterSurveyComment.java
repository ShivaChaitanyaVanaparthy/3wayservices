package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.SURVEY.LeadDetailsSurvey;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterSurveyComment extends RecyclerView.Adapter<RecyclerAdapterSurveyComment.ViewHolder>{

Context context;
    List<AgentLeadsCommentsResponse.DataBean> dataBeans;



    public RecyclerAdapterSurveyComment(LeadDetailsSurvey leadDetailsSurvey, List<AgentLeadsCommentsResponse.DataBean> dataBeans) {

        this.context=leadDetailsSurvey;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterSurveyComment.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterSurveyComment.ViewHolder holder, final int position) {


        holder.text.setText(dataBeans.get(position).getComments());




    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            text=itemView.findViewById(R.id.text);



        }
    }
}
