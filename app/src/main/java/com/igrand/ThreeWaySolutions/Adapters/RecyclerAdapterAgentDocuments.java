package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsProcurementAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage1;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjectsDetails;
import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterAgentDocuments extends RecyclerView.Adapter<RecyclerAdapterAgentDocuments.ViewHolder>{

    LeadDetailsSubAgent leadDetailsSubAgent;
    String document1,document2;
    String[] document,document3,document4;
    String Document;
    String[] documentx;
    List<String> strings;
    LeadDetailsProcurementAdmin leadDetailsProcurementAdmin;
    LeadDetailsAdmin leadDetailsAdmin;
    LeadDetailsAgent leadDetailsAgent;
    Context context;
    String stringsss;
    ArrayList<String> list=new ArrayList<>();





    public RecyclerAdapterAgentDocuments(LeadDetailsSubAgent leadDetailsSubAgent, String[] document2, String document, List<String> strings) {
        this.context=leadDetailsSubAgent;
        this.documentx=document2;
        this.Document=document;
        this.strings=strings;


    }

    public RecyclerAdapterAgentDocuments(LeadDetailsProcurementAdmin leadDetailsProcurementAdmin, String[] document2, String document, List<String> strings) {
        this.context=leadDetailsProcurementAdmin;
        this.documentx=document2;
        this.Document=document;
        this.strings=strings;
    }

    public RecyclerAdapterAgentDocuments(LeadDetailsAdmin leadDetailsAdmin, String[] document2, String document, List<String> strings) {

        this.context=leadDetailsAdmin;
        this.documentx=document2;
        this.Document=document;
        this.strings=strings;

    }

    public RecyclerAdapterAgentDocuments(LeadDetailsAgent leadDetailsAgent, String[] document2, List<String> strings, String document) {

        this.context=leadDetailsAgent;
        this.documentx=document2;
        this.Document=document;
        this.strings=strings;
    }

    public RecyclerAdapterAgentDocuments(NearbyProjectsDetails nearbyProjectsDetails, String[] document2, List<String> strings, String document) {


        this.context=nearbyProjectsDetails;
        this.documentx=document2;
        this.Document=document;
        this.strings=strings;
    }

    public RecyclerAdapterAgentDocuments(LeadDetailsAdmin leadDetailsAdmin, String[] document2, List<String> strings, String document) {
        this.context=leadDetailsAdmin;
        this.documentx=document2;
        this.Document=document;
        this.strings=strings;
    }


    @NonNull
    @Override
    public RecyclerAdapterAgentDocuments.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterAgentDocuments.ViewHolder holder, final int position) {


        document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

        Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);


        list.add(document1);










        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+strings.get(position);

                Intent intent = new Intent(context, ProfileImage1.class);
                intent.putExtra("Image",document1);
                intent.putExtra("List",list);
                context.startActivity(intent);


                   /* holder.itemView.buildDrawingCache();
                    Bitmap bitmap = holder.itemView.getDrawingCache();*/

                  /*  ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, _bs);
                    intent.putExtra("byteArray", _bs.toByteArray());*/





                    }
                });



            }



    @Override
    public int getItemCount() {


        return strings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        //PDFView pdfView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            image=itemView.findViewById(R.id.image);
            //pdfView= itemView.findViewById(R.id.pdfView);



        }
    }
}
