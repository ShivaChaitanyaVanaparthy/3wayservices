package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.ValuesList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VendorsList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.AdminValuesList;
import com.igrand.ThreeWaySolutions.Response.AdminVendorList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterValueList extends RecyclerView.Adapter<RecyclerAdapterValueList.Holder> {
    List<AdminValuesList.DataBean> dataBeans;
    Context context;



    public RecyclerAdapterValueList(ValuesList projectsList, List<AdminValuesList.DataBean> dataBeans) {

        this.context=projectsList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterValueList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_values, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterValueList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.materialtype.setText(dataBeans.get(position).getMaterial_type());
        holder.date.setText(dataBeans.get(position).getDatetime());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }

    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView materialtype,status,date;
        public Holder(@NonNull View itemView) {
            super(itemView);
            materialtype=itemView.findViewById(R.id.materialtype);
            status=itemView.findViewById(R.id.status);
            date=itemView.findViewById(R.id.date);

        }
    }
}
