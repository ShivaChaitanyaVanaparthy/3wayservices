package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.CommentResponse;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterComments extends RecyclerView.Adapter<RecyclerAdapterComments.ViewHolder>{

Context context;
    List<CommentResponse.DataBean> dataBeans;

    public RecyclerAdapterComments(Context applicationContext, List<CommentResponse.DataBean> dataBeans) {
        this.context=applicationContext;
        this.dataBeans=dataBeans;


    }

    @NonNull
    @Override
    public RecyclerAdapterComments.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_comments1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterComments.ViewHolder holder, final int position) {




        holder.userName.setText(dataBeans.get(position).getUsername());
        holder.userType.setText(dataBeans.get(position).getUsertype());
        holder.comments.setText(dataBeans.get(position).getComments());
        holder.mobile.setText(dataBeans.get(position).getUser_mobile());
        holder.date.setText(dataBeans.get(position).getDatetime());








    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView userType,userName,comments,mobile,date;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            userType=itemView.findViewById(R.id.userType);
            userName=itemView.findViewById(R.id.userName);
            comments=itemView.findViewById(R.id.comments);
            mobile=itemView.findViewById(R.id.mobile);
            date=itemView.findViewById(R.id.date);

        }
    }
}
