package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkReportListDate;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkReports;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;
import com.igrand.ThreeWaySolutions.Response.WorkReportLIst;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerWorkReport1 extends RecyclerView.Adapter<RecyclerWorkReport1.ViewHolder>{

   Context context;
    List<AdminWorkReportList.DataBean> dataBeans;
    List<List<WorkReportLIst.DataBean>> dataBeans1;






    public RecyclerWorkReport1(WorkReports context, List<List<WorkReportLIst.DataBean>> dataBeans) {

        this.context=context;
        this.dataBeans1=dataBeans;


    }

    @NonNull
    @Override
    public RecyclerWorkReport1.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_workreport1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerWorkReport1.ViewHolder holder, final int position) {



 if(dataBeans1!=null){

            holder.projectname.setText(dataBeans1.get(position).get(0).getProject_name());
            holder.date.setText(dataBeans1.get(position).get(0).getDate());

        }


 holder.viewDetails.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View view) {
         Intent intent=new Intent(context, WorkReportListDate.class);
         intent.putExtra("ID",dataBeans1.get(position).get(0).getProject_id());
         intent.putExtra("Date",dataBeans1.get(position).get(0).getDate());
         context.startActivity(intent);
     }
 });







    }

    @Override
    public int getItemCount() {

        return dataBeans1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView projectname,date,amount;
        LinearLayout viewDetails;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            projectname=itemView.findViewById(R.id.projectname);
            date=itemView.findViewById(R.id.date);
            amount=itemView.findViewById(R.id.amount);
            viewDetails=itemView.findViewById(R.id.viewDetails);

        }
    }
}
