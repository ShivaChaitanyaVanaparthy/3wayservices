package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.SubWorkTypeList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkTypeList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterSubWorkTypeList extends RecyclerView.Adapter<RecyclerAdapterSubWorkTypeList.Holder> {
    List<AdminSubWorkTypeList.DataBean> dataBeans;
    Context context;



    public RecyclerAdapterSubWorkTypeList(SubWorkTypeList subWorkTypeList, List<AdminSubWorkTypeList.DataBean> dataBeans) {

        this.context=subWorkTypeList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterSubWorkTypeList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_subworktype, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterSubWorkTypeList.Holder holder, final int position) {

        final Activity activity = (Activity) context;


        holder.cityname.setText(dataBeans.get(position).getWork_name());
        holder.date.setText(dataBeans.get(position).getId());
        holder.subworkname.setText(dataBeans.get(position).getSubwork_name());
        holder.itemno.setText(dataBeans.get(position).getItem_head_no());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }



    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,cityname,status,subworkname,itemno;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            cityname=itemView.findViewById(R.id.cityname);
            status=itemView.findViewById(R.id.status);
            subworkname=itemView.findViewById(R.id.subworkname);
            itemno=itemView.findViewById(R.id.itemno);
        }
    }
}
