package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LegalTeamDocProcurement;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LegalDocResponse;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterLegalTeamDocProcurement extends RecyclerView.Adapter<RecyclerAdapterLegalTeamDocProcurement.ViewHolder>{

Context context;
    List<LegalDocResponse.DocumentNameListBean> dataBeans;





    public RecyclerAdapterLegalTeamDocProcurement(LegalTeamDocProcurement legalTeamDocProcurement, List<LegalDocResponse.DocumentNameListBean> dataBeans) {

        this.context=legalTeamDocProcurement;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterLegalTeamDocProcurement.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_legaldoc, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterLegalTeamDocProcurement.ViewHolder holder, final int position) {




        holder.text.setText(dataBeans.get(position).getDocument_name());





    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text,property,date,village,status,acres,survey;
        LinearLayout viewDetails;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            text=itemView.findViewById(R.id.text);

           /* viewDetails=itemView.findViewById(R.id.viewDetails);
            id=itemView.findViewById(R.id.id);
            property=itemView.findViewById(R.id.property);
            date=itemView.findViewById(R.id.date);
            village=itemView.findViewById(R.id.village);
            status=itemView.findViewById(R.id.status);
            acres=itemView.findViewById(R.id.acres);
            survey=itemView.findViewById(R.id.survey);*/


        }
    }
}
