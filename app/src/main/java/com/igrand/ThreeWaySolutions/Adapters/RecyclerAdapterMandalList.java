package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.DistrictList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MandalList;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminDistrictList;
import com.igrand.ThreeWaySolutions.Response.AdminMandalList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterMandalList extends RecyclerView.Adapter<RecyclerAdapterMandalList.Holder> {
    List<AdminMandalList.DataBean> dataBeans;
    Context context;







    public RecyclerAdapterMandalList(MandalList mandalList, List<AdminMandalList.DataBean> dataBeans) {

        this.context=mandalList;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public RecyclerAdapterMandalList.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_man, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMandalList.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBeans.get(position).getDatetime());
        holder.cityname.setText(dataBeans.get(position).getState_name());
        holder.dstname.setText(dataBeans.get(position).getDistrict_name());
        holder.mandal.setText(dataBeans.get(position).getMandal_name());


        if(dataBeans.get(position).getStatus().equals("1")) {

            holder.status.setText("ACTIVE");

        } else if (dataBeans.get(position).getStatus().equals("0")){

            holder.status.setText("IN-ACTIVE");

        }



    }

    @Override
    public int getItemCount() {
        
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,cityname,status,dstname,mandal;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            cityname=itemView.findViewById(R.id.cityname);
            status=itemView.findViewById(R.id.status);
            dstname=itemView.findViewById(R.id.dstname);
            mandal=itemView.findViewById(R.id.mandal);
        }
    }
}
