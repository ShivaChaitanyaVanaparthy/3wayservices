package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.LeadDetailsLegal;
import com.igrand.ThreeWaySolutions.Activities.LESION.LeadDetailsLesion;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage1;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjectsDetails;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Activities.SURVEY.LeadDetailsSurvey;
import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterCheckingDocuments6 extends RecyclerView.Adapter<RecyclerAdapterCheckingDocuments6.ViewHolder>{

Context context;
    String[] document;
    String document1,document2,document3;

    ArrayList<String> list=new ArrayList<>();



    public RecyclerAdapterCheckingDocuments6(LeadDetailsLegal leadDetailsLegal, String[] document, String document2) {

        this.context=leadDetailsLegal;
        this.document=document;
    }



    @NonNull
    @Override
    public RecyclerAdapterCheckingDocuments6.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_document, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterCheckingDocuments6.ViewHolder holder, final int position) {



            document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+document[position];
            if(document1!=null){

                Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(holder.image);
            }


            list.add(document1);






        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+document[position];

                Intent intent = new Intent(context, ProfileImage1.class);
                intent.putExtra("Image",document1);
                intent.putExtra("List",list);
                context.startActivity(intent);

                      /*  holder.itemView.buildDrawingCache();
                        Bitmap bitmap = holder.itemView.getDrawingCache();
                        Intent intent = new Intent(context, ProfileImage.class);
                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        context.startActivity(intent);*/
                    }
                });
            }


    @Override
    public int getItemCount() {
        return document.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            image=itemView.findViewById(R.id.image);



        }
    }
}
