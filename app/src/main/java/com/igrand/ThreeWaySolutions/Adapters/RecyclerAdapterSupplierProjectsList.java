package com.igrand.ThreeWaySolutions.Adapters;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddSupplierReport;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VendorsAdmin;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMaterialUsage;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMeasurementSheet;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL1.AddWorkBOQ;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddSupplier;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryMaterialResponse;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterSupplierProjectsList extends RecyclerView.Adapter<RecyclerAdapterSupplierProjectsList.ViewHolder>{


    List<AdminAddSupplier.DataBean> dataBeans;
    List<AdminWorkTypeList.DataBean> dataBeans1;
    List<AdminSubWorkTypeListbyId.DataBean> dataBeans2;
    List<AdminInventoryMaterialResponse.DataBean> dataBeans3;
    List<AdminUOMList.DataBean> dataBeans4;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    public int mSelectedItem1 = -1;
    TextView cityname3;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid,selectedworkid1,selectedwork1,selectedworkid2,selectedwork2,selectedwork3,selectedworkid3,selectedworkid4,selectedwork4;
    AddSupplierReport addEngagerReport;
    String key,key1,key2,key3,key4,key5,key6,key7,key8,key9,key10,key11;
    AddWorkBOQ addWorkBOQ;
    AddMeasurementSheet addMeasurementSheet;
    AddMaterialUsage addMaterialUsage;
    VendorsAdmin vendorsAdmin;




    public RecyclerAdapterSupplierProjectsList(AddSupplierReport addSupplierReport, List<AdminAddSupplier.DataBean> dataBeans, TextView suppliername, Dialog dialog,String key) {

        this.addEngagerReport=addSupplierReport;
        this.dataBeans=dataBeans;
        this.cityname3=suppliername;
        this.dialog= dialog;
        this.key=key;
    }

    public RecyclerAdapterSupplierProjectsList(AddSupplierReport addSupplierReport, List<AdminWorkTypeList.DataBean> dataBeans1, TextView worktype, Dialog dialog, AddSupplierReport addSupplierReport1,String key1) {
        this.addEngagerReport=addSupplierReport;
        this.dataBeans1=dataBeans1;
        this.cityname3=worktype;
        this.dialog= dialog;
        this.key1=key1;
    }

    public RecyclerAdapterSupplierProjectsList(AddSupplierReport addSupplierReport, List<AdminSubWorkTypeListbyId.DataBean> dataBeans2, Dialog dialog, AddSupplierReport addSupplierReport1, TextView subworktype,String key2) {
        this.addEngagerReport=addSupplierReport;
        this.dataBeans2=dataBeans2;
        this.cityname3=subworktype;
        this.dialog= dialog;
        this.key2=key2;
    }


    public RecyclerAdapterSupplierProjectsList(AddSupplierReport addSupplierReport, List<AdminInventoryMaterialResponse.DataBean> dataBeans3, Dialog dialog, TextView materialtype,String key3) {

        this.addEngagerReport=addSupplierReport;
        this.dataBeans3=dataBeans3;
        this.cityname3=materialtype;
        this.dialog= dialog;
        this.key3=key3;
    }

    public RecyclerAdapterSupplierProjectsList(AddSupplierReport addSupplierReport, TextView materialtype, List<AdminUOMList.DataBean> dataBeans4, Dialog dialog,String key4) {

        this.addEngagerReport=addSupplierReport;
        this.dataBeans4=dataBeans4;
        this.cityname3=materialtype;
        this.dialog= dialog;
        this.key4=key4;
    }

    public RecyclerAdapterSupplierProjectsList(AddWorkBOQ addWorkBOQ, TextView uom, List<AdminUOMList.DataBean> dataBeans, Dialog dialog, String uom1) {
        this.addWorkBOQ=addWorkBOQ;
        this.dataBeans4=dataBeans;
        this.cityname3=uom;
        this.dialog= dialog;
        this.key5=uom1;
    }

    public RecyclerAdapterSupplierProjectsList(AddMeasurementSheet addMeasurementSheet, TextView uom, List<AdminUOMList.DataBean> dataBeans, Dialog dialog, String uom2) {

        this.addMeasurementSheet=addMeasurementSheet;
        this.dataBeans4=dataBeans;
        this.cityname3=uom;
        this.dialog= dialog;
        this.key6=uom2;
    }

    public RecyclerAdapterSupplierProjectsList(AddMeasurementSheet addMeasurementSheet, List<AdminAddSupplier.DataBean> dataBeans, TextView name, Dialog dialog, String supplier1) {

        this.addMeasurementSheet=addMeasurementSheet;
        this.dataBeans=dataBeans;
        this.cityname3=name;
        this.dialog= dialog;
        this.key7=supplier1;
    }

    public RecyclerAdapterSupplierProjectsList(AddMaterialUsage addMaterialUsage, TextView uom, List<AdminUOMList.DataBean> dataBeans, Dialog dialog, String uom3) {

        this.addMaterialUsage=addMaterialUsage;
        this.dataBeans4=dataBeans;
        this.cityname3=uom;
        this.dialog= dialog;
        this.key7=uom3;
    }

    public RecyclerAdapterSupplierProjectsList(AddMaterialUsage addMaterialUsage, List<AdminAddSupplier.DataBean> dataBeans, TextView name, Dialog dialog, String supplier3) {

        this.addMaterialUsage=addMaterialUsage;
        this.dataBeans=dataBeans;
        this.cityname3=name;
        this.dialog= dialog;
        this.key8=supplier3;
    }

    public RecyclerAdapterSupplierProjectsList(AddMaterialUsage addMaterialUsage, List<AdminInventoryMaterialResponse.DataBean> dataBeans, Dialog dialog, TextView materialtype, String material3) {
        this.addMaterialUsage=addMaterialUsage;
        this.dataBeans3=dataBeans;
        this.cityname3=materialtype;
        this.dialog= dialog;
        this.key9=material3;
    }

    public RecyclerAdapterSupplierProjectsList(VendorsAdmin vendorsAdmin, TextView uom, List<AdminUOMList.DataBean> dataBeans, Dialog dialog, String uomvendor) {

        this.vendorsAdmin=vendorsAdmin;
        this.dataBeans4=dataBeans;
        this.cityname3=uom;
        this.dialog= dialog;
        this.key10=uomvendor;
    }

    public RecyclerAdapterSupplierProjectsList(VendorsAdmin vendorsAdmin, List<AdminInventoryMaterialResponse.DataBean> dataBeans, Dialog dialog, TextView materialtype, String materialvendor) {
        this.vendorsAdmin=vendorsAdmin;
        this.dataBeans3=dataBeans;
        this.cityname3=materialtype;
        this.dialog= dialog;
        this.key11=materialvendor;
    }



    @NonNull
    @Override
    public RecyclerAdapterSupplierProjectsList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_radio, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterSupplierProjectsList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);


        if(dataBeans!=null) {

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans.get(position).getPerson_name());
        }
         if(dataBeans1!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans1.get(position).getWork_name());
        }

         if(dataBeans2!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans2.get(position).getSubwork_name());
        }

        if(dataBeans3!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans3.get(position).getMaterial_type());
        }if(dataBeans4!=null){

            holder.radio.setChecked(position == mSelectedItem);
            holder.radio.setText(dataBeans4.get(position).getUom());
        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {






                if (key!= null) {
                    if (key.equals("supplier")) {
                        if (selectedwork != null) {
                            cityname3.setText(selectedwork);
                            dialog.dismiss();
                        } else {

                            Toast.makeText(addEngagerReport, "Please select Supplier", Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                if (key7!= null) {
                    if (key7.equals("supplier1")) {
                        if (selectedwork != null) {
                            cityname3.setText(selectedwork);
                            dialog.dismiss();
                        } else {

                            Toast.makeText(addMeasurementSheet, "Please select Name", Toast.LENGTH_SHORT).show();

                        }
                    }
                }


                if (key1 != null) {
                    if (key1.equals("work")) {
                        if (selectedwork1 != null) {
                            cityname3.setText(selectedwork1);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addEngagerReport, "Please select WorkType", Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                if (key2!= null) {
                    if (key2.equals("sub")) {
                if(selectedwork2!=null){
                    cityname3.setText(selectedwork2);
                    dialog.dismiss();
                }else {

                    Toast.makeText(addEngagerReport, "Please select SubWorkType", Toast.LENGTH_SHORT).show();

                }
                    }}

                if (key3!= null) {
                    if (key3.equals("material")) {
                        if (selectedwork3 != null) {
                            cityname3.setText(selectedwork3);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addEngagerReport, "Please select Material", Toast.LENGTH_SHORT).show();

                        }
                    }
                }if (key11!= null) {
                    if (key11.equals("materialvendor")) {
                        if (selectedwork3 != null) {
                            cityname3.setText(selectedwork3);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addEngagerReport, "Please select Material", Toast.LENGTH_SHORT).show();

                        }
                    }
                }
                if (key4!= null) {
                    if (key4.equals("uom")) {
                        if (selectedwork4 != null) {
                            cityname3.setText(selectedwork4);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addEngagerReport, "Please select Units", Toast.LENGTH_SHORT).show();

                        }
                    }
                }if (key10!= null) {
                    if (key10.equals("uomvendor")) {
                        if (selectedwork4 != null) {
                            cityname3.setText(selectedwork4);
                            dialog.dismiss();
                        }else {
                            Toast.makeText(addEngagerReport, "Please select Units", Toast.LENGTH_SHORT).show();

                        }
                    }
                } if (key5!= null) {
                    if (key5.equals("uom1")) {
                        if (selectedwork4 != null) {
                            cityname3.setText(selectedwork4);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addWorkBOQ, "Please select Units", Toast.LENGTH_SHORT).show();

                        }
                    }
                }if (key6!= null) {
                    if (key6.equals("uom2")) {
                        if (selectedwork4 != null) {
                            cityname3.setText(selectedwork4);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addMeasurementSheet, "Please select Units", Toast.LENGTH_SHORT).show();

                        }
                    }
                }if (key7!= null) {
                    if (key7.equals("uom3")) {
                        if (selectedwork4 != null) {
                            cityname3.setText(selectedwork4);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addMaterialUsage, "Please select Units", Toast.LENGTH_SHORT).show();

                        }
                    }
                }if (key8!= null) {
                    if (key8.equals("supplier3")) {
                        if (selectedwork != null) {
                            cityname3.setText(selectedwork);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addMaterialUsage, "Please select Name", Toast.LENGTH_SHORT).show();

                        }
                    }
                }if (key9!= null) {
                    if (key9.equals("material3")) {
                        if (selectedwork3 != null) {
                            cityname3.setText(selectedwork3);
                            dialog.dismiss();
                        }else {

                            Toast.makeText(addMaterialUsage, "Please select MaterialType", Toast.LENGTH_SHORT).show();

                        }
                    }
                }

            }
        });





    }

    @Override
    public int getItemCount() {

        if(dataBeans!=null){
            return dataBeans.size();
        } else if(dataBeans1!=null) {
            return dataBeans1.size();
        }
        else if(dataBeans2!=null){
            return dataBeans2.size();
        }
        else if(dataBeans3!=null){
            return dataBeans3.size();
        }else if(dataBeans4!=null){
            return dataBeans4.size();
        }
            return dataBeans.size();


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    if(dataBeans!=null){
                        selectedwork=dataBeans.get(mSelectedItem).getPerson_name();
                        selectedworkid=dataBeans.get(mSelectedItem).getId();

                        if(addEngagerReport!=null){
                            addEngagerReport.getId0(selectedworkid);
                        }
                        if(addMeasurementSheet!=null){
                            addMeasurementSheet.getId0(selectedworkid);
                        }if(addMaterialUsage!=null){
                            addMaterialUsage.getId0(selectedworkid);
                        }

                    }
                   if(dataBeans1!=null){
                        selectedwork1=dataBeans1.get(mSelectedItem).getWork_name();
                        selectedworkid1=dataBeans1.get(mSelectedItem).getId();

                        addEngagerReport.getId1(selectedworkid1);
                    }
                   if(dataBeans2!=null){
                        selectedwork2=dataBeans2.get(mSelectedItem).getSubwork_name();
                        selectedworkid2=dataBeans2.get(mSelectedItem).getId();

                        addEngagerReport.getId2(selectedworkid2);
                    }
//
                    if(dataBeans3!=null){
                        selectedwork3=dataBeans3.get(mSelectedItem).getMaterial_type();
                        selectedworkid3=dataBeans3.get(mSelectedItem).getId();

                        if(addEngagerReport!=null){
                            addEngagerReport.getId3(selectedworkid3);
                        } if(addMaterialUsage!=null){
                            addMaterialUsage.getId3(selectedworkid3);
                        }if(vendorsAdmin!=null){
                            vendorsAdmin.getId3(selectedworkid3);
                        }

                    } if(dataBeans4!=null){
                        selectedwork4=dataBeans4.get(mSelectedItem).getUom();
                        selectedworkid4=dataBeans4.get(mSelectedItem).getId();

                       if(addEngagerReport!=null){
                           addEngagerReport.getId4(selectedworkid4);
                       }
                       if (addWorkBOQ!=null){
                           addWorkBOQ.getId4(selectedworkid4);
                       }
                       if(addMeasurementSheet!=null){
                           addMeasurementSheet.getId4(selectedworkid4);
                       }if(addMaterialUsage!=null){
                           addMaterialUsage.getId4(selectedworkid4);
                       }if(vendorsAdmin!=null){
                           vendorsAdmin.getId4(selectedworkid4);
                       }

                    }

                }
            };

            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);


        }
    }
}
