package com.igrand.ThreeWaySolutions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.MARKETING.NotificationsMarketing;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.NotificationsAgentListResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterNotificationsMarketing extends RecyclerView.Adapter<RecyclerAdapterNotificationsMarketing.ViewHolder>{

Context context;
    List<NotificationsAgentListResponse.DataBean> dataBeans1;


    public RecyclerAdapterNotificationsMarketing(NotificationsMarketing notificationsMarketing, List<NotificationsAgentListResponse.DataBean> dataBeans1) {
        this.context=notificationsMarketing;
        this.dataBeans1=dataBeans1;
    }

    @NonNull
    @Override
    public RecyclerAdapterNotificationsMarketing.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications_marketing, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterNotificationsMarketing.ViewHolder holder, int position) {


        holder.description.setText(dataBeans1.get(position).getNotification());
        holder.date.setText(dataBeans1.get(position).getDate());
        holder.notificationfor.setText(dataBeans1.get(position).getNotification_for());
    }



    @Override
    public int getItemCount() {
        return dataBeans1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView viewDetails,date,description,notificationfor;
        CardView card;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            date=itemView.findViewById(R.id.date);
            description=itemView.findViewById(R.id.description);
            notificationfor=itemView.findViewById(R.id.notificationfor);




        }
    }
}
