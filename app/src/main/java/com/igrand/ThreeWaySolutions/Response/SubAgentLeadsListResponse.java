package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class SubAgentLeadsListResponse {


    /**
     * status : {"code":200,"message":"leads list"}
     * data : [{"id":"97","unique_id":"3WL20040497","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"oiuy","survey_no":"hjgfd","property_name":"uiytru","acres":"lkjhgfh","address":"dfgchvb","latitude":"37.421998","longitude":"-122.084","document":"temp61.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-04-04 15:56:24"},{"id":"89","unique_id":"3WL20040289","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"rtdfghj","survey_no":"trfgh","property_name":"TEst","acres":"rfth","address":"rdtfg","latitude":"37.421998","longitude":"-122.084","document":"IMG42163998179264365621.jpg,IMG6881536579474767074.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-04-02 10:41:57"},{"id":"77","unique_id":"3WL20031876","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Finish","survey_no":"coming","property_name":"Confirm","acres":"figure","address":"Finish","latitude":"17.4469245","longitude":"78.4462945","document":"gst.jpg,demoImage.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-03-18 17:38:11"},{"id":"41","unique_id":"3WL20030641","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Fjdg","survey_no":"jsvs","property_name":"Pdftest","acres":"jsgd","address":"Jzkd","latitude":"17.449886666666664","longitude":"78.45042666666667","document":"Chandan_kumar3.pdf,compiler_design_ques1.pdf","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-03-06 11:10:15"},{"id":"40","unique_id":"3WL20030640","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Bsr","survey_no":"nsr","property_name":"Dhj","acres":"vguj","address":"Next","latitude":"17.449886666666664","longitude":"78.45042666666667","document":"compiler_design_ques.pdf","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-03-06 10:41:19"},{"id":"39","unique_id":"3WL20030539","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Jfh","survey_no":"hdh","property_name":"Fjjd","acres":"dhj","address":"Fnig","latitude":"17.450081666666666","longitude":"78.45020500000001","document":"Chandan_kumar1.pdf,Chandan_kumar2.pdf","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-03-05 17:10:44"},{"id":"38","unique_id":"3WL20030538","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Fj","survey_no":"fiu","property_name":"Test","acres":"he","address":"Cji","latitude":"17.450081666666666","longitude":"78.45020500000001","document":"Chandan_kumar.pdf","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-03-05 15:37:34"},{"id":"37","unique_id":"3WL20030537","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Gjg","survey_no":"hdh","property_name":"Euvg","acres":"diy","address":"Fiu","latitude":"17.450081666666666","longitude":"78.45020500000001","document":"temp11.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-03-05 15:32:43"},{"id":"32","unique_id":"3WL20030432","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Test","survey_no":"Test","property_name":"Test","acres":"Test","address":"Test","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20200227_1902013.jpg,IMG_20200227_1901531.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-03-04 12:06:36"},{"id":"25","unique_id":"3WL20020625","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"dtfghj","survey_no":"fgvhbj","property_name":"TestCamera","acres":"gvhbjn","address":"fghvb","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"temp1.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-06 13:49:23"},{"id":"22","unique_id":"3WL20020622","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"fxcgvhbnFGvhjb","survey_no":"tfghjk","property_name":"SUBAGENTTEST","acres":"drtghjk","address":"ftghj","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20191119_1249576.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-06 12:44:45"},{"id":"21","unique_id":"3WL20020621","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"dfghjk","survey_no":"fghj","property_name":"LeadTest","acres":"fghjk","address":"fghj","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20200204_1125511.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-06 12:42:08"},{"id":"5","unique_id":"3WL2002035","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"ewrtyui","survey_no":"tyuio","property_name":"wqertyu","acres":"ertyui","address":"vbnm","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20191119_1249572.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"04-02-2020 05:32:31","mt_update_dt":"04-02-2020 05:50:38","pt_update_dt":"05-02-2020 06:13:39","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-03 14:47:03"},{"id":"1","unique_id":"3WL2002031","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"Siyari","survey_no":"1234","property_name":"Xyz","acres":"100","address":"Hzb","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"Screenshot_2020-02-02-15-16-12-20.png","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"0","mt_status":"3","pt_status":"2","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"03-02-2020 11:40:21","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-03 11:32:43"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : leads list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 97
         * unique_id : 3WL20040497
         * user_mobile : 8789160492
         * district_name :
         * mandal_name :
         * village_name : oiuy
         * survey_no : hjgfd
         * property_name : uiytru
         * acres : lkjhgfh
         * address : dfgchvb
         * latitude : 37.421998
         * longitude : -122.084
         * document : temp61.jpg
         * legal_team_documents : null
         * property_description : null
         * agent_id : 37
         * status : 0
         * ct_status : 2
         * mt_status : 3
         * pt_status : 2
         * lgt_status : 2
         * ls_status : 2
         * admin_status : 0
         * st_status : 2
         * st_update_dt :
         * ct_update_dt :
         * mt_update_dt :
         * pt_update_dt :
         * lgt_update_dt :
         * ls_update_dt :
         * datetime : 2020-04-04 15:56:24
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String address;
        private String latitude;
        private String longitude;
        private String document;
        private Object legal_team_documents;
        private Object property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String lgt_status;
        private String ls_status;
        private String admin_status;
        private String st_status;
        private String st_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String lgt_update_dt;
        private String ls_update_dt;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public Object getLegal_team_documents() {
            return legal_team_documents;
        }

        public void setLegal_team_documents(Object legal_team_documents) {
            this.legal_team_documents = legal_team_documents;
        }

        public Object getProperty_description() {
            return property_description;
        }

        public void setProperty_description(Object property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getLgt_status() {
            return lgt_status;
        }

        public void setLgt_status(String lgt_status) {
            this.lgt_status = lgt_status;
        }

        public String getLs_status() {
            return ls_status;
        }

        public void setLs_status(String ls_status) {
            this.ls_status = ls_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getSt_status() {
            return st_status;
        }

        public void setSt_status(String st_status) {
            this.st_status = st_status;
        }

        public String getSt_update_dt() {
            return st_update_dt;
        }

        public void setSt_update_dt(String st_update_dt) {
            this.st_update_dt = st_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getLgt_update_dt() {
            return lgt_update_dt;
        }

        public void setLgt_update_dt(String lgt_update_dt) {
            this.lgt_update_dt = lgt_update_dt;
        }

        public String getLs_update_dt() {
            return ls_update_dt;
        }

        public void setLs_update_dt(String ls_update_dt) {
            this.ls_update_dt = ls_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}