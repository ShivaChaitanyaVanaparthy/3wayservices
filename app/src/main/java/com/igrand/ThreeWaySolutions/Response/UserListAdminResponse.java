package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class UserListAdminResponse {


    /**
     * status : {"code":200,"message":"users list"}
     * data : [{"datetime":"2019-12-07 17:50:55","username":"Chaitanya","usertype":"agent","profile":"http://igrandit.site/3way-solutions/admin_assets/uploads/users/IMG_20191119_124957.jpg","email":"Shiva.chaitanya62@gmail.com","mobile":"7286882452"},{"datetime":"2019-12-07 17:23:28","username":"narendra","usertype":"agent","profile":"http://igrandit.site/3way-solutions/admin_assets/uploads/users/agent1.jpg","email":"","mobile":"9441868259"},{"datetime":"2019-12-07 16:46:45","username":"vinay narendra","usertype":"admin","profile":"http://igrandit.site/3way-solutions/admin_assets/uploads/users/","email":null,"mobile":"7013332509"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : users list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * datetime : 2019-12-07 17:50:55
         * username : Chaitanya
         * usertype : agent
         * profile : http://igrandit.site/3way-solutions/admin_assets/uploads/users/IMG_20191119_124957.jpg
         * email : Shiva.chaitanya62@gmail.com
         * mobile : 7286882452
         */

        private String datetime;
        private String username;
        private String usertype;
        private String profile;
        private String email;
        private String mobile;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
