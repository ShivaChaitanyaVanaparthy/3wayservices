package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LoginAdminResponse {


    /**
     * status : {"code":200,"message":"User Data"}
     * data : [{"user_id":"29","usertype":"sub_agent","mobile":"1234567888","username":"chaitanya","email":"chaitanya@gmail.com","gender":null,"password":"e10adc3949ba59abbe56e057f20f883e","agent_id":"2","status":"1","profile":"http://igrandit.site/3way-services/admin_assets/uploads/users/IMG_20200106_10492620.jpg"}]
     */

    public StatusBean status;
    public List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : User Data
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * user_id : 29
         * usertype : sub_agent
         * mobile : 1234567888
         * username : chaitanya
         * email : chaitanya@gmail.com
         * gender : null
         * password : e10adc3949ba59abbe56e057f20f883e
         * agent_id : 2
         * status : 1
         * profile : http://igrandit.site/3way-services/admin_assets/uploads/users/IMG_20200106_10492620.jpg
         */

        private String user_id;
        private String usertype;
        private String mobile;
        private String username;
        private String email;
        private Object gender;
        private String password;
        private String agent_id;
        private String status;
        private String profile;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getGender() {
            return gender;
        }

        public void setGender(Object gender) {
            this.gender = gender;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }
    }
}
