package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminLegalList {


    /**
     * status : {"code":200,"message":"List of Legal Team Documents"}
     * data : [{"datetime":"2019-12-24 12:23:19","document_name":"LKJH","status":"0"},{"datetime":"2019-12-24 12:23:10","document_name":"POIUY","status":"1"},{"datetime":"2019-12-24 12:22:20","document_name":"ASDF","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Legal Team Documents
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * datetime : 2019-12-24 12:23:19
         * document_name : LKJH
         * status : 0
         */

        private String datetime;
        private String document_name;
        private String status;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getDocument_name() {
            return document_name;
        }

        public void setDocument_name(String document_name) {
            this.document_name = document_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
