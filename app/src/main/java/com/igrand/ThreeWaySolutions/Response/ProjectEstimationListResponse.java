package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class ProjectEstimationListResponse {


    /**
     * status : {"code":200,"message":"List of sub-work Names"}
     * data : [{"id":"4","project_id":"2","work_id":"1","subwork_id":"1","description":"<p>asdsad<\/p>\r\n","uom_id":"1","no_quantity":"3","rate":"4","amount":"12","project_name":"Pankaj Project","work_name":"Road work","subwork_name":"road cutting","uom":"cm"},{"id":"3","project_id":"2","work_id":"1","subwork_id":"1","description":"<p>dsasa<\/p>\r\n","uom_id":"1","no_quantity":"7","rate":"2","amount":"14","project_name":"Pankaj Project","work_name":"Road work","subwork_name":"road cutting","uom":"cm"},{"id":"2","project_id":"2","work_id":"1","subwork_id":"1","description":"<p>tretfsd<\/p>\r\n","uom_id":"1","no_quantity":"74","rate":"8","amount":"592","project_name":"Pankaj Project","work_name":"Road work","subwork_name":"road cutting","uom":"cm"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of sub-work Names
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 4
         * project_id : 2
         * work_id : 1
         * subwork_id : 1
         * description : <p>asdsad</p>
         * uom_id : 1
         * no_quantity : 3
         * rate : 4
         * amount : 12
         * project_name : Pankaj Project
         * work_name : Road work
         * subwork_name : road cutting
         * uom : cm
         */

        private String id;
        private String project_id;
        private String work_id;
        private String subwork_id;
        private String description;
        private String uom_id;
        private String no_quantity;
        private String rate;
        private String amount;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String uom;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUom_id() {
            return uom_id;
        }

        public void setUom_id(String uom_id) {
            this.uom_id = uom_id;
        }

        public String getNo_quantity() {
            return no_quantity;
        }

        public void setNo_quantity(String no_quantity) {
            this.no_quantity = no_quantity;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getUom() {
            return uom;
        }

        public void setUom(String uom) {
            this.uom = uom;
        }
    }
}
