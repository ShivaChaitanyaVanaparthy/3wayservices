package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LeadIdResponse {


    /**
     * status : {"code":200,"message":"Proposal doc list"}
     * document_name_list : [{"lead_id":"1"}]
     */

    private StatusBean status;
    private List<DocumentNameListBean> document_name_list;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DocumentNameListBean> getDocument_name_list() {
        return document_name_list;
    }

    public void setDocument_name_list(List<DocumentNameListBean> document_name_list) {
        this.document_name_list = document_name_list;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Proposal doc list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DocumentNameListBean {
        /**
         * lead_id : 1
         */

        private String lead_id;

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }
    }
}
