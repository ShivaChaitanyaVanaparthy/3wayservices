package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class VendorListResponse {


    /**
     * status : {"code":200,"message":"Project wise Labour Contractor","vendors":[{"id":"1","vendor_type":"1","person_name":"Contractor1","project_id":"2","phone_no":"1234567890","pan_no":"ddfdffdf","work_id":"1","subwork_id":"1","remarks":"good","skill_rate":"4","male_rate":"5","female_rate":"6","description":"","uom_id":"","rate":"","material_type_id":"","machine_type_id":"","batha_amount":"","work_name":"Work","subwork_name":"SubWork","project_name":"test project2","uoms":"","machine_type":"","material_type":""}]}
     */

    private StatusBean status;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Project wise Labour Contractor
         * vendors : [{"id":"1","vendor_type":"1","person_name":"Contractor1","project_id":"2","phone_no":"1234567890","pan_no":"ddfdffdf","work_id":"1","subwork_id":"1","remarks":"good","skill_rate":"4","male_rate":"5","female_rate":"6","description":"","uom_id":"","rate":"","material_type_id":"","machine_type_id":"","batha_amount":"","work_name":"Work","subwork_name":"SubWork","project_name":"test project2","uoms":"","machine_type":"","material_type":""}]
         */

        private int code;
        private String message;
        private List<VendorsBean> vendors;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<VendorsBean> getVendors() {
            return vendors;
        }

        public void setVendors(List<VendorsBean> vendors) {
            this.vendors = vendors;
        }

        public static class VendorsBean {
            /**
             * id : 1
             * vendor_type : 1
             * person_name : Contractor1
             * project_id : 2
             * phone_no : 1234567890
             * pan_no : ddfdffdf
             * work_id : 1
             * subwork_id : 1
             * remarks : good
             * skill_rate : 4
             * male_rate : 5
             * female_rate : 6
             * description :
             * uom_id :
             * rate :
             * material_type_id :
             * machine_type_id :
             * batha_amount :
             * work_name : Work
             * subwork_name : SubWork
             * project_name : test project2
             * uoms :
             * machine_type :
             * material_type :
             */

            private String id;
            private String vendor_type;
            private String person_name;
            private String project_id;
            private String phone_no;
            private String pan_no;
            private String work_id;
            private String subwork_id;
            private String remarks;
            private String skill_rate;
            private String male_rate;
            private String female_rate;
            private String description;
            private String uom_id;
            private String rate;
            private String material_type_id;
            private String machine_type_id;
            private String batha_amount;
            private String work_name;
            private String subwork_name;
            private String project_name;
            private String uoms;
            private String machine_type;
            private String material_type;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getVendor_type() {
                return vendor_type;
            }

            public void setVendor_type(String vendor_type) {
                this.vendor_type = vendor_type;
            }

            public String getPerson_name() {
                return person_name;
            }

            public void setPerson_name(String person_name) {
                this.person_name = person_name;
            }

            public String getProject_id() {
                return project_id;
            }

            public void setProject_id(String project_id) {
                this.project_id = project_id;
            }

            public String getPhone_no() {
                return phone_no;
            }

            public void setPhone_no(String phone_no) {
                this.phone_no = phone_no;
            }

            public String getPan_no() {
                return pan_no;
            }

            public void setPan_no(String pan_no) {
                this.pan_no = pan_no;
            }

            public String getWork_id() {
                return work_id;
            }

            public void setWork_id(String work_id) {
                this.work_id = work_id;
            }

            public String getSubwork_id() {
                return subwork_id;
            }

            public void setSubwork_id(String subwork_id) {
                this.subwork_id = subwork_id;
            }

            public String getRemarks() {
                return remarks;
            }

            public void setRemarks(String remarks) {
                this.remarks = remarks;
            }

            public String getSkill_rate() {
                return skill_rate;
            }

            public void setSkill_rate(String skill_rate) {
                this.skill_rate = skill_rate;
            }

            public String getMale_rate() {
                return male_rate;
            }

            public void setMale_rate(String male_rate) {
                this.male_rate = male_rate;
            }

            public String getFemale_rate() {
                return female_rate;
            }

            public void setFemale_rate(String female_rate) {
                this.female_rate = female_rate;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getUom_id() {
                return uom_id;
            }

            public void setUom_id(String uom_id) {
                this.uom_id = uom_id;
            }

            public String getRate() {
                return rate;
            }

            public void setRate(String rate) {
                this.rate = rate;
            }

            public String getMaterial_type_id() {
                return material_type_id;
            }

            public void setMaterial_type_id(String material_type_id) {
                this.material_type_id = material_type_id;
            }

            public String getMachine_type_id() {
                return machine_type_id;
            }

            public void setMachine_type_id(String machine_type_id) {
                this.machine_type_id = machine_type_id;
            }

            public String getBatha_amount() {
                return batha_amount;
            }

            public void setBatha_amount(String batha_amount) {
                this.batha_amount = batha_amount;
            }

            public String getWork_name() {
                return work_name;
            }

            public void setWork_name(String work_name) {
                this.work_name = work_name;
            }

            public String getSubwork_name() {
                return subwork_name;
            }

            public void setSubwork_name(String subwork_name) {
                this.subwork_name = subwork_name;
            }

            public String getProject_name() {
                return project_name;
            }

            public void setProject_name(String project_name) {
                this.project_name = project_name;
            }

            public String getUoms() {
                return uoms;
            }

            public void setUoms(String uoms) {
                this.uoms = uoms;
            }

            public String getMachine_type() {
                return machine_type;
            }

            public void setMachine_type(String machine_type) {
                this.machine_type = machine_type;
            }

            public String getMaterial_type() {
                return material_type;
            }

            public void setMaterial_type(String material_type) {
                this.material_type = material_type;
            }
        }
    }
}
