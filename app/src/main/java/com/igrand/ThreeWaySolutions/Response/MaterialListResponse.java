package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class MaterialListResponse {


    /**
     * status : {"code":200,"message":"muse List"}
     * data : [{"id":"1","project_id":"1","contractor_id":"1","work_id":"1","subwork_id":"1","description":"<p>Z<\/p>\r\n","uom_id":"1","material_id":"1","opening_stock":"66","recived":"45","consumption":"40","date":"08/15/2020","uom":"cm","project_name":"Project","work_name":"Work","subwork_name":"SubWork","person_name":"Contractor1","material_type":"material"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : muse List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * project_id : 1
         * contractor_id : 1
         * work_id : 1
         * subwork_id : 1
         * description : <p>Z</p>
         * uom_id : 1
         * material_id : 1
         * opening_stock : 66
         * recived : 45
         * consumption : 40
         * date : 08/15/2020
         * uom : cm
         * project_name : Project
         * work_name : Work
         * subwork_name : SubWork
         * person_name : Contractor1
         * material_type : material
         */

        private String id;
        private String project_id;
        private String contractor_id;
        private String work_id;
        private String subwork_id;
        private String description;
        private String uom_id;
        private String material_id;
        private String opening_stock;
        private String recived;
        private String consumption;
        private String date;
        private String uom;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String person_name;
        private String material_type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getContractor_id() {
            return contractor_id;
        }

        public void setContractor_id(String contractor_id) {
            this.contractor_id = contractor_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUom_id() {
            return uom_id;
        }

        public void setUom_id(String uom_id) {
            this.uom_id = uom_id;
        }

        public String getMaterial_id() {
            return material_id;
        }

        public void setMaterial_id(String material_id) {
            this.material_id = material_id;
        }

        public String getOpening_stock() {
            return opening_stock;
        }

        public void setOpening_stock(String opening_stock) {
            this.opening_stock = opening_stock;
        }

        public String getRecived() {
            return recived;
        }

        public void setRecived(String recived) {
            this.recived = recived;
        }

        public String getConsumption() {
            return consumption;
        }

        public void setConsumption(String consumption) {
            this.consumption = consumption;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getUom() {
            return uom;
        }

        public void setUom(String uom) {
            this.uom = uom;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getMaterial_type() {
            return material_type;
        }

        public void setMaterial_type(String material_type) {
            this.material_type = material_type;
        }
    }
}
