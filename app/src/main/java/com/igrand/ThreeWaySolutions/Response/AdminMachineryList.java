package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminMachineryList {


    /**
     * status : {"code":200,"message":"List of supplier Report"}
     * data : [{"id":"3","project_id":"2","supplier_id":"4","work_id":"1","subwork_id":"1","description":"sada","material_id":"3","ts":"sda","uom_id":"1","quantity":"45","date":"02/26/2020","project_name":"Pankaj Project","work_name":"Road work","subwork_name":"road cutting","person_name":"ravi","material_type":"Sand","uom":"cm"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of supplier Report
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 3
         * project_id : 2
         * supplier_id : 4
         * work_id : 1
         * subwork_id : 1
         * description : sada
         * material_id : 3
         * ts : sda
         * uom_id : 1
         * quantity : 45
         * date : 02/26/2020
         * project_name : Pankaj Project
         * work_name : Road work
         * subwork_name : road cutting
         * person_name : ravi
         * material_type : Sand
         * uom : cm
         */

        private String id;
        private String project_id;
        private String supplier_id;
        private String work_id;
        private String subwork_id;
        private String description;
        private String material_id;
        private String ts;
        private String uom_id;
        private String quantity;
        private String date;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String person_name;
        private String material_type;
        private String uom;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getSupplier_id() {
            return supplier_id;
        }

        public void setSupplier_id(String supplier_id) {
            this.supplier_id = supplier_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMaterial_id() {
            return material_id;
        }

        public void setMaterial_id(String material_id) {
            this.material_id = material_id;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public String getUom_id() {
            return uom_id;
        }

        public void setUom_id(String uom_id) {
            this.uom_id = uom_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getMaterial_type() {
            return material_type;
        }

        public void setMaterial_type(String material_type) {
            this.material_type = material_type;
        }

        public String getUom() {
            return uom;
        }

        public void setUom(String uom) {
            this.uom = uom;
        }
    }
}
