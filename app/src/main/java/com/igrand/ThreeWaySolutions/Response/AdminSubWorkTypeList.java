package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminSubWorkTypeList {


    /**
     * status : {"code":200,"message":"List of sub-work Names"}
     * data : [{"id":"1","work_id":"1","subwork_name":"road cutting","item_head_no":"CW1","status":"1","work_name":"Road work"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of sub-work Names
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * work_id : 1
         * subwork_name : road cutting
         * item_head_no : CW1
         * status : 1
         * work_name : Road work
         */

        private String id;
        private String work_id;
        private String subwork_name;
        private String item_head_no;
        private String status;
        private String work_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getItem_head_no() {
            return item_head_no;
        }

        public void setItem_head_no(String item_head_no) {
            this.item_head_no = item_head_no;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }
    }
}
