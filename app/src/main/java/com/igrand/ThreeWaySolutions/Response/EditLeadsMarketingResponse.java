package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class EditLeadsMarketingResponse {


    /**
     * status : {"code":200,"message":"Lead Details For Update"}
     * data : [{"mt_status":"3"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * mt_status : 3
         */

        private String mt_status;

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }
    }
}
