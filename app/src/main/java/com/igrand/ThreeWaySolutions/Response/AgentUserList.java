package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AgentUserList {


    /**
     * status : {"code":200,"message":"users list"}
     * data : [{"datetime":"2020-01-08 15:29:44","username":"Chaitanya","usertype":"SUB-AGENT","profile":"http://igrandit.site/3way-services/admin_assets/uploads/users/IMG_20200106_10492613.jpg","email":"chait@gmail.com","mobile":"1234567899"},{"datetime":"2020-01-08 14:52:20","username":"Shiva","usertype":"SubAgent","profile":"http://igrandit.site/3way-services/admin_assets/uploads/users/1.jpg","email":"shiva@Gmail.com","mobile":"7286882452"},{"datetime":"2019-12-23 10:38:01","username":"Mahesh","usertype":"sub_agent","profile":"http://igrandit.site/3way-services/admin_assets/uploads/users/subagent1.jpg","email":"mahesh@gmail.com","mobile":"1234567890"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : users list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * datetime : 2020-01-08 15:29:44
         * username : Chaitanya
         * usertype : SUB-AGENT
         * profile : http://igrandit.site/3way-services/admin_assets/uploads/users/IMG_20200106_10492613.jpg
         * email : chait@gmail.com
         * mobile : 1234567899
         */

        private String datetime;
        private String username;
        private String usertype;
        private String profile;
        private String email;
        private String mobile;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
