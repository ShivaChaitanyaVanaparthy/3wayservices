package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminSubWorkTypeListbyId {


    /**
     * status : {"code":200,"message":"Subwork By work id"}
     * data : [{"id":"1","work_id":"1","subwork_name":"road cutting","item_head_no":"CW1","status":"1"},{"id":"2","work_id":"1","subwork_name":"SubWork","item_head_no":"C11o1","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Subwork By work id
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * work_id : 1
         * subwork_name : road cutting
         * item_head_no : CW1
         * status : 1
         */

        private String id;
        private String work_id;
        private String subwork_name;
        private String item_head_no;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getItem_head_no() {
            return item_head_no;
        }

        public void setItem_head_no(String item_head_no) {
            this.item_head_no = item_head_no;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
