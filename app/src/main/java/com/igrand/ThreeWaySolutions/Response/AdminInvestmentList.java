package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminInvestmentList {


    /**
     * status : {"code":200,"message":"List of Investment"}
     * data : [{"datetime":"2020-01-08 11:03:53","project_name":"Buying Land","username":"Ilaya raju","invest_amount":"1","invest_date":"08/01/2020","mature_date":"09/01/2020","duration":"1","mature_amount":"10","document":["http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/1.jpg"],"status":"1"},{"datetime":"2020-01-08 13:16:19","project_name":"Buying Land","username":"inumarthi","invest_amount":"100","invest_date":"08/01/2020","mature_date":"09/01/2020","duration":"1","mature_amount":"200","document":["http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/IMG_20200106_104926.jpg"],"status":"1"},{"datetime":"2020-01-04 11:44:10","project_name":"Constructions","username":"inumarthi","invest_amount":"1","invest_date":"05-01-2020","mature_date":"25-01-2020","duration":"20","mature_amount":"2","document":["http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/file1.jpg","http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/file2.jpg","http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/file3.jpg"],"status":"1"},{"datetime":"2020-01-04 13:27:13","project_name":"Constructions","username":"inumarthi","invest_amount":"123","invest_date":"27-01-2020","mature_date":"28-01-2020","duration":"1","mature_amount":"3223","document":["http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/docum_(2).jpg","http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/docum_(1).jpg","http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/docum_(3).jpg"],"status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Investment
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * datetime : 2020-01-08 11:03:53
         * project_name : Buying Land
         * username : Ilaya raju
         * invest_amount : 1
         * invest_date : 08/01/2020
         * mature_date : 09/01/2020
         * duration : 1
         * mature_amount : 10
         * document : ["http://igrandit.site/3way-services//admin_assets/uploads/investment/documents/1.jpg"]
         * status : 1
         */

        private String datetime;
        private String project_name;
        private String username;
        private String invest_amount;
        private String invest_date;
        private String mature_date;
        private String duration;
        private String mature_amount;
        private String status;
        private List<String> document;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getInvest_amount() {
            return invest_amount;
        }

        public void setInvest_amount(String invest_amount) {
            this.invest_amount = invest_amount;
        }

        public String getInvest_date() {
            return invest_date;
        }

        public void setInvest_date(String invest_date) {
            this.invest_date = invest_date;
        }

        public String getMature_date() {
            return mature_date;
        }

        public void setMature_date(String mature_date) {
            this.mature_date = mature_date;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getMature_amount() {
            return mature_amount;
        }

        public void setMature_amount(String mature_amount) {
            this.mature_amount = mature_amount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getDocument() {
            return document;
        }

        public void setDocument(List<String> document) {
            this.document = document;
        }
    }
}
