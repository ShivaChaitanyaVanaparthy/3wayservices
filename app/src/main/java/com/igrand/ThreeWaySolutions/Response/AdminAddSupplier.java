package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminAddSupplier {


    /**
     * status : {"code":200,"message":"List of Supplier"}
     * data : [{"id":"4","vendor_type":"2","business_name":"ravi.pvt.ltd","city_id":"1","person_name":"ravi","user_mobile":"9517534561","alt_mobile":"9517534561","email":"info@ravi.com","description":"dfds","status":"1","datetime":"2020-02-27 13:21:17"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Supplier
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 4
         * vendor_type : 2
         * business_name : ravi.pvt.ltd
         * city_id : 1
         * person_name : ravi
         * user_mobile : 9517534561
         * alt_mobile : 9517534561
         * email : info@ravi.com
         * description : dfds
         * status : 1
         * datetime : 2020-02-27 13:21:17
         */

        private String id;
        private String vendor_type;
        private String business_name;
        private String city_id;
        private String person_name;
        private String user_mobile;
        private String alt_mobile;
        private String email;
        private String description;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVendor_type() {
            return vendor_type;
        }

        public void setVendor_type(String vendor_type) {
            this.vendor_type = vendor_type;
        }

        public String getBusiness_name() {
            return business_name;
        }

        public void setBusiness_name(String business_name) {
            this.business_name = business_name;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getAlt_mobile() {
            return alt_mobile;
        }

        public void setAlt_mobile(String alt_mobile) {
            this.alt_mobile = alt_mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
