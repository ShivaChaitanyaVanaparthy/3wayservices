package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class EditUserResponse {


    /**
     * status : {"code":200,"message":"Users Details For Update"}
     * data : [{"usertype":"agent","username":"Chaitanya","mobile":"7286882452","email":"shiva.chaitanya61@gmail.com","status":"1","profile":"http://igrandit.site/3way-solutions/admin_assets/uploads/users/13.jpg"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Users Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * usertype : agent
         * username : Chaitanya
         * mobile : 7286882452
         * email : shiva.chaitanya61@gmail.com
         * status : 1
         * profile : http://igrandit.site/3way-solutions/admin_assets/uploads/users/13.jpg
         */

        private String usertype;
        private String username;
        private String mobile;
        private String email;
        private String status;
        private String profile;

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }
    }
}
