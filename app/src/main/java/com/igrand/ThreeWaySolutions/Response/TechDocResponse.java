package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class TechDocResponse {


    /**
     * status : {"code":200,"message":"All documents of Leads"}
     * data : [{"id":"2","user_mobile":"0987654444","lead_id":"98","doc_description":"","image_description":"Test Image","document_image":"","document":""},{"id":"3","user_mobile":"0987654444","lead_id":"98","doc_description":"","image_description":"Testing","document_image":"IMG8630941099840512141.jpg","document":""},{"id":"4","user_mobile":"0987654444","lead_id":"98","doc_description":"Test PDF","image_description":"","document_image":"","document":"Chandan_kumar2.pdf"},{"id":"5","user_mobile":"0987654444","lead_id":"98","doc_description":"Test PDF","image_description":"Test Image","document_image":"IMG86309410998405121411.jpg","document":"Chandan_kumar21.pdf"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : All documents of Leads
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 2
         * user_mobile : 0987654444
         * lead_id : 98
         * doc_description :
         * image_description : Test Image
         * document_image :
         * document :
         */

        private String id;
        private String user_mobile;
        private String lead_id;
        private String doc_description;
        private String image_description;
        private String document_image;
        private String document;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getDoc_description() {
            return doc_description;
        }

        public void setDoc_description(String doc_description) {
            this.doc_description = doc_description;
        }

        public String getImage_description() {
            return image_description;
        }

        public void setImage_description(String image_description) {
            this.image_description = image_description;
        }

        public String getDocument_image() {
            return document_image;
        }

        public void setDocument_image(String document_image) {
            this.document_image = document_image;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }
    }
}
