package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class EarnsListResponse {


    /**
     * status : {"code":200,"message":"Earns List"}
     * data : [{"id":"3","title":"Title","description":"Description","datetime":"2020-05-16 18:35:07","status":"0"},{"id":"2","title":"Title","description":"Description","datetime":"2020-05-16 18:22:54","status":"1"},{"id":"1","title":"Title","description":"Description","datetime":"2020-05-16 18:22:29","status":"Status"}]
     * counts : ["",0,0,0]
     */

    private StatusBean status;
    private List<DataBean> data;
    private List<String> counts;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<String> getCounts() {
        return counts;
    }

    public void setCounts(List<String> counts) {
        this.counts = counts;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Earns List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 3
         * title : Title
         * description : Description
         * datetime : 2020-05-16 18:35:07
         * status : 0
         */

        private String id;
        private String title;
        private String description;
        private String datetime;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
