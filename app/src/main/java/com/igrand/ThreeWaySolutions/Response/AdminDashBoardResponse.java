package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

public class AdminDashBoardResponse {


    /**
     * status : {"code":200,"message":"Admin Dashboard"}
     * data : {"user count":33,"leads count":33}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin Dashboard
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        @SerializedName("user count")
        private int _$UserCount53; // FIXME check this code
        @SerializedName("leads count")
        private int _$LeadsCount147; // FIXME check this code

        public int get_$UserCount53() {
            return _$UserCount53;
        }

        public void set_$UserCount53(int _$UserCount53) {
            this._$UserCount53 = _$UserCount53;
        }

        public int get_$LeadsCount147() {
            return _$LeadsCount147;
        }

        public void set_$LeadsCount147(int _$LeadsCount147) {
            this._$LeadsCount147 = _$LeadsCount147;
        }
    }
}
