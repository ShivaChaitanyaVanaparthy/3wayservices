package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditLeadsSurveyResponse {


    /**
     * status : {"code":200,"message":"Lead Details For Update"}
     * data : [{"lead id":"25","survey team status":"2","survey team update time":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        @SerializedName("lead id")
        private String _$LeadId249; // FIXME check this code
        @SerializedName("survey team status")
        private String _$SurveyTeamStatus229; // FIXME check this code
        @SerializedName("survey team update time")
        private String _$SurveyTeamUpdateTime235; // FIXME check this code

        public String get_$LeadId249() {
            return _$LeadId249;
        }

        public void set_$LeadId249(String _$LeadId249) {
            this._$LeadId249 = _$LeadId249;
        }

        public String get_$SurveyTeamStatus229() {
            return _$SurveyTeamStatus229;
        }

        public void set_$SurveyTeamStatus229(String _$SurveyTeamStatus229) {
            this._$SurveyTeamStatus229 = _$SurveyTeamStatus229;
        }

        public String get_$SurveyTeamUpdateTime235() {
            return _$SurveyTeamUpdateTime235;
        }

        public void set_$SurveyTeamUpdateTime235(String _$SurveyTeamUpdateTime235) {
            this._$SurveyTeamUpdateTime235 = _$SurveyTeamUpdateTime235;
        }
    }
}
