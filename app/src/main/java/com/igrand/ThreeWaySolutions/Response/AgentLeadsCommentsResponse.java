package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AgentLeadsCommentsResponse {


    /**
     * status : {"code":200,"message":"All comments of Leads"}
     * data : [{"id":"3","comments":"CheckingTeam Comment","user_mobile":"8210402087","lead_id":"5","status":"1","datetime":"2020-02-04 17:32:31","usertype":"checking_team"},{"id":"4","comments":"dfs","user_mobile":"8210402087","lead_id":"5","status":"1","datetime":"2020-02-04 17:49:14","usertype":"checking_team"},{"id":"5","comments":"Marketing Comments.!","user_mobile":"8789160492","lead_id":"5","status":"1","datetime":"2020-02-04 17:50:38","usertype":"sub_agent"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : All comments of Leads
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 3
         * comments : CheckingTeam Comment
         * user_mobile : 8210402087
         * lead_id : 5
         * status : 1
         * datetime : 2020-02-04 17:32:31
         * usertype : checking_team
         */

        private String id;
        private String comments;
        private String user_mobile;
        private String lead_id;
        private String status;
        private String datetime;
        private String usertype;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }
    }
}

