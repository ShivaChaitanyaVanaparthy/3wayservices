package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LeadsDetailResponse {


    /**
     * status : {"code":200,"message":"User Details along with Leads"}
     * data : [{"username":"rajesh","usertype":"legal_team","mobile":"8688543727","status":"1","profile":"http://igrandit.site/3way-solutions/admin_assets/uploads/users/","leads":[{"time":"2019-12-06 15:33:06","village_name":"wqeqe","survey_no":"","property_name":"wqeqe","acres":"5","address":"","google_location":"qweqwe","document":"","comments":"wqeeq"},{"time":"2019-12-06 18:30:44","village_name":"zxcv","survey_no":"","property_name":"qwert","acres":"2","address":"erwerw","google_location":"ererwr","document":"document3.jpg,document2.jpg,document1.jpg","comments":"erwerw"}]}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : User Details along with Leads
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * username : rajesh
         * usertype : legal_team
         * mobile : 8688543727
         * status : 1
         * profile : http://igrandit.site/3way-solutions/admin_assets/uploads/users/
         * leads : [{"time":"2019-12-06 15:33:06","village_name":"wqeqe","survey_no":"","property_name":"wqeqe","acres":"5","address":"","google_location":"qweqwe","document":"","comments":"wqeeq"},{"time":"2019-12-06 18:30:44","village_name":"zxcv","survey_no":"","property_name":"qwert","acres":"2","address":"erwerw","google_location":"ererwr","document":"document3.jpg,document2.jpg,document1.jpg","comments":"erwerw"}]
         */

        private String username;
        private String usertype;
        private String mobile;
        private String status;
        private String profile;
        private List<LeadsBean> leads;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public List<LeadsBean> getLeads() {
            return leads;
        }

        public void setLeads(List<LeadsBean> leads) {
            this.leads = leads;
        }

        public static class LeadsBean {
            /**
             * time : 2019-12-06 15:33:06
             * village_name : wqeqe
             * survey_no :
             * property_name : wqeqe
             * acres : 5
             * address :
             * google_location : qweqwe
             * document :
             * comments : wqeeq
             */

            private String time;
            private String village_name;
            private String survey_no;
            private String property_name;
            private String acres;
            private String address;
            private String google_location;
            private String document;
            private String comments;

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getVillage_name() {
                return village_name;
            }

            public void setVillage_name(String village_name) {
                this.village_name = village_name;
            }

            public String getSurvey_no() {
                return survey_no;
            }

            public void setSurvey_no(String survey_no) {
                this.survey_no = survey_no;
            }

            public String getProperty_name() {
                return property_name;
            }

            public void setProperty_name(String property_name) {
                this.property_name = property_name;
            }

            public String getAcres() {
                return acres;
            }

            public void setAcres(String acres) {
                this.acres = acres;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getGoogle_location() {
                return google_location;
            }

            public void setGoogle_location(String google_location) {
                this.google_location = google_location;
            }

            public String getDocument() {
                return document;
            }

            public void setDocument(String document) {
                this.document = document;
            }

            public String getComments() {
                return comments;
            }

            public void setComments(String comments) {
                this.comments = comments;
            }
        }
    }
}
