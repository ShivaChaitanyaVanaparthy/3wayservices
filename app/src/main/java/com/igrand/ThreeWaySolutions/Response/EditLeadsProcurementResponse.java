package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class EditLeadsProcurementResponse {


    /**
     * status : {"code":200,"message":"Lead Details For Update"}
     * data : [{"lead_id":"28","pt_status":"2","pro_doc":"IMG_20200106_1049261.jpg","pro_notes":"Proposal"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * lead_id : 28
         * pt_status : 2
         * pro_doc : IMG_20200106_1049261.jpg
         * pro_notes : Proposal
         */

        private String lead_id;
        private String pt_status;
        private String pro_doc;
        private String pro_notes;

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getPro_doc() {
            return pro_doc;
        }

        public void setPro_doc(String pro_doc) {
            this.pro_doc = pro_doc;
        }

        public String getPro_notes() {
            return pro_notes;
        }

        public void setPro_notes(String pro_notes) {
            this.pro_notes = pro_notes;
        }
    }
}
