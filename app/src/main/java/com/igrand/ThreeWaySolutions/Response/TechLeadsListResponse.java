package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class TechLeadsListResponse {

    /**
     * status : {"code":200,"message":"leads list"}
     * data : [{"id":"6","unique_id":"3WL2005146","user_mobile":"7286882452","district_name":"wert","mandal_name":"wqert","village_name":"qwerty","survey_no":"wer","property_name":"Testing","acres":"werty","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan1.pdf,46.5_x_30_East_face_Plan2.pdf","images":"","legal_team_documents":null,"property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 21:43:53","ad_image":"","ad_comments":""},{"id":"5","unique_id":"3WL2005135","user_mobile":"7286882452","district_name":"hyd","mandal_name":"hyd","village_name":"oiuytr","survey_no":"981","property_name":"oiuy","acres":"oiuy","latitude":"87654","longitude":"76854","document":"NN7812251491030.ETicket.pdf","images":"","legal_team_documents":null,"property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 14:41:25","ad_image":"","ad_comments":""},{"id":"4","unique_id":"3WL2005134","user_mobile":"7286882452","district_name":"hyd","mandal_name":"hyd","village_name":"oiuytr","survey_no":"981","property_name":"oiuy","acres":"oiuy","latitude":"87654","longitude":"76854","document":"","images":"","legal_team_documents":null,"property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 14:38:42","ad_image":"","ad_comments":""},{"id":"3","unique_id":"3WL2005133","user_mobile":"7286882452","district_name":"Test","mandal_name":"Test","village_name":"Test","survey_no":"Test","property_name":"Test","acres":"Test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan_(1).pdf","images":"","legal_team_documents":null,"property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 10:27:50","ad_image":"","ad_comments":""},{"id":"2","unique_id":"3WL2005122","user_mobile":"9848174994","district_name":"Medchel","mandal_name":"Bibinagar","village_name":"Kondamadugu","survey_no":"165","property_name":"3way","acres":"4","latitude":"17.3414983","longitude":"78.5423097","document":"46.5_x_30_East_face_Plan.pdf","images":"","legal_team_documents":null,"property_description":null,"agent_id":"4","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-12 19:34:47","ad_image":"","ad_comments":""},{"id":"1","unique_id":"3WL2005121","user_mobile":"9848174994","district_name":"Ranga reddy","mandal_name":"Ghatkesar","village_name":"Korremula","survey_no":"423,424,425","property_name":"3way","acres":"12","latitude":"17.3414973","longitude":"78.5423043","document":"IMG-20200510-WA0008.jpg,IMG-20200510-WA0005.jpg","images":"","legal_team_documents":null,"property_description":null,"agent_id":"4","status":"1","ct_status":"1","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-12 19:24:14","ad_image":"","ad_comments":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : leads list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 6
         * unique_id : 3WL2005146
         * user_mobile : 7286882452
         * district_name : wert
         * mandal_name : wqert
         * village_name : qwerty
         * survey_no : wer
         * property_name : Testing
         * acres : werty
         * latitude : 17.312548333333332
         * longitude : 78.53632833333333
         * document : 46.5_x_30_East_face_Plan1.pdf,46.5_x_30_East_face_Plan2.pdf
         * images :
         * legal_team_documents : null
         * property_description : null
         * agent_id : 5
         * status : 1
         * ct_status : 2
         * mt_status : 3
         * pt_status : 2
         * tt_status : 2
         * admin_status : 0
         * tt_update_dt :
         * ct_update_dt :
         * mt_update_dt :
         * pt_update_dt :
         * datetime : 2020-05-14 21:43:53
         * ad_image :
         * ad_comments :
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String latitude;
        private String longitude;
        private String document;
        private String images;
        private Object legal_team_documents;
        private Object property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String tt_status;
        private String admin_status;
        private String tt_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String datetime;
        private String ad_image;
        private String ad_comments;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public Object getLegal_team_documents() {
            return legal_team_documents;
        }

        public void setLegal_team_documents(Object legal_team_documents) {
            this.legal_team_documents = legal_team_documents;
        }

        public Object getProperty_description() {
            return property_description;
        }

        public void setProperty_description(Object property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getTt_status() {
            return tt_status;
        }

        public void setTt_status(String tt_status) {
            this.tt_status = tt_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getTt_update_dt() {
            return tt_update_dt;
        }

        public void setTt_update_dt(String tt_update_dt) {
            this.tt_update_dt = tt_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getAd_comments() {
            return ad_comments;
        }

        public void setAd_comments(String ad_comments) {
            this.ad_comments = ad_comments;
        }
    }
}
