package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminCityList {


    /**
     * status : {"code":200,"message":"List of City Names"}
     * data : [{"datetime":"2019-12-27 11:51:57","city_name":"Hyderabad","status":"1"},{"datetime":"2019-12-27 11:50:06","city_name":"Vijayawada","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of City Names
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * datetime : 2019-12-27 11:51:57
         * city_name : Hyderabad
         * status : 1
         */

        private String datetime;
        private String city_name;
        private String status;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
