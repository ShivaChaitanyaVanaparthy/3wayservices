package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AddUserAgentResponse {


    /**
     * status : {"code":200,"message":"Agent added Sub agent Successfully"}
     * data : [{"user_id":"21","agent_id":"2","usertype":"SubAgent","mobile":"7286882452","username":"Shiva","email":"shiva@Gmail.com","gender":null,"password":"4921c5f5fa2a30ebb0827e96c60a3f1c","profile":"http://igrandit.site/3way-services/admin_assets/uploads/users/1.jpg","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Agent added Sub agent Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * user_id : 21
         * agent_id : 2
         * usertype : SubAgent
         * mobile : 7286882452
         * username : Shiva
         * email : shiva@Gmail.com
         * gender : null
         * password : 4921c5f5fa2a30ebb0827e96c60a3f1c
         * profile : http://igrandit.site/3way-services/admin_assets/uploads/users/1.jpg
         * status : 1
         */

        private String user_id;
        private String agent_id;
        private String usertype;
        private String mobile;
        private String username;
        private String email;
        private Object gender;
        private String password;
        private String profile;
        private String status;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getGender() {
            return gender;
        }

        public void setGender(Object gender) {
            this.gender = gender;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
