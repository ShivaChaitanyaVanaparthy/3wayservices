package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckingDashBoardResponse {


    /**
     * status : {"code":200,"message":"Leads Count"}
     * data : [{"Total leads":11,"Rejected leads":0,"Approved leads":3,"Pending leads":8}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Leads Count
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        @SerializedName("Total leads")
        private int _$TotalLeads293; // FIXME check this code
        @SerializedName("Rejected leads")
        private int _$RejectedLeads193; // FIXME check this code
        @SerializedName("Approved leads")
        private int _$ApprovedLeads190; // FIXME check this code
        @SerializedName("Pending leads")
        private int _$PendingLeads266; // FIXME check this code

        public int get_$TotalLeads293() {
            return _$TotalLeads293;
        }

        public void set_$TotalLeads293(int _$TotalLeads293) {
            this._$TotalLeads293 = _$TotalLeads293;
        }

        public int get_$RejectedLeads193() {
            return _$RejectedLeads193;
        }

        public void set_$RejectedLeads193(int _$RejectedLeads193) {
            this._$RejectedLeads193 = _$RejectedLeads193;
        }

        public int get_$ApprovedLeads190() {
            return _$ApprovedLeads190;
        }

        public void set_$ApprovedLeads190(int _$ApprovedLeads190) {
            this._$ApprovedLeads190 = _$ApprovedLeads190;
        }

        public int get_$PendingLeads266() {
            return _$PendingLeads266;
        }

        public void set_$PendingLeads266(int _$PendingLeads266) {
            this._$PendingLeads266 = _$PendingLeads266;
        }
    }
}
