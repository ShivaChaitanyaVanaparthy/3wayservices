package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminEngagerList {


    /**
     * status : {"code":200,"message":"List of engagor"}
     * data : [{"id":"1","project_id":"2","engagor_id":"4","work_id":"1","subwork_id":"1","description":"good","machine_id":"1","vehicle_no":"dsad","start_time":"06:15 AM","end_time":"06:15 AM","start_time2":"06:15 AM","end_time2":"06:15 AM","date":"09/26/2020","project_name":"test project2","work_name":"Work","subwork_name":"SubWork","person_name":"Contractor4","type_name":"Tractor"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of engagor
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * project_id : 2
         * engagor_id : 4
         * work_id : 1
         * subwork_id : 1
         * description : good
         * machine_id : 1
         * vehicle_no : dsad
         * start_time : 06:15 AM
         * end_time : 06:15 AM
         * start_time2 : 06:15 AM
         * end_time2 : 06:15 AM
         * date : 09/26/2020
         * project_name : test project2
         * work_name : Work
         * subwork_name : SubWork
         * person_name : Contractor4
         * type_name : Tractor
         */

        private String id;
        private String project_id;
        private String engagor_id;
        private String work_id;
        private String subwork_id;
        private String description;
        private String machine_id;
        private String vehicle_no;
        private String start_time;
        private String end_time;
        private String start_time2;
        private String end_time2;
        private String date;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String person_name;
        private String type_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getEngagor_id() {
            return engagor_id;
        }

        public void setEngagor_id(String engagor_id) {
            this.engagor_id = engagor_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMachine_id() {
            return machine_id;
        }

        public void setMachine_id(String machine_id) {
            this.machine_id = machine_id;
        }

        public String getVehicle_no() {
            return vehicle_no;
        }

        public void setVehicle_no(String vehicle_no) {
            this.vehicle_no = vehicle_no;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public String getStart_time2() {
            return start_time2;
        }

        public void setStart_time2(String start_time2) {
            this.start_time2 = start_time2;
        }

        public String getEnd_time2() {
            return end_time2;
        }

        public void setEnd_time2(String end_time2) {
            this.end_time2 = end_time2;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getType_name() {
            return type_name;
        }

        public void setType_name(String type_name) {
            this.type_name = type_name;
        }
    }
}
