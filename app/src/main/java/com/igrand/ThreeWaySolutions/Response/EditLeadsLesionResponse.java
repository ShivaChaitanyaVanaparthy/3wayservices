package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditLeadsLesionResponse {


    /**
     * status : {"code":200,"message":"Lead Details For Update"}
     * data : [{"lead id":"25","legal team status":"1","legal team update time":"26-01-2020 11:48:55"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        @SerializedName("lead id")
        private String _$LeadId107; // FIXME check this code
        @SerializedName("legal team status")
        private String _$LegalTeamStatus71; // FIXME check this code
        @SerializedName("legal team update time")
        private String _$LegalTeamUpdateTime304; // FIXME check this code

        public String get_$LeadId107() {
            return _$LeadId107;
        }

        public void set_$LeadId107(String _$LeadId107) {
            this._$LeadId107 = _$LeadId107;
        }

        public String get_$LegalTeamStatus71() {
            return _$LegalTeamStatus71;
        }

        public void set_$LegalTeamStatus71(String _$LegalTeamStatus71) {
            this._$LegalTeamStatus71 = _$LegalTeamStatus71;
        }

        public String get_$LegalTeamUpdateTime304() {
            return _$LegalTeamUpdateTime304;
        }

        public void set_$LegalTeamUpdateTime304(String _$LegalTeamUpdateTime304) {
            this._$LegalTeamUpdateTime304 = _$LegalTeamUpdateTime304;
        }
    }
}
