package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LeadsListMarketing {


    /**
     * status : {"code":200,"message":"pending lead list"}
     * data : [{"id":"4","unique_id":"3WL2005134","user_mobile":"7286882452","district_name":"hyd","mandal_name":"hyd","village_name":"oiuytr","survey_no":"981","property_name":"oiuy","acres":"oiuy","latitude":"87654","longitude":"76854","document":"","images":"","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 14:38:42","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"5","unique_id":"3WL2005135","user_mobile":"7286882452","district_name":"hyd","mandal_name":"hyd","village_name":"oiuytr","survey_no":"981","property_name":"oiuy","acres":"oiuy","latitude":"87654","longitude":"76854","document":"NN7812251491030.ETicket.pdf","images":"","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 14:41:25","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"6","unique_id":"3WL2005146","user_mobile":"7286882452","district_name":"wert","mandal_name":"wqert","village_name":"qwerty","survey_no":"wer","property_name":"Testing","acres":"werty","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan1.pdf,46.5_x_30_East_face_Plan2.pdf","images":"","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 21:43:53","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"7","unique_id":"3WL2005147","user_mobile":"7286882452","district_name":"iouytr","mandal_name":"ertyui","village_name":"oiuytr","survey_no":"uyt","property_name":"Image/Pdf Test","acres":"tyui","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan3.pdf,NN7812251491030.ETicket1.pdf","images":"IMG_20200513_110140.jpg,IMG_20200513_110139.jpg,IMG_20200513_110136.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 21:59:37","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"8","unique_id":"3WL2005148","user_mobile":"7286882452","district_name":"kjhgf","mandal_name":"iuhygtfdrf","village_name":"oiuytr","survey_no":"kjhgf","property_name":"TEsttty","acres":"jkhgf","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan4.pdf","images":"IMG_20200513_1101401.jpg,IMG_20200513_1101391.jpg,IMG_20200513_1101361.jpg,IMG_20200513_1101402.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 22:13:45","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"9","unique_id":"3WL2005149","user_mobile":"7286882452","district_name":"kjlh","mandal_name":"fgh","village_name":"Klj","survey_no":"lkjh","property_name":"rfghjb","acres":"fghj","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG_20200513_1101403.jpg,IMG_20200513_1101392.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:24:14","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"10","unique_id":"3WL20051410","user_mobile":"7286882452","district_name":"jhg","mandal_name":"fgh","village_name":"kjh","survey_no":"khj","property_name":"rtfgh","acres":"fgh","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG_20200513_1101404.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:26:24","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"11","unique_id":"3WL20051411","user_mobile":"7286882452","district_name":"fghj","mandal_name":"kljmnb","village_name":"fghvb","survey_no":"dfghvkjh","property_name":"Titile","acres":"kjh","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG_20200513_1101405.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:40:58","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"12","unique_id":"3WL20051412","user_mobile":"7286882452","district_name":"rtfghj","mandal_name":"lkjh","village_name":"ftghj","survey_no":"rfghj","property_name":"rtfyghj","acres":"kjh","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan5.pdf","images":"IMG_20200513_1101406.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:46:39","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"16","unique_id":"3WL20051916","user_mobile":"9848174994","district_name":"Test","mandal_name":"Test","village_name":"Test","survey_no":"468","property_name":"Test","acres":"45","latitude":"17.34147655","longitude":"78.54223115","document":"014452_LT_SMD_FLT_U6_HMDA_27082018.pdf,015856_SMD_LT_U6_HMDA_01102018.pdf","images":"IMG-20200510-WA00082.jpg,IMG-20200510-WA00052.jpg","property_description":null,"agent_id":"4","status":"1","ct_status":"","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"19-05-2020 05:14:01","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-19 14:18:16","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"21","unique_id":"3WL20052721","user_mobile":"7286882452","district_name":"test","mandal_name":"test","village_name":"test","survey_no":"test","property_name":"test","acres":"test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG55100202948899998212.jpg,IMG65221776774516791632.jpg","property_description":"testttttt","agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-27 17:18:15","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"22","unique_id":"3WL20052822","user_mobile":"9848174994","district_name":"33","mandal_name":"33","village_name":"33","survey_no":"33","property_name":"33","acres":"33","latitude":"17.34145712","longitude":"78.54231601","document":"014452_LT_SMD_FLT_U6_HMDA_270820184.pdf","images":"IMG-20200528-WA0002.jpg","property_description":"33","agent_id":"4","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-28 10:29:04","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"23","unique_id":"3WL20053123","user_mobile":"8341684410","district_name":"Yadadri","mandal_name":"Bhonagiri","village_name":"Bhonagiri","survey_no":"8,9,10","property_name":"E krishna","acres":"12","latitude":"17.50944875","longitude":"78.89336834","document":"","images":"","property_description":"back side bhonagiri rock","agent_id":"10","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-31 12:11:53","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"24","unique_id":"3WL20053124","user_mobile":"9848174994","district_name":"44","mandal_name":"44","village_name":"44","survey_no":"44","property_name":"44","acres":"44","latitude":"17.34139137","longitude":"78.5422971","document":"014452_LT_SMD_FLT_U6_HMDA_270820185.pdf","images":"IMG-20200527-WA0005.jpg,IMG-20200510-WA00054.jpg","property_description":"44","agent_id":"4","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-31 14:42:08","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"25","unique_id":"3WL20060425","user_mobile":"8341684410","district_name":"Medchal","mandal_name":"X","village_name":"Ghatkesar","survey_no":"x","property_name":"X","acres":"73","latitude":"17.42991643","longitude":"78.69433684","document":"","images":"","property_description":"details will follow","agent_id":"10","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-06-04 10:44:38","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"26","unique_id":"3WL20060926","user_mobile":"7286882452","district_name":"test","mandal_name":"TEt","village_name":"Test","survey_no":"test","property_name":"TestLead","acres":"test","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"","images":"IMG_20200528_113436.jpg","property_description":"test","agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-06-09 10:46:01","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"28","unique_id":"3WL20061728","user_mobile":"9848174994","district_name":"99","mandal_name":"99","village_name":"99","survey_no":"99","property_name":"99","acres":"99","latitude":"17.34146404","longitude":"78.54229196","document":"18_x_37_East_duplex_plan.pdf","images":"","property_description":"99","agent_id":"4","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-06-17 20:22:01","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"29","unique_id":"3WL20061729","user_mobile":"7286882452","district_name":"test","mandal_name":"test","village_name":"test","survey_no":"test","property_name":"test","acres":"test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"18.5_x_36_West_Duplex_Plan_(1).pdf","images":"IMG55100202948899998213.jpg","property_description":"test","agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-06-17 20:48:55","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"32","unique_id":"3WL20061732","user_mobile":"9848174994","district_name":"77","mandal_name":"77","village_name":"77","survey_no":"77","property_name":"77","acres":"77","latitude":"17.34142516","longitude":"78.54228338","document":"3091.pdf,3092.pdf","images":"IMG5249672710473661821.jpg,IMG-20200617-WA00133.jpg,IMG-20200615-WA00002.jpg,IMG5249672710473661822.jpg,IMG-20200617-WA00134.jpg","property_description":"77","agent_id":"4","status":"1","ct_status":"0","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"17-06-2020 09:10:38","mt_update_dt":"","pt_update_dt":"","datetime":"2020-06-17 21:06:32","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"35","unique_id":"3WL20062935","user_mobile":"7286882452","district_name":"Hzb","mandal_name":"Hzb","village_name":"Sindur","survey_no":"123456","property_name":"Chandan testing","acres":"200","latitude":"24.0288437","longitude":"85.3751407","document":"advance_java1.pdf,advance_java2.pdf","images":"Screenshot_2020-06-28-21-01-53-091.png,Screenshot_2020-06-28-21-01-34-411.png,Screenshot_2020-06-28-21-01-53-092.png,Screenshot_2020-06-28-21-01-34-412.png","property_description":"test","agent_id":"5","status":"1","ct_status":"0","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"04-07-2020 10:07:42","mt_update_dt":"","pt_update_dt":"","datetime":"2020-06-29 10:25:15","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"37","unique_id":"3WL20070437","user_mobile":"9848174994","district_name":"66","mandal_name":"66","village_name":"66","survey_no":"66","property_name":"66","acres":"66","latitude":"17.34146219","longitude":"78.542291","document":"12_x_40_x_framing(1).pdf","images":"IMG-20200703-WA0007.jpg,IMG-20200703-WA0008.jpg,IMG-20200704-WA0007.jpg,_uhdanimals5641.jpg","property_description":"66","agent_id":"4","status":"1","ct_status":"0","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"06-07-2020 12:11:58","mt_update_dt":"","pt_update_dt":"","datetime":"2020-07-04 21:54:07","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"40","unique_id":"3WL20070740","user_mobile":"7286882452","district_name":"test","mandal_name":"test","village_name":"test","survey_no":"test","property_name":"test","acres":"test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"18.5_x_36_West_Duplex_Plan_(1)1.pdf,30921.pdf","images":"IMG55100202948899998215.jpg,IMG65221776774516791634.jpg","property_description":"test","agent_id":"5","status":"1","ct_status":"1","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"07-07-2020 09:33:52","mt_update_dt":"","pt_update_dt":"","datetime":"2020-07-07 09:16:24","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : pending lead list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 4
         * unique_id : 3WL2005134
         * user_mobile : 7286882452
         * district_name : hyd
         * mandal_name : hyd
         * village_name : oiuytr
         * survey_no : 981
         * property_name : oiuy
         * acres : oiuy
         * latitude : 87654
         * longitude : 76854
         * document :
         * images :
         * property_description : null
         * agent_id : 5
         * status : 1
         * ct_status : 2
         * mt_status : 3
         * pt_status : 2
         * tt_status : 2
         * admin_status : 0
         * tt_update_dt :
         * ct_update_dt :
         * mt_update_dt :
         * pt_update_dt :
         * datetime : 2020-05-13 14:38:42
         * ad_image :
         * ad_comments :
         * road_connectivity :
         * expected_sales_value :
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String latitude;
        private String longitude;
        private String document;
        private String images;
        private String property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String tt_status;
        private String admin_status;
        private String tt_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String datetime;
        private String ad_image;
        private String ad_comments;
        private String road_connectivity;
        private String expected_sales_value;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getProperty_description() {
            return property_description;
        }

        public void setProperty_description(String property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getTt_status() {
            return tt_status;
        }

        public void setTt_status(String tt_status) {
            this.tt_status = tt_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getTt_update_dt() {
            return tt_update_dt;
        }

        public void setTt_update_dt(String tt_update_dt) {
            this.tt_update_dt = tt_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getAd_comments() {
            return ad_comments;
        }

        public void setAd_comments(String ad_comments) {
            this.ad_comments = ad_comments;
        }

        public String getRoad_connectivity() {
            return road_connectivity;
        }

        public void setRoad_connectivity(String road_connectivity) {
            this.road_connectivity = road_connectivity;
        }

        public String getExpected_sales_value() {
            return expected_sales_value;
        }

        public void setExpected_sales_value(String expected_sales_value) {
            this.expected_sales_value = expected_sales_value;
        }
    }
}
