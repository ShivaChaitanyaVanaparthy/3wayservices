package com.igrand.ThreeWaySolutions.Response;


import java.util.List;

public class ProcDocumentResponse {


    /**
     * status : {"code":200,"message":"Proposal Document"}
     * proposal_documents : [{"id":"3","user_mobile":"9999999999","lead_id":"27","proposal_notes":"img4","proposal_document":"IMG_20200528_113436.jpg","proposal_document_pdf":"","pd_datetime":"2020-06-09 11:43:23"},{"id":"7","user_mobile":"9999999999","lead_id":"27","proposal_notes":"test","proposal_document":"141478445IMG5510020294889999821.jpg","proposal_document_pdf":"106965274618_5_x_36_West_Duplex_Plan.pdf","pd_datetime":"2020-06-26 14:40:35"}]
     */

    private StatusBean status;
    private List<ProposalDocumentsBean> proposal_documents;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<ProposalDocumentsBean> getProposal_documents() {
        return proposal_documents;
    }

    public void setProposal_documents(List<ProposalDocumentsBean> proposal_documents) {
        this.proposal_documents = proposal_documents;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Proposal Document
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class ProposalDocumentsBean {
        /**
         * id : 3
         * user_mobile : 9999999999
         * lead_id : 27
         * proposal_notes : img4
         * proposal_document : IMG_20200528_113436.jpg
         * proposal_document_pdf :
         * pd_datetime : 2020-06-09 11:43:23
         */

        private String id;
        private String user_mobile;
        private String lead_id;
        private String proposal_notes;
        private String proposal_document;
        private String proposal_document_pdf;
        private String pd_datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getProposal_notes() {
            return proposal_notes;
        }

        public void setProposal_notes(String proposal_notes) {
            this.proposal_notes = proposal_notes;
        }

        public String getProposal_document() {
            return proposal_document;
        }

        public void setProposal_document(String proposal_document) {
            this.proposal_document = proposal_document;
        }

        public String getProposal_document_pdf() {
            return proposal_document_pdf;
        }

        public void setProposal_document_pdf(String proposal_document_pdf) {
            this.proposal_document_pdf = proposal_document_pdf;
        }

        public String getPd_datetime() {
            return pd_datetime;
        }

        public void setPd_datetime(String pd_datetime) {
            this.pd_datetime = pd_datetime;
        }
    }
}
