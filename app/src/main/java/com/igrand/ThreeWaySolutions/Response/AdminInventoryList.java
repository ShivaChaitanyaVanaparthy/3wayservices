package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminInventoryList {


    /**
     * status : {"code":200,"message":"List of Inventory"}
     * data : [{"datetime":"2019-12-14 17:15:01","project_name":"TEsting","material_type":"Sand","quantity":"2","descriptions":"fgvgdgf","status":"1"},{"datetime":"2019-12-14 17:15:56","project_name":"fghjk","material_type":"poiuytr","quantity":"6","descriptions":"fdfsfsdfs","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Inventory
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * datetime : 2019-12-14 17:15:01
         * project_name : TEsting
         * material_type : Sand
         * quantity : 2
         * descriptions : fgvgdgf
         * status : 1
         */

        private String datetime;
        private String project_name;
        private String material_type;
        private String quantity;
        private String descriptions;
        private String status;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getMaterial_type() {
            return material_type;
        }

        public void setMaterial_type(String material_type) {
            this.material_type = material_type;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getDescriptions() {
            return descriptions;
        }

        public void setDescriptions(String descriptions) {
            this.descriptions = descriptions;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
