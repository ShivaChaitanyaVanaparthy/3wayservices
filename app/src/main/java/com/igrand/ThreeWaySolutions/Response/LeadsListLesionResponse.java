package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LeadsListLesionResponse {


    /**
     * status : {"code":200,"message":"Leads Count"}
     * data : [{"id":"98","unique_id":"3WL20040698","user_mobile":"8688543727","district_name":"District","mandal_name":"Mandal","village_name":"Village","survey_no":"21","property_name":"Nam","acres":"2","address":"iuytfghvbn","latitude":"37.421998","longitude":"-122.084","document":"IMG3371210130297622133.jpg,IMG4024192141707376717.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"1","admin_status":"1","st_status":"1","st_update_dt":"06-04-2020 11:27:43","ct_update_dt":"06-04-2020 11:16:09","mt_update_dt":"06-04-2020 11:17:08","pt_update_dt":"06-04-2020 11:21:23","lgt_update_dt":"06-04-2020 11:24:40","ls_update_dt":"06-04-2020 11:32:56","datetime":"2020-04-06 10:47:59","ch_image":"","ad_image":"temp.jpg","ad_comments":"Hello"},{"id":"9","unique_id":"3WL2002049","user_mobile":"7286882452","district_name":"","mandal_name":"","village_name":"Fnj","survey_no":"dhj","property_name":"Lead","acres":"fhj","address":"Zbn","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG-20200204-WA0001.jpg,IMG-20200204-WA0000.jpg,IMG-20200203-WA0000.jpg","legal_team_documents":null,"property_description":null,"agent_id":"42","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"2","admin_status":"0","st_status":"1","st_update_dt":"10-02-2020 11:03:15","ct_update_dt":"04-02-2020 03:58:33","mt_update_dt":"04-02-2020 05:14:35","pt_update_dt":"04-02-2020 05:15:43","lgt_update_dt":"10-02-2020 11:03:43","ls_update_dt":"","datetime":"2020-02-04 11:53:43","ch_image":"","ad_image":"","ad_comments":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Leads Count
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 98
         * unique_id : 3WL20040698
         * user_mobile : 8688543727
         * district_name : District
         * mandal_name : Mandal
         * village_name : Village
         * survey_no : 21
         * property_name : Nam
         * acres : 2
         * address : iuytfghvbn
         * latitude : 37.421998
         * longitude : -122.084
         * document : IMG3371210130297622133.jpg,IMG4024192141707376717.jpg
         * legal_team_documents : null
         * property_description : null
         * agent_id : 37
         * status : 1
         * ct_status : 1
         * mt_status : 1
         * pt_status : 5
         * lgt_status : 1
         * ls_status : 1
         * admin_status : 1
         * st_status : 1
         * st_update_dt : 06-04-2020 11:27:43
         * ct_update_dt : 06-04-2020 11:16:09
         * mt_update_dt : 06-04-2020 11:17:08
         * pt_update_dt : 06-04-2020 11:21:23
         * lgt_update_dt : 06-04-2020 11:24:40
         * ls_update_dt : 06-04-2020 11:32:56
         * datetime : 2020-04-06 10:47:59
         * ch_image :
         * ad_image : temp.jpg
         * ad_comments : Hello
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String address;
        private String latitude;
        private String longitude;
        private String document;
        private Object legal_team_documents;
        private Object property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String lgt_status;
        private String ls_status;
        private String admin_status;
        private String st_status;
        private String st_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String lgt_update_dt;
        private String ls_update_dt;
        private String datetime;
        private String ch_image;
        private String ad_image;
        private String ad_comments;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public Object getLegal_team_documents() {
            return legal_team_documents;
        }

        public void setLegal_team_documents(Object legal_team_documents) {
            this.legal_team_documents = legal_team_documents;
        }

        public Object getProperty_description() {
            return property_description;
        }

        public void setProperty_description(Object property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getLgt_status() {
            return lgt_status;
        }

        public void setLgt_status(String lgt_status) {
            this.lgt_status = lgt_status;
        }

        public String getLs_status() {
            return ls_status;
        }

        public void setLs_status(String ls_status) {
            this.ls_status = ls_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getSt_status() {
            return st_status;
        }

        public void setSt_status(String st_status) {
            this.st_status = st_status;
        }

        public String getSt_update_dt() {
            return st_update_dt;
        }

        public void setSt_update_dt(String st_update_dt) {
            this.st_update_dt = st_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getLgt_update_dt() {
            return lgt_update_dt;
        }

        public void setLgt_update_dt(String lgt_update_dt) {
            this.lgt_update_dt = lgt_update_dt;
        }

        public String getLs_update_dt() {
            return ls_update_dt;
        }

        public void setLs_update_dt(String ls_update_dt) {
            this.ls_update_dt = ls_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getCh_image() {
            return ch_image;
        }

        public void setCh_image(String ch_image) {
            this.ch_image = ch_image;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getAd_comments() {
            return ad_comments;
        }

        public void setAd_comments(String ad_comments) {
            this.ad_comments = ad_comments;
        }
    }
}
