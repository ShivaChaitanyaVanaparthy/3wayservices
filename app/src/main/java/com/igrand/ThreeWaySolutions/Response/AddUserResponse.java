package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AddUserResponse {


    /**
     * status : {"code":200,"message":"Admin added User Successfully"}
     * data : [{"user_id":"30","usertype":"Agent","mobile":"7330977622","username":"Srinivass","email":"shiva.chaitanya62@gmail.com","password":"38230a4f78e1da15ff8ca58647b69804","profile":"http://igrandit.site/3way-solutions/admin_assets/uploads/users/408c7a82402d9e6dd7e37d80631dd0981.jpg","status":"Active"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added User Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * user_id : 30
         * usertype : Agent
         * mobile : 7330977622
         * username : Srinivass
         * email : shiva.chaitanya62@gmail.com
         * password : 38230a4f78e1da15ff8ca58647b69804
         * profile : http://igrandit.site/3way-solutions/admin_assets/uploads/users/408c7a82402d9e6dd7e37d80631dd0981.jpg
         * status : Active
         */

        private String user_id;
        private String usertype;
        private String mobile;
        private String username;
        private String email;
        private String password;
        private String profile;
        private String status;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
