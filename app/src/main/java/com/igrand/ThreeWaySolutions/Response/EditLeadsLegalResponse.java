package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditLeadsLegalResponse {


    /**
     * status : {"code":200,"message":"Lead Details For Update"}
     * data : [{"lead id":"25","legal team status":"2","legal team update time":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        @SerializedName("lead id")
        private String _$LeadId126; // FIXME check this code
        @SerializedName("legal team status")
        private String _$LegalTeamStatus15; // FIXME check this code
        @SerializedName("legal team update time")
        private String _$LegalTeamUpdateTime320; // FIXME check this code

        public String get_$LeadId126() {
            return _$LeadId126;
        }

        public void set_$LeadId126(String _$LeadId126) {
            this._$LeadId126 = _$LeadId126;
        }

        public String get_$LegalTeamStatus15() {
            return _$LegalTeamStatus15;
        }

        public void set_$LegalTeamStatus15(String _$LegalTeamStatus15) {
            this._$LegalTeamStatus15 = _$LegalTeamStatus15;
        }

        public String get_$LegalTeamUpdateTime320() {
            return _$LegalTeamUpdateTime320;
        }

        public void set_$LegalTeamUpdateTime320(String _$LegalTeamUpdateTime320) {
            this._$LegalTeamUpdateTime320 = _$LegalTeamUpdateTime320;
        }
    }
}
