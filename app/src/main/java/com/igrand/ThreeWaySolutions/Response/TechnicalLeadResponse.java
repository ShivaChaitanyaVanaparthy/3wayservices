package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

// FIXME generate failure  field _$LeadsComment331
public class TechnicalLeadResponse {


    /**
     * status : {"code":200,"message":"Lead Details"}
     * data : [{"id":"31","datetime":"2020-06-17 21:04:49","village_name":"77","document":["http://www.igranddeveloper.xyz/3way-services//admin_assets/uploads/leads/documents/309.pdf"],"property_name":"77","latitude":"17.34142516","longitude":"78.54228338","property_description":"77","survey_no":"77","acres":"77","checking team status":"1","marketing team status":"1","procurement team status":"2","Checking Update Date":"19-06-2020 05:28:42","Marketing Update Date":"19-06-2020 06:01:50","Procurement Update Date":"","status":"1"}]
     * leads comment : [{"id":"25","comments":"7777","user_mobile":"8210402087","lead_id":"31","status":"1","datetime":"2020-06-19 17:28:42"},{"id":"27","comments":"Good","user_mobile":"9848174994","lead_id":"31","status":"1","datetime":"2020-06-19 18:01:50"},{"id":"28","comments":"good","user_mobile":"1234567890","lead_id":"31","status":"1","datetime":"2020-06-19 18:11:55"}]
     */

    private StatusBean status;
    private List<DataBean> data;
    private List<CommentBean> commentBean;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 31
         * datetime : 2020-06-17 21:04:49
         * village_name : 77
         * document : ["http://www.igranddeveloper.xyz/3way-services//admin_assets/uploads/leads/documents/309.pdf"]
         * property_name : 77
         * latitude : 17.34142516
         * longitude : 78.54228338
         * property_description : 77
         * survey_no : 77
         * acres : 77
         * checking team status : 1
         * marketing team status : 1
         * procurement team status : 2
         * Checking Update Date : 19-06-2020 05:28:42
         * Marketing Update Date : 19-06-2020 06:01:50
         * Procurement Update Date :
         * status : 1
         */

        private String id;
        private String datetime;
        private String village_name;
        private String property_name;
        private String latitude;
        private String longitude;
        private String property_description;
        private String survey_no;
        private String acres;
        @com.google.gson.annotations.SerializedName("checking team status")
        private String _$CheckingTeamStatus204; // FIXME check this code
        @com.google.gson.annotations.SerializedName("marketing team status")
        private String _$MarketingTeamStatus287; // FIXME check this code
        @com.google.gson.annotations.SerializedName("procurement team status")
        private String _$ProcurementTeamStatus319; // FIXME check this code
        @com.google.gson.annotations.SerializedName("Checking Update Date")
        private String _$CheckingUpdateDate80; // FIXME check this code
        @com.google.gson.annotations.SerializedName("Marketing Update Date")
        private String _$MarketingUpdateDate231; // FIXME check this code
        @com.google.gson.annotations.SerializedName("Procurement Update Date")
        private String _$ProcurementUpdateDate254; // FIXME check this code
        private String status;
        private java.util.List<String> document;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getProperty_description() {
            return property_description;
        }

        public void setProperty_description(String property_description) {
            this.property_description = property_description;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String get_$CheckingTeamStatus204() {
            return _$CheckingTeamStatus204;
        }

        public void set_$CheckingTeamStatus204(String _$CheckingTeamStatus204) {
            this._$CheckingTeamStatus204 = _$CheckingTeamStatus204;
        }

        public String get_$MarketingTeamStatus287() {
            return _$MarketingTeamStatus287;
        }

        public void set_$MarketingTeamStatus287(String _$MarketingTeamStatus287) {
            this._$MarketingTeamStatus287 = _$MarketingTeamStatus287;
        }

        public String get_$ProcurementTeamStatus319() {
            return _$ProcurementTeamStatus319;
        }

        public void set_$ProcurementTeamStatus319(String _$ProcurementTeamStatus319) {
            this._$ProcurementTeamStatus319 = _$ProcurementTeamStatus319;
        }

        public String get_$CheckingUpdateDate80() {
            return _$CheckingUpdateDate80;
        }

        public void set_$CheckingUpdateDate80(String _$CheckingUpdateDate80) {
            this._$CheckingUpdateDate80 = _$CheckingUpdateDate80;
        }

        public String get_$MarketingUpdateDate231() {
            return _$MarketingUpdateDate231;
        }

        public void set_$MarketingUpdateDate231(String _$MarketingUpdateDate231) {
            this._$MarketingUpdateDate231 = _$MarketingUpdateDate231;
        }

        public String get_$ProcurementUpdateDate254() {
            return _$ProcurementUpdateDate254;
        }

        public void set_$ProcurementUpdateDate254(String _$ProcurementUpdateDate254) {
            this._$ProcurementUpdateDate254 = _$ProcurementUpdateDate254;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getDocument() {
            return document;
        }

        public void setDocument(List<String> document) {
            this.document = document;
        }
    }

    public static class CommentBean {
        /**
         * id : 25
         * comments : 7777
         * user_mobile : 8210402087
         * lead_id : 31
         * status : 1
         * datetime : 2020-06-19 17:28:42
         */

        private String id;
        private String comments;
        private String user_mobile;
        private String lead_id;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
