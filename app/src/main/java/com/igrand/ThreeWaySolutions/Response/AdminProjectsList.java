package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminProjectsList {


    /**
     * status : {"code":200,"message":"List of Projects"}
     * data : [{"id":"2","device":"","ip_address":"","project_name":"Pankaj Project","village_name":"Balkampet","status":"1","datetime":"2020-02-18 15:44:08"},{"id":"1","device":"","ip_address":"","project_name":"Tukkuguda new project","village_name":"Tukkuguda","status":"1","datetime":"2020-02-04 23:34:56"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Projects
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 2
         * device :
         * ip_address :
         * project_name : Pankaj Project
         * village_name : Balkampet
         * status : 1
         * datetime : 2020-02-18 15:44:08
         */

        private String id;
        private String device;
        private String ip_address;
        private String project_name;
        private String village_name;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDevice() {
            return device;
        }

        public void setDevice(String device) {
            this.device = device;
        }

        public String getIp_address() {
            return ip_address;
        }

        public void setIp_address(String ip_address) {
            this.ip_address = ip_address;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
