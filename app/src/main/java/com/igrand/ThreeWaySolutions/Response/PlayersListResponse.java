package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class PlayersListResponse {


    /**
     * status : {"code":200,"message":"Players list"}
     * data : [{"id":"4","player_first_name":"Suresh","player_last_name":"Reddy","image":"temp.jpg","phone":"8555013687","game_type_id":"05/05/1990","dob":"05/05/1990","state_id":"2","district_id":"2","mandal_id":"3","village_id":"3","aadhar_no":"9897654","aadhar_image":"temp1.jpg","address":"iujghf","pin":"89765","state_name":"Telengana","district_name":"Hyderabad","mandal_name":"Saroornagar","village_name":"Champapet","game_type":""},{"id":"3","player_first_name":"Firste","player_last_name":"Last","image":"12.jpg","phone":"9876543w567","game_type_id":"2","dob":"iuhgyh","state_id":"2","district_id":"2","mandal_id":"2","village_id":"1","aadhar_no":"12","aadhar_image":"13.jpg","address":"wef","pin":"123","state_name":"Telengana","district_name":"Hyderabad","mandal_name":"Daru","village_name":"Daru","game_type":"cricket"},{"id":"1","player_first_name":"Name","player_last_name":"Last","image":"1.jpg","phone":"9876543w567","game_type_id":"2","dob":"iuhgyh","state_id":"2","district_id":"2","mandal_id":"2","village_id":"1","aadhar_no":"12","aadhar_image":"11.jpg","address":"wef","pin":"123","state_name":"Telengana","district_name":"Hyderabad","mandal_name":"Daru","village_name":"Daru","game_type":"cricket"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Players list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 4
         * player_first_name : Suresh
         * player_last_name : Reddy
         * image : temp.jpg
         * phone : 8555013687
         * game_type_id : 05/05/1990
         * dob : 05/05/1990
         * state_id : 2
         * district_id : 2
         * mandal_id : 3
         * village_id : 3
         * aadhar_no : 9897654
         * aadhar_image : temp1.jpg
         * address : iujghf
         * pin : 89765
         * state_name : Telengana
         * district_name : Hyderabad
         * mandal_name : Saroornagar
         * village_name : Champapet
         * game_type :
         */

        private String id;
        private String player_first_name;
        private String player_last_name;
        private String image;
        private String phone;
        private String game_type_id;
        private String dob;
        private String state_id;
        private String district_id;
        private String mandal_id;
        private String village_id;
        private String aadhar_no;
        private String aadhar_image;
        private String address;
        private String pin;
        private String state_name;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String game_type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPlayer_first_name() {
            return player_first_name;
        }

        public void setPlayer_first_name(String player_first_name) {
            this.player_first_name = player_first_name;
        }

        public String getPlayer_last_name() {
            return player_last_name;
        }

        public void setPlayer_last_name(String player_last_name) {
            this.player_last_name = player_last_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getGame_type_id() {
            return game_type_id;
        }

        public void setGame_type_id(String game_type_id) {
            this.game_type_id = game_type_id;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getMandal_id() {
            return mandal_id;
        }

        public void setMandal_id(String mandal_id) {
            this.mandal_id = mandal_id;
        }

        public String getVillage_id() {
            return village_id;
        }

        public void setVillage_id(String village_id) {
            this.village_id = village_id;
        }

        public String getAadhar_no() {
            return aadhar_no;
        }

        public void setAadhar_no(String aadhar_no) {
            this.aadhar_no = aadhar_no;
        }

        public String getAadhar_image() {
            return aadhar_image;
        }

        public void setAadhar_image(String aadhar_image) {
            this.aadhar_image = aadhar_image;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getGame_type() {
            return game_type;
        }

        public void setGame_type(String game_type) {
            this.game_type = game_type;
        }
    }
}
