package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class WorkReportLIst {


    /**
     * status : {"code":200,"message":"Date wise Daily Progress report"}
     * data : [[{"id":"16","project_id":"3","contractor_id":"1","work_id":"1","subwork_id":"2","work_description":"Test","skilled":"12","skilled_rate":"12","male_no":"13","male_rate":"13","female_no":"13","female_rate":"13","amount":"482","date":"03/11/2020","project_name":"Miracle. One","work_name":"Road work","subwork_name":"SubWork","person_name":"vinay"}],[{"id":"18","project_id":"3","contractor_id":"1","work_id":"3","subwork_id":"3","work_description":"Test","skilled":"10","skilled_rate":"10","male_no":"10","male_rate":"10","female_no":"10","female_rate":"10","amount":"300","date":"03/17/2020","project_name":"Miracle. One","work_name":"drainage work","subwork_name":"Sub-Drainage","person_name":"vinay"},{"id":"14","project_id":"3","contractor_id":"1","work_id":"1","subwork_id":"2","work_description":"Test","skilled":"10","skilled_rate":"10","male_no":"10","male_rate":"10","female_no":"10","female_rate":"10","amount":"300","date":"03/17/2020","project_name":"Miracle. One","work_name":"Road work","subwork_name":"SubWork","person_name":"vinay"},{"id":"12","project_id":"3","contractor_id":"1","work_id":"4","subwork_id":"4","work_description":"Test","skilled":"2","skilled_rate":"2","male_no":"2","male_rate":"2","female_no":"2","female_rate":"2","amount":"12","date":"03/17/2020","project_name":"Miracle. One","work_name":"metal laying","subwork_name":"metal subwork","person_name":"vinay"}],[{"id":"13","project_id":"2","contractor_id":"1","work_id":"3","subwork_id":"3","work_description":"Test","skilled":"3","skilled_rate":"5","male_no":"3","male_rate":"6","female_no":"3","female_rate":"6","amount":"51","date":"03/17/2020","project_name":"Pankaj Project","work_name":"drainage work","subwork_name":"Sub-Drainage","person_name":"vinay"}],[{"id":"11","project_id":"1","contractor_id":"1","work_id":"1","subwork_id":"1","work_description":"Test","skilled":"1","skilled_rate":"1","male_no":"1","male_rate":"1","female_no":"1","female_rate":"1","amount":"3","date":"03/17/2020","project_name":"Tukkuguda new project","work_name":"Road work","subwork_name":"road cutting","person_name":"vinay"}],[{"id":"15","project_id":"3","contractor_id":"2","work_id":"1","subwork_id":"2","work_description":"Test","skilled":"98","skilled_rate":"98","male_no":"45","male_rate":"87","female_no":"56","female_rate":"7","amount":"13911","date":"03/19/2020","project_name":"Miracle. One","work_name":"Road work","subwork_name":"SubWork","person_name":"sa"}],[{"id":"17","project_id":"3","contractor_id":"1","work_id":"3","subwork_id":"3","work_description":"fgchv","skilled":"12","skilled_rate":"10","male_no":"10","male_rate":"10","female_no":"10","female_rate":"10","amount":"320","date":"03/25/2020","project_name":"Miracle. One","work_name":"drainage work","subwork_name":"Sub-Drainage","person_name":"vinay"}]]
     * Amount : [482,612,51,3,13911,320]
     */

    private StatusBean status;
    private List<List<DataBean>> data;
    private List<Integer> Amount;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public List<Integer> getAmount() {
        return Amount;
    }

    public void setAmount(List<Integer> Amount) {
        this.Amount = Amount;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Date wise Daily Progress report
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 16
         * project_id : 3
         * contractor_id : 1
         * work_id : 1
         * subwork_id : 2
         * work_description : Test
         * skilled : 12
         * skilled_rate : 12
         * male_no : 13
         * male_rate : 13
         * female_no : 13
         * female_rate : 13
         * amount : 482
         * date : 03/11/2020
         * project_name : Miracle. One
         * work_name : Road work
         * subwork_name : SubWork
         * person_name : vinay
         */

        private String id;
        private String project_id;
        private String contractor_id;
        private String work_id;
        private String subwork_id;
        private String work_description;
        private String skilled;
        private String skilled_rate;
        private String male_no;
        private String male_rate;
        private String female_no;
        private String female_rate;
        private String amount;
        private String date;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String person_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getContractor_id() {
            return contractor_id;
        }

        public void setContractor_id(String contractor_id) {
            this.contractor_id = contractor_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getWork_description() {
            return work_description;
        }

        public void setWork_description(String work_description) {
            this.work_description = work_description;
        }

        public String getSkilled() {
            return skilled;
        }

        public void setSkilled(String skilled) {
            this.skilled = skilled;
        }

        public String getSkilled_rate() {
            return skilled_rate;
        }

        public void setSkilled_rate(String skilled_rate) {
            this.skilled_rate = skilled_rate;
        }

        public String getMale_no() {
            return male_no;
        }

        public void setMale_no(String male_no) {
            this.male_no = male_no;
        }

        public String getMale_rate() {
            return male_rate;
        }

        public void setMale_rate(String male_rate) {
            this.male_rate = male_rate;
        }

        public String getFemale_no() {
            return female_no;
        }

        public void setFemale_no(String female_no) {
            this.female_no = female_no;
        }

        public String getFemale_rate() {
            return female_rate;
        }

        public void setFemale_rate(String female_rate) {
            this.female_rate = female_rate;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }
    }
}
