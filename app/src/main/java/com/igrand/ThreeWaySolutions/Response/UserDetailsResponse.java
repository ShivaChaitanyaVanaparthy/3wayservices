package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserDetailsResponse {


    /**
     * status : {"code":200,"message":"User Details along with Leads"}
     * data : [{"username":"Pankaj","usertype":"agent","email":"pankaj@gmail.com","mobile":"9135306477","status":"1","profile":"http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/users/IMG_20200227_190201.jpg","leads-count":1,"leads":[{"time":"2020-03-04 11:40:23","village_name":"Test","survey_no":"yt4","property_name":"Test","acres":"12","address":"Hyd","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"temp6.jpg","property_description":null}]}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : User Details along with Leads
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * username : Pankaj
         * usertype : agent
         * email : pankaj@gmail.com
         * mobile : 9135306477
         * status : 1
         * profile : http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/users/IMG_20200227_190201.jpg
         * leads-count : 1
         * leads : [{"time":"2020-03-04 11:40:23","village_name":"Test","survey_no":"yt4","property_name":"Test","acres":"12","address":"Hyd","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"temp6.jpg","property_description":null}]
         */

        private String username;
        private String usertype;
        private String email;
        private String mobile;
        private String status;
        private String profile;
        @SerializedName("leads-count")
        private int leadscount;
        private List<LeadsBean> leads;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public int getLeadscount() {
            return leadscount;
        }

        public void setLeadscount(int leadscount) {
            this.leadscount = leadscount;
        }

        public List<LeadsBean> getLeads() {
            return leads;
        }

        public void setLeads(List<LeadsBean> leads) {
            this.leads = leads;
        }

        public static class LeadsBean {
            /**
             * time : 2020-03-04 11:40:23
             * village_name : Test
             * survey_no : yt4
             * property_name : Test
             * acres : 12
             * address : Hyd
             * latitude : 37.421998333333335
             * longitude : -122.08400000000002
             * document : temp6.jpg
             * property_description : null
             */

            private String time;
            private String village_name;
            private String survey_no;
            private String property_name;
            private String acres;
            private String address;
            private String latitude;
            private String longitude;
            private String document;
            private Object property_description;

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getVillage_name() {
                return village_name;
            }

            public void setVillage_name(String village_name) {
                this.village_name = village_name;
            }

            public String getSurvey_no() {
                return survey_no;
            }

            public void setSurvey_no(String survey_no) {
                this.survey_no = survey_no;
            }

            public String getProperty_name() {
                return property_name;
            }

            public void setProperty_name(String property_name) {
                this.property_name = property_name;
            }

            public String getAcres() {
                return acres;
            }

            public void setAcres(String acres) {
                this.acres = acres;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getDocument() {
                return document;
            }

            public void setDocument(String document) {
                this.document = document;
            }

            public Object getProperty_description() {
                return property_description;
            }

            public void setProperty_description(Object property_description) {
                this.property_description = property_description;
            }
        }
    }
}
