package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminActiveDistList {


    /**
     * status : {"code":200,"message":"Active District List"}
     * data : [{"id":"1","state_id":"1","district_name":"Hazaribag","status":"1","datetime":"2020-04-17 16:40:03"},{"id":"2","state_id":"2","district_name":"Hyderabad","status":"1","datetime":"2020-04-18 14:28:43"},{"id":"3","state_id":"3","district_name":"Kurnool","status":"1","datetime":"2020-04-18 14:34:41"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Active District List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * state_id : 1
         * district_name : Hazaribag
         * status : 1
         * datetime : 2020-04-17 16:40:03
         */

        private String id;
        private String state_id;
        private String district_name;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
