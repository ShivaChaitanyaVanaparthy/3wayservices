package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminValuesList {


    /**
     * status : {"code":200,"message":"List of Material Types"}
     * data : [{"datetime":"2019-12-13 16:24:40","material_type":"Sand","status":"1"},{"datetime":"2019-12-13 16:12:30","material_type":"iron","status":"1"},{"datetime":"2019-12-13 16:11:57","material_type":"iron","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Material Types
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * datetime : 2019-12-13 16:24:40
         * material_type : Sand
         * status : 1
         */

        private String datetime;
        private String material_type;
        private String status;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getMaterial_type() {
            return material_type;
        }

        public void setMaterial_type(String material_type) {
            this.material_type = material_type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
