package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class UserDetailsAgentResponse {


    /**
     * status : {"code":200,"message":"leads list"}
     * data : [{"id":"21","unique_id":"3WL20012321","user_mobile":"7286882455","village_name":"fghj","survey_no":"FGHVBJ","property_name":"iujyhgHGJB","acres":"1","address":"fghb","google_location":"fcghv","document":"13.jpg","legal_team_documents":null,"property_description":"fghvb","agent_id":"15","status":"0","ct_status":"2","mt_status":"3","pt_status":"2","lgt_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","lgt_update_dt":"","datetime":"2020-01-23 15:29:52"},{"id":"22","unique_id":"3WL20012322","user_mobile":"7286882455","village_name":"test","survey_no":"21s","property_name":"subagent lead","acres":"2","address":"Test","google_location":"Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull","document":"IMG_20200106_1049262.jpg","legal_team_documents":null,"property_description":null,"agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","lgt_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"23-01-2020 04:33:19","mt_update_dt":"23-01-2020 06:20:30","pt_update_dt":"","lgt_update_dt":"","datetime":"2020-01-23 15:30:04"},{"id":"23","unique_id":"3WL20012323","user_mobile":"7286882455","village_name":"fghj","survey_no":"FGHVBJ","property_name":"iujyhgHGJB","acres":"1","address":"fghb","google_location":"fcghv","document":"14.jpg","legal_team_documents":null,"property_description":"fghvb","agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","lgt_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"23-01-2020 05:38:29","mt_update_dt":"24-01-2020 10:45:55","pt_update_dt":"","lgt_update_dt":"","datetime":"2020-01-23 15:30:44"},{"id":"24","unique_id":"3WL20012324","user_mobile":"7286882455","village_name":"test","survey_no":"21s","property_name":"subagent lead","acres":"2","address":"Test","google_location":"Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull","document":"IMG_20200106_1049263.jpg","legal_team_documents":null,"property_description":null,"agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","lgt_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"24-01-2020 02:14:24","mt_update_dt":"24-01-2020 02:36:37","pt_update_dt":"","lgt_update_dt":"","datetime":"2020-01-23 15:30:53"},{"id":"25","unique_id":"3WL20012425","user_mobile":"7286882455","village_name":"rdfghjk","survey_no":"rtdfghj","property_name":"SubAgentChaitanya","acres":"fghjbnk","address":"fghvbj","google_location":"Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull","document":"IMG_20200106_1049264.jpg","legal_team_documents":null,"property_description":null,"agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","lgt_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"24-01-2020 02:45:11","mt_update_dt":"24-01-2020 02:47:21","pt_update_dt":"","lgt_update_dt":"","datetime":"2020-01-24 14:38:34"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : leads list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 21
         * unique_id : 3WL20012321
         * user_mobile : 7286882455
         * village_name : fghj
         * survey_no : FGHVBJ
         * property_name : iujyhgHGJB
         * acres : 1
         * address : fghb
         * google_location : fcghv
         * document : 13.jpg
         * legal_team_documents : null
         * property_description : fghvb
         * agent_id : 15
         * status : 0
         * ct_status : 2
         * mt_status : 3
         * pt_status : 2
         * lgt_status : 2
         * admin_status : 0
         * st_status : 2
         * st_update_dt :
         * ct_update_dt :
         * mt_update_dt :
         * pt_update_dt :
         * lgt_update_dt :
         * datetime : 2020-01-23 15:29:52
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String address;
        private String google_location;
        private String document;
        private Object legal_team_documents;
        private String property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String lgt_status;
        private String admin_status;
        private String st_status;
        private String st_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String lgt_update_dt;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGoogle_location() {
            return google_location;
        }

        public void setGoogle_location(String google_location) {
            this.google_location = google_location;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public Object getLegal_team_documents() {
            return legal_team_documents;
        }

        public void setLegal_team_documents(Object legal_team_documents) {
            this.legal_team_documents = legal_team_documents;
        }

        public String getProperty_description() {
            return property_description;
        }

        public void setProperty_description(String property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getLgt_status() {
            return lgt_status;
        }

        public void setLgt_status(String lgt_status) {
            this.lgt_status = lgt_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getSt_status() {
            return st_status;
        }

        public void setSt_status(String st_status) {
            this.st_status = st_status;
        }

        public String getSt_update_dt() {
            return st_update_dt;
        }

        public void setSt_update_dt(String st_update_dt) {
            this.st_update_dt = st_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getLgt_update_dt() {
            return lgt_update_dt;
        }

        public void setLgt_update_dt(String lgt_update_dt) {
            this.lgt_update_dt = lgt_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
