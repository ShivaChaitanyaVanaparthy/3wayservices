package com.igrand.ThreeWaySolutions.Response;

public class AdminAddVendors {


    /**
     * status : {"code":200,"message":"Admin added Vendors Successfully"}
     * data : {"business_name":"fghjk","city_name":"fghvbj","person_name":"fghbjn","user_mobile":"4567899876","email":"fghvbjn","description":"fghjbnkm","status":"1"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Vendors Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * business_name : fghjk
         * city_name : fghvbj
         * person_name : fghbjn
         * user_mobile : 4567899876
         * email : fghvbjn
         * description : fghjbnkm
         * status : 1
         */

        private String business_name;
        private String city_name;
        private String person_name;
        private String user_mobile;
        private String email;
        private String description;
        private String status;

        public String getBusiness_name() {
            return business_name;
        }

        public void setBusiness_name(String business_name) {
            this.business_name = business_name;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
