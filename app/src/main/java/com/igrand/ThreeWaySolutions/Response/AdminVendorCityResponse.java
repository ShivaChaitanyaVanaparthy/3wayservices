package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminVendorCityResponse {


    /**
     * status : {"code":200,"message":"List of Cities in Vendor Table"}
     * data : [{"id":"2","datetime":"2019-12-27 11:51:57","city_name":"Hyderabad"},{"id":"1","datetime":"2019-12-27 11:50:06","city_name":"Vijayawada"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Cities in Vendor Table
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 2
         * datetime : 2019-12-27 11:51:57
         * city_name : Hyderabad
         */

        private String id;
        private String datetime;
        private String city_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }
    }
}
