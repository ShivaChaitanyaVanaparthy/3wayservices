package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminInventoryMaterialResponse {


    /**
     * status : {"code":200,"message":"List of Material Types in Inventory Table"}
     * data : [{"id":"7","datetime":"2019-12-14 14:58:43","material_type":"water"},{"id":"6","datetime":"2019-12-13 17:07:57","material_type":"poiuytr"},{"id":"4","datetime":"2019-12-13 16:39:25","material_type":"Steel"},{"id":"3","datetime":"2019-12-13 16:24:40","material_type":"Sand"},{"id":"2","datetime":"2019-12-13 16:12:30","material_type":"iron"},{"id":"1","datetime":"2019-12-13 16:11:57","material_type":"iron"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Material Types in Inventory Table
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 7
         * datetime : 2019-12-14 14:58:43
         * material_type : water
         */

        private String id;
        private String datetime;
        private String material_type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getMaterial_type() {
            return material_type;
        }

        public void setMaterial_type(String material_type) {
            this.material_type = material_type;
        }
    }
}
