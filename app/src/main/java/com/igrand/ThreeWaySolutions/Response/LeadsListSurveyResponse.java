package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LeadsListSurveyResponse {


    /**
     * status : {"code":200,"message":"Leads Count"}
     * data : [{"id":"98","unique_id":"3WL20040698","user_mobile":"8688543727","district_name":"District","mandal_name":"Mandal","village_name":"Village","survey_no":"21","property_name":"Nam","acres":"2","address":"iuytfghvbn","latitude":"37.421998","longitude":"-122.084","document":"IMG3371210130297622133.jpg,IMG4024192141707376717.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"1","admin_status":"1","st_status":"1","st_update_dt":"06-04-2020 11:27:43","ct_update_dt":"06-04-2020 11:16:09","mt_update_dt":"06-04-2020 11:17:08","pt_update_dt":"06-04-2020 11:21:23","lgt_update_dt":"06-04-2020 11:24:40","ls_update_dt":"06-04-2020 11:32:56","datetime":"2020-04-06 10:47:59","ch_image":"","ad_image":"temp.jpg","ad_comments":"Hello"},{"id":"96","unique_id":"3WL20040396","user_mobile":"9135306477","district_name":"","mandal_name":"","village_name":"Yamjal","survey_no":"234,235,236,245,239,545,678,989,678,489","property_name":"Ibp","acres":"45","address":"Hyderabad","latitude":"17.3415055","longitude":"78.5421697","document":"IMG41576372688962483252.jpg,IMG41576372688962483253.jpg,IMG-20200325-WA00121.jpg,IMG-20200325-WA00191.jpg","legal_team_documents":null,"property_description":null,"agent_id":"82","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"1","st_status":"2","st_update_dt":"","ct_update_dt":"03-04-2020 04:18:43","mt_update_dt":"03-04-2020 05:15:01","pt_update_dt":"03-04-2020 05:46:42","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-04-03 15:30:49","ch_image":"","ad_image":"","ad_comments":""},{"id":"91","unique_id":"3WL20040291","user_mobile":"8688543727","district_name":"","mandal_name":"","village_name":"Begumpet","survey_no":"s987654","property_name":"Divya shakti property","acres":"7","address":"Gate1 , divya shakti apartments ","latitude":"17.4370591","longitude":"78.4514914","document":"IMG-20200402-WA01211.jpg,IMG-20200402-WA01221.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"1","st_status":"2","st_update_dt":"","ct_update_dt":"04-04-2020 02:03:45","mt_update_dt":"04-04-2020 02:04:27","pt_update_dt":"04-04-2020 02:05:43","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-04-02 17:27:20","ch_image":"","ad_image":"leaf.png","ad_comments":"shbdksadfd"},{"id":"28","unique_id":"3WL20022828","user_mobile":"8688543727","district_name":"","mandal_name":"","village_name":"Village","survey_no":"8765","property_name":"TestingLead","acres":"23","address":"Hyd","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"temp2.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"1","st_status":"2","st_update_dt":"","ct_update_dt":"28-02-2020 03:18:09","mt_update_dt":"28-02-2020 03:22:33","pt_update_dt":"28-02-2020 03:29:37","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-28 15:16:38","ch_image":"","ad_image":"","ad_comments":""},{"id":"27","unique_id":"3WL20022827","user_mobile":"8688543727","district_name":"","mandal_name":"","village_name":"Trst","survey_no":"6754fghnm","property_name":"Test","acres":"14","address":"Hyderabad","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20200227_1902012.jpg,IMG_20200227_190153.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"28-02-2020 02:52:17","mt_update_dt":"28-02-2020 03:11:43","pt_update_dt":"03-04-2020 06:16:29","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-28 14:50:07","ch_image":"","ad_image":"","ad_comments":""},{"id":"19","unique_id":"3WL20020419","user_mobile":"8688543727","district_name":"","mandal_name":"","village_name":"fghjb","survey_no":"fhgj","property_name":"RajeshLead","acres":"fghvbm","address":"fhg","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20200204_112551.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"04-02-2020 03:51:04","mt_update_dt":"04-02-2020 03:52:18","pt_update_dt":"03-04-2020 06:17:11","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-04 15:50:09","ch_image":"","ad_image":"","ad_comments":""},{"id":"9","unique_id":"3WL2002049","user_mobile":"7286882452","district_name":"","mandal_name":"","village_name":"Fnj","survey_no":"dhj","property_name":"Lead","acres":"fhj","address":"Zbn","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG-20200204-WA0001.jpg,IMG-20200204-WA0000.jpg,IMG-20200203-WA0000.jpg","legal_team_documents":null,"property_description":null,"agent_id":"42","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"2","admin_status":"0","st_status":"1","st_update_dt":"10-02-2020 11:03:15","ct_update_dt":"04-02-2020 03:58:33","mt_update_dt":"04-02-2020 05:14:35","pt_update_dt":"04-02-2020 05:15:43","lgt_update_dt":"10-02-2020 11:03:43","ls_update_dt":"","datetime":"2020-02-04 11:53:43","ch_image":"","ad_image":"","ad_comments":""},{"id":"7","unique_id":"3WL2002037","user_mobile":"7286882452","district_name":"","mandal_name":"","village_name":"ghvbj","survey_no":"gvhbjn","property_name":"LEAD","acres":"vghbnm","address":"ghj","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20191119_1249574.jpg","legal_team_documents":null,"property_description":null,"agent_id":"42","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"03-02-2020 04:21:48","mt_update_dt":"03-02-2020 04:22:43","pt_update_dt":"03-02-2020 04:24:02","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-03 16:19:54","ch_image":"","ad_image":"","ad_comments":""},{"id":"5","unique_id":"3WL2002035","user_mobile":"8789160492","district_name":"","mandal_name":"","village_name":"ewrtyui","survey_no":"tyuio","property_name":"wqertyu","acres":"ertyui","address":"vbnm","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20191119_1249572.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"04-02-2020 05:32:31","mt_update_dt":"04-02-2020 05:50:38","pt_update_dt":"05-02-2020 06:13:39","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-03 14:47:03","ch_image":"","ad_image":"","ad_comments":""},{"id":"4","unique_id":"3WL2002034","user_mobile":"7286882452","district_name":"","mandal_name":"","village_name":"VGHJBNM","survey_no":"ghj","property_name":"fghfj`","acres":"HBJN","address":"vhjb","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20191119_1249571.jpg","legal_team_documents":null,"property_description":null,"agent_id":"42","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"03-02-2020 03:21:42","mt_update_dt":"03-02-2020 03:05:19","pt_update_dt":"03-02-2020 03:06:48","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-03 14:35:02","ch_image":"","ad_image":"","ad_comments":""},{"id":"3","unique_id":"3WL2002033","user_mobile":"7286882452","district_name":"","mandal_name":"","village_name":"fghj","survey_no":"fghjb","property_name":"LEad","acres":"gfhvbj","address":"fghj","latitude":"37.421998333333335","longitude":"-122.08400000000002","document":"IMG_20191119_124957.jpg","legal_team_documents":null,"property_description":null,"agent_id":"42","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"0","st_status":"2","st_update_dt":"","ct_update_dt":"03-02-2020 01:33:54","mt_update_dt":"03-02-2020 01:34:47","pt_update_dt":"03-02-2020 01:36:33","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-03 13:31:33","ch_image":"","ad_image":"","ad_comments":""},{"id":"2","unique_id":"3WL2002032","user_mobile":"8688543727","district_name":"","mandal_name":"","village_name":"Madugula","survey_no":"12ra13","property_name":"Raju leads","acres":"25","address":"4153,raviechittu junction,madugula,Visakhapatnam,ap","latitude":"\r\n37.421998333333335","longitude":"-122.08400000000002","document":"Screenshot_2020-02-03-07-10-32-337_lockscreen.jpg,Screenshot_2020-02-03-07-10-32-337_lockscreen1.jpg","legal_team_documents":null,"property_description":null,"agent_id":"37","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"2","ls_status":"2","admin_status":"1","st_status":"2","st_update_dt":"","ct_update_dt":"03-02-2020 11:38:27","mt_update_dt":"03-02-2020 11:51:19","pt_update_dt":"03-02-2020 12:03:21","lgt_update_dt":"","ls_update_dt":"","datetime":"2020-02-03 11:34:09","ch_image":"","ad_image":"","ad_comments":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Leads Count
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 98
         * unique_id : 3WL20040698
         * user_mobile : 8688543727
         * district_name : District
         * mandal_name : Mandal
         * village_name : Village
         * survey_no : 21
         * property_name : Nam
         * acres : 2
         * address : iuytfghvbn
         * latitude : 37.421998
         * longitude : -122.084
         * document : IMG3371210130297622133.jpg,IMG4024192141707376717.jpg
         * legal_team_documents : null
         * property_description : null
         * agent_id : 37
         * status : 1
         * ct_status : 1
         * mt_status : 1
         * pt_status : 5
         * lgt_status : 1
         * ls_status : 1
         * admin_status : 1
         * st_status : 1
         * st_update_dt : 06-04-2020 11:27:43
         * ct_update_dt : 06-04-2020 11:16:09
         * mt_update_dt : 06-04-2020 11:17:08
         * pt_update_dt : 06-04-2020 11:21:23
         * lgt_update_dt : 06-04-2020 11:24:40
         * ls_update_dt : 06-04-2020 11:32:56
         * datetime : 2020-04-06 10:47:59
         * ch_image :
         * ad_image : temp.jpg
         * ad_comments : Hello
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String address;
        private String latitude;
        private String longitude;
        private String document;
        private Object legal_team_documents;
        private Object property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String lgt_status;
        private String ls_status;
        private String admin_status;
        private String st_status;
        private String st_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String lgt_update_dt;
        private String ls_update_dt;
        private String datetime;
        private String ch_image;
        private String ad_image;
        private String ad_comments;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public Object getLegal_team_documents() {
            return legal_team_documents;
        }

        public void setLegal_team_documents(Object legal_team_documents) {
            this.legal_team_documents = legal_team_documents;
        }

        public Object getProperty_description() {
            return property_description;
        }

        public void setProperty_description(Object property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getLgt_status() {
            return lgt_status;
        }

        public void setLgt_status(String lgt_status) {
            this.lgt_status = lgt_status;
        }

        public String getLs_status() {
            return ls_status;
        }

        public void setLs_status(String ls_status) {
            this.ls_status = ls_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getSt_status() {
            return st_status;
        }

        public void setSt_status(String st_status) {
            this.st_status = st_status;
        }

        public String getSt_update_dt() {
            return st_update_dt;
        }

        public void setSt_update_dt(String st_update_dt) {
            this.st_update_dt = st_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getLgt_update_dt() {
            return lgt_update_dt;
        }

        public void setLgt_update_dt(String lgt_update_dt) {
            this.lgt_update_dt = lgt_update_dt;
        }

        public String getLs_update_dt() {
            return ls_update_dt;
        }

        public void setLs_update_dt(String ls_update_dt) {
            this.ls_update_dt = ls_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getCh_image() {
            return ch_image;
        }

        public void setCh_image(String ch_image) {
            this.ch_image = ch_image;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getAd_comments() {
            return ad_comments;
        }

        public void setAd_comments(String ad_comments) {
            this.ad_comments = ad_comments;
        }
    }
}
