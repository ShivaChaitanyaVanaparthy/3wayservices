package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class ProcurementLeadsListAgentResponse {


    /**
     * status : {"code":200,"message":"Leads approved by Admin"}
     * data : [{"id":"25","unique_id":"3WL20012425","user_mobile":"7286882455","village_name":"rdfghjk","survey_no":"rtdfghj","property_name":"SubAgentChaitanya","acres":"fghjbnk","address":"fghvbj","google_location":"Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull","document":"IMG_20200106_1049264.jpg","legal_team_documents":null,"property_description":null,"agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"2","admin_status":"1","st_status":"1","st_update_dt":"26-01-2020 11:48:55","ct_update_dt":"24-01-2020 06:21:41","mt_update_dt":"24-01-2020 02:47:21","pt_update_dt":"25-01-2020 06:09:18","lgt_update_dt":"26-01-2020 10:41:42","ls_update_dt":"27-01-2020 10:49:22","datetime":"2020-01-24 14:38:34","proposal_notes":"zdxfsafdsafsdfsfsdsdfsdffsd","proposal_document":"hh9.jpg","pd_datetime":"2020-01-25 15:52:10"},{"id":"25","unique_id":"3WL20012425","user_mobile":"7286882455","village_name":"rdfghjk","survey_no":"rtdfghj","property_name":"SubAgentChaitanya","acres":"fghjbnk","address":"fghvbj","google_location":"Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull","document":"IMG_20200106_1049264.jpg","legal_team_documents":null,"property_description":null,"agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"2","admin_status":"1","st_status":"1","st_update_dt":"26-01-2020 11:48:55","ct_update_dt":"24-01-2020 06:21:41","mt_update_dt":"24-01-2020 02:47:21","pt_update_dt":"25-01-2020 06:09:18","lgt_update_dt":"26-01-2020 10:41:42","ls_update_dt":"27-01-2020 10:49:22","datetime":"2020-01-24 14:38:34","proposal_notes":"Hey","proposal_document":"1.jpg","pd_datetime":"2020-01-25 15:53:03"},{"id":"25","unique_id":"3WL20012425","user_mobile":"7286882455","village_name":"rdfghjk","survey_no":"rtdfghj","property_name":"SubAgentChaitanya","acres":"fghjbnk","address":"fghvbj","google_location":"Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull","document":"IMG_20200106_1049264.jpg","legal_team_documents":null,"property_description":null,"agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"2","admin_status":"1","st_status":"1","st_update_dt":"26-01-2020 11:48:55","ct_update_dt":"24-01-2020 06:21:41","mt_update_dt":"24-01-2020 02:47:21","pt_update_dt":"25-01-2020 06:09:18","lgt_update_dt":"26-01-2020 10:41:42","ls_update_dt":"27-01-2020 10:49:22","datetime":"2020-01-24 14:38:34","proposal_notes":"Hey","proposal_document":"17.jpg","pd_datetime":"2020-01-25 16:44:56"},{"id":"25","unique_id":"3WL20012425","user_mobile":"7286882455","village_name":"rdfghjk","survey_no":"rtdfghj","property_name":"SubAgentChaitanya","acres":"fghjbnk","address":"fghvbj","google_location":"Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull","document":"IMG_20200106_1049264.jpg","legal_team_documents":null,"property_description":null,"agent_id":"15","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","lgt_status":"1","ls_status":"2","admin_status":"1","st_status":"1","st_update_dt":"26-01-2020 11:48:55","ct_update_dt":"24-01-2020 06:21:41","mt_update_dt":"24-01-2020 02:47:21","pt_update_dt":"25-01-2020 06:09:18","lgt_update_dt":"26-01-2020 10:41:42","ls_update_dt":"27-01-2020 10:49:22","datetime":"2020-01-24 14:38:34","proposal_notes":"Name","proposal_document":"IMG_20200106_104926.jpg","pd_datetime":"2020-01-25 16:47:34"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Leads approved by Admin
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 25
         * unique_id : 3WL20012425
         * user_mobile : 7286882455
         * village_name : rdfghjk
         * survey_no : rtdfghj
         * property_name : SubAgentChaitanya
         * acres : fghjbnk
         * address : fghvbj
         * google_location : Jaggannapet, Telangana, IndiaJaggannapetTelanganaIndianull
         * document : IMG_20200106_1049264.jpg
         * legal_team_documents : null
         * property_description : null
         * agent_id : 15
         * status : 1
         * ct_status : 1
         * mt_status : 1
         * pt_status : 5
         * lgt_status : 1
         * ls_status : 2
         * admin_status : 1
         * st_status : 1
         * st_update_dt : 26-01-2020 11:48:55
         * ct_update_dt : 24-01-2020 06:21:41
         * mt_update_dt : 24-01-2020 02:47:21
         * pt_update_dt : 25-01-2020 06:09:18
         * lgt_update_dt : 26-01-2020 10:41:42
         * ls_update_dt : 27-01-2020 10:49:22
         * datetime : 2020-01-24 14:38:34
         * proposal_notes : zdxfsafdsafsdfsfsdsdfsdffsd
         * proposal_document : hh9.jpg
         * pd_datetime : 2020-01-25 15:52:10
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String address;
        private String google_location;
        private String document;
        private Object legal_team_documents;
        private Object property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String lgt_status;
        private String ls_status;
        private String admin_status;
        private String st_status;
        private String st_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String lgt_update_dt;
        private String ls_update_dt;
        private String datetime;
        private String proposal_notes;
        private String proposal_document;
        private String pd_datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGoogle_location() {
            return google_location;
        }

        public void setGoogle_location(String google_location) {
            this.google_location = google_location;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public Object getLegal_team_documents() {
            return legal_team_documents;
        }

        public void setLegal_team_documents(Object legal_team_documents) {
            this.legal_team_documents = legal_team_documents;
        }

        public Object getProperty_description() {
            return property_description;
        }

        public void setProperty_description(Object property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getLgt_status() {
            return lgt_status;
        }

        public void setLgt_status(String lgt_status) {
            this.lgt_status = lgt_status;
        }

        public String getLs_status() {
            return ls_status;
        }

        public void setLs_status(String ls_status) {
            this.ls_status = ls_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getSt_status() {
            return st_status;
        }

        public void setSt_status(String st_status) {
            this.st_status = st_status;
        }

        public String getSt_update_dt() {
            return st_update_dt;
        }

        public void setSt_update_dt(String st_update_dt) {
            this.st_update_dt = st_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getLgt_update_dt() {
            return lgt_update_dt;
        }

        public void setLgt_update_dt(String lgt_update_dt) {
            this.lgt_update_dt = lgt_update_dt;
        }

        public String getLs_update_dt() {
            return ls_update_dt;
        }

        public void setLs_update_dt(String ls_update_dt) {
            this.ls_update_dt = ls_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getProposal_notes() {
            return proposal_notes;
        }

        public void setProposal_notes(String proposal_notes) {
            this.proposal_notes = proposal_notes;
        }

        public String getProposal_document() {
            return proposal_document;
        }

        public void setProposal_document(String proposal_document) {
            this.proposal_document = proposal_document;
        }

        public String getPd_datetime() {
            return pd_datetime;
        }

        public void setPd_datetime(String pd_datetime) {
            this.pd_datetime = pd_datetime;
        }
    }
}
