package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class EngagerReportLIst {


    /**
     * status : {"code":200,"message":"Date wise Engagor report"}
     * data : [[{"id":"3","project_id":"3","engagor_id":"3","work_id":"1","subwork_id":"1","description":"Excavation","machine_id":"1","vehicle_no":"5236","start_time":"9:30","end_time":"12:00","date":"25/1/2003","project_name":"Miracle. One","work_name":"Road work","subwork_name":"road cutting","person_name":"Ramu","type_name":"JCB"}],[{"id":"4","project_id":"1","engagor_id":"1","work_id":"1","subwork_id":"1","description":"fghvbj","machine_id":"1","vehicle_no":"123","start_time":"12","end_time":"12","date":"123","project_name":"Tukkuguda new project","work_name":"Road work","subwork_name":"road cutting","person_name":"vinay","type_name":"JCB"}],[{"id":"2","project_id":"2","engagor_id":"3","work_id":"1","subwork_id":"1","description":"aS","machine_id":"2","vehicle_no":"sdfds","start_time":"08:45 AM","end_time":"09:45 AM","date":"02/26/2020","project_name":"Pankaj Project","work_name":"Road work","subwork_name":"road cutting","person_name":"Ramu","type_name":"Crane"}],[{"id":"5","project_id":"3","engagor_id":"3","work_id":"1","subwork_id":"2","description":"test","machine_id":"5","vehicle_no":"Ap29bn7744","start_time":"11:22","end_time":"3:22","date":"03/04/2020","project_name":"Miracle. One","work_name":"Road work","subwork_name":"SubWork","person_name":"Ramu","type_name":"Tractor"}],[{"id":"7","project_id":"3","engagor_id":"3","work_id":"3","subwork_id":"3","description":"","machine_id":"4","vehicle_no":"ihujg","start_time":"16:18","end_time":"5:18","date":"03/12/2020","project_name":"Miracle. One","work_name":"drainage work","subwork_name":"Sub-Drainage","person_name":"Ramu","type_name":"Lorry"}],[{"id":"6","project_id":"3","engagor_id":"3","work_id":"1","subwork_id":"2","description":"","machine_id":"5","vehicle_no":"5678","start_time":"5:16","end_time":"6:16","date":"03/18/2020","project_name":"Miracle. One","work_name":"Road work","subwork_name":"SubWork","person_name":"Ramu","type_name":"Tractor"}]]
     */

    private StatusBean status;
    private List<List<DataBean>> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Date wise Engagor report
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 3
         * project_id : 3
         * engagor_id : 3
         * work_id : 1
         * subwork_id : 1
         * description : Excavation
         * machine_id : 1
         * vehicle_no : 5236
         * start_time : 9:30
         * end_time : 12:00
         * date : 25/1/2003
         * project_name : Miracle. One
         * work_name : Road work
         * subwork_name : road cutting
         * person_name : Ramu
         * type_name : JCB
         */

        private String id;
        private String project_id;
        private String engagor_id;
        private String work_id;
        private String subwork_id;
        private String description;
        private String machine_id;
        private String vehicle_no;
        private String start_time;
        private String end_time;
        private String date;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String person_name;
        private String type_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getEngagor_id() {
            return engagor_id;
        }

        public void setEngagor_id(String engagor_id) {
            this.engagor_id = engagor_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMachine_id() {
            return machine_id;
        }

        public void setMachine_id(String machine_id) {
            this.machine_id = machine_id;
        }

        public String getVehicle_no() {
            return vehicle_no;
        }

        public void setVehicle_no(String vehicle_no) {
            this.vehicle_no = vehicle_no;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getType_name() {
            return type_name;
        }

        public void setType_name(String type_name) {
            this.type_name = type_name;
        }
    }
}
