package com.igrand.ThreeWaySolutions.Response;

public class ProcurementLeadResponse {


    /**
     * status : {"code":200,"message":"Lead Details"}
     * data : {"id":"31","unique_id":"3WL20061731","user_mobile":"9848174994","district_name":"77","mandal_name":"77","village_name":"77","survey_no":"77","property_name":"77","acres":"77","latitude":"17.34142516","longitude":"78.54228338","document":"309.pdf","images":"IMG524967271047366182.jpg,IMG-20200617-WA00132.jpg,IMG-20200615-WA00001.jpg","property_description":"77","agent_id":"4","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","tt_status":"1","admin_status":"0","tt_update_dt":"19-06-2020 06:11:55","ct_update_dt":"19-06-2020 05:28:42","mt_update_dt":"19-06-2020 06:01:50","pt_update_dt":"","datetime":"2020-06-17 21:04:49","ad_image":"","ad_comments":"","road_connectivity":"40","expected_sales_value":""}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 31
         * unique_id : 3WL20061731
         * user_mobile : 9848174994
         * district_name : 77
         * mandal_name : 77
         * village_name : 77
         * survey_no : 77
         * property_name : 77
         * acres : 77
         * latitude : 17.34142516
         * longitude : 78.54228338
         * document : 309.pdf
         * images : IMG524967271047366182.jpg,IMG-20200617-WA00132.jpg,IMG-20200615-WA00001.jpg
         * property_description : 77
         * agent_id : 4
         * status : 1
         * ct_status : 1
         * mt_status : 1
         * pt_status : 2
         * tt_status : 1
         * admin_status : 0
         * tt_update_dt : 19-06-2020 06:11:55
         * ct_update_dt : 19-06-2020 05:28:42
         * mt_update_dt : 19-06-2020 06:01:50
         * pt_update_dt :
         * datetime : 2020-06-17 21:04:49
         * ad_image :
         * ad_comments :
         * road_connectivity : 40
         * expected_sales_value :
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String latitude;
        private String longitude;
        private String document;
        private String images;
        private String property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String tt_status;
        private String admin_status;
        private String tt_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String datetime;
        private String ad_image;
        private String ad_comments;
        private String road_connectivity;
        private String expected_sales_value;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getProperty_description() {
            return property_description;
        }

        public void setProperty_description(String property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getTt_status() {
            return tt_status;
        }

        public void setTt_status(String tt_status) {
            this.tt_status = tt_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getTt_update_dt() {
            return tt_update_dt;
        }

        public void setTt_update_dt(String tt_update_dt) {
            this.tt_update_dt = tt_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getAd_comments() {
            return ad_comments;
        }

        public void setAd_comments(String ad_comments) {
            this.ad_comments = ad_comments;
        }

        public String getRoad_connectivity() {
            return road_connectivity;
        }

        public void setRoad_connectivity(String road_connectivity) {
            this.road_connectivity = road_connectivity;
        }

        public String getExpected_sales_value() {
            return expected_sales_value;
        }

        public void setExpected_sales_value(String expected_sales_value) {
            this.expected_sales_value = expected_sales_value;
        }
    }
}
