package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminInventoryProjectResponse {


    /**
     * status : {"code":200,"message":"List of Projects in Inventory Table"}
     * data : [{"id":"7","datetime":"2019-12-14 14:57:46","project_name":"TEST"},{"id":"6","datetime":"2019-12-14 14:55:37","project_name":"TEsting"},{"id":"5","datetime":"2019-12-13 17:05:22","project_name":"Test"},{"id":"4","datetime":"2019-12-13 17:05:10","project_name":"test"},{"id":"3","datetime":"2019-12-13 17:03:28","project_name":"fghjk"},{"id":"2","datetime":"2019-12-13 16:55:30","project_name":"fghj"},{"id":"1","datetime":"2019-12-13 16:47:46","project_name":"Project"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Projects in Inventory Table
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 7
         * datetime : 2019-12-14 14:57:46
         * project_name : TEST
         */

        private String id;
        private String datetime;
        private String project_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }
    }
}
