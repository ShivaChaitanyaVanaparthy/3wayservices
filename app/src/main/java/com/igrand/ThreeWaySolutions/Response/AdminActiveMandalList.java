package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminActiveMandalList {


    /**
     * status : {"code":200,"message":"District Wise Mandal"}
     * data : [{"id":"3","state_id":"2","district_id":"2","mandal_name":"Saroornagar","status":"1","datetime":"2020-04-18 16:06:46"},{"id":"4","state_id":"2","district_id":"2","mandal_name":"saroornagar1","status":"1","datetime":"2020-04-18 16:18:25"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : District Wise Mandal
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 3
         * state_id : 2
         * district_id : 2
         * mandal_name : Saroornagar
         * status : 1
         * datetime : 2020-04-18 16:06:46
         */

        private String id;
        private String state_id;
        private String district_id;
        private String mandal_name;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
