package com.igrand.ThreeWaySolutions.Response;

public class AddInvestmentResponse {


    /**
     * status : {"code":200,"message":"Admin added Investment Successfully"}
     * data : {"project_id":"1","investor_id":"11","invest_amount":"1","invest_date":"08/01/2020","mature_date":"09/01/2020","duration":"1","mature_amount":"10","status":"1","document":"1.jpg"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Investment Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * project_id : 1
         * investor_id : 11
         * invest_amount : 1
         * invest_date : 08/01/2020
         * mature_date : 09/01/2020
         * duration : 1
         * mature_amount : 10
         * status : 1
         * document : 1.jpg
         */

        private String project_id;
        private String investor_id;
        private String invest_amount;
        private String invest_date;
        private String mature_date;
        private String duration;
        private String mature_amount;
        private String status;
        private String document;

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getInvestor_id() {
            return investor_id;
        }

        public void setInvestor_id(String investor_id) {
            this.investor_id = investor_id;
        }

        public String getInvest_amount() {
            return invest_amount;
        }

        public void setInvest_amount(String invest_amount) {
            this.invest_amount = invest_amount;
        }

        public String getInvest_date() {
            return invest_date;
        }

        public void setInvest_date(String invest_date) {
            this.invest_date = invest_date;
        }

        public String getMature_date() {
            return mature_date;
        }

        public void setMature_date(String mature_date) {
            this.mature_date = mature_date;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getMature_amount() {
            return mature_amount;
        }

        public void setMature_amount(String mature_amount) {
            this.mature_amount = mature_amount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }
    }
}
