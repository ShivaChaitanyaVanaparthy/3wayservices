package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class BoqListResponse {


    /**
     * status : {"code":200,"message":"Boq List"}
     * data : [{"id":"15","project_id":"1","work_id":"1","subwork_id":"1","description":"","uom_id":"1","no":"1","length":"1","width":"1","depth":"1","quantity":"1","project_name":"Project","work_name":"Work","subwork_name":"SubWork","uom":"cm"},{"id":"5","project_id":"1","work_id":"1","subwork_id":"1","description":"jhhgf","uom_id":"1","no":"12","length":"100","width":"100","depth":"1","quantity":"2","project_name":"Project","work_name":"Work","subwork_name":"SubWork","uom":"cm"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Boq List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 15
         * project_id : 1
         * work_id : 1
         * subwork_id : 1
         * description :
         * uom_id : 1
         * no : 1
         * length : 1
         * width : 1
         * depth : 1
         * quantity : 1
         * project_name : Project
         * work_name : Work
         * subwork_name : SubWork
         * uom : cm
         */

        private String id;
        private String project_id;
        private String work_id;
        private String subwork_id;
        private String description;
        private String uom_id;
        private String no;
        private String length;
        private String width;
        private String depth;
        private String quantity;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String uom;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUom_id() {
            return uom_id;
        }

        public void setUom_id(String uom_id) {
            this.uom_id = uom_id;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }

        public String getLength() {
            return length;
        }

        public void setLength(String length) {
            this.length = length;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getDepth() {
            return depth;
        }

        public void setDepth(String depth) {
            this.depth = depth;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getUom() {
            return uom;
        }

        public void setUom(String uom) {
            this.uom = uom;
        }
    }
}
