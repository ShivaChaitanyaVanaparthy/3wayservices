package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminActiveStateList {


    /**
     * status : {"code":200,"message":"Active State List"}
     * data : [{"id":"1","state_name":"jharkhand","status":"1","datetime":"2020-04-17 16:35:27"},{"id":"2","state_name":"Telengana","status":"1","datetime":"2020-04-18 11:21:17"},{"id":"3","state_name":"Andhra Pradesh","status":"1","datetime":"2020-04-18 11:45:32"},{"id":"4","state_name":"Bihar","status":"1","datetime":"2020-04-18 12:07:43"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Active State List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * state_name : jharkhand
         * status : 1
         * datetime : 2020-04-17 16:35:27
         */

        private String id;
        private String state_name;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
