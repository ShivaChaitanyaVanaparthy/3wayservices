package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminInvestementResponse {


    /**
     * status : {"code":200,"message":"List of Investors in Investment Table"}
     * data : [{"id":"11","datetime":"2020-01-03 12:40:36","username":"Ilaya raju","mobile":"1111111111"},{"id":"12","datetime":"2020-01-03 12:42:19","username":"inumarthi","mobile":"2222222222"},{"id":"13","datetime":"2020-01-03 13:11:48","username":"Seshu","mobile":"3333333333"},{"id":"14","datetime":"2020-01-03 13:13:23","username":"Shankar","mobile":"4444444444"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Investors in Investment Table
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 11
         * datetime : 2020-01-03 12:40:36
         * username : Ilaya raju
         * mobile : 1111111111
         */

        private String id;
        private String datetime;
        private String username;
        private String mobile;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
