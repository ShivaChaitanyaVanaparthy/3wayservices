package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class NearByProjectResponse {


    /**
     * status : {"code":200,"message":"List of near by Project"}
     * data : [{"id":"23","lead_id":"100","user_id":"","project_name":"iyutrewt","acres":"iouytrew","plot_size":"ouiytre","fast_moving_plot_size":"ouiytre","cost":"iuytre","google_location":"1600 Amphitheatre Pkwy, Mountain View, CA 94043, USAMountain ViewCaliforniaUnited States94043","comment":"","brochure":"IMG3371210130297622133.jpg","image":"IMG3371210130297622133.jpg","status":"1","datetime":"2020-04-06 12:38:17"},{"id":"24","lead_id":"100","user_id":"","project_name":"9uytrett","acres":"ouiytrew","plot_size":"oiuytrew","fast_moving_plot_size":"uiytrew","cost":"iuytre","google_location":"1600 Amphitheatre Pkwy, Mountain View, CA 94043, USAMountain ViewCaliforniaUnited States94043","comment":"","brochure":"IMG33712101302976221331.jpg","image":"IMG33712101302976221331.jpg","status":"1","datetime":"2020-04-06 12:47:33"},{"id":"25","lead_id":"100","user_id":"","project_name":"TEST","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"","brochure":"1.jpg","image":"1.jpg","status":"1","datetime":"2020-04-06 13:08:06"},{"id":"26","lead_id":"100","user_id":"","project_name":"TEST","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"","brochure":"11.jpg","image":"11.jpg","status":"1","datetime":"2020-04-06 13:25:57"},{"id":"27","lead_id":"100","user_id":"","project_name":"TEST11","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"","brochure":"12.jpg","image":"12.jpg","status":"1","datetime":"2020-04-06 13:45:22"},{"id":"28","lead_id":"100","user_id":"","project_name":"TEST12","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"","brochure":"13.jpg","image":"13.jpg","status":"1","datetime":"2020-04-06 13:45:51"},{"id":"29","lead_id":"100","user_id":"","project_name":"TEST12","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"","brochure":"14.jpg","image":"14.jpg","status":"1","datetime":"2020-04-06 13:50:28"},{"id":"30","lead_id":"100","user_id":"","project_name":"TEST13","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"sdjkksabdkjsa","brochure":"15.jpg","image":"15.jpg","status":"1","datetime":"2020-04-06 13:51:16"},{"id":"31","lead_id":"100","user_id":"","project_name":"TEST14","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"","brochure":"16.jpg","image":"16.jpg","status":"1","datetime":"2020-04-06 13:55:46"},{"id":"32","lead_id":"100","user_id":"","project_name":"TEST15","acres":"187","plot_size":"jbhm","fast_moving_plot_size":"yguhj","cost":"tfgh","google_location":"fgh","comment":"iuyfg","brochure":"17.jpg","image":"17.jpg","status":"1","datetime":"2020-04-06 14:04:57"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of near by Project
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 23
         * lead_id : 100
         * user_id :
         * project_name : iyutrewt
         * acres : iouytrew
         * plot_size : ouiytre
         * fast_moving_plot_size : ouiytre
         * cost : iuytre
         * google_location : 1600 Amphitheatre Pkwy, Mountain View, CA 94043, USAMountain ViewCaliforniaUnited States94043
         * comment :
         * brochure : IMG3371210130297622133.jpg
         * image : IMG3371210130297622133.jpg
         * status : 1
         * datetime : 2020-04-06 12:38:17
         */

        private String id;
        private String lead_id;
        private String user_id;
        private String project_name;
        private String acres;
        private String plot_size;
        private String fast_moving_plot_size;
        private String cost;
        private String google_location;
        private String comment;
        private String brochure;
        private String image;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getPlot_size() {
            return plot_size;
        }

        public void setPlot_size(String plot_size) {
            this.plot_size = plot_size;
        }

        public String getFast_moving_plot_size() {
            return fast_moving_plot_size;
        }

        public void setFast_moving_plot_size(String fast_moving_plot_size) {
            this.fast_moving_plot_size = fast_moving_plot_size;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getGoogle_location() {
            return google_location;
        }

        public void setGoogle_location(String google_location) {
            this.google_location = google_location;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getBrochure() {
            return brochure;
        }

        public void setBrochure(String brochure) {
            this.brochure = brochure;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
