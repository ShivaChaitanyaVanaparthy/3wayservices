package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LegalDocResponse {


    /**
     * status : {"code":200,"message":"document name list"}
     * document_name_list : [{"id":"1","document_name":"doc 1","status":"1","datetime":"2020-01-21 13:19:46"},{"id":"2","document_name":"doc 2","status":"1","datetime":"2020-01-21 15:32:11"}]
     */

    private StatusBean status;
    private List<DocumentNameListBean> document_name_list;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DocumentNameListBean> getDocument_name_list() {
        return document_name_list;
    }

    public void setDocument_name_list(List<DocumentNameListBean> document_name_list) {
        this.document_name_list = document_name_list;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : document name list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DocumentNameListBean {
        /**
         * id : 1
         * document_name : doc 1
         * status : 1
         * datetime : 2020-01-21 13:19:46
         */

        private String id;
        private String document_name;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDocument_name() {
            return document_name;
        }

        public void setDocument_name(String document_name) {
            this.document_name = document_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
