package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminMandalList {


    /**
     * status : {"code":200,"message":"Mandal List"}
     * data : [{"id":"2","state_id":"1","district_id":"1","mandal_name":"Daru","status":"1","datetime":"2020-04-17 16:43:15","state_name":"jharkhand","district_name":"Hazaribag"},{"id":"1","state_id":"1","district_id":"1","mandal_name":"sadar","status":"1","datetime":"2020-04-17 16:43:02","state_name":"jharkhand","district_name":"Hazaribag"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Mandal List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 2
         * state_id : 1
         * district_id : 1
         * mandal_name : Daru
         * status : 1
         * datetime : 2020-04-17 16:43:15
         * state_name : jharkhand
         * district_name : Hazaribag
         */

        private String id;
        private String state_id;
        private String district_id;
        private String mandal_name;
        private String status;
        private String datetime;
        private String state_name;
        private String district_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }
    }
}
