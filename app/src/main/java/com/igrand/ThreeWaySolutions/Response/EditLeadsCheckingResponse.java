package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditLeadsCheckingResponse {


    /**
     * status : {"code":200,"message":"Lead Details For Update"}
     * data : [{"lead id":"5","checking team status":"1","checking team update time":"10-01-2020 04:42:13"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Lead Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        @SerializedName("lead id")
        private String _$LeadId194; // FIXME check this code
        @SerializedName("checking team status")
        private String _$CheckingTeamStatus267; // FIXME check this code
        @SerializedName("checking team update time")
        private String _$CheckingTeamUpdateTime18; // FIXME check this code

        public String get_$LeadId194() {
            return _$LeadId194;
        }

        public void set_$LeadId194(String _$LeadId194) {
            this._$LeadId194 = _$LeadId194;
        }

        public String get_$CheckingTeamStatus267() {
            return _$CheckingTeamStatus267;
        }

        public void set_$CheckingTeamStatus267(String _$CheckingTeamStatus267) {
            this._$CheckingTeamStatus267 = _$CheckingTeamStatus267;
        }

        public String get_$CheckingTeamUpdateTime18() {
            return _$CheckingTeamUpdateTime18;
        }

        public void set_$CheckingTeamUpdateTime18(String _$CheckingTeamUpdateTime18) {
            this._$CheckingTeamUpdateTime18 = _$CheckingTeamUpdateTime18;
        }
    }
}
