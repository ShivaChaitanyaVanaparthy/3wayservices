package com.igrand.ThreeWaySolutions.Response;

public class AdminValueResponse {


    /**
     * status : {"code":200,"message":"Admin added Material Type Successfully"}
     * data : {"material_type":"iron"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Material Type Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * material_type : iron
         */

        private String material_type;

        public String getMaterial_type() {
            return material_type;
        }

        public void setMaterial_type(String material_type) {
            this.material_type = material_type;
        }
    }
}
