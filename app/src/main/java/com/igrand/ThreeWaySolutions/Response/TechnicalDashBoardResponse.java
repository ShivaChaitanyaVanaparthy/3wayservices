package com.igrand.ThreeWaySolutions.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TechnicalDashBoardResponse {


    /**
     * status : {"code":200,"message":"Leads Count"}
     * data : [{"Total leads":60,"Rejected leads":0,"Approved leads":14,"Processing Leads":0,"Open Leads":46}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Leads Count
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        @SerializedName("Total leads")
        private int _$TotalLeads190; // FIXME check this code
        @SerializedName("Rejected leads")
        private int _$RejectedLeads46; // FIXME check this code
        @SerializedName("Approved leads")
        private int _$ApprovedLeads11; // FIXME check this code
        @SerializedName("Processing Leads")
        private int _$ProcessingLeads249; // FIXME check this code
        @SerializedName("Open Leads")
        private int _$OpenLeads32; // FIXME check this code

        public int get_$TotalLeads190() {
            return _$TotalLeads190;
        }

        public void set_$TotalLeads190(int _$TotalLeads190) {
            this._$TotalLeads190 = _$TotalLeads190;
        }

        public int get_$RejectedLeads46() {
            return _$RejectedLeads46;
        }

        public void set_$RejectedLeads46(int _$RejectedLeads46) {
            this._$RejectedLeads46 = _$RejectedLeads46;
        }

        public int get_$ApprovedLeads11() {
            return _$ApprovedLeads11;
        }

        public void set_$ApprovedLeads11(int _$ApprovedLeads11) {
            this._$ApprovedLeads11 = _$ApprovedLeads11;
        }

        public int get_$ProcessingLeads249() {
            return _$ProcessingLeads249;
        }

        public void set_$ProcessingLeads249(int _$ProcessingLeads249) {
            this._$ProcessingLeads249 = _$ProcessingLeads249;
        }

        public int get_$OpenLeads32() {
            return _$OpenLeads32;
        }

        public void set_$OpenLeads32(int _$OpenLeads32) {
            this._$OpenLeads32 = _$OpenLeads32;
        }
    }
}
