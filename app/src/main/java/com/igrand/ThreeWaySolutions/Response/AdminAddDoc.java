package com.igrand.ThreeWaySolutions.Response;

public class AdminAddDoc {


    /**
     * status : {"code":200,"message":"Admin added Legal Team Documents Successfully"}
     * data : {"document_name":"qwerty"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Legal Team Documents Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * document_name : qwerty
         */

        private String document_name;

        public String getDocument_name() {
            return document_name;
        }

        public void setDocument_name(String document_name) {
            this.document_name = document_name;
        }
    }
}
