package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminActiveVillageList {


    /**
     * status : {"code":200,"message":"Mandal Wise Village"}
     * data : [{"id":"1","state_id":"1","district_id":"1","mandal_id":"2","village_name":"Daru","status":"1","datetime":"2020-04-17 16:46:38"},{"id":"2","state_id":"2","district_id":"2","mandal_id":"2","village_name":"Meerpet","status":"1","datetime":"2020-04-18 16:49:41"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Mandal Wise Village
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * state_id : 1
         * district_id : 1
         * mandal_id : 2
         * village_name : Daru
         * status : 1
         * datetime : 2020-04-17 16:46:38
         */

        private String id;
        private String state_id;
        private String district_id;
        private String mandal_id;
        private String village_name;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getMandal_id() {
            return mandal_id;
        }

        public void setMandal_id(String mandal_id) {
            this.mandal_id = mandal_id;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
