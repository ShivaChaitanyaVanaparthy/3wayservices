package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class LeadsListCheckingResponse {


    /**
     * status : {"code":200,"message":"leads list"}
     * data : [{"id":"21","unique_id":"3WL20052721","user_mobile":"7286882452","district_name":"test","mandal_name":"test","village_name":"test","survey_no":"test","property_name":"test","acres":"test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG55100202948899998212.jpg,IMG65221776774516791632.jpg","property_description":"testttttt","agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-27 17:18:15","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"20","unique_id":"3WL20052720","user_mobile":"7286882452","district_name":"Test","mandal_name":"Test","village_name":"Test","survey_no":"Test","property_name":"TestLead","acres":"Test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG55100202948899998211.jpg,IMG65221776774516791631.jpg","property_description":"HI ITS agent ","agent_id":"5","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","tt_status":"1","admin_status":"0","tt_update_dt":"27-05-2020 04:46:23","ct_update_dt":"27-05-2020 03:46:37","mt_update_dt":"27-05-2020 04:13:00","pt_update_dt":"27-05-2020 04:52:14","datetime":"2020-05-27 15:13:28","ad_image":"","ad_comments":"","road_connectivity":"200","expected_sales_value":""},{"id":"19","unique_id":"3WL20052319","user_mobile":"9848174994","district_name":"22","mandal_name":"22","village_name":"22","survey_no":"22","property_name":"22","acres":"22","latitude":"17.34147708","longitude":"78.54218549","document":"014452_LT_SMD_FLT_U6_HMDA_270820183.pdf","images":"IMG29464195959652809201.jpg","property_description":null,"agent_id":"4","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"23-05-2020 12:50:48","mt_update_dt":"27-05-2020 03:45:53","pt_update_dt":"","datetime":"2020-05-23 12:24:01","ad_image":"","ad_comments":"","road_connectivity":"200","expected_sales_value":""},{"id":"18","unique_id":"3WL20052318","user_mobile":"9848174994","district_name":"11","mandal_name":"11","village_name":"11","survey_no":"11","property_name":"11","acres":"11","latitude":"17.34159177","longitude":"78.54169262","document":"014452_LT_SMD_FLT_U6_HMDA_270820182.pdf","images":"IMG2946419595965280920.jpg","property_description":null,"agent_id":"4","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","tt_status":"1","admin_status":"0","tt_update_dt":"23-05-2020 01:47:21","ct_update_dt":"23-05-2020 12:44:13","mt_update_dt":"23-05-2020 01:25:04","pt_update_dt":"","datetime":"2020-05-23 12:22:02","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"17","unique_id":"3WL20051917","user_mobile":"9848174994","district_name":"Asd","mandal_name":"Asd","village_name":"Asd","survey_no":"asd","property_name":"Asd","acres":"asd","latitude":"17.34141911","longitude":"78.54220801","document":"014452_LT_SMD_FLT_U6_HMDA_270820181.pdf","images":"IMG-20200510-WA00083.jpg,IMG-20200510-WA00053.jpg","property_description":null,"agent_id":"4","status":"1","ct_status":"1","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"19-05-2020 06:35:20","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-19 17:16:47","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"16","unique_id":"3WL20051916","user_mobile":"9848174994","district_name":"Test","mandal_name":"Test","village_name":"Test","survey_no":"468","property_name":"Test","acres":"45","latitude":"17.34147655","longitude":"78.54223115","document":"014452_LT_SMD_FLT_U6_HMDA_27082018.pdf,015856_SMD_LT_U6_HMDA_01102018.pdf","images":"IMG-20200510-WA00082.jpg,IMG-20200510-WA00052.jpg","property_description":null,"agent_id":"4","status":"1","ct_status":"","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"19-05-2020 05:14:01","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-19 14:18:16","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"15","unique_id":"3WL20051915","user_mobile":"9848174994","district_name":"Ranga Reddy","mandal_name":"Shankarpally","village_name":"Mokila","survey_no":"135","property_name":"3way","acres":"10","latitude":"17.34148384","longitude":"78.54219657","document":"017270_SMD_LT_U6_HMDA_14112018.pdf","images":"IMG-20200510-WA00081.jpg,IMG-20200510-WA00051.jpg","property_description":null,"agent_id":"4","status":"1","ct_status":"1","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"19-05-2020 02:31:09","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-19 14:13:12","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"14","unique_id":"3WL20051914","user_mobile":"7286882452","district_name":"Test","mandal_name":"TEst","village_name":"Test","survey_no":"Test","property_name":"Test","acres":"Test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan6.pdf","images":"IMG5510020294889999821.jpg,IMG6522177677451679163.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","tt_status":"1","admin_status":"0","tt_update_dt":"19-05-2020 01:40:19","ct_update_dt":"19-05-2020 01:25:41","mt_update_dt":"19-05-2020 01:26:11","pt_update_dt":"19-05-2020 01:48:28","datetime":"2020-05-19 13:21:06","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"13","unique_id":"3WL20051413","user_mobile":"7286882452","district_name":"fgh","mandal_name":"dfcg","village_name":"dfgvh","survey_no":"dfgh","property_name":"ttttt","acres":"h","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG_20200513_1101407.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"1","mt_status":"1","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"15-05-2020 08:56:30","mt_update_dt":"27-05-2020 09:41:01","pt_update_dt":"","datetime":"2020-05-14 23:47:15","ad_image":"","ad_comments":"","road_connectivity":"100","expected_sales_value":""},{"id":"12","unique_id":"3WL20051412","user_mobile":"7286882452","district_name":"rtfghj","mandal_name":"lkjh","village_name":"ftghj","survey_no":"rfghj","property_name":"rtfyghj","acres":"kjh","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan5.pdf","images":"IMG_20200513_1101406.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:46:39","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"11","unique_id":"3WL20051411","user_mobile":"7286882452","district_name":"fghj","mandal_name":"kljmnb","village_name":"fghvb","survey_no":"dfghvkjh","property_name":"Titile","acres":"kjh","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG_20200513_1101405.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:40:58","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"10","unique_id":"3WL20051410","user_mobile":"7286882452","district_name":"jhg","mandal_name":"fgh","village_name":"kjh","survey_no":"khj","property_name":"rtfgh","acres":"fgh","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG_20200513_1101404.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:26:24","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"9","unique_id":"3WL2005149","user_mobile":"7286882452","district_name":"kjlh","mandal_name":"fgh","village_name":"Klj","survey_no":"lkjh","property_name":"rfghjb","acres":"fghj","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"","images":"IMG_20200513_1101403.jpg,IMG_20200513_1101392.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 23:24:14","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"8","unique_id":"3WL2005148","user_mobile":"7286882452","district_name":"kjhgf","mandal_name":"iuhygtfdrf","village_name":"oiuytr","survey_no":"kjhgf","property_name":"TEsttty","acres":"jkhgf","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan4.pdf","images":"IMG_20200513_1101401.jpg,IMG_20200513_1101391.jpg,IMG_20200513_1101361.jpg,IMG_20200513_1101402.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 22:13:45","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"7","unique_id":"3WL2005147","user_mobile":"7286882452","district_name":"iouytr","mandal_name":"ertyui","village_name":"oiuytr","survey_no":"uyt","property_name":"Image/Pdf Test","acres":"tyui","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan3.pdf,NN7812251491030.ETicket1.pdf","images":"IMG_20200513_110140.jpg,IMG_20200513_110139.jpg,IMG_20200513_110136.jpg","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 21:59:37","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"6","unique_id":"3WL2005146","user_mobile":"7286882452","district_name":"wert","mandal_name":"wqert","village_name":"qwerty","survey_no":"wer","property_name":"Testing","acres":"werty","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan1.pdf,46.5_x_30_East_face_Plan2.pdf","images":"","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-14 21:43:53","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"5","unique_id":"3WL2005135","user_mobile":"7286882452","district_name":"hyd","mandal_name":"hyd","village_name":"oiuytr","survey_no":"981","property_name":"oiuy","acres":"oiuy","latitude":"87654","longitude":"76854","document":"NN7812251491030.ETicket.pdf","images":"","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 14:41:25","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"4","unique_id":"3WL2005134","user_mobile":"7286882452","district_name":"hyd","mandal_name":"hyd","village_name":"oiuytr","survey_no":"981","property_name":"oiuy","acres":"oiuy","latitude":"87654","longitude":"76854","document":"","images":"","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 14:38:42","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"3","unique_id":"3WL2005133","user_mobile":"7286882452","district_name":"Test","mandal_name":"Test","village_name":"Test","survey_no":"Test","property_name":"Test","acres":"Test","latitude":"17.312548333333332","longitude":"78.53632833333333","document":"46.5_x_30_East_face_Plan_(1).pdf","images":"","property_description":null,"agent_id":"5","status":"1","ct_status":"2","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-13 10:27:50","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"2","unique_id":"3WL2005122","user_mobile":"9848174994","district_name":"Medchel","mandal_name":"Bibinagar","village_name":"Kondamadugu","survey_no":"165","property_name":"3way","acres":"4","latitude":"17.3414983","longitude":"78.5423097","document":"46.5_x_30_East_face_Plan.pdf","images":"","property_description":null,"agent_id":"4","status":"1","ct_status":"1","mt_status":"3","pt_status":"2","tt_status":"2","admin_status":"0","tt_update_dt":"","ct_update_dt":"19-05-2020 02:34:29","mt_update_dt":"","pt_update_dt":"","datetime":"2020-05-12 19:34:47","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""},{"id":"1","unique_id":"3WL2005121","user_mobile":"9848174994","district_name":"Ranga reddy","mandal_name":"Ghatkesar","village_name":"Korremula","survey_no":"423,424,425","property_name":"3way","acres":"12","latitude":"17.3414973","longitude":"78.5423043","document":"IMG-20200510-WA0008.jpg,IMG-20200510-WA0005.jpg","images":"","property_description":"hgshGS","agent_id":"4","status":"1","ct_status":"1","mt_status":"1","pt_status":"5","tt_status":"1","admin_status":"0","tt_update_dt":"18-05-2020 04:44:16","ct_update_dt":"","mt_update_dt":"18-05-2020 04:32:05","pt_update_dt":"18-05-2020 04:50:35","datetime":"2020-05-12 19:24:14","ad_image":"","ad_comments":"","road_connectivity":"","expected_sales_value":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : leads list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 21
         * unique_id : 3WL20052721
         * user_mobile : 7286882452
         * district_name : test
         * mandal_name : test
         * village_name : test
         * survey_no : test
         * property_name : test
         * acres : test
         * latitude : 17.312548333333332
         * longitude : 78.53632833333333
         * document :
         * images : IMG55100202948899998212.jpg,IMG65221776774516791632.jpg
         * property_description : testttttt
         * agent_id : 5
         * status : 1
         * ct_status : 2
         * mt_status : 3
         * pt_status : 2
         * tt_status : 2
         * admin_status : 0
         * tt_update_dt :
         * ct_update_dt :
         * mt_update_dt :
         * pt_update_dt :
         * datetime : 2020-05-27 17:18:15
         * ad_image :
         * ad_comments :
         * road_connectivity :
         * expected_sales_value :
         */

        private String id;
        private String unique_id;
        private String user_mobile;
        private String district_name;
        private String mandal_name;
        private String village_name;
        private String survey_no;
        private String property_name;
        private String acres;
        private String latitude;
        private String longitude;
        private String document;
        private String images;
        private String property_description;
        private String agent_id;
        private String status;
        private String ct_status;
        private String mt_status;
        private String pt_status;
        private String tt_status;
        private String admin_status;
        private String tt_update_dt;
        private String ct_update_dt;
        private String mt_update_dt;
        private String pt_update_dt;
        private String datetime;
        private String ad_image;
        private String ad_comments;
        private String road_connectivity;
        private String expected_sales_value;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getMandal_name() {
            return mandal_name;
        }

        public void setMandal_name(String mandal_name) {
            this.mandal_name = mandal_name;
        }

        public String getVillage_name() {
            return village_name;
        }

        public void setVillage_name(String village_name) {
            this.village_name = village_name;
        }

        public String getSurvey_no() {
            return survey_no;
        }

        public void setSurvey_no(String survey_no) {
            this.survey_no = survey_no;
        }

        public String getProperty_name() {
            return property_name;
        }

        public void setProperty_name(String property_name) {
            this.property_name = property_name;
        }

        public String getAcres() {
            return acres;
        }

        public void setAcres(String acres) {
            this.acres = acres;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getProperty_description() {
            return property_description;
        }

        public void setProperty_description(String property_description) {
            this.property_description = property_description;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCt_status() {
            return ct_status;
        }

        public void setCt_status(String ct_status) {
            this.ct_status = ct_status;
        }

        public String getMt_status() {
            return mt_status;
        }

        public void setMt_status(String mt_status) {
            this.mt_status = mt_status;
        }

        public String getPt_status() {
            return pt_status;
        }

        public void setPt_status(String pt_status) {
            this.pt_status = pt_status;
        }

        public String getTt_status() {
            return tt_status;
        }

        public void setTt_status(String tt_status) {
            this.tt_status = tt_status;
        }

        public String getAdmin_status() {
            return admin_status;
        }

        public void setAdmin_status(String admin_status) {
            this.admin_status = admin_status;
        }

        public String getTt_update_dt() {
            return tt_update_dt;
        }

        public void setTt_update_dt(String tt_update_dt) {
            this.tt_update_dt = tt_update_dt;
        }

        public String getCt_update_dt() {
            return ct_update_dt;
        }

        public void setCt_update_dt(String ct_update_dt) {
            this.ct_update_dt = ct_update_dt;
        }

        public String getMt_update_dt() {
            return mt_update_dt;
        }

        public void setMt_update_dt(String mt_update_dt) {
            this.mt_update_dt = mt_update_dt;
        }

        public String getPt_update_dt() {
            return pt_update_dt;
        }

        public void setPt_update_dt(String pt_update_dt) {
            this.pt_update_dt = pt_update_dt;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getAd_comments() {
            return ad_comments;
        }

        public void setAd_comments(String ad_comments) {
            this.ad_comments = ad_comments;
        }

        public String getRoad_connectivity() {
            return road_connectivity;
        }

        public void setRoad_connectivity(String road_connectivity) {
            this.road_connectivity = road_connectivity;
        }

        public String getExpected_sales_value() {
            return expected_sales_value;
        }

        public void setExpected_sales_value(String expected_sales_value) {
            this.expected_sales_value = expected_sales_value;
        }
    }
}
