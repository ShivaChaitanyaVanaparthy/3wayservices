package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class ProfileSubAgentResponse {


    /**
     * status : {"code":200,"message":"My Profile"}
     * data : [{"profile":"http://igrandit.site/3way-services/admin_assets/uploads/users/IMG_20200106_10492620.jpg","email":"chaitanya@gmail.com","username":"chaitanya"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : My Profile
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * profile : http://igrandit.site/3way-services/admin_assets/uploads/users/IMG_20200106_10492620.jpg
         * email : chaitanya@gmail.com
         * username : chaitanya
         */

        private String profile;
        private String email;
        private String username;

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
