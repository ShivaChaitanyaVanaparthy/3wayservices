package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class CommentResponse {


    /**
     * status : {"code":200,"message":"All comments of Leads"}
     * data : [{"id":"25","comments":"7777","user_mobile":"8210402087","lead_id":"31","status":"1","datetime":"2020-06-19 17:28:42","usertype":"checking_team","username":"Pankaj Narayan"},{"id":"27","comments":"Good","user_mobile":"9848174994","lead_id":"31","status":"1","datetime":"2020-06-19 18:01:50","usertype":"agent","username":"Sravan"},{"id":"28","comments":"good","user_mobile":"1234567890","lead_id":"31","status":"1","datetime":"2020-06-19 18:11:55","usertype":"techenical_team","username":"Technical Team"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : All comments of Leads
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 25
         * comments : 7777
         * user_mobile : 8210402087
         * lead_id : 31
         * status : 1
         * datetime : 2020-06-19 17:28:42
         * usertype : checking_team
         * username : Pankaj Narayan
         */

        private String id;
        private String comments;
        private String user_mobile;
        private String lead_id;
        private String status;
        private String datetime;
        private String usertype;
        private String username;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
