package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class AdminWorkReportList {


    /**
     * status : {"code":200,"message":"List of daily work report"}
     * data : [{"id":"2","project_id":"3","contractor_id":"7","work_id":"5","subwork_id":"5","work_description":"test","skilled":"100","male_no":"100","female_no":"100","transport_charge":"100","date":"09/07/2020","project_name":"test3","work_name":"work","subwork_name":"subework","person_name":"test"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of daily work report
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 2
         * project_id : 3
         * contractor_id : 7
         * work_id : 5
         * subwork_id : 5
         * work_description : test
         * skilled : 100
         * male_no : 100
         * female_no : 100
         * transport_charge : 100
         * date : 09/07/2020
         * project_name : test3
         * work_name : work
         * subwork_name : subework
         * person_name : test
         */

        private String id;
        private String project_id;
        private String contractor_id;
        private String work_id;
        private String subwork_id;
        private String work_description;
        private String skilled;
        private String male_no;
        private String female_no;
        private String transport_charge;
        private String date;
        private String project_name;
        private String work_name;
        private String subwork_name;
        private String person_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getContractor_id() {
            return contractor_id;
        }

        public void setContractor_id(String contractor_id) {
            this.contractor_id = contractor_id;
        }

        public String getWork_id() {
            return work_id;
        }

        public void setWork_id(String work_id) {
            this.work_id = work_id;
        }

        public String getSubwork_id() {
            return subwork_id;
        }

        public void setSubwork_id(String subwork_id) {
            this.subwork_id = subwork_id;
        }

        public String getWork_description() {
            return work_description;
        }

        public void setWork_description(String work_description) {
            this.work_description = work_description;
        }

        public String getSkilled() {
            return skilled;
        }

        public void setSkilled(String skilled) {
            this.skilled = skilled;
        }

        public String getMale_no() {
            return male_no;
        }

        public void setMale_no(String male_no) {
            this.male_no = male_no;
        }

        public String getFemale_no() {
            return female_no;
        }

        public void setFemale_no(String female_no) {
            this.female_no = female_no;
        }

        public String getTransport_charge() {
            return transport_charge;
        }

        public void setTransport_charge(String transport_charge) {
            this.transport_charge = transport_charge;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getWork_name() {
            return work_name;
        }

        public void setWork_name(String work_name) {
            this.work_name = work_name;
        }

        public String getSubwork_name() {
            return subwork_name;
        }

        public void setSubwork_name(String subwork_name) {
            this.subwork_name = subwork_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }
    }
}
