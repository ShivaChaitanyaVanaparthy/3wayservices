package com.igrand.ThreeWaySolutions.Response;

public class AdditinalInfoResponse {


    /**
     * status : {"code":200,"message":"Additional Information"}
     * additional_info : {"id":"1","lead_id":"2","road_connectivity":"100","expected_purchase_value":"100","expected_sales_value":"29","status":"1","datetime":"2020-01-29 13:05:01"}
     */

    private StatusBean status;
    private AdditionalInfoBean additional_info;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public AdditionalInfoBean getAdditional_info() {
        return additional_info;
    }

    public void setAdditional_info(AdditionalInfoBean additional_info) {
        this.additional_info = additional_info;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Additional Information
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class AdditionalInfoBean {
        /**
         * id : 1
         * lead_id : 2
         * road_connectivity : 100
         * expected_purchase_value : 100
         * expected_sales_value : 29
         * status : 1
         * datetime : 2020-01-29 13:05:01
         */

        private String id;
        private String lead_id;
        private String road_connectivity;
        private String expected_purchase_value;
        private String expected_sales_value;
        private String status;
        private String datetime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getRoad_connectivity() {
            return road_connectivity;
        }

        public void setRoad_connectivity(String road_connectivity) {
            this.road_connectivity = road_connectivity;
        }

        public String getExpected_purchase_value() {
            return expected_purchase_value;
        }

        public void setExpected_purchase_value(String expected_purchase_value) {
            this.expected_purchase_value = expected_purchase_value;
        }

        public String getExpected_sales_value() {
            return expected_sales_value;
        }

        public void setExpected_sales_value(String expected_sales_value) {
            this.expected_sales_value = expected_sales_value;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
