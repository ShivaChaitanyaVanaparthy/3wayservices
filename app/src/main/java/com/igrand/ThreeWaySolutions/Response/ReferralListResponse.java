package com.igrand.ThreeWaySolutions.Response;

import java.util.List;

public class ReferralListResponse {


    /**
     * status : {"code":200,"message":"My Total Referal List"}
     * data : [{"id":"1","earn_id":"1","user_mobile":"7286882452","referal_phone_no":"1234567890","referal_name":"test","username":"Chaitanya","title":"Title"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : My Total Referal List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * earn_id : 1
         * user_mobile : 7286882452
         * referal_phone_no : 1234567890
         * referal_name : test
         * username : Chaitanya
         * title : Title
         */

        private String id;
        private String earn_id;
        private String user_mobile;
        private String referal_phone_no;
        private String referal_name;
        private String username;
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEarn_id() {
            return earn_id;
        }

        public void setEarn_id(String earn_id) {
            this.earn_id = earn_id;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getReferal_phone_no() {
            return referal_phone_no;
        }

        public void setReferal_phone_no(String referal_phone_no) {
            this.referal_phone_no = referal_phone_no;
        }

        public String getReferal_name() {
            return referal_name;
        }

        public void setReferal_name(String referal_name) {
            this.referal_name = referal_name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
