package com.igrand.ThreeWaySolutions.Response;

public class AdminProjectCost {


    /**
     * status : {"code":200,"message":"Total estimation cost of project"}
     * data : 9976
     */

    private StatusBean status;
    private int data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Total estimation cost of project
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
