package com.igrand.ThreeWaySolutions.Interface;

import com.igrand.ThreeWaySolutions.Response.*;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("login")
    Call<LoginAdminResponse>adminLogin(@Field("mobile") String mobile, @Field("password") String password);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("lead-details-in-techenical-team")
    Call<ProcurementLeadResponse>LeadDetailTech(@Field("id") String id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("lead-details-in-procurement-team")
    Call<ProcurementLeadResponse>LeadDetailProc(@Field("lead_id") String lead_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-and-type-wise-vendor-list")
    Call<VendorListResponse>adminVendorList(@Field("project_id") String project_id,
                                            @Field("type_id") String type_id);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("comment-by-lead-id-checkingteam")
    Call<CommentResponse>comments(@Field("lead_id") String lead_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("forget-password")
    Call<AdminForgetPassword>adminForgetPassword(@Field("mobile") String mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("players-details")
    Call<PlayersDetailsResponse>adminPlayersListDetail(@Field("player_id") String player_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("state-wise-players")
    Call<PlayersListResponse>adminPlayersFilterState(@Field("state_id") String state_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("mandal-wise-players")
    Call<PlayersListResponse>adminPlayersFilterMandal(@Field("mandal_id") String mandal_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("district-wise-players")
    Call<PlayersListResponse>adminPlayersFilterDst(@Field("district_id") String district_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("village-wise-players")
    Call<PlayersListResponse>adminPlayersFilterVillage(@Field("village_id") String village_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("daily-work-report-list")
    Call<AdminWorkReportList>adminWorkReportList(@Field("project_id") String project_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-and-vendor-wise-daily-work-report-list")
    Call<AdminWorkReportList>vendorwiseWork(@Field("project_id") String project_id,
                                            @Field("vendor_id") String vendor_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-and-vendor-wise-engagor-report-list")
    Call<AdminEngagerList>vendorwiseEngager(@Field("project_id") String project_id,
                                            @Field("vendor_id") String vendor_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-and-vendor-wise-supplier-report-list")
    Call<AdminMachineryList>vendorwiseSupplier(@Field("project_id") String project_id,
                                            @Field("vendor_id") String vendor_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-and-vendor-wise-msheet-report-list")
    Call<MeasurementSheetResponse>vendorwiseMeasurement(@Field("project_id") String project_id,
                                            @Field("vendor_id") String vendor_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-and-vendor-wise-muse-list")
    Call<MaterialListResponse>vendorwiseMaterial(@Field("project_id") String project_id,
                                            @Field("vendor_id") String vendor_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("boq-list")
    Call<BoqListResponse>adminBoqList(@Field("project_id") String project_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("msheet-list")
    Call<MeasurementSheetResponse>measurementList(@Field("project_id") String project_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("muse-list")
    Call<MaterialListResponse>materialList(@Field("project_id") String project_id);

   @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("dpr-report-of-project-by-date")
    Call<AdminWorkReportList>adminWorkReportList1(@Field("project_id") String project_id,
                                                  @Field("date") String date);
   @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("dpr-report-by-date")
    Call<AdminWorkReportList>adminWorkReportList2(@Field("project_id") String project_id,
                                                  @Field("date") String date);



   @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("muse-report-by-date")
    Call<MaterialListResponse>adminWorkReportList3(@Field("project_id") String project_id,
                                                  @Field("date") String date);

   @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("msheet-report-by-date ")
    Call<MeasurementSheetResponse>adminWorkReportList4(@Field("project_id") String project_id,
                                                  @Field("date") String date);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("engagor-report-list")
    Call<AdminEngagerList>adminEngagerReportList(@Field("project_id") String project_id);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("engagor-report-of-project-by-date")
    Call<AdminEngagerList>adminEngagerReportList1(@Field("project_id") String project_id,
                                                 @Field("date") String date);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("supplier-report-list")
    Call<AdminMachineryList>adminMachinertReportList(@Field("project_id") String project_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("supplier-report-of-project-by-date")
    Call<SupplierReportResponse>adminMachinertReportList1(@Field("project_id") String project_id,
                                                          @Field("date") String date);
    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("msheet-report-by-date")
    Call<MeasurementListResponse>adminMeasurementReportList(@Field("project_id") String project_id,
                                                          @Field("date") String date);
    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("muse-report-by-date")
    Call<MaterialListResponse>adminMaterialReportList(@Field("project_id") String project_id,
                                                          @Field("date") String date);



@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-total-estimation-cost")
    Call<AdminProjectCost>adminProjectCost(@Field("project_id") String project_id);


  @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("subwork-by-workid")
    Call<AdminSubWorkTypeListbyId>adminFsubworkbyid(@Field("work_id") String work_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("agent-profile")
    Call<ProfileAgentResponse>adminProfile(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("techenical-team-my-profile")
    Call<ProfileAgentResponse>techProfile(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("siteengineer-team-my-profile")
    Call<ProfileAgentResponse>siteEngineerProfile(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("admin-profile")
    Call<ProfileAgentResponse>agentProfile(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("approved-leads-for-agent")
    Call<UpdateLeadResponse>adminupdates(@Field("lead_id") String lead_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-earn")
    Call<AddEarnResponse>addEarn(@Field("title") String title,
    @Field("description") String description,
    @Field("status") String status);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-referal")
    Call<AddReferralAgentResponse>addReferral(@Field("earn_id") String earn_id,
    @Field("user_mobile") String user_mobile,
    @Field("referal_name") String referal_name,
    @Field("referal_phone_no") String referal_phone_no);



@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("aditional-info-by-lead-id")
    Call<AdditinalInfoResponse>additinalinfo(@Field("lead_id") String lead_id);


@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("porposal-document-by-lead-id")
    Call<ProposalDocumentsResponse>proposaldoc(@Field("lead_id") String lead_id);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("leads_list_of_subagent")
    Call<UserDetailsAgentResponse>adminLeadsAgentDetails(@Field("user_mobile") String user_mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("checking-team-profile")
    Call<ProfileAgentResponse>profileChecking(@Field("mobile") String mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("procurement-team-notification-count")
    Call<NotificationCountResponse>notificationcountprocurement(@Field("mobile") String mobile);


@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("agent-notification-list")
    Call<NotificationsAgentListResponse>notificationListAgent(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("techenical-team-notification")
    Call<NotificationsAgentListResponse>notificationListTech(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("siteengineer-team-notification ")
    Call<NotificationsAgentListResponse>notificationListSiteEngineer(@Field("mobile") String mobile);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("checking-team-notification-list")
    Call<NotificationsAgentListResponse>notificationListChecking(@Field("mobile") String mobile);
@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("lesion-team-notification-list")
    Call<NotificationsAgentListResponse>notificationListLesion(@Field("mobile") String mobile);
@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("survey-team-notification-list")
    Call<NotificationsAgentListResponse>notificationListSurvey(@Field("mobile") String mobile);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("legal-team-notification-list")
    Call<NotificationsAgentListResponse>notificationListLegal(@Field("mobile") String mobile);
@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("marketing-team-notification-list")
    Call<NotificationsAgentListResponse>notificationListMarketing(@Field("mobile") String mobile);


@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("legal-team-profile")
    Call<ProfileLegalResponse>profileLegal(@Field("mobile") String mobile);


@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("leads-approved-by-admin")
    Call<ProcurementLeadsListAgentResponse>agentProcurementLeadsList(@Field("agent_id") String agent_id);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("lesion-team-profile")
    Call<ProfileLesionResponse>profileLesion(@Field("mobile") String mobile);


@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("checking-team-notification-count")
    Call<NotificationCountResponse>notificationcountchecking(@Field("mobile") String mobile);
@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("lesion-team-notification-count")
    Call<NotificationCountResponse>notificationcountlesion(@Field("mobile") String mobile);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("survey-team-notification-count")
    Call<NotificationCountResponse>notificationcountsurvey(@Field("mobile") String mobile);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("legal-team-notification-count")
    Call<NotificationCountResponse>notificationcountlegal(@Field("mobile") String mobile);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("marketing-team-notification-count")
    Call<NotificationCountResponse>notificationcountmarketing(@Field("mobile") String mobile);


@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("marketing-team-notification-list")
    Call<NotificationsAgentListResponse>notificationlistmarketing(@Field("mobile") String mobile);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("procurement-team-notification-list")
    Call<NotificationsAgentListResponse>notificationlistprocurement(@Field("mobile") String mobile);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("survey-team-profile")
    Call<ProfileSurveyResponse>profileSurvey(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("list_of_near_by_project")
    Call<NearByProjectResponse>nearByProjectList(@Field("lead_id") String lead_id);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("active")
    Call<AgentUpdateSubAgentLeadResponse>agentupdatelead(@Field("lead_id") String lead_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("techenical-team-documelt-list-by-leadid")
    Call<TechDocResponse>TechList(@Field("lead_id") String lead_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("marketing-team-documelt-list-by-leadid")
    Call<TechDocResponse>MarkList(@Field("lead_id") String lead_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("checking-team-documelt-list-by-leadid")
    Call<TechDocResponse>CheckList(@Field("lead_id") String lead_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("porposal-document-by-lead-id")
    Call<ProcDocumentResponse>ProcList(@Field("lead_id") String lead_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("deactive")
    Call<AgentUpdateSubAgentRejectLeadResponse>agentupdateleadreject(@Field("lead_id") String lead_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-projects")
    Call<AdminAddProjects>adminAddProjects(@Field("mobile") String mobile,
                                           @Field("project_name") String project_name,
                                           @Field("village_name") String village_name,
                                           @Field("status") String status);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("state-wise-district-list")
    Call<AdminActiveDistList>admindstActiveList(@Field("state_id") String state_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("district-wise-mandal-list")
    Call<AdminActiveMandalList>adminmandalActiveList(@Field("district_id") String district_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("mandal-wise-village-list")
    Call<AdminActiveVillageList>adminvillageActiveList(@Field("mandal_id") String mandal_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("store-notification")
    Call<NotificationsAdminResponse>addnotification(@Field("notification_for[]") List<String> notification_for,
                                                    @Field("notification") String notification);
    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-material-type")
    Call<AdminValueResponse>adminAddValues(@Field("mobile") String mobile,
                                           @Field("material_type") String material_type);





    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("update-lead-in-techenical-team")
    Call<UpdateLeadsTechResponse>techUpdateLead(@Field("lead_id") String lead_id,
                                                    @Field("tt_status") String tt_status,
                                                    @Field("comments") String comments,
                                                    @Field("user_mobile") String user_mobile);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("comment-by-lead-id")
    Call<AgentLeadsCommentsResponse>agentLeadsComments(@Field("lead_id") String lead_id);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-wise-vendor-add")
    Call<AdminAddVendors>adminAddVendors(@Field("person_name") String person_name,
                                         @Field("project_id") String project_id,
                                         @Field("pan_no") String pan_no,
                                         @Field("work_id") String work_id,
                                         @Field("subwork_id") String subwork_id,
                                         @Field("vendor_type") String vendor_type,
                                         @Field("description") String description,
                                         @Field("uom_id") String uom_id,
                                         @Field("rate") String rate,
                                         @Field("phone_no") String phone_no,
                                         @Field("remarks") String remarks);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-wise-vendor-add")
    Call<AdminAddVendors>adminAddVendors1(@Field("person_name") String person_name,
                                         @Field("project_id") String project_id,
                                         @Field("pan_no") String pan_no,
                                         @Field("work_id") String work_id,
                                         @Field("subwork_id") String subwork_id,
                                         @Field("vendor_type") String vendor_type,
                                         @Field("skill_rate") String skill_rate,
                                         @Field("male_rate") String male_rate,
                                         @Field("female_rate") String female_rate,
                                         @Field("phone_no") String phone_no,
                                         @Field("remarks") String remarks);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-wise-vendor-add")
    Call<AdminAddVendors>adminAddVendors2(@Field("person_name") String person_name,
                                         @Field("project_id") String project_id,
                                         @Field("pan_no") String pan_no,
                                         @Field("work_id") String work_id,
                                         @Field("subwork_id") String subwork_id,
                                         @Field("vendor_type") String vendor_type,
                                         @Field("material_type_id") String material_type_id,
                                         @Field("uom_id") String uom_id,
                                         @Field("rate") String rate,
                                         @Field("phone_no") String phone_no,
                                         @Field("remarks") String remarks);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-project-estimation")
    Call<AdminProjectEstimation>adminprojectestimation(@Field("mobile") String mobile,
                                                       @Field("project_id") String project_id,
                                                       @Field("work_id") String work_id,
                                                       @Field("subwork_id") String subwork_id,
                                                       @Field("description") String description,
                                                       @Field("no_quantity") String no_quantity,
                                                       @Field("rate") String rate,
                                                       @Field("uom_id") String uom_id,
                                                       @Field("amount") String amount);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("engagor-report-add")
    Call<AdminEngagerReport>adminAddEngager(@Field("mobile") String mobile,
                                            @Field("project_id") String project_id,
                                            @Field("work_id") String work_id,
                                            @Field("subwork_id") String subwork_id,
                                            @Field("engagor_id") String engagor_id,
                                            @Field("machine_id") String machine_id,
                                            @Field("vehicle_no") String vehicle_no,
                                            @Field("description") String description,
                                            @Field("date") String date,
                                            @Field("start_time") String start_time,
                                            @Field("end_time") String end_time,
                                            @Field("start_time2") String start_time2,
                                            @Field("end_time2") String end_time2);



 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("boq-add ")
    Call<AddBoqResponse>addBoq(@Field("project_id") String project_id,
                                            @Field("work_id") String work_id,
                                            @Field("subwork_id") String subwork_id,
                                            @Field("uom_id") String uom_id,
                                            @Field("no") String no,
                                            @Field("length") String length,
                                            @Field("width") String width,
                                            @Field("depth") String depth,
                                            @Field("quantity") String quantity);

 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("msheet-add")
    Call<AddBoqResponse>addMsheet(@Field("project_id") String project_id,
                                            @Field("work_id") String work_id,
                                            @Field("subwork_id") String subwork_id,
                                            @Field("uom_id") String uom_id,
                                            @Field("no") String no,
                                            @Field("length") String length,
                                            @Field("width") String width,
                                            @Field("depth") String depth,
                                            @Field("quantity") String quantity,
                                            @Field("bill_no") String bill_no,
                                            @Field("contractor_id") String contractor_id,
                                            @Field("date") String date);

 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("muse-add")
    Call<AddBoqResponse>addmaterial(@Field("project_id") String project_id,
                                            @Field("work_id") String work_id,
                                            @Field("subwork_id") String subwork_id,
                                            @Field("uom_id") String uom_id,
                                            @Field("material_id") String material_id,
                                            @Field("opening_stock") String opening_stock,
                                            @Field("recived") String recived,
                                            @Field("consumption") String consumption,
                                            @Field("contractor_id") String contractor_id,
                                            @Field("date") String date);

 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("supplier-report-add")
    Call<AdminSupplierReport>adminAddSupplier(@Field("mobile") String mobile,
                                            @Field("project_id") String project_id,
                                            @Field("work_id") String work_id,
                                            @Field("subwork_id") String subwork_id,
                                            @Field("supplier_id") String supplier_id,
                                            @Field("material_id") String material_id,
                                            @Field("uom_id") String uom_id,
                                            @Field("ts") String ts,
                                            @Field("description") String description,
                                            @Field("quantity") String quantity,
                                            @Field("date") String date,
                                            @Field("unloading_charge") String unloading_charge,
                                            @Field("transport_charge") String transport_charge);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-dpr")
    Call<AdminWorkReport>adminWorkReport(@Field("mobile") String mobile,
                                         @Field("project_id") String project_id,
                                         @Field("contractor_id") String contractor_id,
                                         @Field("work_id") String work_id,
                                         @Field("subwork_id") String subwork_id,
                                         @Field("work_description") String work_description,
                                         @Field("skilled") String skilled,
                                         @Field("male_no") String male_no,
                                         @Field("female_no") String female_no,
                                         @Field("date") String date,
                                         @Field("transport_charge") String transport_charge);




    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("marketing-team-change-password")
    Call<ChangePasswordResponse>agentChangePassword2(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("procurement-team-change-password")
    Call<ChangePasswordResponse>agentChangePassword3(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("user-details")
    Call<UserDetailsResponse>adminLeadsDetails(@Field("mobile") String mobile);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("lead-details-in-procurement-team")
    Call<LeadDetailResponse>leadDetailprocurement(@Field("user_mobile") String user_mobile,
                                                  @Field("lead_id") String lead_id);




    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-additional-info")
    Call<AdditionalInfoResponse>additionalInfo(@Field("lead_id") String lead_id,
                                               @Field("mobile")   String mobile,
                                                @Field("road_connectivity") String road_connectivity,
                                                @Field("expected_purchase_value")  String expected_purchase_value,
                                               @Field("expected_sales_value")  String expected_sales_value,
                                               @Field("status")  String status);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("edit-user")
    Call<EditUserResponse>adminEditUser(@Field("mobile") String mobile);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("agent-notification-count")
    Call<NotificationCountResponse>notificationcountagent(@Field("mobile") String mobile);

   @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("techenical-team-notification-count")
    Call<NotificationCountResponse>notificationcounttech(@Field("mobile") String mobile);


   @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("siteengineer-team-notification-count")
    Call<NotificationCountResponse>notificationcountsite(@Field("mobile") String mobile);






    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("insert-otp")
    Call<OTP>adminOtp(@Field("mobile")String mobile, @Field("otp") String otp );


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("player-otp-verification")
    Call<OTPResponse>playerOtp(@Field("mobile_no")String mobile_no, @Field("otp") String otp );


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("change-password1")
    Call<ChangePasswordResponse>adminChangePassword(@Field("mobile") String mobile, @Field("password") String password, @Field("confpassword") String confpassword);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("admin-change-password")
    Call<ChangePasswordResponse>adminChangePassword1(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("agent-change-password")
    Call<ChangePasswordResponse>agentChangePassword1(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("techenical-team-change-password")
    Call<ChangePasswordResponse>techChangePassword(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("siteengineer-team-change-password")
    Call<ChangePasswordResponse>siteEngineerChangePassword(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("sub-agent-change-password")
    Call<ChangePasswordResponse>agentChangePassword11(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("checking-team-change-password")
    Call<ChangePasswordResponse>checkingChangePassword1(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);



@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("legal-team-change-password")
    Call<ChangePasswordResponse>legalChangePassword1(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);
@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("lesion-team-change-password")
    Call<ChangePasswordResponse>lesionChangePassword1(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);

@FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("survey-team-change-password")
    Call<ChangePasswordResponse>surveyChangePassword1(@Field("mobile") String mobile, @Field("old_password") String password, @Field("new_password") String confpassword);




    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("marketing-team-profile")
    Call<ProfileMarketingResponse>marketingProfile(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("procurement-team-profile")
    Call<ProfileProcurementResponse>procurementProfile(@Field("mobile") String mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("leads-list-in-agent")
    Call<AgentLeadsListResponse>agentLeadsList(@Field("user_mobile") String user_mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("my-approved-leads-list")
    Call<AgentApprovedLeadResponse>agentApprovedLeadsList(@Field("user_mobile") String user_mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("my-rejected-leads-list")
    Call<AgentApprovedLeadResponse>agentRejectedLeadsList(@Field("user_mobile") String user_mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("leads-list-in-sub-agent")
    Call<SubAgentLeadsListResponse>subagentLeadsList(@Field("user_mobile") String user_mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("sub-agent-profile")
    Call<ProfileSubAgentResponse>subAgentProfile(@Field("mobile") String mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("edit-leads-in-checking-team")
    Call<EditLeadsCheckingResponse>editLeadsChecking(@Field("lead_id") String lead_id,
                                                     @Field("user_mobile") String user_mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("edit-leads-in-legal-team")
    Call<EditLeadsLegalResponse>editLeadsLegal(@Field("lead_id") String lead_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("edit-leads-in-lesion-team")
    Call<EditLeadsLesionResponse>editLeadsLesion(@Field("lead_id") String lead_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("edit-leads-in-survey-team")
    Call<EditLeadsSurveyResponse>editLeadsSurvey(@Field("lead_id") String lead_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("edit-leads-in-marketing-team")
    Call<EditLeadsMarketingResponse>editLeadsMarketing(@Field("lead_id") String lead_id,
                                                       @Field("user_mobile") String user_mobile);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("edit-leads-in-procurement-team")
    Call<EditLeadsProcurementResponse>editLeadsProcurement(@Field("lead_id") String lead_id,
                                                           @Field("user_mobile") String user_mobile);







    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("update-leads-in-checking-team")
    Call<UpdateLeadsCheckingResponse>updateLeadsChecking(@Field("lead_id") String lead_id ,
                                                         @Field("ct_status") String ct_status,
                                                         @Field("comments") String comments,
                                                         @Field("user_mobile") String user_mobile);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("update-leads-in-legal-team")
    Call<UpdateLeadsLegalResponse>updateLeadsLegal(@Field("lead_id") String lead_id ,
                                                   @Field("lgt_status") String lgt_status,
                                                   @Field("comments") String comments);
    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("update-leads-in-lesion-team")
    Call<UpdateLeadsLesionResponse>updateLeadsLesion(@Field("lead_id") String lead_id ,
                                                     @Field("ls_status") String ls_status,
                                                     @Field("comments") String comments);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("update-leads-in-survey-team")
    Call<UpdateLeadsSurveyResponse>updateLeadsSurvey(@Field("lead_id") String lead_id ,
                                                     @Field("st_status") String st_status,
                                                     @Field("comments") String comments);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("update-leads-in-marketing-team")
    Call<UpdateLeadsMarketingResponse>updateLeadsMarketing(@Field("lead_id") String lead_id ,
                                                           @Field("user_mobile") String user_mobile ,
                                                           @Field("mt_status") String mt_status,
                                                           @Field("comments") String comments,
                                                           @Field("road_connectivity") String road_connectivity,
                                                           @Field("expected_sales_value") String expected_sales_value);




    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("update-leads-in-procurement-team")
    Call<UpdateLeadsProcurementResponse>updateLeadsProcurement(@Part("lead_id") RequestBody lead_id,
                                                          @Part("pt_status") RequestBody pt_status,
                                                          @Part("proposal_notes") RequestBody proposal_notes,
                                                          @Part MultipartBody.Part file);




    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-inventory")
    Call<AddInventoryResponse>adminAddInventory(@Field("mobile") String mobile ,
                                                @Field("project_id") String project_id,
                                                @Field("material_id") String material_id,
                                                @Field("quantity") String quantity,
                                                @Field("descriptions") String descriptions);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-city")
    Call<AdminAddCity>adminAddCity(@Field("mobile") String mobile ,
                                   @Field("city_name") String city_name);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-state")
    Call<AdminAddState>adminAddState(@Field("state_name") String state_name);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-game-type")
    Call<AdminGameState>adminAddGame(@Field("game_type") String game_type);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("work-type-add")
    Call<AdminAddWorkType>adminAddWorkType(@Field("mobile") String mobile ,
                                           @Field("work_name") String work_name,
                                           @Field("status") String status);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("uom-add")
    Call<AdminAddWorkType>adminAddUOM(@Field("mobile") String mobile ,
                                           @Field("uom") String uom,
                                           @Field("status") String status);

 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-machine-type")
    Call<AdminAddMachineType>adminAddMachine(@Field("mobile") String mobile ,
                                             @Field("type_name") String type_name,
                                             @Field("status") String status);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("total-my-referal-list")
    Call<ReferralListResponse>referralList(@Field("user_mobile") String user_mobile);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("earn-wise-my-referal ")
    Call<ReferralListResponse>referralListId(@Field("user_mobile") String user_mobile,
                                             @Field("earn_id") String earn_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("earn-wise-referal")
    Call<ReferralListResponse>referralListIdAdmin(@Field("earn_id") String earn_id);


 @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("subwork-type-add")
    Call<AdminSubAddWorkType>adminAddSubWorkType(@Field("mobile") String mobile ,
                                                 @Field("work_id") String work_id,
                                                 @Field("subwork_name") String subwork_name,
                                                 @Field("item_head_no") String item_head_no,

                                                 @Field("status") String status);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-district")
    Call<AdminAddDistrict>adminAddDistrict(@Field("district_name") String district_name ,
                                                 @Field("state_id") String state_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-mandal")
    Call<AdminAddMandal>adminAddMandal(@Field("mandal_name") String mandal_name ,
                                       @Field("state_id") String state_id,
                                       @Field("district_id") String district_id);


    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-village")
    Call<AdminAddVillage>adminAddVillage(@Field("village_name") String village_name ,
                                       @Field("state_id") String state_id,
                                       @Field("district_id") String district_id,
                                         @Field("mandal_id") String mandal_id);



    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("user1-list-by-agent")
    Call<AgentUserList>agentUserList(@Field("agent_id") String agent_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("project-estimation-list")
    Call<ProjectEstimationListResponse>projectEstimationList(@Field("project_id") String project_id);

    @FormUrlEncoded
    @Headers("x-api-key:3waysol@123")
    @POST("add-ltd")
    Call<AdminAddDoc>adminAddDoc(@Field("mobile") String mobile ,
                                 @Field("document_name") String document_name);



    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("register")
    Call<AddUserResponse>adminAddUser(@Part("usertype") RequestBody usertype,
                                      @Part("username") RequestBody username,
                                      @Part("mobile") RequestBody  mobile,
                                      @Part("email") RequestBody email,
                                      //@Part("status") RequestBody status,
                                      //@Part("gender") RequestBody gender,
                                      @Part MultipartBody.Part file);




    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("add-player")
    Call<AddPlayerResponse>adminAddPlayer(@Part("player_first_name") RequestBody player_first_name,
                                      @Part("player_last_name") RequestBody player_last_name,
                                      @Part("phone") RequestBody  phone,
                                      @Part("aadhar_no") RequestBody aadhar_no,
                                      @Part("address") RequestBody address,
                                      @Part("pin") RequestBody pin,
                                        @Part("dob") RequestBody dob,
                                        @Part("state_id") RequestBody state_id,
                                        @Part("district_id") RequestBody district_id,
                                        @Part("mandal_id") RequestBody mandal_id,
                                        @Part("village_id") RequestBody village_id,
                                        @Part("game_type_id") RequestBody game_type_id,
                                      @Part MultipartBody.Part file,
                                        @Part MultipartBody.Part file1);











    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("add-users-by-agent")
    Call<AddUserAgentResponse>agentAddUser(@Part("agent_mobile") RequestBody agent_mobile,
                                           @Part("agent_id") RequestBody agent_id,
                                           @Part("usertype") RequestBody usertype,
                                           @Part("username") RequestBody username,
                                           @Part("mobile") RequestBody  mobile,
                                           @Part("email") RequestBody email,
                                           @Part("status") RequestBody status,
                                           @Part MultipartBody.Part file);


    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("techenical-team-store-document")
    Call<TechnicalDocResponse>techAddDoc(@Part("lead_id") RequestBody lead_id,
                                      @Part("user_mobile") RequestBody user_mobile,
                                      @Part("image_description") RequestBody  image_description,
                                      @Part("doc_description") RequestBody doc_description,
                                      @Part MultipartBody.Part file,
                                         @Part MultipartBody.Part file1);

    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("marketing-team-store-document")
    Call<TechnicalDocResponse>MarkeAddDoc(@Part("lead_id") RequestBody lead_id,
                                      @Part("user_mobile") RequestBody user_mobile,
                                      @Part("image_description") RequestBody  image_description,
                                      @Part("doc_description") RequestBody doc_description,
                                      @Part MultipartBody.Part file,
                                         @Part MultipartBody.Part file1);

    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("checking-team-store-document")
    Call<TechnicalDocResponse>checkingAddDoc(@Part("lead_id") RequestBody lead_id,
                                      @Part("user_mobile") RequestBody user_mobile,
                                      @Part("image_description") RequestBody  image_description,
                                      @Part("doc_description") RequestBody doc_description,
                                      @Part MultipartBody.Part file,
                                         @Part MultipartBody.Part file1);





    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("add-leads")
    Call<UpdateUserResponse>agentAddLead(@Part("village_name") RequestBody village_name,
                                         @Part("property_name") RequestBody property_name,
                                         @Part("acres") RequestBody  acres,
                                         @Part("latitude") RequestBody latitude,
                                         @Part("longitude") RequestBody longitude,
                                         @Part("property_description") RequestBody property_description,
                                         @Part("status") RequestBody status,
                                         @Part("survey_no") RequestBody survey_no,
                                         @Part("user_mobile") RequestBody user_mobile,
                                         @Part List<MultipartBody.Part> file,
                                         @Part List<MultipartBody.Part> file1,
                                         @Part("mandal_name") RequestBody mandal_name,
                                         @Part("district_name") RequestBody district_name);







    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("add-nearby-projects")
    Call<AddNearbyProjectResponse>adminNearbyProject(@Part("mobile") RequestBody mobile,
                                                     @Part("lead_id") RequestBody lead_id,
                                                     @Part("project_name") RequestBody  project_name,
                                                     @Part("acres") RequestBody acres,
                                                     @Part("plot_size") RequestBody plot_size,
                                                     @Part("fast_moving_plot_size") RequestBody fast_moving_plot_size,
                                                     @Part("cost") RequestBody cost,
                                                     @Part("google_location") RequestBody google_location,
                                                     @Part List<MultipartBody.Part> documentImages,
                                                     @Part List<MultipartBody.Part> file1,
                                                     @Part("status") RequestBody status,
                                                     @Part("comment") RequestBody comment);



    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("add-leads2")
    Call<SubAgentAddLeadResponse>subagentAddLead(@Part("agent_id") RequestBody agent_id,
                                                 @Part("village_name") RequestBody village_name,
                                                 @Part("property_name") RequestBody property_name,
                                                 @Part("acres") RequestBody  acres,
                                                 @Part("latitude") RequestBody latitude,
                                                 @Part("longitude") RequestBody longitude,
                                                 @Part("comments") RequestBody comments,
                                                 @Part("status") RequestBody status,
                                                 @Part("survey_no") RequestBody survey_no,
                                                 @Part("user_mobile") RequestBody user_mobile,
                                                 @Part("address") RequestBody address,
                                                 @Part List<MultipartBody.Part> file,
                                                 @Part("mandal_name") RequestBody mandal_name,
                                                 @Part("district_name") RequestBody district_name);


    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("investment-add")
    Call<AddInvestmentResponse>adminAddInvestment(@Part("mobile") RequestBody mobile,
                                                  @Part("project_id") RequestBody project_id,
                                                  @Part("investor_id") RequestBody  investor_id,
                                                  @Part("invest_amount") RequestBody invest_amount,
                                                  @Part("invest_date") RequestBody invest_date,
                                                  @Part("mature_date") RequestBody mature_date,
                                                  @Part("duration") RequestBody duration,
                                                  @Part("mature_amount") RequestBody mature_amount,
                                                  @Part("status") RequestBody status,
                                                  @Part MultipartBody.Part file);





    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("update-user")
    Call<UpdateUserResponse>adminUpdateUser(@Part("email") RequestBody email,
                                      @Part("mobile") RequestBody mobile,
                                      @Part("status") RequestBody status,
                                      @Part MultipartBody.Part file);






    @Headers("x-api-key:3waysol@123")
    @GET("users-list")
    Call<UserListAdminResponse>adminUserList();

    @Headers("x-api-key:3waysol@123")
    @GET("leads-list")
    Call<LeadsListResponse>adminLeadsList();

    @Headers("x-api-key:3waysol@123")
    @GET("earn-list-in-admin")
    Call<EarnsListResponse>adminEarnList();

    @Headers("x-api-key:3waysol@123")
    @GET("earn-list-in-agent")
    Call<EarnsListResponse>agentEarnList();

    @Headers("x-api-key:3waysol@123")
    @GET("players-list")
    Call<PlayersListResponse>adminPlayersList();



    @Headers("x-api-key:3waysol@123")
    @GET("procrument-leads")
    Call<ProcurementLeadsListAdminResponse>adminProcurementLeadsList();



    @Headers("x-api-key:3waysol@123")
    @GET("admin-dashboard")
    Call<AdminDashBoardResponse>adminDashBoard();

    @Headers("x-api-key:3waysol@123")
    @GET("project-list")
    Call<AdminProjectsList>adminProjectsList();

    @Headers("x-api-key:3waysol@123")
    @GET("list-of-contractor")
    Call<AdminContractorList>adminContractorList();





    @Headers("x-api-key:3waysol@123")
    @GET("checking-team-dashboard")
    Call<CheckingDashBoardResponse>checkingDashBoard();

    @Headers("x-api-key:3waysol@123")
    @GET("techenical-team-dashboard")
    Call<TechnicalDashBoardResponse>techDashBoard();


    @Headers("x-api-key:3waysol@123")
    @GET("leads-list-in-checking-team")
    Call<LeadsListCheckingResponse>checkingLeadsList();

    @Headers("x-api-key:3waysol@123")
    @GET("active-district-list")
    Call<AdminActiveDistList>activedistlist();

    @Headers("x-api-key:3waysol@123")
    @GET("active-mandal-list")
    Call<AdminActiveMandalList>activemandallist();

    @Headers("x-api-key:3waysol@123")
    @GET("active-village-list")
    Call<AdminActiveVillageList>activevillagelist();

    @Headers("x-api-key:3waysol@123")
    @GET("total-referal-list-in-admin")
    Call<ReferralListResponse>referrallist();


    @Headers("x-api-key:3waysol@123")
    @GET("checking-team-pending-leads")
    Call<PendingLeadsListCheckingResponse>pendingcheckingLeadsList();

    @Headers("x-api-key:3waysol@123")
    @GET("checking-team-approved-leads")
    Call<ApprovedLeadsListCheckingResponse>approvedcheckingLeadsList();

    @Headers("x-api-key:3waysol@123")
    @GET("material-type-list")
    Call<AdminValuesList>adminValuesList();


    @Headers("x-api-key:3waysol@123")
    @GET("project-type-in-inventory")
    Call<AdminInventoryProjectResponse>adminInventoryProjetcs();

    @Headers("x-api-key:3waysol@123")
    @GET("material-type-in-inventory")
    Call<AdminInventoryMaterialResponse>adminInventoryMaterial();

    @Headers("x-api-key:3waysol@123")
    @GET("inventory-list")
    Call<AdminInventoryList>adminInventoryList();


    @Headers("x-api-key:3waysol@123")
    @GET("city-name-in-vendors")
    Call<AdminVendorCityResponse>adminVendorCity();

    @Headers("x-api-key:3waysol@123")
    @GET("city-list")
    Call<AdminCityList>adminCityList();


    @Headers("x-api-key:3waysol@123")
    @GET("state-list")
    Call<AdminStateList>adminStateList();

    @Headers("x-api-key:3waysol@123")
    @GET("game-type-list")
    Call<AdminGameList>adminGameList();

    @Headers("x-api-key:3waysol@123")
    @GET("district-list")
    Call<AdminDistrictList>adminDistList();


       @Headers("x-api-key:3waysol@123")
    @GET("mandal-list")
    Call<AdminMandalList>adminMandalList();

    @Headers("x-api-key:3waysol@123")
    @GET("village-list")
    Call<AdminVillageList>adminVillageList();


    @Headers("x-api-key:3waysol@123")
    @GET("work-list")
    Call<AdminWorkTypeList>adminWorkList();


    @Headers("x-api-key:3waysol@123")
    @GET("active-state-list")
    Call<AdminActiveStateList>adminstateActiveList();







 @Headers("x-api-key:3waysol@123")
    @GET("subwork-list")
    Call<AdminSubWorkTypeList>adminSubWorkList();

 @Headers("x-api-key:3waysol@123")
    @GET("uom-list")
    Call<AdminUOMList>uomList();

 @Headers("x-api-key:3waysol@123")
    @GET("list-of-machine-type")
    Call<AdminMachineList>machineList();


     @Headers("x-api-key:3waysol@123")
    @GET("ltd-list")
    Call<AdminLegalList>adminLegalList();


     @Headers("x-api-key:3waysol@123")
    @GET("investor-name-in-investment")
    Call<AdminInvestementResponse>adminInvestments();


 @Headers("x-api-key:3waysol@123")
    @GET("investment-list")
    Call<AdminInvestmentList>adminInvestmentList();


@Headers("x-api-key:3waysol@123")
    @GET("leads-list-in-marketing-team")
    Call<MarketingLeadsListResponse>marketingLeadsList();

@Headers("x-api-key:3waysol@123")
    @GET("leads-list-in-procurement-team")
    Call<ProcurementLeadsListResponse>procurementLeadsList();



@Headers("x-api-key:3waysol@123")
    @GET("document-name-list")
    Call<LegalDocResponse>addLegalDoc();


@Headers("x-api-key:3waysol@123")
    @GET("techenical-team-approved-lead-list")
    Call<AgentApprovedLeadResponse>approvedLeadsTechnical();


@Headers("x-api-key:3waysol@123")
    @GET("techenical-team-open-lead-list")
    Call<AgentApprovedLeadResponse>rejectedLeadsTechnical();



@Headers("x-api-key:3waysol@123")
    @GET("leads-list-in-legal-team")
    Call<LeadsListLegalResponse>legalLeadsList();


@Headers("x-api-key:3waysol@123")
    @GET("leads-list-in-survey-team")
    Call<LeadsListSurveyResponse>surveyLeadsList();

@Headers("x-api-key:3waysol@123")
    @GET("leads-list-in-lesion-team")
    Call<LeadsListLesionResponse>lesionLeadsList();

@Headers("x-api-key:3waysol@123")
    @GET("proposal-doc-list")
    Call<LeadIdResponse>leadIdprocurement();


@Headers("x-api-key:3waysol@123")
    @GET("list-of-notification")
    Call<NotificationsAdminListResponse>notificationList();



@Headers("x-api-key:3waysol@123")
    @GET("list-of-engagor")
    Call<AdminAddEngager>adminEngagerList();

@Headers("x-api-key:3waysol@123")
    @GET("approved-leads-in-marketing-team")
    Call<LeadsListMarketing>approvedMarketingleads();

@Headers("x-api-key:3waysol@123")
    @GET("pending-leads-in-marketing-team")
    Call<LeadsListMarketing>pendingMarketingleads();

@Headers("x-api-key:3waysol@123")
    @GET("rejected-leads-in-marketing-team")
    Call<LeadsListMarketing>rejectedMarketingleads();

@Headers("x-api-key:3waysol@123")
    @GET("list-of-supplier")
    Call<AdminAddSupplier>adminSupplierList();

@Headers("x-api-key:3waysol@123")
    @GET("date-wise-progress-report-list")
    Call<WorkReportLIst>adminWorkReports();

@Headers("x-api-key:3waysol@123")
    @GET("date-wise-supplier-report-list")
    Call<SupplierReportLIst>adminSupplierReports();

@Headers("x-api-key:3waysol@123")
    @GET("date-wise-msheet-report-list")
    Call<SupplierReportLIst>adminMeasurementReports();


@Headers("x-api-key:3waysol@123")
    @GET("date-wise-engagor-report-list")
    Call<EngagerReportLIst>EngagerReportLIst();


@Headers("x-api-key:3waysol@123")
    @GET("lead-list-in-techenical-team")
    Call<TechLeadsListResponse>techLeadsList();


    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("add-documents-by-procurement-team")
    Call<AddProposalDocumentsResponse>addproposaldocument(@Part("lead_id") RequestBody lead_id,
                                                          @Part("user_mobile") RequestBody user_mobile,
                                                          @Part("proposal_notes") RequestBody proposal_notes,
                                                          @Part MultipartBody.Part file,
                                                          @Part List<MultipartBody.Part> file1);



    @Multipart
    @Headers("x-api-key:3waysol@123")
    @POST("update-lead-in-agent")
    Call<UpdateLead1Response>agentupdates(@Part("lead_id") RequestBody lead_id,
                                          @Part("ad_comments") RequestBody ad_comments,
                                          @Part MultipartBody.Part file);


}
