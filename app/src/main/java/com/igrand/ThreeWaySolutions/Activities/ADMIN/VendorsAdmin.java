package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ChangePassword;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMaterialUsage;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.AddMeasurementSheet;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterContractorProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEngagerProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterInventory;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSupplierProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterVendorCity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddProjects;
import com.igrand.ThreeWaySolutions.Response.AdminAddVendors;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryMaterialResponse;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryProjectResponse;
import com.igrand.ThreeWaySolutions.Response.AdminMachineList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;
import com.igrand.ThreeWaySolutions.Response.AdminVendorCityResponse;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class VendorsAdmin extends BaseActivity {

    ImageView back;
    String mobileNumber;
    EditText name,number,pan,skillrate,malerate,femalerate,remarks,desc,rate,remarkswork,descwork,ratematerial,remarksmaterial;
    String Name,Number,Pan,SkillRate,MaleRate,FemalRate,Remarks,UOMID,Description,Rate,MaterialId,MachineId;
    Button add;
    ApiInterface apiInterface;
    AdminAddVendors.StatusBean statusBean;
    AdminWorkTypeList.StatusBean statusBean1;
    AdminSubWorkTypeListbyId.StatusBean statusBean2;
    TextView vendor,worktype,subworktype,uom,materialtype,uomaterial,machinetype,uomachine;
    RecyclerAdapterVendorCity recyclerAdapter;
    String VendorId,ProjectID,WorkId,SubWorkId;
    LinearLayout machinerylinear,labourlinear,worklinear,materiallinear;
    RecyclerAdapterContractorProjectsList recyclerAdapterContractorProjectsList;
    AdminUOMList.StatusBean statusBean4;
    RecyclerAdapterSupplierProjectsList recyclerAdapterSupplierProjectsList1;
    RecyclerAdapterSupplierProjectsList recyclerAdapterContractorProjectsList1;
    RecyclerAdapterEngagerProjectsList recyclerAdapterEngagerProjectsList;
    AdminInventoryMaterialResponse.StatusBean statusBean3;
    AdminMachineList.StatusBean statusBean5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors_admin);
        back=findViewById(R.id.back);
        name=findViewById(R.id.name);
        number=findViewById(R.id.number);
        add=findViewById(R.id.add);
        vendor=findViewById(R.id.vendor);
        pan=findViewById(R.id.pan);
        machinerylinear=findViewById(R.id.machinerylinear);
        labourlinear=findViewById(R.id.labourlinear);
        worklinear=findViewById(R.id.worklinear);
        materiallinear=findViewById(R.id.materiallinear);
        worktype=findViewById(R.id.worktype);
        subworktype=findViewById(R.id.subworktype);
        skillrate=findViewById(R.id.skilled);
        malerate=findViewById(R.id.male);
        femalerate=findViewById(R.id.female);
        remarks=findViewById(R.id.remarkslabour);

        rate=findViewById(R.id.rate);
        remarkswork=findViewById(R.id.remarkswork);
        uom=findViewById(R.id.uom);
        descwork=findViewById(R.id.desc);

        materialtype=findViewById(R.id.materialtype);
        ratematerial=findViewById(R.id.ratematerial);
        remarksmaterial=findViewById(R.id.remarksmaterial);
        uomaterial=findViewById(R.id.uomaterial);

        machinetype=findViewById(R.id.machinetype);
        uomachine=findViewById(R.id.uomachine);



        vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(VendorsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_vendor);

                dialog.show();

                final RadioButton labour,work,material,machinery;
                Button add;

                labour=dialog.findViewById(R.id.labour);
                work=dialog.findViewById(R.id.work);
                material=dialog.findViewById(R.id.material);
                machinery=dialog.findViewById(R.id.machinery);
                add=dialog.findViewById(R.id.add);


                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (labour.isChecked()) {
                            dialog.dismiss();
                            vendor.setText("Labour contractor");
                            VendorId="1";
                            labourlinear.setVisibility(View.VISIBLE);
                            machinerylinear.setVisibility(View.GONE);
                            materiallinear.setVisibility(View.GONE);
                            worklinear.setVisibility(View.GONE);
                            addVendorLabour();
                        }

                        if (work.isChecked()) {
                            dialog.dismiss();
                            vendor.setText("Work Contractor");
                            VendorId="2";
                            labourlinear.setVisibility(View.GONE);
                            machinerylinear.setVisibility(View.GONE);
                            materiallinear.setVisibility(View.GONE);
                            worklinear.setVisibility(View.VISIBLE);
                            addVendorWork();
                        }
                        if (material.isChecked()) {
                            dialog.dismiss();
                            vendor.setText("Material Suppliers");
                            VendorId="3";
                            labourlinear.setVisibility(View.GONE);
                            machinerylinear.setVisibility(View.GONE);
                            materiallinear.setVisibility(View.VISIBLE);
                            worklinear.setVisibility(View.GONE);
                            addVendorMaterial();
                        } if (machinery.isChecked()) {
                            dialog.dismiss();
                            vendor.setText("Machinery Engagers");
                            VendorId="4";
                            labourlinear.setVisibility(View.GONE);
                            machinerylinear.setVisibility(View.VISIBLE);
                            materiallinear.setVisibility(View.GONE);
                            worklinear.setVisibility(View.GONE);
                            addVendorMachinery();
                        }

                    }

                });


            }


        });



        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            ProjectID=getIntent().getStringExtra("ID");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        materialtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final Dialog dialog = new Dialog(VendorsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminInventoryMaterialResponse> call = apiInterface.adminInventoryMaterial();
                call.enqueue(new Callback<AdminInventoryMaterialResponse>() {
                    @Override
                    public void onResponse(Call<AdminInventoryMaterialResponse> call, Response<AdminInventoryMaterialResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminInventoryMaterialResponse.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(VendorsAdmin.this));
                            recyclerAdapterContractorProjectsList1 = new RecyclerAdapterSupplierProjectsList(VendorsAdmin.this, dataBeans,dialog, materialtype,"materialvendor");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(VendorsAdmin.this, "No Materials...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminInventoryMaterialResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(VendorsAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });

        uom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(VendorsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminUOMList> call = apiInterface.uomList();
                call.enqueue(new Callback<AdminUOMList>() {
                    @Override
                    public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean4 = response.body() != null ? response.body().getStatus() : null;
                            // Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminUOMList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(VendorsAdmin.this));
                            recyclerAdapterSupplierProjectsList1 = new RecyclerAdapterSupplierProjectsList(VendorsAdmin.this,uom, dataBeans,dialog,"uomvendor");
                            recyclerView.setAdapter(recyclerAdapterSupplierProjectsList1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminUOMList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(VendorsAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });







            }
        });

        uomaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(VendorsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminUOMList> call = apiInterface.uomList();
                call.enqueue(new Callback<AdminUOMList>() {
                    @Override
                    public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean4 = response.body() != null ? response.body().getStatus() : null;
                            // Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminUOMList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(VendorsAdmin.this));
                            recyclerAdapterSupplierProjectsList1 = new RecyclerAdapterSupplierProjectsList(VendorsAdmin.this,uomaterial, dataBeans,dialog,"uomvendor");
                            recyclerView.setAdapter(recyclerAdapterSupplierProjectsList1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminUOMList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(VendorsAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });



            }
        });

        uomachine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(VendorsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminUOMList> call = apiInterface.uomList();
                call.enqueue(new Callback<AdminUOMList>() {
                    @Override
                    public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean4 = response.body() != null ? response.body().getStatus() : null;
                            // Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminUOMList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(VendorsAdmin.this));
                            recyclerAdapterSupplierProjectsList1 = new RecyclerAdapterSupplierProjectsList(VendorsAdmin.this,uomachine, dataBeans,dialog,"uomvendor");
                            recyclerView.setAdapter(recyclerAdapterSupplierProjectsList1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminUOMList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(VendorsAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });



            }
        });


        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(VendorsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(VendorsAdmin.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(VendorsAdmin.this, dataBeans, worktype, dialog, VendorsAdmin.this,"workvendor");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(VendorsAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSubWorkType();
            }
        });


        machinetype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(VendorsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminMachineList> call = apiInterface.machineList();
                call.enqueue(new Callback<AdminMachineList>() {
                    @Override
                    public void onResponse(Call<AdminMachineList> call, Response<AdminMachineList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean5 = response.body() != null ? response.body().getStatus() : null;
                            // Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminMachineList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(VendorsAdmin.this));
                            recyclerAdapterEngagerProjectsList = new RecyclerAdapterEngagerProjectsList(VendorsAdmin.this, dataBeans,dialog, machinetype,"machinevendor");
                            recyclerView.setAdapter(recyclerAdapterEngagerProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(VendorsAdmin.this, "No MachineTypes...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminMachineList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(VendorsAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


    }


    private void getSubWorkType() {


        final Dialog dialog = new Dialog(VendorsAdmin.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog_work);

        dialog.show();

        final RecyclerView recyclerView;
        final RelativeLayout linear;
        final Button add;

        recyclerView = dialog.findViewById(R.id.recyclerView);
        linear = dialog.findViewById(R.id.linear);
        add = dialog.findViewById(R.id.add);


        final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminSubWorkTypeListbyId> call = apiInterface.adminFsubworkbyid(WorkId);
        call.enqueue(new Callback<AdminSubWorkTypeListbyId>() {
            @Override
            public void onResponse(Call<AdminSubWorkTypeListbyId> call, Response<AdminSubWorkTypeListbyId> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminSubWorkTypeListbyId.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(VendorsAdmin.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(VendorsAdmin.this, dataBeans, dialog, VendorsAdmin.this, subworktype,"subvendor");
                    recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(VendorsAdmin.this, "No SubWorks...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminSubWorkTypeListbyId> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(VendorsAdmin.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }


    private void addVendorMaterial() {

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Name=name.getText().toString();
                Number=number.getText().toString();
                Pan=pan.getText().toString();

                Rate=ratematerial.getText().toString();
                Remarks=remarksmaterial.getText().toString();


                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminAddVendors> call = apiInterface.adminAddVendors2(Name,ProjectID, Pan,WorkId,SubWorkId,VendorId,SkillRate,MaleRate,FemalRate,Number,Remarks);
                call.enqueue(new Callback<AdminAddVendors>() {
                    @Override
                    public void onResponse(Call<AdminAddVendors> call, Response<AdminAddVendors> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(VendorsAdmin.this, "Material Supplier Added Successfully......", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(VendorsAdmin.this, VendorsList.class);
                            intent.putExtra("MobileNumber",mobileNumber);
                            intent.putExtra("ID",ProjectID);
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminAddVendors> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(VendorsAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });
    }

    private void addVendorMachinery() {

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Name=name.getText().toString();
                Number=number.getText().toString();
                Pan=pan.getText().toString();

                Rate=ratematerial.getText().toString();
                Remarks=remarksmaterial.getText().toString();


                final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminAddVendors> call = apiInterface.adminAddVendors2(Name,ProjectID, Pan,WorkId,SubWorkId,VendorId,SkillRate,MaleRate,FemalRate,Number,Remarks);
                call.enqueue(new Callback<AdminAddVendors>() {
                    @Override
                    public void onResponse(Call<AdminAddVendors> call, Response<AdminAddVendors> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(VendorsAdmin.this, "Machinery Engager Added Successfully......", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(VendorsAdmin.this, VendorsList.class);
                            intent.putExtra("MobileNumber",mobileNumber);
                            intent.putExtra("ID",ProjectID);
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminAddVendors> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(VendorsAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });
    }


    private void addVendorLabour() {

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


        Name=name.getText().toString();
        Number=number.getText().toString();
        Pan=pan.getText().toString();
        SkillRate=skillrate.getText().toString();
        MaleRate=malerate.getText().toString();
        FemalRate=femalerate.getText().toString();
        Remarks=remarks.getText().toString();


        final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminAddVendors> call = apiInterface.adminAddVendors(Name,ProjectID, Pan,WorkId,SubWorkId,VendorId,SkillRate,MaleRate,FemalRate,Number,Remarks);
        call.enqueue(new Callback<AdminAddVendors>() {
            @Override
            public void onResponse(Call<AdminAddVendors> call, Response<AdminAddVendors> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(VendorsAdmin.this, "Vendor Added Successfully......", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(VendorsAdmin.this, VendorsList.class);
                    intent.putExtra("MobileNumber",mobileNumber);
                    intent.putExtra("ID",ProjectID);
                    startActivity(intent);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<AdminAddVendors> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(VendorsAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

            }
        });


    }


    private void addVendorWork() {

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


        Name=name.getText().toString();
        Number=number.getText().toString();
        Pan=pan.getText().toString();
        Description=descwork.getText().toString();
        Rate=rate.getText().toString();
        Remarks=remarks.getText().toString();


        final ProgressDialog progressDialog = new ProgressDialog(VendorsAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminAddVendors> call = apiInterface.adminAddVendors1(Name,ProjectID, Pan,WorkId,SubWorkId,VendorId,Description,UOMID,Rate,Number,Remarks);
        call.enqueue(new Callback<AdminAddVendors>() {
            @Override
            public void onResponse(Call<AdminAddVendors> call, Response<AdminAddVendors> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Intent intent=new Intent(VendorsAdmin.this, VendorsList.class);
                    intent.putExtra("MobileNumber",mobileNumber);
                    intent.putExtra("ID",ProjectID);
                    startActivity(intent);
                    Toast.makeText(VendorsAdmin.this, "Work Contractor Added Successfully......", Toast.LENGTH_SHORT).show();

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(VendorsAdmin.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<AdminAddVendors> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(VendorsAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

            }
        });


    }





    public void getId1(String selectedworkid1) {

        WorkId=selectedworkid1;
    }

    public void getId2(String selectedworkid2) {

        SubWorkId=selectedworkid2;
    }

    public void getId4(String selectedworkid4) {
        UOMID=selectedworkid4;
    }

    public void getId3(String selectedworkid3) {

        MaterialId=selectedworkid3;
    }

    public void getId5(String selectedworkid3) {
        MachineId=selectedworkid3;
    }
}
