package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminForgetPassword;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPassword extends BaseActivity {

    ImageView back;
    Button next;
    EditText number;
    String mobileNumber;
    ApiInterface apiInterface;
    AdminForgetPassword.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        back=findViewById(R.id.back);
        next=findViewById(R.id.next);
        number=findViewById(R.id.number);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mobileNumber=number.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(ForgetPassword.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminForgetPassword> call = apiInterface.adminForgetPassword(mobileNumber);
                call.enqueue(new Callback<AdminForgetPassword>() {
                    @Override
                    public void onResponse(Call<AdminForgetPassword> call, Response<AdminForgetPassword> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(ForgetPassword.this, "OTP Sent Successfully...", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(ForgetPassword.this, OTP.class);
                            intent.putExtra("Number",mobileNumber);
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ForgetPassword.this, "Error while generating OTP...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminForgetPassword> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(ForgetPassword.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });
    }
}
