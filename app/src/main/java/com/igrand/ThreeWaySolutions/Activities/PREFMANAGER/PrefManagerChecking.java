package com.igrand.ThreeWaySolutions.Activities.PREFMANAGER;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class PrefManagerChecking {


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "3WayServicesChecking";
    private static final String KEY_MOBILENUMBER = "mobilenumber";
    private static final String KEY_AGENTID = "agentid";
    private static final String KEY_USER_NAME = "username";

    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";

    public PrefManagerChecking(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }




    public void createLogin(String mobileNumber,String userName) {

        editor.putString(KEY_MOBILENUMBER,mobileNumber);
        editor.putString(KEY_USER_NAME,userName);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }



    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();

        profile.put("mobilenumber",pref.getString(KEY_MOBILENUMBER,null));
        profile.put("username",pref.getString(KEY_USER_NAME,null));

        return profile;
    }



}
