package com.igrand.ThreeWaySolutions.Activities.AGENT;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEarns;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterReferrals;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.EarnsListResponse;
import com.igrand.ThreeWaySolutions.Response.ReferralListResponse;

import java.util.HashMap;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferralsListAgent extends BaseActivity {

    ImageView back;
    RecyclerView recyclerView;
    RecyclerAdapterReferrals recyclerAdapter;
    Button add;
    ApiInterface apiInterface;
    ShimmerFrameLayout mShimmerViewContainer;
    ReferralListResponse.StatusBean statusBean;
    PrefManagerAgent prefManagerAgent;
    String MobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referrals_list1);

        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        add = findViewById(R.id.add);

        prefManagerAgent=new PrefManagerAgent(ReferralsListAgent.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ReferralsListAgent.this,DashBoardAgent.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ReferralsListAgent.this,AddReferralAgent.class);
                startActivity(intent);
            }
        });

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ReferralListResponse> call = apiInterface.referralList(MobileNumber);
        call.enqueue(new Callback<ReferralListResponse>() {
            @Override
            public void onResponse(Call<ReferralListResponse> call, Response<ReferralListResponse> response) {

                if (response.code() == 200) {
                    // progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ReferralListResponse.DataBean> dataBeans = response.body().getData();
                    //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(ReferralsListAgent.this));
                    recyclerAdapter=new RecyclerAdapterReferrals(ReferralsListAgent.this,dataBeans,"List");
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    //progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(ReferralsListAgent.this, "No Referral's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<ReferralListResponse> call, Throwable t) {
                // progressDialog1.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ReferralsListAgent.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

}
