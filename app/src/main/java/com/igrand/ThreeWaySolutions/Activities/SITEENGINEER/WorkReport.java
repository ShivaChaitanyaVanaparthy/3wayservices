package com.igrand.ThreeWaySolutions.Activities.SITEENGINEER;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddWorkReport;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkName;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class WorkReport extends BaseActivity {

    Button add,submit;
    RecyclerView recyclerView;
    //TextView userType;
    RecyclerWorkReport recyclerUser;
    RecyclerWorkName recyclerUser1;
    ApiInterface apiInterface;
    String ID, Project, key, key1;
    AdminWorkReportList.StatusBean statusBean;
    ImageView back;
    private SearchView searchView;
    List<AdminWorkReportList.DataBean> dataBeans=new ArrayList<>();
    Toolbar toolbar;
    TextView contractor;
    String PersonId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_report);
        add = findViewById(R.id.add);
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        contractor = findViewById(R.id.contractor);
        submit = findViewById(R.id.submit);

        //userType=v.findViewById(R.id.userType);



        if (getIntent() != null) {

            ID = getIntent().getStringExtra("ID");
            Project = getIntent().getStringExtra("Project");
            key = getIntent().getStringExtra("key");
            key1 = getIntent().getStringExtra("key1");
            //SUM = args.getString("SUM");
        }


       // toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WorkReport.this, SiteEngineerProjectDetails.class);
                intent.putExtra("ID", ID);
                intent.putExtra("key", key);
                intent.putExtra("key1", key1);
                intent.putExtra("keysite", "keysite");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                //finish();
            }
        });


        getWorkReport();

        contractor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(WorkReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(WorkReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkReportList> call = apiInterface.adminWorkReportList(ID);
                call.enqueue(new Callback<AdminWorkReportList>() {
                    @Override
                    public void onResponse(Call<AdminWorkReportList> call, Response<AdminWorkReportList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AdminWorkReportList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(WorkReport.this));
                            recyclerUser1 = new RecyclerWorkName(WorkReport.this, dataBeans,contractor, dialog,add);
                            recyclerView.setAdapter(recyclerUser1);


                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status = error.getStatus();
                                Toast.makeText(WorkReport.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkReportList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(WorkReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WorkReport.this, AddWorkReport.class);
                intent.putExtra("ID", ID);
                intent.putExtra("Project", Project);
                intent.putExtra("key", "key");
                intent.putExtra("key1", "key1");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

        });

        submit.setOnClickListener(view -> {

            final ProgressDialog progressDialog = new ProgressDialog(WorkReport.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AdminWorkReportList> call = apiInterface.vendorwiseWork(ID,PersonId);
            call.enqueue(new Callback<AdminWorkReportList>() {
                @Override
                public void onResponse(Call<AdminWorkReportList> call, Response<AdminWorkReportList> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<AdminWorkReportList.DataBean> dataBeans = response.body().getData();
                        recyclerView.setLayoutManager(new LinearLayoutManager(WorkReport.this));
                        recyclerUser = new RecyclerWorkReport(WorkReport.this, dataBeans);
                        recyclerView.setAdapter(recyclerUser);


                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIError> converter =
                                ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                        APIError error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIError.StatusBean status = error.getStatus();
                            Toast.makeText(WorkReport.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                }


                @Override
                public void onFailure(Call<AdminWorkReportList> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(WorkReport.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });


        });


    }






    private void getWorkReport() {

        final ProgressDialog progressDialog = new ProgressDialog(WorkReport.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminWorkReportList> call = apiInterface.adminWorkReportList(ID);
        call.enqueue(new Callback<AdminWorkReportList>() {
            @Override
            public void onResponse(Call<AdminWorkReportList> call, Response<AdminWorkReportList> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AdminWorkReportList.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(WorkReport.this));
                    recyclerUser = new RecyclerWorkReport(WorkReport.this, dataBeans);
                    recyclerView.setAdapter(recyclerUser);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status = error.getStatus();
                        Toast.makeText(WorkReport.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }

            }


            @Override
            public void onFailure(Call<AdminWorkReportList> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(WorkReport.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getWorkReport();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_item, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getCallingActivity()));
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<AdminWorkReportList.DataBean> filteredList = new ArrayList<>();
                for (AdminWorkReportList.DataBean data : dataBeans) {
                    String cityName = data.getPerson_name().toLowerCase();
                    if (cityName.contains(newText)) {
                        filteredList.add(data);
                    }
                }
                recyclerUser.setFilter(filteredList);
                return true;
            }
        });
        return true;
    }

    public void getId0(String workid) {
        PersonId=workid;
    }
}

