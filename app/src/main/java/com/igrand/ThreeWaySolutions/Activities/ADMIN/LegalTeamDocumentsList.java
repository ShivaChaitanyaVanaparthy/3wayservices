package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCityList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterLegalList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminCityList;
import com.igrand.ThreeWaySolutions.Response.AdminLegalList;

import java.util.List;

public class LegalTeamDocumentsList extends BaseActivity {

    RecyclerView recyclerView;
    ImageView back;
    RecyclerAdapterLegalList recyclerUser;
    ApiInterface apiInterface;
    AdminLegalList.StatusBean statusBean;
    Button add;
    String mobileNumber;
    ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal_team_documents);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LegalTeamDocumentsList.this,DashBoardAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LegalTeamDocumentsList.this,LegalAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
            }
        });




       /* final ProgressDialog progressDialog = new ProgressDialog(LegalTeamDocumentsList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminLegalList> call = apiInterface.adminLegalList();
        call.enqueue(new Callback<AdminLegalList>() {
            @Override
            public void onResponse(Call<AdminLegalList> call, Response<AdminLegalList> response) {

                if (response.code() == 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(LegalTeamDocumentsList.this, "Legal Doc List......", Toast.LENGTH_SHORT).show();
                    List<AdminLegalList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(LegalTeamDocumentsList.this));
                    recyclerUser = new RecyclerAdapterLegalList(LegalTeamDocumentsList.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(LegalTeamDocumentsList.this, "No Legal Doc...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminLegalList> call, Throwable t) {
                //progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LegalTeamDocumentsList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
