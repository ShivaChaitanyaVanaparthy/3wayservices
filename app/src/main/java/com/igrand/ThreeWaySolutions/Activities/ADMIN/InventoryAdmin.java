package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterInventory;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMaterial;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddInventoryResponse;
import com.igrand.ThreeWaySolutions.Response.AdminAddProjects;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryMaterialResponse;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryProjectResponse;

import java.util.List;

public class InventoryAdmin extends BaseActivity {

    ImageView back;
    String mobileNumber,TEXT,project_name,material_name,Project,Material,Quantity,Comment,ProjectID,MaterialID,projectID,materialId;
    EditText number,comment;
    TextView project,material;
    ApiInterface apiInterface;
    AdminInventoryProjectResponse.StatusBean statusBean;
    RecyclerAdapterInventory recyclerAdapter;
    AdminInventoryMaterialResponse.StatusBean statusBean1;
    RecyclerAdapterMaterial recyclerAdapterMaterial;
    Button add;
    AddInventoryResponse.StatusBean statusBean2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_admin);
        back=findViewById(R.id.back);
        number=findViewById(R.id.quantity);
        comment=findViewById(R.id.comment);
        project=findViewById(R.id.project);
        material=findViewById(R.id.material);
        add=findViewById(R.id.add);

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }


        project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(InventoryAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.recyclerview_inventor);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.show();
                final RecyclerView recyclerView ;
                final Button submit;
                final LinearLayout linearLayout;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                submit = dialog.findViewById(R.id.submit);
                linearLayout = dialog.findViewById(R.id.linear);



                final ProgressDialog progressDialog = new ProgressDialog(InventoryAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminInventoryProjectResponse> call = apiInterface.adminInventoryProjetcs();
                call.enqueue(new Callback<AdminInventoryProjectResponse>() {
                    @Override
                    public void onResponse(Call<AdminInventoryProjectResponse> call, Response<AdminInventoryProjectResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linearLayout.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AdminInventoryProjectResponse.DataBean> dataBeans=response.body().getData();


                           /* for(int i=0;i<=dataBeans.size();i++) {
                                project_name=dataBeans.get(0).getProject_name();
                                break;
                            }
*/
                            Toast.makeText(InventoryAdmin.this, "Project's List...", Toast.LENGTH_SHORT).show();

                            recyclerView.setLayoutManager(new LinearLayoutManager(InventoryAdmin.this));
                            recyclerAdapter = new RecyclerAdapterInventory(InventoryAdmin.this,dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(InventoryAdmin.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminInventoryProjectResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(InventoryAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        //project.setText(project_name);

                    }
                });

            }
        });


        material.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final Dialog dialog = new Dialog(InventoryAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.recyclerview_inventor);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.show();
                final RecyclerView recyclerView ;
                final Button submit;
                final LinearLayout linear;
                recyclerView = dialog.findViewById(R.id.recyclerView);
                submit = dialog.findViewById(R.id.submit);
                linear = dialog.findViewById(R.id.linear);



                final ProgressDialog progressDialog = new ProgressDialog(InventoryAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminInventoryMaterialResponse> call = apiInterface.adminInventoryMaterial();
                call.enqueue(new Callback<AdminInventoryMaterialResponse>() {
                    @Override
                    public void onResponse(Call<AdminInventoryMaterialResponse> call, Response<AdminInventoryMaterialResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            List<AdminInventoryMaterialResponse.DataBean> dataBeans=response.body().getData();
                           /* for(int i=0;i<=dataBeans.size();i++) {
                                material_name=dataBeans.get(i).getMaterial_type();
                                break;
                            }*/

                            Toast.makeText(InventoryAdmin.this, "Material's List...", Toast.LENGTH_SHORT).show();

                            recyclerView.setLayoutManager(new LinearLayoutManager(InventoryAdmin.this));
                            recyclerAdapterMaterial = new RecyclerAdapterMaterial(InventoryAdmin.this,dataBeans);
                            recyclerView.setAdapter(recyclerAdapterMaterial);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(InventoryAdmin.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminInventoryMaterialResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(InventoryAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                       // material.setText(material_name);
                        // material.append(material_name);

                    }
                });



            }
        });




        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                addInventory();
            }


        });

    }


    public void Textget(String selectionproject, String materialId) {

        material.setText(selectionproject);
        MaterialID=materialId;
    }

    public void TextgetProject(String selection, String projectID) {

        project.setText(selection);
        ProjectID=projectID;

    }


    private void addInventory() {





        Project=project.getText().toString();
        Material=material.getText().toString();
        Quantity=number.getText().toString();
        Comment=comment.getText().toString();


        final ProgressDialog progressDialog = new ProgressDialog(InventoryAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddInventoryResponse> call = apiInterface.adminAddInventory(mobileNumber,ProjectID,MaterialID,Quantity,Comment);
        call.enqueue(new Callback<AddInventoryResponse>() {
            @Override
            public void onResponse(Call<AddInventoryResponse> call, Response<AddInventoryResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(InventoryAdmin.this, "Project Added Successfully......", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(InventoryAdmin.this, InventoryList.class);
                    intent.putExtra("MobileNumber",mobileNumber);
                    startActivity(intent);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(InventoryAdmin.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AddInventoryResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(InventoryAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }







  /*  public void Textget(String project_name) {

        project.setText(project_name);
    }*/
}
