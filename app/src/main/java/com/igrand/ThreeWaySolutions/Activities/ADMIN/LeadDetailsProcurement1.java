package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.CheckingDocList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.Comments;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.MarketingDocList;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.AddProposalDocuments;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.DashBoardProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.EditProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.ProcDocList;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.TechDocList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments5;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadDetailResponse;
import com.igrand.ThreeWaySolutions.Response.LeadIdResponse;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsProcurement1 extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved00, approved1, approvedp,approveds,approvedl,approvedle,approvedt;
    String Id, Checking, StatusProperty, Date, Village, Comments, GoogleLocation, Address, CheckingStatus, MarketingStatus;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11, statusp, datep,statusl,statuss,datel,dates,statusle,datele,mandal,district,statust,datet,add1;
    Dialog dialog;
    Button edit, comment11;
    ApiInterface apiInterface;
    String MobileNumber, ct_status, ct_comments, CommentsChecking, MobileNumber1, Property, MOBILE, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate,Mandal,District;
    LeadDetailResponse.StatusBean statusBean;
    NestedScrollView scroll;
    EditText commentsonly;
    Button commentsbutton,add;
    LinearLayout mLayout, additionalinfo;
    String LegalStatus,SurveyStatus,LegalDate,SurveyDate;
    String ProposalName;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    String imagePic, picturePath,Name;
    Boolean number;
    String Document,ProposalNotes,LesionDate,LesionStatus,Latitude,Longitude,TechnicalStatus,TechnicalDate,key,pdf,Image;
    String[] document2;
    RecyclerView recyclerView,recyclerVieww;
    RecyclerAdapterCheckingDocuments5 recyclerAdapter1;
    LeadIdResponse.StatusBean statusBean2;
    LinearLayout marketing,procurement,tech,remarks1;

    List<String> strings,stringsimg;
    String[] document3,imagedoc;
    RecyclerAdapterAgentImages recyclerAdapter3;
    TextView comments;
    String Propertydesc;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;
    LinearLayout comments1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_procurement1);

        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        // document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.dateee);
        date11 = findViewById(R.id.date11);
        approved = findViewById(R.id.approved);
        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        recyclerView = findViewById(R.id.recyclerView);
        add = findViewById(R.id.add);

        statuss = findViewById(R.id.statuss);
        dates = findViewById(R.id.dates);
        approveds = findViewById(R.id.approveds);
        statusl = findViewById(R.id.statusl);
        datel = findViewById(R.id.datel);
        approvedl = findViewById(R.id.approvedl);
        edit = findViewById(R.id.edit);
        scroll = findViewById(R.id.scroll);
        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
        marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);
        district = findViewById(R.id.district);
        mandal = findViewById(R.id.mandal);
        statust = findViewById(R.id.statust);
        datet = findViewById(R.id.datet);
        approvedt = findViewById(R.id.approvedt);
        tech = findViewById(R.id.tech);
        add1 = findViewById(R.id.add1);
        recyclerVieww = findViewById(R.id.recyclerVieww);
        remarks1 = findViewById(R.id.remarks1);
        comments1 = findViewById(R.id.commentss);

        comments = findViewById(R.id.comments);




        if (getIntent() != null) {


            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Image = getIntent().getStringExtra("Image");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            //Status = getIntent().getStringExtra("Status");
            Propertydesc = getIntent().getStringExtra("Propertydesc");
            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            TechnicalStatus = getIntent().getStringExtra("TechnicalStatus");
            TechnicalDate = getIntent().getStringExtra("TechnicalDate");
            MobileNumber = getIntent().getStringExtra("MobileNumber");
            MobileNumber1 = getIntent().getStringExtra("MobileNumber1");
            ProposalNotes = getIntent().getStringExtra("ProposalNotes");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");
            key = getIntent().getStringExtra("key");
            date1.setText(MarketingDate);
            district.setText(District);
            mandal.setText(Mandal);
            comments.setText(Propertydesc);
        }

        comments1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement1.this, com.igrand.ThreeWaySolutions.Activities.MARKETING.Comments.class);
                intent.putExtra("ID",Id);

                startActivity(intent);
            }
        });


        if(Document.equals("")){

        }else {

            document2 = Document.split(",");

            strings = Arrays.asList(Document.split(","));

            for (int i = 0; i < strings.size(); i++) {


                document3 = strings.get(i).split("\\.");
                for (int j = 0; j < document2.length; j++) {

                    pdf = document2[j].substring(document2[j].length() - 3);
                    if (pdf.equals("pdf")) {

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsProcurement1.this, document2, Document, strings, "keyppp");
                        recyclerView.setAdapter(recyclerAdapter2);

                    }

                }

            }

        }
        imagedoc=Image.split(",");

        stringsimg = Arrays.asList(Image.split(","));

        recyclerVieww.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter3 = new RecyclerAdapterAgentImages(LeadDetailsProcurement1.this,imagedoc,stringsimg,Image,"keyprocurementadmin");
        recyclerVieww.setAdapter(recyclerAdapter3);

        if(key.equals("key")){

            add1.setVisibility(View.GONE);
            add.setVisibility(View.GONE);
            edit.setVisibility(View.GONE);

            procurement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*Intent intent=new Intent(LeadDetailsProcurement.this, ProposalDocuments.class);
                    intent.putExtra("ID",Id);
                    startActivity(intent);*/

                    Intent intent=new Intent(LeadDetailsProcurement1.this,ProcDocList.class);
                    intent.putExtra("ID",Id);
                    startActivity(intent);

                }
            });
        } else {

            add.setVisibility(View.VISIBLE);
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(LeadDetailsProcurement1.this,AddProposalDocuments.class);
                    intent.putExtra("MobileNumber",MobileNumber);
                    intent.putExtra("ID",Id);

                    startActivity(intent);

                }
            });

        }

        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement1.this,ProcDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });

        marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement1.this, MarketingDocList.class);
                intent.putExtra("ID",Id);
                intent.putExtra("Key","Key");
                startActivity(intent);
            }
        });

        remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement1.this, CheckingDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });



        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });



        tech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement1.this, TechDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadDetailsProcurement1.this, DashBoardAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber);
                intent.putExtra("Id", Id);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            }
        });



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent intent=new Intent(LeadDetailsProcurement1.this,EditProcurement.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("ID",Id);
                startActivity(intent);


            }
        });








        if (Checking.equals("0")) {

            status.setVisibility(View.VISIBLE);
            status.setText("Rejected");
            approved.setVisibility(View.VISIBLE);
            approved.setImageResource(R.drawable.rejected);
            date1.setText(CheckingDate);

        } else if (Checking.equals("1")) {
            status.setVisibility(View.VISIBLE);
            status.setText("Approved");
            approved.setVisibility(View.VISIBLE);
            approved.setImageResource(R.drawable.approved);
            date1.setText(CheckingDate);
        } else if (Checking.equals("2")) {
            status.setVisibility(View.VISIBLE);
            status.setText("Pending");
            approved.setVisibility(View.VISIBLE);
            approved.setImageResource(R.drawable.question);
            date1.setText(CheckingDate);
        }


        if (MarketingStatus != null) {

            if (MarketingStatus.equals("0")) {

                status11.setVisibility(View.VISIBLE);
                status11.setText("Rejected");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.rejected);
                date11.setText(MarketingDate);

            } else if (MarketingStatus.equals("1")) {
                status11.setVisibility(View.VISIBLE);
                status11.setText("Approved");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.approved);
                date11.setText(MarketingDate);
            } else if (MarketingStatus.equals("2")) {
                status11.setVisibility(View.VISIBLE);
                status11.setText("Processing");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.question);
                date11.setText(MarketingDate);
            } else if (MarketingStatus.equals("3")) {
                status11.setVisibility(View.VISIBLE);
                status11.setText("Open");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.open);
                date11.setText(MarketingDate);
            }

            if (TechnicalStatus != null) {

                if (TechnicalStatus.equals("2")) {

                    statust.setVisibility(View.VISIBLE);
                    statust.setText("Open");
                    approvedt.setVisibility(View.VISIBLE);
                    approvedt.setImageResource(R.drawable.rejected);
                    datet.setText(TechnicalDate);

                } else if (TechnicalStatus.equals("1")) {
                    statust.setVisibility(View.VISIBLE);
                    statust.setText("Approved");
                    approvedt.setVisibility(View.VISIBLE);
                    approvedt.setImageResource(R.drawable.approved);
                    datet.setText(TechnicalDate);
                    edit.setVisibility(View.GONE);
                } else if (TechnicalStatus.equals("0")) {
                    statust.setVisibility(View.VISIBLE);
                    statust.setText("Reject");
                    approvedt.setVisibility(View.VISIBLE);
                    approvedt.setImageResource(R.drawable.question);
                    datet.setText(TechnicalDate);
                    edit.setVisibility(View.GONE);
                }
            }


            if (ProcurementStatus != null) {

                if (ProcurementStatus.equals("4")) {
                    edit.setVisibility(View.GONE);
                    add.setVisibility(View.GONE);
                    statusp.setText("Rejected");
                    approvedp.setImageResource(R.drawable.rejected);
                    datep.setText(ProcurementDate);

                } else if (ProcurementStatus.equals("5")) {
                    edit.setVisibility(View.GONE);
                    add.setVisibility(View.VISIBLE);
                    statusp.setText("Approved");
                    approvedp.setImageResource(R.drawable.approved);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("3")) {
                    edit.setVisibility(View.VISIBLE);
                    add.setVisibility(View.GONE);
                    statusp.setText("Processing");
                    approvedp.setImageResource(R.drawable.processing);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("2")) {
                    edit.setVisibility(View.VISIBLE);
                    add.setVisibility(View.VISIBLE);
                    statusp.setText("Open");
                    approvedp.setImageResource(R.drawable.open);
                    datep.setText(ProcurementDate);
                }



            }




                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsProcurement1.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<LeadDetailResponse> call1 = apiInterface.leadDetailprocurement(MobileNumber1, Id);
                call1.enqueue(new Callback<LeadDetailResponse>() {
                    @Override
                    public void onResponse(Call<LeadDetailResponse> call, Response<LeadDetailResponse> response) {

                        if (response.code() == 200) {
                            scroll.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();

                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            LeadDetailResponse.DataBean dataBeans = response.body().getData();


                            id.setText(dataBeans.getId());
                            property.setText(dataBeans.getProperty_name());
                            date.setText(dataBeans.getDatetime());
                            village.setText(dataBeans.getVillage_name());
                            //google.setText(GoogleLocation);
                            address.setText(dataBeans.getAddress());
                            //remarks.setText(Comments);

                            // remarks1.setText(Comments);


                            if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(LeadDetailsProcurement1.this, "Error...", Toast.LENGTH_SHORT).show();

                            }

                        }
                    }


                    @Override
                    public void onFailure(Call<LeadDetailResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsProcurement1.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });
            }
        }
    }
