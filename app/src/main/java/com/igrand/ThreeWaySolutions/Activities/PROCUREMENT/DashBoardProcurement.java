package com.igrand.ThreeWaySolutions.Activities.PROCUREMENT;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ChangePassword;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.DashBoardMarketing;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerChecking;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerMarketing;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerProcurement;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMarketing;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProcurement;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.MarketingLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.NotificationCountResponse;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadsListResponse;

import java.util.HashMap;
import java.util.List;

public class DashBoardProcurement extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    ImageView toggle;
    RecyclerView recyclerView;
    RecyclerAdapterProcurement recyclerAdapterChecking;
    ImageView notification;
    LinearLayout add,logout1;
    ImageView edit;
    Boolean exit=false;
    String MobileNumber,MobileNumber1,UserName;
    ApiInterface apiInterface;
    ProcurementLeadsListResponse.StatusBean statusBean;
    PrefManagerProcurement prefManagerProcurement;
    TextView name;

    TextView txt_count;
    NotificationCountResponse.StatusBean statusBean3;
    RelativeLayout relative_image;
    Handler handler = new Handler();
    Runnable refresh;
    SwipeRefreshLayout pullToRefresh;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_procurement);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




        toggle=findViewById(R.id.toggle11);
        recyclerView=findViewById(R.id.recyclerView);
        add=findViewById(R.id.add);
        notification=findViewById(R.id.notification);
        logout1=findViewById(R.id.logout1);
        name=findViewById(R.id.name);
        txt_count=findViewById(R.id.txt_count);
        relative_image=findViewById(R.id.relative_image);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });

        prefManagerProcurement=new PrefManagerProcurement(DashBoardProcurement.this);
        HashMap<String, String> profile=prefManagerProcurement.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        UserName=profile.get("username");

        name.setText(UserName);
        if(getIntent()!=null){

           // MobileNumber=getIntent().getStringExtra("MobileNumber");
        }



                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<NotificationCountResponse> call1 = apiInterface.notificationcountprocurement(MobileNumber);
                call1.enqueue(new Callback<NotificationCountResponse>() {
                    @Override
                    public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                        if (response.code() == 200) {
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;


                            if(statusBean3.getCount()!=0){
                                relative_image.setVisibility(View.VISIBLE);
                                txt_count.setText(String.valueOf(statusBean3.getCount()));

                            }


                        } else if (response.code() != 200) {

                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardChecking.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
                    }
                });




        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerProcurement.clearSession();
                Intent intent = new Intent(DashBoardProcurement.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });



        final ProgressDialog progressDialog = new ProgressDialog(DashBoardProcurement.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProcurementLeadsListResponse> call = apiInterface.procurementLeadsList();
        call.enqueue(new Callback<ProcurementLeadsListResponse>() {
            @Override
            public void onResponse(Call<ProcurementLeadsListResponse> call, Response<ProcurementLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProcurementLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardProcurement.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardProcurement.this));
                    recyclerAdapterChecking = new RecyclerAdapterProcurement(DashBoardProcurement.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapterChecking);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardProcurement.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProcurementLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardProcurement.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });


        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardProcurement.this, NotificationsProcurement.class);
                startActivity(intent);
            }
        });

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void refreshData() {


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationCountResponse> call1 = apiInterface.notificationcountprocurement(MobileNumber);
        call1.enqueue(new Callback<NotificationCountResponse>() {
            @Override
            public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                if (response.code() == 200) {
                    statusBean3 = response.body() != null ? response.body().getStatus() : null;


                    if(statusBean3.getCount()!=0){
                        relative_image.setVisibility(View.VISIBLE);
                        txt_count.setText(String.valueOf(statusBean3.getCount()));

                    }


                } else if (response.code() != 200) {

                }
            }

            @Override
            public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardChecking.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
            }
        });




        final ProgressDialog progressDialog = new ProgressDialog(DashBoardProcurement.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProcurementLeadsListResponse> call = apiInterface.procurementLeadsList();
        call.enqueue(new Callback<ProcurementLeadsListResponse>() {
            @Override
            public void onResponse(Call<ProcurementLeadsListResponse> call, Response<ProcurementLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProcurementLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardProcurement.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardProcurement.this));
                    recyclerAdapterChecking = new RecyclerAdapterProcurement(DashBoardProcurement.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapterChecking);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardProcurement.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProcurementLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardProcurement.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });

    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardProcurement.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        item.setChecked(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawers();

        if (id == R.id.nav_home) {



        } else if (id == R.id.nav_wallet) {

           /* Intent intent = new Intent(DashBoardAgent.this, AddLeadsAgent.class);
            startActivity(intent);*/

        } else if (id == R.id.nav_digital) {

            Intent intent = new Intent(DashBoardProcurement.this, ProfileProcurement.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);


        } else if (id == R.id. nav_changepin) {

            Intent intent = new Intent(DashBoardProcurement.this, ChangePasswordProcurement.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);

        }else if (id == R.id.nav_notifications) {

            Intent intent = new Intent(DashBoardProcurement.this, NotificationsProcurement.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);


        }
/*else if (id == R.id.nav_doc) {

            Intent intent = new Intent(DashBoardProcurement.this, LegalTeamDocProcurement.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);


        }*/

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}

