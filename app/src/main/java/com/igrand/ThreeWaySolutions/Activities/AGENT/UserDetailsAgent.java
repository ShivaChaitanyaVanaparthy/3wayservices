package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.UserDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentLeads;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterUserLeads;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.UserDetailsAgentResponse;
import com.igrand.ThreeWaySolutions.Response.UserDetailsResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserDetailsAgent extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterAgentLeads recyclerAdapter;
    ImageView back;
    ApiInterface apiInterface;
    UserDetailsAgentResponse.StatusBean statusBean;
    ImageView image;
    TextView name,email,phone,leads;
    String MobileNumber,Profile,Email,Name,type;
    Integer sizee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details_agent);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        image=findViewById(R.id.image);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        phone=findViewById(R.id.phone);
        leads=findViewById(R.id.leads);

        SharedPreferences sharedPreference = getSharedPreferences("Data", Context.MODE_PRIVATE);
        type = sharedPreference.getString("b_type", "");

        if(getIntent()!=null){

            MobileNumber=getIntent().getStringExtra("MobileNumber");
            Profile=getIntent().getStringExtra("Profile");
            Email=getIntent().getStringExtra("Email");
            Name=getIntent().getStringExtra("Name");

            email.setText(Email);
            name.setText(Name);
            Picasso.get().load(Profile).error(R.drawable.profilepic).into(image);
            phone.setText(MobileNumber);

            Picasso.get().load(Profile).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(image);

        }

        final ProgressDialog progressDialog = new ProgressDialog(UserDetailsAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserDetailsAgentResponse> call = apiInterface.adminLeadsAgentDetails(MobileNumber);
        call.enqueue(new Callback<UserDetailsAgentResponse>() {
            @Override
            public void onResponse(Call<UserDetailsAgentResponse> call, Response<UserDetailsAgentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    recyclerView.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<UserDetailsAgentResponse.DataBean> dataBeans=response.body().getData();


                    recyclerView.setLayoutManager(new LinearLayoutManager(UserDetailsAgent.this));
                    recyclerAdapter = new RecyclerAdapterAgentLeads(UserDetailsAgent.this,dataBeans,sizee);
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.notifyDataSetChanged();

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(UserDetailsAgent.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<UserDetailsAgentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(UserDetailsAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    public void getSize(Integer sizee, Context context) {

        leads.setText(type);
    }
}
