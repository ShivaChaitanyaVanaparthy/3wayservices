package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddProjects;
import com.igrand.ThreeWaySolutions.Response.AdminValueResponse;

public class ValuesAdmin extends BaseActivity {

    EditText materialtype;
    ImageView back;
    Button add;
    ApiInterface apiInterface;
    AdminValueResponse.StatusBean statusBean;
    String Material,mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_values_admin);

        back=findViewById(R.id.back);
        materialtype=findViewById(R.id.materialtype);
        add=findViewById(R.id.add);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Material=materialtype.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(ValuesAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminValueResponse> call = apiInterface.adminAddValues(mobileNumber,Material);
                call.enqueue(new Callback<AdminValueResponse>() {
                    @Override
                    public void onResponse(Call<AdminValueResponse> call, Response<AdminValueResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(ValuesAdmin.this, "Values Added Successfully......", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(ValuesAdmin.this, ValuesList.class);
                            intent.putExtra("MobileNumber",mobileNumber);
                            intent.putExtra("keytech","keytech");
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ValuesAdmin.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminValueResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(ValuesAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });





            }
        });



    }
}
