package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments2;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments3;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.R;

import java.util.Arrays;
import java.util.List;

public class NearbyProjectsDetails extends BaseActivity {

    TextView date,id,projectname,acres,plotsize,fastmoving,cost,googlelocation,comment;
    ImageView broucher,image;
    String Date,Project,Acres,Plotsize,FastMoving,GoogleLocation,Broucher,Image,ID,Cost;
    ImageView back;
    String Document,Comment,pdf;
    String[] document2,document3,imagedoc;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;
    RecyclerAdapterAgentImages recyclerAdapter3;
    RecyclerView recyclerView,recyclerView1;
    List<String> strings,stringsimg;
    RecyclerAdapterAgentDocuments recyclerAdapter1;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_projects_details);

        date=findViewById(R.id.date);
        id=findViewById(R.id.Id);
        projectname=findViewById(R.id.projectname);
        acres=findViewById(R.id.acres);
        plotsize=findViewById(R.id.plotsize);
        fastmoving=findViewById(R.id.fastmoving);
        cost=findViewById(R.id.cost);
        googlelocation=findViewById(R.id.googlelocation);
       // broucher=findViewById(R.id.broucher);
        image=findViewById(R.id.image);
        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView1=findViewById(R.id.recyclerView1);
        comment=findViewById(R.id.comment);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(getIntent()!=null){


            Date=getIntent().getStringExtra("Date");
            Project=getIntent().getStringExtra("Project");
            Acres=getIntent().getStringExtra("Acres");
            Plotsize=getIntent().getStringExtra("Plotsize");
            FastMoving=getIntent().getStringExtra("FastMoving");
            GoogleLocation=getIntent().getStringExtra("GoogleLocation");
            Broucher=getIntent().getStringExtra("Broucher");
            Image=getIntent().getStringExtra("Image");
            ID=getIntent().getStringExtra("ID");
            Cost=getIntent().getStringExtra("Cost");
            Comment=getIntent().getStringExtra("Comment");


        }

        if(Broucher.equals("")){

        }else {




            document2 = Broucher.split(",");

            strings = Arrays.asList(Broucher.split(","));

            for (int i = 0; i < strings.size(); i++) {


                document3 = strings.get(i).split("\\.");
                for (int j = 0; j < document2.length; j++) {

                    pdf = document2[j].substring(document2[j].length() - 3);
                    if (pdf.equals("pdf")) {

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(NearbyProjectsDetails.this, document2, Broucher, strings, "key5");
                        recyclerView.setAdapter(recyclerAdapter2);

                    } /*else {
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter1 = new RecyclerAdapterAgentDocuments(NearbyProjectsDetails.this, document2, strings, Broucher);
                        recyclerView.setAdapter(recyclerAdapter1);

                    }*/

                }

            }

        }
        imagedoc=Image.split(",");

        stringsimg = Arrays.asList(Image.split(","));

        recyclerView1.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter3 = new RecyclerAdapterAgentImages(NearbyProjectsDetails.this,imagedoc,stringsimg,Image,"key");
        recyclerView1.setAdapter(recyclerAdapter3);

/*

        document2= Broucher.split(",");

        recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjectsDetails.this,LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter2 = new RecyclerAdapterCheckingDocuments2(NearbyProjectsDetails.this,document2,Broucher);
        recyclerView.setAdapter(recyclerAdapter2);
*/


       /* document3= Image.split(",");

        recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjectsDetails.this,LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter3 = new RecyclerAdapterCheckingDocuments3(NearbyProjectsDetails.this,document3,Image);
        recyclerView1.setAdapter(recyclerAdapter3);*/


        date.setText(Date);
        projectname.setText(Project);
        id.setText(ID);
        acres.setText(Acres);
        plotsize.setText(Plotsize);
        fastmoving.setText(FastMoving);
        cost.setText(Cost);
        googlelocation.setText(GoogleLocation);
    }
}
