package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProjectList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterVendorList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.AdminVendorList;
import com.igrand.ThreeWaySolutions.Response.VendorListResponse;

import java.util.List;

public class VendorsList extends BaseActivity {


    RecyclerView recyclerView,recyclerView1,recyclerView2,recyclerView3;
    ImageView back;
    RecyclerAdapterVendorList recyclerUser;
    ApiInterface apiInterface;
    VendorListResponse.StatusBean statusBean;
    Button add;
    String mobileNumber,keytech,ID;
    NestedScrollView scrollview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors_list);


        recyclerView=findViewById(R.id.recyclerView);
        recyclerView1=findViewById(R.id.recyclerView1);
        recyclerView2=findViewById(R.id.recyclerView2);
        recyclerView3=findViewById(R.id.recyclerView3);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        scrollview=findViewById(R.id.scrollview);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            keytech=getIntent().getStringExtra("keytech");
            ID=getIntent().getStringExtra("ID");
        }



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent intent=new Intent(VendorsList.this,DashBoardAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/

              finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(VendorsList.this,VendorsAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                intent.putExtra("ID",ID);
                startActivity(intent);
            }
        });




        final ProgressDialog progressDialog = new ProgressDialog(VendorsList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<VendorListResponse> call = apiInterface.adminVendorList(ID,"1");
        call.enqueue(new Callback<VendorListResponse>() {
            @Override
            public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    scrollview.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<VendorListResponse.StatusBean.VendorsBean> vendorsBeans=response.body().getStatus().getVendors();
                    recyclerView.setLayoutManager(new LinearLayoutManager(VendorsList.this));
                    recyclerUser = new RecyclerAdapterVendorList(getApplicationContext(),vendorsBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();

                }

            }


            @Override
            public void onFailure(Call<VendorListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(VendorsList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<VendorListResponse> call1 = apiInterface.adminVendorList(ID,"2");
        call1.enqueue(new Callback<VendorListResponse>() {
            @Override
            public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                if (response.code() == 200) {
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<VendorListResponse.StatusBean.VendorsBean> vendorsBeans=response.body().getStatus().getVendors();
                    recyclerView1.setLayoutManager(new LinearLayoutManager(VendorsList.this));
                    recyclerUser = new RecyclerAdapterVendorList(VendorsList.this,vendorsBeans);
                    recyclerView1.setAdapter(recyclerUser);

                } else if (response.code() != 200) {


                }

            }


            @Override
            public void onFailure(Call<VendorListResponse> call, Throwable t) {
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(VendorsList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
 apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<VendorListResponse> call2 = apiInterface.adminVendorList(ID,"3");
        call2.enqueue(new Callback<VendorListResponse>() {
            @Override
            public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                if (response.code() == 200) {
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<VendorListResponse.StatusBean.VendorsBean> vendorsBeans=response.body().getStatus().getVendors();
                    recyclerView2.setLayoutManager(new LinearLayoutManager(VendorsList.this));
                    recyclerUser = new RecyclerAdapterVendorList(VendorsList.this,vendorsBeans);
                    recyclerView2.setAdapter(recyclerUser);

                } else if (response.code() != 200) {


                }

            }


            @Override
            public void onFailure(Call<VendorListResponse> call, Throwable t) {
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(VendorsList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<VendorListResponse> call3 = apiInterface.adminVendorList(ID,"4");
        call3.enqueue(new Callback<VendorListResponse>() {
            @Override
            public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                if (response.code() == 200) {
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<VendorListResponse.StatusBean.VendorsBean> vendorsBeans=response.body().getStatus().getVendors();
                    recyclerView3.setLayoutManager(new LinearLayoutManager(VendorsList.this));
                    recyclerUser = new RecyclerAdapterVendorList(VendorsList.this,vendorsBeans);
                    recyclerView3.setAdapter(recyclerUser);

                } else if (response.code() != 200) {


                }

            }


            @Override
            public void onFailure(Call<VendorListResponse> call, Throwable t) {
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(VendorsList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }
}
