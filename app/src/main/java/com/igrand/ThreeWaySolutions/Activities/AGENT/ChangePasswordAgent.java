package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.ChangePasswordAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ChangePasswordResponse;

public class ChangePasswordAgent extends BaseActivity {


    Button changepassword;
    ImageView back;
    String mobileNumber,Password,NewPassword,ConfirmPassword,AgentId;
    EditText password1,password2,confirmpassword;
    ApiInterface apiInterface;
    ChangePasswordResponse.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_agent);

        changepassword=findViewById(R.id.changepasswordcheck);
        back=findViewById(R.id.back);
        password1=findViewById(R.id.password1);
        password2=findViewById(R.id.password2);
        confirmpassword=findViewById(R.id.confirmpassword);

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            AgentId=getIntent().getStringExtra("AgentId");
        }


        password1.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do something, e.g. set your TextView here via .setText()
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        password1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value = String.valueOf(s.length());

                if (value.equals("6")) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });




        password2.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do something, e.g. set your TextView here via .setText()
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        password2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value = String.valueOf(s.length());

                if (value.equals("6")) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });




        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }


        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Password = password1.getText().toString();
                NewPassword = password2.getText().toString();
                ConfirmPassword = confirmpassword.getText().toString();


                if(NewPassword.equals(ConfirmPassword)) {
                if (!TextUtils.isEmpty(Password) && !TextUtils.isEmpty(NewPassword) && !TextUtils.isEmpty(ConfirmPassword)) {


                    final ProgressDialog progressDialog = new ProgressDialog(ChangePasswordAgent.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<ChangePasswordResponse> call = apiInterface.agentChangePassword1(mobileNumber, Password, NewPassword);
                    call.enqueue(new Callback<ChangePasswordResponse>() {
                        @Override
                        public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(ChangePasswordAgent.this, "Password Changed Successfully...", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ChangePasswordAgent.this, Login.class);
                                startActivity(intent);
                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(ChangePasswordAgent.this, "Please Check the Password and try again...", Toast.LENGTH_SHORT).show();

                            }

                        }

                        @Override
                        public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast = Toast.makeText(ChangePasswordAgent.this,
                                    t.getMessage(), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });




                } else {
                    Toast.makeText(ChangePasswordAgent.this, "Please Change your Password", Toast.LENGTH_SHORT).show();

                } } else {
                    Toast.makeText(ChangePasswordAgent.this, "Please check Confirm Password...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}

