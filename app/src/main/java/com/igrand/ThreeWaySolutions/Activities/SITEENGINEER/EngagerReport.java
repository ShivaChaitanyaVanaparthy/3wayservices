package com.igrand.ThreeWaySolutions.Activities.SITEENGINEER;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddEngagerReport;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerEngagerReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkName;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class EngagerReport extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerEngagerReport recyclerUser;
    Button add,submit;
    TextView contractor;
    String ID,Project,key,key1,PersonId;
    ApiInterface apiInterface;
    AdminEngagerList.StatusBean statusBean;
    ImageView back;
    RecyclerWorkName recyclerUser1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engager_report);
        add=findViewById(R.id.add);
        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        contractor = findViewById(R.id.contractor);
        submit = findViewById(R.id.submit);



        if(getIntent()!=null){

            ID = getIntent().getStringExtra("ID");
            Project = getIntent().getStringExtra("Project");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
            //SUM = args.getString("SUM");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(EngagerReport.this,SiteEngineerProjectDetails.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                intent.putExtra("keysite","keysite");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                //finish();
            }
        });


        getEngagerReport();


        contractor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(EngagerReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(EngagerReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminEngagerList> call = apiInterface.adminEngagerReportList(ID);
                call.enqueue(new Callback<AdminEngagerList>() {
                    @Override
                    public void onResponse(Call<AdminEngagerList> call, Response<AdminEngagerList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AdminEngagerList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(EngagerReport.this));
                            recyclerUser1 = new RecyclerWorkName(EngagerReport.this, dataBeans,contractor, dialog,add);
                            recyclerView.setAdapter(recyclerUser1);


                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status = error.getStatus();
                                Toast.makeText(EngagerReport.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                        }

                    }


                    @Override
                    public void onFailure(Call<AdminEngagerList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(EngagerReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });
            }
        });


        submit.setOnClickListener(view -> {

            final ProgressDialog progressDialog = new ProgressDialog(EngagerReport.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AdminEngagerList> call = apiInterface.vendorwiseEngager(ID, PersonId);
            call.enqueue(new Callback<AdminEngagerList>() {
                @Override
                public void onResponse(Call<AdminEngagerList> call, Response<AdminEngagerList> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<AdminEngagerList.DataBean> dataBeans = response.body().getData();
                        recyclerView.setLayoutManager(new LinearLayoutManager(EngagerReport.this));
                        recyclerUser = new RecyclerEngagerReport(EngagerReport.this, dataBeans);
                        recyclerView.setAdapter(recyclerUser);


                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIError> converter =
                                ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                        APIError error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIError.StatusBean status = error.getStatus();
                            Toast.makeText(EngagerReport.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                }


                @Override
                public void onFailure(Call<AdminEngagerList> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(EngagerReport.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });


            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(EngagerReport.this, AddEngagerReport.class);
                    intent.putExtra("ID", ID);
                    intent.putExtra("key", "key");
                    intent.putExtra("key1", "key1");
                    intent.putExtra("Project", Project);
                    startActivity(intent);
                }
            });

        });
    }

    private void getEngagerReport() {

        final ProgressDialog progressDialog = new ProgressDialog(EngagerReport.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminEngagerList> call = apiInterface.adminEngagerReportList(ID);
        call.enqueue(new Callback<AdminEngagerList>() {
            @Override
            public void onResponse(Call<AdminEngagerList> call, Response<AdminEngagerList> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AdminEngagerList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(EngagerReport.this));
                    recyclerUser = new RecyclerEngagerReport(EngagerReport.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(EngagerReport.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<AdminEngagerList> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(EngagerReport.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getEngagerReport();
    }

    public void getId0(String workid) {
        PersonId=workid;
    }
}