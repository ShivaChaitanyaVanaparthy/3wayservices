package com.igrand.ThreeWaySolutions.Activities.PROCUREMENT;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.EditUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.EditLeadsProcurementResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsProcurementResponse;
/*import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;*/
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProcurement extends BaseActivity {

    ImageView back,imageupload;
     RadioGroup rg;
     RadioButton accept, reject, open, processing;
     EditText comment;
    Button submit;
     LinearLayout linear;
     ApiInterface apiInterface;
    EditLeadsProcurementResponse.StatusBean statusBean1;
    UpdateLeadsProcurementResponse.StatusBean statusBean2;
    String Id,MobileNumber,ct_status,ct_comments,imagePic,Image,document1,picturePath;
    String emptydocument="";
    //private ArrayList<Image> images;

    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_procurement);

        back=findViewById(R.id.back);
        rg = findViewById(R.id.rg);
        accept = findViewById(R.id.approved);
        reject = findViewById(R.id.reject);
        open = findViewById(R.id.open);
        processing = findViewById(R.id.processing);
        comment = findViewById(R.id.comments);
        submit = findViewById(R.id.submit);
        linear = findViewById(R.id.linear);
        imageupload = findViewById(R.id.imageupload);

        if (getIntent() != null) {

            Id = getIntent().getStringExtra("ID");
            MobileNumber = getIntent().getStringExtra("MobileNumber");
        }



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(EditProcurement.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditLeadsProcurementResponse> call1 = apiInterface.editLeadsProcurement(Id, MobileNumber);
        call1.enqueue(new Callback<EditLeadsProcurementResponse>() {
            @Override
            public void onResponse(Call<EditLeadsProcurementResponse> call, Response<EditLeadsProcurementResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    List<EditLeadsProcurementResponse.DataBean> dataBeans = response.body().getData();



                    if (dataBeans.get(0).getPt_status().equals("4")) {
                        rg.check(R.id.reject);
                    } else if (dataBeans.get(0).getPt_status().equals("5")) {
                        rg.check(R.id.accept);
                    } else if (dataBeans.get(0).getPt_status().equals("3")) {
                        rg.check(R.id.processing);
                    } else if (dataBeans.get(0).getPt_status().equals("2")) {
                        rg.check(R.id.open);
                    }


                     comment.setText(dataBeans.get(0).getPro_notes());
                    Image=dataBeans.get(0).getPro_doc();
                    document1="http://igrandit.site/3way-services/admin_assets/uploads/leads/proposal_documents/"+Image;
                    Picasso.get().load(document1).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(imageupload);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProcurement.this, "Data already uploaded...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<EditLeadsProcurementResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(EditProcurement.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




        imageupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);

                /*ImagePicker.with(EditProcurement.this)                         //  Initialize ImagePicker with activity or fragment context
                        .setToolbarColor("#212121")         //  Toolbar color
                        .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
                        .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                        .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                        .setProgressBarColor("#4CAF50")     //  ProgressBar color
                        .setBackgroundColor("#212121")      //  Background color
                        .setCameraOnly(false)               //  Camera mode
                        .setMultipleMode(false)              //  Select multiple images or single image
                        .setFolderMode(true)                //  Folder mode
                        .setShowCamera(true)                //  Show camera button
                        .setFolderTitle("Albums")           //  Folder title (works with FolderMode = true)
                        .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
                        .setDoneTitle("Done")               //  Done button title
                        .setLimitMessage("You have reached selection limit")    // Selection limit message
                        .setMaxSize(10)                     //  Max images can be selected
                        .setSavePath("ImagePicker")         //  Image capture folder name
                        .setSelectedImages(images)          //  Selected images
                        .setAlwaysShowDoneButton(true)      //  Set always show done button in multiple mode
                        .setKeepScreenOn(true)              //  Keep screen on when selecting images
                        .start();*/
            }


        });



        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (accept.isChecked()) {

                    ct_status = "5";


                } else if (reject.isChecked()) {

                    ct_status = "4";

                } else if (processing.isChecked()) {

                    ct_status = "3";

                } else if (open.isChecked()) {

                    ct_status = "2";

                }

                ct_comments = comment.getText().toString();


                updateStatus(Id,MobileNumber,ct_status,ct_comments);




            }
        });

    }



    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       /* if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);

            try {
                if (images != null) {
                    for (int i = 0; i < images.size(); i++) {
                        Uri uri = Uri.fromFile(new File(images.get(0).getPath()));
                        Picasso.get().load(uri).into(imageupload);
//                        Picasso.with(getApplicationContext()).load(uri).into(profile_image);
                        image = null;
                        String filepath = images.get(0).getPath();
                        if (filepath != null && !filepath.equals("")) {
                            image = new File(filepath);
                        }
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }*/
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);



            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void updateStatus(String id, String mobileNumber, String ct_status, String ct_comments) {

        final ProgressDialog progressDialog=new ProgressDialog(EditProcurement.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("proposal_document[]", image.getName(), requestFile);
        }else {

            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
            body = MultipartBody.Part.createFormData("proposal_document[]", emptydocument, requestFile);
        }

            RequestBody ID1 = RequestBody.create(MediaType.parse("multipart/form-data"), id);
            //final RequestBody MobileNumber1 = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
            RequestBody StatusActive = RequestBody.create(MediaType.parse("multipart/form-data"), ct_status);
            RequestBody Comments = RequestBody.create(MediaType.parse("multipart/form-data"), ct_comments);

            Call<UpdateLeadsProcurementResponse> call1 = apiInterface.updateLeadsProcurement(ID1, StatusActive, Comments, body);
            call1.enqueue(new Callback<UpdateLeadsProcurementResponse>() {
                @Override
                public void onResponse(Call<UpdateLeadsProcurementResponse> call, Response<UpdateLeadsProcurementResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean2 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(EditProcurement.this, "Leads Updated Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplication(), DashBoardProcurement.class);
                        intent.putExtra("MobileNumber", MobileNumber);
                        startActivity(intent);
                        //comment11.setVisibility(View.VISIBLE);


                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(EditProcurement.this, "Error...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<UpdateLeadsProcurementResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(EditProcurement.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });






    }

}

