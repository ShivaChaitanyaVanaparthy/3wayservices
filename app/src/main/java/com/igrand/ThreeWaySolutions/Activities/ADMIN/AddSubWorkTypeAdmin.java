package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubworkWorkList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterWorkTypeList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddWorkType;
import com.igrand.ThreeWaySolutions.Response.AdminSubAddWorkType;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

public class AddSubWorkTypeAdmin extends BaseActivity {

    ImageView back;
    ApiInterface apiInterface;
    EditText villagename;
    TextView cityname;
    EditText subwork,itemno;
    RadioButton active,inactive;
    RadioGroup rg;
    AdminSubAddWorkType.StatusBean statusBean;
    String mobileNumber,Project,Village,Status;
    Button add1;
    AdminWorkTypeList.StatusBean statusBean1;
    RecyclerAdapterSubworkWorkList recyclerAdapter;
    String SubWork,ItemNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sub_work_type_admin);



        back=findViewById(R.id.back);

        cityname=findViewById(R.id.cityname);
        add1=findViewById(R.id.add);
        itemno=findViewById(R.id.itemno);
        subwork=findViewById(R.id.subwork);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        cityname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddSubWorkTypeAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);



                final ProgressDialog progressDialog = new ProgressDialog(AddSubWorkTypeAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans=response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddSubWorkTypeAdmin.this));
                            recyclerAdapter = new RecyclerAdapterSubworkWorkList(AddSubWorkTypeAdmin.this,dataBeans,cityname,dialog,AddSubWorkTypeAdmin.this);
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddSubWorkTypeAdmin.this, "No City's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(AddSubWorkTypeAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });





            }
        });




    }

    public void getId(final String selectedworkid) {




        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Project=cityname.getText().toString();
                SubWork=subwork.getText().toString();
                ItemNo=itemno.getText().toString();



                final ProgressDialog progressDialog = new ProgressDialog(AddSubWorkTypeAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminSubAddWorkType> call = apiInterface.adminAddSubWorkType(mobileNumber,selectedworkid,SubWork,ItemNo,"1");
                call.enqueue(new Callback<AdminSubAddWorkType>() {
                    @Override
                    public void onResponse(Call<AdminSubAddWorkType> call, Response<AdminSubAddWorkType> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(AddSubWorkTypeAdmin.this, "Project Added Successfully......", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(AddSubWorkTypeAdmin.this, SubWorkTypeList.class);
                            intent.putExtra("MobileNumber",mobileNumber);
                            intent.putExtra("keytech","keytech");
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddSubWorkTypeAdmin.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminSubAddWorkType> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(AddSubWorkTypeAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });
            }
        });



    }
}
