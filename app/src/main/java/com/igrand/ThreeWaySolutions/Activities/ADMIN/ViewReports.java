package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.MeasurementSheet;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEngagerProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEngagorReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMeasurementReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSupplierReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerMaterialList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerMeasurementList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;
import com.igrand.ThreeWaySolutions.Response.MaterialListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementSheetResponse;
import com.igrand.ThreeWaySolutions.Response.SupplierReportResponse;

import org.w3c.dom.Text;

import java.util.List;

public class ViewReports extends BaseActivity {

    RecyclerView recyclerView1, recyclerView2, recyclerView3,recyclerView4,recyclerView5;
    ApiInterface apiInterface;
    RecyclerAdapterSupplierReport recyclerAdapterContractorProjectsList;
    RecyclerAdapterEngagorReport recyclerAdapterContractorProjectsList1;
    RecyclerWorkReport recyclerAdapterContractorProjectsList2;
    SupplierReportResponse.StatusBean statusBean;
    AdminEngagerList.StatusBean statusBean1;
    AdminWorkReportList.StatusBean statusBean2;
    MaterialListResponse.StatusBean statusBean3;
    String Date, WorkId;
    TextView supplier,engagor,drp,machinery,measurement;
    ImageView back;
    RecyclerMaterialList recyclerUser;
    RecyclerMeasurementList recyclerUser1;
    MeasurementSheetResponse.StatusBean statusBean5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_reports);

        recyclerView1 = findViewById(R.id.recyclerView1);
        recyclerView2 = findViewById(R.id.recyclerView2);
        recyclerView3 = findViewById(R.id.recyclerView3);
        recyclerView4 = findViewById(R.id.recyclerView4);
        recyclerView5 = findViewById(R.id.recyclerView5);
        supplier = findViewById(R.id.supplier);
        engagor = findViewById(R.id.engagor);
        drp = findViewById(R.id.dpr);
        machinery = findViewById(R.id.machinery);
        measurement = findViewById(R.id.measurement);
        back = findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (getIntent() != null) {

            WorkId = getIntent().getStringExtra("WorkId");
            Date = getIntent().getStringExtra("Date");
        }


        final ProgressDialog progressDialog = new ProgressDialog(ViewReports.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SupplierReportResponse> call = apiInterface.adminMachinertReportList1(WorkId, Date);
        call.enqueue(new Callback<SupplierReportResponse>() {
            @Override
            public void onResponse(Call<SupplierReportResponse> call, Response<SupplierReportResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    supplier.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<SupplierReportResponse.DataBean> dataBeans = response.body().getData();
                    recyclerView1.setLayoutManager(new LinearLayoutManager(ViewReports.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterSupplierReport(ViewReports.this, dataBeans);
                    recyclerView1.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ViewReports.this, "No Supplier Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<SupplierReportResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ViewReports.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });



        final ProgressDialog progressDialog1 = new ProgressDialog(ViewReports.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminEngagerList> call1 = apiInterface.adminEngagerReportList1(WorkId, Date);
        call1.enqueue(new Callback<AdminEngagerList>() {
            @Override
            public void onResponse(Call<AdminEngagerList> call, Response<AdminEngagerList> response) {

                if (response.code() == 200) {
                    progressDialog1.dismiss();
                    engagor.setVisibility(View.VISIBLE);
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminEngagerList.DataBean> dataBeans = response.body().getData();
                    recyclerView2.setLayoutManager(new LinearLayoutManager(ViewReports.this));
                    recyclerAdapterContractorProjectsList1 = new RecyclerAdapterEngagorReport(ViewReports.this, dataBeans);
                    recyclerView2.setAdapter(recyclerAdapterContractorProjectsList1);

                } else if (response.code() != 200) {
                    progressDialog1.dismiss();
                    Toast.makeText(ViewReports.this, "No Engagor Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminEngagerList> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ViewReports.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });



        final ProgressDialog progressDialog2 = new ProgressDialog(ViewReports.this);
        progressDialog2.setMessage("Loading.....");
        progressDialog2.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminWorkReportList> call2 = apiInterface.adminWorkReportList1(WorkId, Date);
        call2.enqueue(new Callback<AdminWorkReportList>() {
            @Override
            public void onResponse(Call<AdminWorkReportList> call, Response<AdminWorkReportList> response) {

                if (response.code() == 200) {
                    progressDialog2.dismiss();
                    drp.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminWorkReportList.DataBean> dataBeans = response.body().getData();
                    recyclerView3.setLayoutManager(new LinearLayoutManager(ViewReports.this));
                    recyclerAdapterContractorProjectsList2 = new RecyclerWorkReport(ViewReports.this, dataBeans);
                    recyclerView3.setAdapter(recyclerAdapterContractorProjectsList2);

                } else if (response.code() != 200) {
                    progressDialog2.dismiss();
                    Toast.makeText(ViewReports.this, "No Work Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminWorkReportList> call, Throwable t) {
                progressDialog2.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ViewReports.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });

        final ProgressDialog progressDialog3 = new ProgressDialog(ViewReports.this);
        progressDialog2.setMessage("Loading.....");
        progressDialog2.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MaterialListResponse> call3 = apiInterface.adminWorkReportList3(WorkId, Date);
        call3.enqueue(new Callback<MaterialListResponse>() {
            @Override
            public void onResponse(Call<MaterialListResponse> call, Response<MaterialListResponse> response) {

                if (response.code() == 200) {
                    progressDialog3.dismiss();
                    machinery.setVisibility(View.VISIBLE);
                    statusBean3 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<MaterialListResponse.DataBean> dataBeans = response.body().getData();
                    recyclerView4.setLayoutManager(new LinearLayoutManager(ViewReports.this));
                    recyclerUser = new RecyclerMaterialList(ViewReports.this,dataBeans);
                    recyclerView4.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog3.dismiss();
                    //Toast.makeText(ViewReports.this, "No Work Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<MaterialListResponse> call, Throwable t) {
                progressDialog3.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ViewReports.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });

        final ProgressDialog progressDialog4 = new ProgressDialog(ViewReports.this);
        progressDialog2.setMessage("Loading.....");
        progressDialog2.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MeasurementSheetResponse> call4 = apiInterface.adminWorkReportList4(WorkId, Date);
        call4.enqueue(new Callback<MeasurementSheetResponse>() {
            @Override
            public void onResponse(Call<MeasurementSheetResponse> call, Response<MeasurementSheetResponse> response) {

                if (response.code() == 200) {
                    progressDialog4.dismiss();
                    measurement.setVisibility(View.VISIBLE);
                    statusBean5 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<MeasurementSheetResponse.DataBean> dataBeans = response.body().getData();
                    recyclerView5.setLayoutManager(new LinearLayoutManager(ViewReports.this));
                    recyclerUser1 = new RecyclerMeasurementList(ViewReports.this,dataBeans);
                    recyclerView5.setAdapter(recyclerUser1);

                } else if (response.code() != 200) {
                    progressDialog4.dismiss();
                    //Toast.makeText(ViewReports.this, "No Work Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<MeasurementSheetResponse> call, Throwable t) {
                progressDialog4.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ViewReports.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });
    }
}
