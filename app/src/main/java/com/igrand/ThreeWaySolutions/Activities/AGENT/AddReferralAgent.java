package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddEarn;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.EarnsList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddEarnResponse;
import com.igrand.ThreeWaySolutions.Response.AddReferralAgentResponse;

import java.util.HashMap;

public class AddReferralAgent extends BaseActivity {

    EditText name,phone;
    String Name,Phone,MobileNumber,Id;
    ImageView back;
    Button add;
    ApiInterface apiInterface;
    AddReferralAgentResponse.StatusBean statusBean;
    PrefManagerAgent prefManagerAgent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referrals_list_agent);

        name=findViewById(R.id.name);
        phone=findViewById(R.id.phone);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);

        prefManagerAgent=new PrefManagerAgent(AddReferralAgent.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        MobileNumber=profile.get("mobilenumber");

        if(getIntent()!=null){
            Id=getIntent().getStringExtra("Id");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Name=name.getText().toString();
                Phone=phone.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(AddReferralAgent.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddReferralAgentResponse> call = apiInterface.addReferral(Id,MobileNumber,Name,Phone);
                call.enqueue(new Callback<AddReferralAgentResponse>() {
                    @Override
                    public void onResponse(Call<AddReferralAgentResponse> call, Response<AddReferralAgentResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Intent intent=new Intent(AddReferralAgent.this, ReferralsListAgent.class);
                            startActivity(intent);


                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddReferralAgent.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AddReferralAgentResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddReferralAgent.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });

    }
}
