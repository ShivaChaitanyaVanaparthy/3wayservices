package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterComments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterNearByProjectList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.CommentResponse;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class Comments extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterComments recyclerAdapterComments;
    ApiInterface apiInterface;
    ImageView back;
    String ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);

        if(getIntent()!=null){


            ID=getIntent().getStringExtra("ID");

        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });





        final ProgressDialog progressDialog = new ProgressDialog(Comments.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CommentResponse> call = apiInterface.comments(ID);
        call.enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    CommentResponse.StatusBean statusBean = response.body() != null ? response.body().getStatus() : null;

                    List<CommentResponse.DataBean> dataBeans=response.body().getData();


                    recyclerView.setLayoutManager(new LinearLayoutManager(Comments.this));
                    recyclerAdapterComments = new RecyclerAdapterComments(Comments.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapterComments);



                } else if(response.code()==404)
                {

                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(Comments.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }


            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(Comments.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });






    }
}
