package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEngagerProjectsList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddEarnResponse;
import com.igrand.ThreeWaySolutions.Response.AdminAddEngager;

import java.util.List;

public class AddEarn extends BaseActivity {

    EditText title,desc;
    String Title,Desc,Active;
    ImageView back;
    Button add;
    RadioButton active,inactive;
    RadioGroup rg;
    ApiInterface apiInterface;
    AddEarnResponse.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_earn);

        title=findViewById(R.id.title);
        desc=findViewById(R.id.desc);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        active=findViewById(R.id.active);
        inactive=findViewById(R.id.inactive);
        rg=findViewById(R.id.rg);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Title=title.getText().toString();
                Desc=desc.getText().toString();


                if (active.isChecked()) {

                    Active = "1";


                } else if (inactive.isChecked()) {

                    Active = "0";

                }

                final ProgressDialog progressDialog = new ProgressDialog(AddEarn.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddEarnResponse> call = apiInterface.addEarn(Title,Desc,Active);
                call.enqueue(new Callback<AddEarnResponse>() {
                    @Override
                    public void onResponse(Call<AddEarnResponse> call, Response<AddEarnResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Intent intent=new Intent(AddEarn.this,EarnsList.class);
                            startActivity(intent);


                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddEarn.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AddEarnResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddEarn.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });




    }
}
