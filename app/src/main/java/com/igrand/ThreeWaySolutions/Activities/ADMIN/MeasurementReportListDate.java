package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMeasurementReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSupplierReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.MeasurementListResponse;
import com.igrand.ThreeWaySolutions.Response.SupplierReportResponse;

import java.util.List;

public class MeasurementReportListDate extends BaseActivity {

    ApiInterface apiInterface;
    RecyclerView recyclerView3;
    RecyclerAdapterMeasurementReport recyclerAdapterContractorProjectsList;
    MeasurementListResponse.StatusBean statusBean;
    String ID,Date;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement_report_list_date);

        recyclerView3 = findViewById(R.id.recyclerView3);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Date=getIntent().getStringExtra("Date");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(MeasurementReportListDate.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MeasurementListResponse> call = apiInterface.adminMeasurementReportList(ID, Date);
        call.enqueue(new Callback<MeasurementListResponse>() {
            @Override
            public void onResponse(Call<MeasurementListResponse> call, Response<MeasurementListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<MeasurementListResponse.DataBean> dataBeans = response.body().getData();
                    recyclerView3.setLayoutManager(new LinearLayoutManager(MeasurementReportListDate.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterMeasurementReport(MeasurementReportListDate.this, dataBeans);
                    recyclerView3.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(MeasurementReportListDate.this, "No Measurement Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<MeasurementListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(MeasurementReportListDate.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });
    }
}