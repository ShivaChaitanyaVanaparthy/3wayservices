package com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NotificationsAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ChangePasswordAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadsListAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.NotificationsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PrefManagerAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ProfileAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.UsersListAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerSubAgent;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.SubAgentLeadsListResponse;

import java.util.HashMap;
import java.util.List;

public class DashBoardSubAgent extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    ImageView toggle;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ImageView notification;
    LinearLayout add,users,logout1;
    Boolean exit=false;
    ApiInterface apiInterface;
    SubAgentLeadsListResponse.StatusBean statusBean;
    String Name,agentid,username,MobileNumber1;
    LinearLayout leads;
    TextView name,size1;
    PrefManagerSubAgent prefManagerSubAgent;
    SwipeRefreshLayout pullToRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_sub_agent);


        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        toggle=findViewById(R.id.toggle11);
        leads=findViewById(R.id.leads);
        recyclerView=findViewById(R.id.recyclerView);
        add=findViewById(R.id.add);
        notification=findViewById(R.id.notification);
        users=findViewById(R.id.users);
        logout1=findViewById(R.id.logout1);
        size1=findViewById(R.id.size);
        name=findViewById(R.id.name);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });


        prefManagerSubAgent=new PrefManagerSubAgent(DashBoardSubAgent.this);
        HashMap<String, String> profile=prefManagerSubAgent.getUserDetails();
        MobileNumber1=profile.get("mobilenumber");
        Name=profile.get("username");
        agentid=profile.get("agent_id");

        name.setText(Name);

        if(getIntent()!=null)
        {
            //MobileNumber=getIntent().getStringExtra("MobileNumber");
            //Name=getIntent().getStringExtra("Name");
           // AgentId=getIntent().getStringExtra("AgentId");
            //agent_id=getIntent().getStringExtra("agent_id");
        }


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerSubAgent.clearSession();
                Intent intent = new Intent(DashBoardSubAgent.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardSubAgent.this, LeadsListAgent.class);
                startActivity(intent);
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(DashBoardSubAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SubAgentLeadsListResponse> call = apiInterface.subagentLeadsList(MobileNumber1);
        call.enqueue(new Callback<SubAgentLeadsListResponse>() {
            @Override
            public void onResponse(Call<SubAgentLeadsListResponse> call, Response<SubAgentLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<SubAgentLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardSubAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardSubAgent.this));
                    recyclerAdapter = new RecyclerAdapter(DashBoardSubAgent.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardSubAgent.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SubAgentLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardSubAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardSubAgent.this, AddLeadsSubAgent1.class);
                intent.putExtra("MobileNumber",MobileNumber1);
                intent.putExtra("agent_id",agentid);
                startActivity(intent);
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardSubAgent.this, NotificationsAgent.class);
                startActivity(intent);
            }
        });

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });

    }

    private void refreshData() {


        final ProgressDialog progressDialog = new ProgressDialog(DashBoardSubAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SubAgentLeadsListResponse> call = apiInterface.subagentLeadsList(MobileNumber1);
        call.enqueue(new Callback<SubAgentLeadsListResponse>() {
            @Override
            public void onResponse(Call<SubAgentLeadsListResponse> call, Response<SubAgentLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<SubAgentLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardSubAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardSubAgent.this));
                    recyclerAdapter = new RecyclerAdapter(DashBoardSubAgent.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardSubAgent.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SubAgentLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardSubAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardSubAgent.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        item.setChecked(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawers();

        if (id == R.id.nav_home) {


        } else if (id == R.id.nav_wallet) {

            Intent intent = new Intent(DashBoardSubAgent.this, LeadsListAgent.class);
            intent.putExtra("MobileNumber",MobileNumber1);
            intent.putExtra("AgentId",agentid);
            startActivity(intent);

        } else if (id == R.id.nav_digital) {

            Intent intent = new Intent(DashBoardSubAgent.this, ProfileSubAgent.class);
            intent.putExtra("MobileNumber",MobileNumber1);
            intent.putExtra("AgentId",agentid);
            startActivity(intent);


        } else if (id == R.id. nav_changepin) {

            Intent intent = new Intent(DashBoardSubAgent.this, ChangePasswordSubAgent.class);
            intent.putExtra("MobileNumber",MobileNumber1);
            intent.putExtra("AgentId",agentid);
            startActivity(intent);

        } else if (id == R.id.nav_notifications) {

          /*  Intent intent = new Intent(DashBoardSubAgent.this, Notif.class);
            intent.putExtra("MobileNumber",MobileNumber1);
            intent.putExtra("AgentId",agentid);
            startActivity(intent);*/


        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
