package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter7;
import com.igrand.ThreeWaySolutions.R;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PdfActivityProjects extends BaseActivity implements OnPageChangeListener, OnLoadCompleteListener{

    private int pageNumber = 0;

    private String pdfFileName;
    //private PDFView pdfView;
    public ProgressDialog pDialog;
    public static final int FILE_PICKER_REQUEST_CODE = 1;
    private String pdfPath;
    Button pickfile,upload;
    String MobileNumber,ID,ProjectName,PlotSize,FastMoving,Cost,Googlelocation,Comments,Acres;
    ArrayList<String> stringList=new ArrayList<>();
    RecyclerView recyclerView,recyclerView1;
    RecyclerAdapter7 recyclerAdapter7;
    ImageView back;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
       // pdfView =  findViewById(R.id.pdfView);
        pickfile =  findViewById(R.id.pickFile);
        upload = findViewById(R.id.upload);
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        recyclerView1 = findViewById(R.id.recyclerView1);

        if(getIntent()!=null){



            MobileNumber=getIntent().getStringExtra("MobileNumber");
            ID=getIntent().getStringExtra("ID");
            ProjectName=getIntent().getStringExtra("ProjectName");
            PlotSize=getIntent().getStringExtra("PlotSize");
            FastMoving=getIntent().getStringExtra("FastMoving");
            Cost=getIntent().getStringExtra("Cost");
            Googlelocation=getIntent().getStringExtra("Googlelocation");
            Comments=getIntent().getStringExtra("Comments");
            Acres=getIntent().getStringExtra("Acres");


        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        pickfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // pdfView.removeView(pdfView);
                launchPicker();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadFile();
            }
        });

        initDialog();
    }


    private void launchPicker() {
        new MaterialFilePicker()
                .withActivity(PdfActivityProjects.this)
                .withRequestCode(FILE_PICKER_REQUEST_CODE)
                .withHiddenFiles(true)
                .withFilter(Pattern.compile(".*\\.pdf$"))
                .withTitle("Select PDF file")
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FILE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            File file = new File(path);
            displayFromFile(file);
            if (path != null) {
                Log.d("Path: ", path);
                pdfPath = path;
                stringList.add(pdfPath);


                recyclerView.setLayoutManager(new LinearLayoutManager(PdfActivityProjects.this));
                recyclerAdapter7 = new RecyclerAdapter7(PdfActivityProjects.this,stringList);
                recyclerView.setAdapter(recyclerAdapter7);
                recyclerAdapter7.notifyDataSetChanged();

                recyclerView1.setLayoutManager(new LinearLayoutManager(PdfActivityProjects.this));
                recyclerAdapter7 = new RecyclerAdapter7(PdfActivityProjects.this,stringList,path,file);
                recyclerView1.setAdapter(recyclerAdapter7);
                recyclerAdapter7.notifyDataSetChanged();


               // Toast.makeText(this, "Picked file: " + path, Toast.LENGTH_LONG).show();
            }
        }

    }

    private void displayFromFile(File file) {

        Uri uri = Uri.fromFile(new File(file.getAbsolutePath()));
        pdfFileName = getFileName(uri);

       /* pdfView.fromFile(file)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                *//*.spacing(10) // in dp
                .onPageError(this)*//*
                .load();*/



    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getLastPathSegment();
        }
        return result;
    }



    @Override
    public void loadComplete(int nbPages) {
       /* PdfDocument.Meta meta = pdfView.getDocumentMeta();

        printBookmarksTree(pdfView.getTableOfContents(), "-");*/
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            //Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }
    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }



    private void uploadFile() {
        if (pdfPath == null) {
            Toast.makeText(this, "Please select PDF File ", Toast.LENGTH_LONG).show();
            return;
        } else {

            Intent intent=new Intent(PdfActivityProjects.this,NearbyProjects1.class);
            intent.putExtra("pdf",pdfPath);
            intent.putExtra("pdflist",stringList);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("ProjectName",ProjectName);
            intent.putExtra("Acres",Acres);
            intent.putExtra("PlotSize",PlotSize);
            intent.putExtra("FastMoving",FastMoving);
            intent.putExtra("Comments",Comments);
            intent.putExtra("Cost",Cost);
            intent.putExtra("Googlelocation",Googlelocation);
            intent.putExtra("Comments",Comments);
            intent.putExtra("ID",ID);

            startActivity(intent);


            /*showpDialog();

            // Map is used to multipart the file using okhttp3.RequestBody
            Map<String, RequestBody> map = new HashMap<>();
            File file = new File(pdfPath);
            // Parsing any Media type file
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), file);
            map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
            ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
            Call<ServerResponse> call = getResponse.upload("token", map);
            call.enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            hidepDialog();
                            ServerResponse serverResponse = response.body();
                            Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }else {
                        hidepDialog();
                        Toast.makeText(getApplicationContext(), "problem image", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    hidepDialog();
                    Log.v("Response gotten is", t.getMessage());
                    Toast.makeText(getApplicationContext(), "problem uploading image " + t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });*/
        }
    }

    protected void initDialog() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(true);
    }


    protected void showpDialog() {

        if (!pDialog.isShowing()) pDialog.show();
    }

    protected void hidepDialog() {

        if (pDialog.isShowing()) pDialog.dismiss();
    }



}

