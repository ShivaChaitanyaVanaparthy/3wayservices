package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MultiImageActivity extends BaseActivity {

    Button addimg;
    int OPEN_MEDIA_PICKER = 2;
    public static ArrayList<String> selectedvideoImgList1 = new ArrayList<>();
    String multi_image_path = "empty";
    List<MultipartBody.Part> images_array = new ArrayList<>();
    int CAMERA_CAPTURE = 1;
    String MobileNumber,ID;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_image);

        addimg=findViewById(R.id.addimg);


        if(getIntent()!=null){

            MobileNumber = getIntent().getStringExtra("MobileNumber");
            ID = getIntent().getStringExtra("ID");
        }


        if (ContextCompat.checkSelfPermission(MultiImageActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(MultiImageActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MultiImageActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(MultiImageActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show an explanation to the user1 *asynchronously* -- don't block
                // this thread waiting for the user1's response! After the user1
                // sees the explanation, try again to request the permission.
            } else {
                ActivityCompat.requestPermissions(MultiImageActivity.this,
                        PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
            }
        }
        final Intent intent = new Intent(MultiImageActivity.this, Gallery.class);
        // Set the title
        intent.putExtra("title", "Select media");
        // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
        intent.putExtra("mode", 2);
        //intent.putExtra("maxSelection", 3); // Optional
        startActivityForResult(intent, OPEN_MEDIA_PICKER);

        addimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1=new Intent();
                intent1.putExtra("List",selectedvideoImgList1);
                intent1.putExtra("MobileNumber",MobileNumber);
                intent1.putExtra("ID",ID);
                setResult(Activity.RESULT_OK,intent1);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
            if (selectionResult.size() > 0) {
                for (int index = 0; index < selectionResult.size(); index++) {

                    selectedvideoImgList1.add(selectionResult.get(index));

                    multi_image_path = selectionResult.get(index);
                    String PickedImgPath = selectionResult.get(index);
                    System.out.println("path" + PickedImgPath);


/*
                    File file = new File(selectionResult.get(index));
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                    images_array.add(MultipartBody.Part.createFormData("brochure[]", file.getName(), surveyBody));*/
                }


               /* relative_image.setVisibility(View.VISIBLE);
                txt_count.setText(String.valueOf(selectedvideoImgList.size()));*/
            }
        } else if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
              //  onCaptureImageResult(data);
            }
        }


        /*if (requestCode == PICK_IMAGE) {

            if (resultCode == RESULT_OK) {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                //  PickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
//                    pick_img_part.setImageBitmap(bm);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                c.close();
            }
        }

        else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
//                pickedDocPath = String.valueOf(filePath);
            pickedDocPath = GalleryUriToPath.getPath(this, filePath);
            document_file.add(new File(pickedDocPath));
            Log.e("path ", pickedDocPath);
            //   Toast.makeText(this, "document has  selected", Toast.LENGTH_SHORT).show();
            String filename = pickedDocPath.substring(pickedDocPath.lastIndexOf("/") + 1);
            //  text_doc_name.setText(filename);
            // text_doc_name.setText("document has  selected");

        }

        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            imageupload.setImageBitmap(converetdImage);

            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");

            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
*/



        }

    }
