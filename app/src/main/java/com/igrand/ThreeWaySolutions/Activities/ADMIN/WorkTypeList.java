package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterWorkTypeList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

public class WorkTypeList extends BaseActivity {

    RecyclerView recyclerView;
    ImageView back;
    RecyclerAdapterWorkTypeList recyclerUser;
    ApiInterface apiInterface;
    AdminWorkTypeList.StatusBean statusBean;
    Button add;
    String mobileNumber,keytech;
    ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_type_list);


        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            keytech=getIntent().getStringExtra("keytech");
        }

        if(keytech!=null){
            if(keytech.equals("keytech")){
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(WorkTypeList.this, DashBoardTechnical.class);
                        intent.putExtra("MobileNumber",mobileNumber);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                    }
                });
            }
        } else {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(WorkTypeList.this,DashBoardAdmin.class);
                    intent.putExtra("MobileNumber",mobileNumber);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                }
            });
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(WorkTypeList.this, AddWorkTypeAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
            }
        });



/*
        final ProgressDialog progressDialog = new ProgressDialog(WorkTypeList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
        call.enqueue(new Callback<AdminWorkTypeList>() {
            @Override
            public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                if (response.code() == 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(WorkTypeList.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminWorkTypeList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(WorkTypeList.this));
                    recyclerUser = new RecyclerAdapterWorkTypeList(WorkTypeList.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(WorkTypeList.this, "No City's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                //progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(WorkTypeList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
