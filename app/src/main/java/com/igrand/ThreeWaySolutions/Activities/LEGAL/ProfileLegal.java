package com.igrand.ThreeWaySolutions.Activities.LEGAL;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.ProfileChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProfileAgentResponse;
import com.igrand.ThreeWaySolutions.Response.ProfileLegalResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProfileLegal extends BaseActivity {


    ImageView back,image;
    ApiInterface apiInterface;
    ProfileLegalResponse.StatusBean statusBean;
    String mobileNumber;
    TextView name,email,mobile;
    FrameLayout userprofile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_legal);
        back=findViewById(R.id.back);
        image=findViewById(R.id.image);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        mobile=findViewById(R.id.mobile);
        userprofile=findViewById(R.id.userprofile);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }


        final ProgressDialog progressDialog = new ProgressDialog(ProfileLegal.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProfileLegalResponse> call = apiInterface.profileLegal(mobileNumber);
        call.enqueue(new Callback<ProfileLegalResponse>() {
            @Override
            public void onResponse(Call<ProfileLegalResponse> call, Response<ProfileLegalResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    userprofile.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProfileLegalResponse.DataBean> dataBeans=response.body().getData();
                    Picasso.get().load(dataBeans.get(0).getProfile()).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(image);
                    name.setText(dataBeans.get(0).getUsername());
                    email.setText(dataBeans.get(0).getEmail());
                    //mobile.setText(dataBeans.get(0).());


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ProfileLegal.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<ProfileLegalResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ProfileLegal.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }
}
