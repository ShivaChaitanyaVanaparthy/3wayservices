package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UsersListAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddUserAgentResponse;
import com.igrand.ThreeWaySolutions.Response.AddUserResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddUserAgent extends BaseActivity {


    TextView userType;
    Button add, submit;
    ImageView back, imageupload;
    EditText userName, phoneNumber, email;
    String mobileNumber,UserType, UserName, PhoneNumber, Email, imagePic, picturePath, Active, UserType1,Gender,AgentId,UserType2;
    Integer Number;
    ApiInterface apiInterface;
    RadioGroup rg,rg1;
    RadioButton active, inactive,male,female;
    EditText uploadimage;
    private Boolean exit = false;
    ImageView telephonebook;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    private String TAG = "mobile";
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_agent);

        back=findViewById(R.id.back);
        add=findViewById(R.id.submit);
        userType = findViewById(R.id.userType);
        userName = findViewById(R.id.userName);
        phoneNumber = findViewById(R.id.phoneNumber);
        email = findViewById(R.id.email);
        rg = findViewById(R.id.rg);
        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        submit = findViewById(R.id.submit);
        uploadimage = findViewById(R.id.uploadimage);
        imageupload = findViewById(R.id.imageupload);
        telephonebook = findViewById(R.id.telephonebook);
        rg1 = findViewById(R.id.rg1);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);

        rg.check(R.id.active);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            AgentId=getIntent().getStringExtra("AgentId");
        }





        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserType=userType.getText().toString();

                if (UserType.equals("SUB-AGENT")) {
                    UserType2 = "sub_agent";
                }

                if (active.isChecked()) {

                    Active = "1";


                } else if (inactive.isChecked()) {

                    Active = "0";

                } else {

                    Toast.makeText(AddUserAgent.this, "Please select Active/InActive...", Toast.LENGTH_SHORT).show();
                }

                UserType=userType.getText().toString();
                UserName = userName.getText().toString();
                PhoneNumber = phoneNumber.getText().toString();
                Email = email.getText().toString();

                addUser(mobileNumber,AgentId,UserType2, UserName, PhoneNumber, Email, Active);
            }


        });


        uploadimage.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(AddUserAgent.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddUserAgent.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(AddUserAgent.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(AddUserAgent.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(AddUserAgent.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddUserAgent.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(AddUserAgent.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(AddUserAgent.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }

                LinearLayout camera, folder;

                dialog = new Dialog(AddUserAgent.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();
                    }
                });

            }

        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else {

        }


    }




    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();



        if (requestCode == 100) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);



                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP


            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {



            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload.setImageBitmap(converetdImage);
            image = new File(Environment.getExternalStorageDirectory(),"temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // new uploadFileToServerTask().execute(destination.getAbsolutePath());






           /* Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            converetdImage = getResizedBitmap(bitmap, 500);
            imageupload.setImageBitmap(converetdImage);*/
            /*try {
                createImagefile();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

        }
    }

    private File createImagefile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        picturePath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    private void addUser(final String mobilenumber, String agentId, String userType, String userName, String phoneNumber, String email, String active) {


        final ProgressDialog progressDialog=new ProgressDialog(AddUserAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("profile", image.getName(), requestFile);

        }

        RequestBody UserType1 = RequestBody.create(MediaType.parse("multipart/form-data"), userType);
        RequestBody UserName1 = RequestBody.create(MediaType.parse("multipart/form-data"), userName);
        RequestBody PhoneNumber1 = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNumber);
        RequestBody Email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email);
        RequestBody Status1 = RequestBody.create(MediaType.parse("multipart/form-data"), active);
        RequestBody MobileNumber1 = RequestBody.create(MediaType.parse("multipart/form-data"), mobilenumber);
        RequestBody AgentId1 = RequestBody.create(MediaType.parse("multipart/form-data"), agentId);
        //RequestBody GENDER = RequestBody.create(MediaType.parse("multipart/form-data"), gender);


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddUserAgentResponse> call = apiInterface.agentAddUser(MobileNumber1,AgentId1,UserType1,UserName1,PhoneNumber1,Email1,Status1,body);
        call.enqueue(new Callback<AddUserAgentResponse>() {
            @Override
            public void onResponse(Call<AddUserAgentResponse> call, Response<AddUserAgentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    AddUserAgentResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(AddUserAgent.this, "User Added Successfully...", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(AddUserAgent.this, UsersListAgent.class);
                    intent.putExtra("MobileNumber",mobileNumber);
                    intent.putExtra("AgentId",AgentId);
                    startActivity(intent);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AddUserAgent.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AddUserAgentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(AddUserAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


    }
}
