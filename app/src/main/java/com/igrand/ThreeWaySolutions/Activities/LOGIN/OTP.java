package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.mukesh.OtpView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTP extends BaseActivity {

    ImageView back;
    Button next;
    OtpView otpView;
    String otp,mobileNumber;
    ApiInterface apiInterface;
    com.igrand.ThreeWaySolutions.Response.OTP.StatusBean statusBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpadmin);

        if(getIntent()!=null){
            mobileNumber=getIntent().getStringExtra("Number");
        }
        back=findViewById(R.id.back);
        next=findViewById(R.id.next);
        otpView = findViewById(R.id.otp_view);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                otp=otpView.getText().toString();


                final ProgressDialog progressDialog = new ProgressDialog(OTP.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<com.igrand.ThreeWaySolutions.Response.OTP> call = apiInterface.adminOtp(mobileNumber,otp);
                call.enqueue(new Callback<com.igrand.ThreeWaySolutions.Response.OTP>() {
                    @Override
                    public void onResponse(Call<com.igrand.ThreeWaySolutions.Response.OTP> call, Response<com.igrand.ThreeWaySolutions.Response.OTP> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(OTP.this, "OTP Verified Successfully...", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(OTP.this, ChangePassword.class);
                            intent.putExtra("Number",mobileNumber);
                            startActivity(intent);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP.this, "Please Enter Valid OTP...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<com.igrand.ThreeWaySolutions.Response.OTP> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(OTP.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });






            }
        });
    }
}
