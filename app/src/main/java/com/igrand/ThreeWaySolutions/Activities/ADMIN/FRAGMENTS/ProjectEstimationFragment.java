package com.igrand.ThreeWaySolutions.Activities.ADMIN.FRAGMENTS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddProjectEstimation;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.AdminProjectDetails;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerProjectEstimation;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProjectEstimationListResponse;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectEstimationFragment extends Fragment {

    ProjectEstimationListResponse.StatusBean statusBean;
    RecyclerProjectEstimation recyclerUser;
    String ID,SUM,Project;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_projectoverview, container, false);


        if(getArguments()!=null){
            final Bundle args = getArguments();
            ID = args.getString("Id");
            SUM = args.getString("SUM");
            Project = args.getString("Project");
        }

        Button add;
        final RecyclerView recyclerView;

        ApiInterface apiInterface;


        add=v.findViewById(R.id.add);
        recyclerView=v.findViewById(R.id.recyclerView);


        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProjectEstimationListResponse> call = apiInterface.projectEstimationList(ID);
        call.enqueue(new Callback<ProjectEstimationListResponse>() {
            @Override
            public void onResponse(Call<ProjectEstimationListResponse> call, Response<ProjectEstimationListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProjectEstimationListResponse.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerUser = new RecyclerProjectEstimation(getContext(),dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Error while adding...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<ProjectEstimationListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), AddProjectEstimation.class);
                intent.putExtra("ID",ID);
                intent.putExtra("Project",Project);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        return v;
    }
}
