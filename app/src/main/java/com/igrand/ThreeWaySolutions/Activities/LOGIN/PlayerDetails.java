package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterPlayerList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.PlayersDetailsResponse;
import com.igrand.ThreeWaySolutions.Response.PlayersListResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class PlayerDetails extends BaseActivity {


    TextView fname,lname,gametype,state,district,mandal,village,phone,dob,aadhar,address,pin;
    ImageView image,aimage,back;
    String Id,Image,AImage;
    ApiInterface apiInterface;
    PlayersDetailsResponse.StatusBean statusBean;
    RelativeLayout relative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_details);

        fname=findViewById(R.id.fname);
        lname=findViewById(R.id.lname);
        gametype=findViewById(R.id.gametype);
        state=findViewById(R.id.state);
        district=findViewById(R.id.district);
        mandal=findViewById(R.id.mandal);
        village=findViewById(R.id.village);
        phone=findViewById(R.id.phone);
        dob=findViewById(R.id.dob);
        aadhar=findViewById(R.id.anum);
        address=findViewById(R.id.address);
        pin=findViewById(R.id.pin);
        image=findViewById(R.id.image);
        aimage=findViewById(R.id.aimage);
        back=findViewById(R.id.back);
        relative=findViewById(R.id.relative);


        if(getIntent()!=null){
            Id=getIntent().getStringExtra("Id");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final ProgressDialog progressDialog1 = new ProgressDialog(PlayerDetails.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PlayersDetailsResponse> call1 = apiInterface.adminPlayersListDetail(Id);
        call1.enqueue(new Callback<PlayersDetailsResponse>() {
            @Override
            public void onResponse(Call<PlayersDetailsResponse> call, Response<PlayersDetailsResponse> response) {

                if (response.code() == 200) {
                    progressDialog1.dismiss();
                    relative.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    PlayersDetailsResponse.DataBean dataBeans = response.body().getData();

                    fname.setText(dataBeans.getPlayer_first_name());
                    lname.setText(dataBeans.getPlayer_last_name());
                    gametype.setText(dataBeans.getGame_type());
                    state.setText(dataBeans.getState_name());
                    district.setText(dataBeans.getDistrict_name());
                    mandal.setText(dataBeans.getMandal_name());
                    village.setText(dataBeans.getVillage_name());
                    phone.setText(dataBeans.getPhone());
                    dob.setText(dataBeans.getDob());
                    aadhar.setText(dataBeans.getAadhar_no());
                    address.setText(dataBeans.getAddress());
                    pin.setText(dataBeans.getPin());

                   Image= "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/player/"+dataBeans.getImage();
                    AImage="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/player/"+dataBeans.getAadhar_image();


                    Picasso.get().load(Image).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(image);
                    Picasso.get().load(AImage).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(aimage);


                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            image.buildDrawingCache();
                            Bitmap bitmap = image.getDrawingCache();
                            Intent intent = new Intent(PlayerDetails.this, ProfileImage.class);
                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                            intent.putExtra("byteArray", _bs.toByteArray());
                            startActivity(intent);

                        }
                    });

                    aimage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            aimage.buildDrawingCache();
                            Bitmap bitmap = aimage.getDrawingCache();
                            Intent intent = new Intent(PlayerDetails.this, ProfileImage.class);
                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                            intent.putExtra("byteArray", _bs.toByteArray());
                            startActivity(intent);

                        }
                    });


                } else if (response.code() != 200) {
                    //progressDialog1.dismiss();
                    Toast.makeText(PlayerDetails.this, "No Players...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<PlayersDetailsResponse> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(PlayerDetails.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }
}
