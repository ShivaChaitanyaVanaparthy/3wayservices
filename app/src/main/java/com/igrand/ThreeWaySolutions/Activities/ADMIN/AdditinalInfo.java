package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdditinalInfoResponse;
import com.igrand.ThreeWaySolutions.Response.NearByProjectResponse;

import java.util.List;

public class AdditinalInfo extends BaseActivity {

    ApiInterface apiInterface;
    AdditinalInfoResponse.StatusBean statusBean1;
    String ID;
    ImageView back;
    TextView id,date,roadconnectivity,purchasevalue,salesvalue;
    LinearLayout card;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additinal_info);


        back=findViewById(R.id.back);
        roadconnectivity=findViewById(R.id.roadconnectivity);
        purchasevalue=findViewById(R.id.purchasevalue);
        salesvalue=findViewById(R.id.salesvalue);
        date=findViewById(R.id.date);
        id=findViewById(R.id.id);
        card=findViewById(R.id.card);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent()!=null){


            ID=getIntent().getStringExtra("ID");
        }



        final ProgressDialog progressDialog = new ProgressDialog(AdditinalInfo.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdditinalInfoResponse> call = apiInterface.additinalinfo(ID);
        call.enqueue(new Callback<AdditinalInfoResponse>() {
            @Override
            public void onResponse(Call<AdditinalInfoResponse> call, Response<AdditinalInfoResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    card.setVisibility(View.VISIBLE);
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(AdditinalInfo.this, "Project List......", Toast.LENGTH_SHORT).show();

                    AdditinalInfoResponse.AdditionalInfoBean dataBeans=response.body().getAdditional_info();
                    id.setText(dataBeans.getLead_id());
                    date.setText(dataBeans.getDatetime());
                    roadconnectivity.setText(dataBeans.getRoad_connectivity());
                    purchasevalue.setText(dataBeans.getExpected_purchase_value());
                    salesvalue.setText(dataBeans.getExpected_sales_value());



                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AdditinalInfo.this, "No project's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdditinalInfoResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(AdditinalInfo.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }
}
