package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PdfActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerMarketing;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter2;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter3;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter6;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterImages;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddNearbyProjectResponse;
import com.squareup.picasso.Picasso;

import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import static com.vincent.filepicker.activity.BaseActivity.IS_NEED_FOLDER_LIST;
import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;



public class NearbyProjects1 extends BaseActivity implements LocationListener {

    EditText projectname, acres, plotsize, fastmoving, cost, uploadimage,comments;
    TextView googlelocation;
    ApiInterface apiInterface;
    RadioGroup rg;
    RadioButton active, inactive;
    ImageView back, imageupload;
    String ProjectName, Acres, PlotSize, FastMoving, Cost, Googlelocation, Uploadimage, Uploadimage1,picturePath, ID,Active,imagePic,imagePic1,picturePath1,Comments;
    Button submit;
    TextView uploadimage1;
    PrefManagerMarketing prefManagerMarketing;
    String docFilePath;


    private Boolean exit = false;
    ImageView telephonebook;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    File image1 = null;
    private Bitmap bitmap,bitmap1;
    Bitmap converetdImage,converetdImage1;
    String MobileNumber;
    private String TAG = "mobile";
    Dialog dialog;

    ArrayList<String> myList;
    RecyclerAdapterImages recyclerAdapter1;
    RecyclerAdapter2 recyclerAdapter2;
    RecyclerView recyclerView,recyclerView1;
    RecyclerAdapter3 recyclerAdapter3;


    public static List<String> selectedvideoImgList = new ArrayList<>();
    public static List<String> selectedvideoImgList1 = new ArrayList<>();
    String multi_image_path = "empty";
    String multi_image_path1 = "empty";
    List<MultipartBody.Part> images_array = new ArrayList<>();
    List<MultipartBody.Part> images_array1 = new ArrayList<>();
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    int CAMERA_CAPTURE = 1;
    int OPEN_MEDIA_PICKER = 2;
    int OPEN_MEDIA_PICKER1 = 2;
    int PICK_IMAGE = 3;
    int PICK_FILE_REQUEST = 7;
    String pickedDocPath = null;
    ArrayList<File> document_file = new ArrayList<>();
    ArrayList<File> document_file1 = new ArrayList<>();
    RelativeLayout relative_image,relative_image1;
    TextView txt_count,txt_count1;
    public static final int MULTIIMAGE = 1100;
    public static final int MULTIIMAGE1 = 1101;
    String imageEncoded,pdf;
    List<String> imagesEncodedList;
    private static final int BUFFER_SIZE = 1024 * 2;
    private static final String IMAGE_DIRECTORY = "/demonuts_upload_gallery";


    private ArrayList<String> mResults = new ArrayList<>();


    private static final int REQUEST_CODE1 = 732;
    private static final int REQUEST_CODE2 = 733;


    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    Double latitude, longitude;

    ArrayList<String> pdflist=new ArrayList<>();
    int number;
    RecyclerAdapter6 recyclerAdapter6;
    String emptydocument="";

    ArrayList<NormalFile> list;
    ArrayList<ImageFile> list1;

    ArrayList<String> ImageList=new ArrayList<>();
    ArrayList<String> PdfList=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_projects);


        projectname = findViewById(R.id.projectname);
        acres = findViewById(R.id.acres);
        plotsize = findViewById(R.id.plotsize);
        fastmoving = findViewById(R.id.fastmoving);
        cost = findViewById(R.id.cost);
        googlelocation = findViewById(R.id.googlelocation);
        uploadimage = findViewById(R.id.uploadimage);
        uploadimage1 = findViewById(R.id.uploadimage1);
        rg = findViewById(R.id.rg);
        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        back = findViewById(R.id.back);
        imageupload = findViewById(R.id.imageupload);
       // imageupload1 = findViewById(R.id.imageupload1);
        submit = findViewById(R.id.submit);

        relative_image=findViewById(R.id.relative_image);
        txt_count=findViewById(R.id.txt_count);
        relative_image1=findViewById(R.id.relative_image1);
        txt_count1=findViewById(R.id.txt_count1);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView1=findViewById(R.id.recyclerView1);
        comments=findViewById(R.id.comments);



        prefManagerMarketing=new PrefManagerMarketing(NearbyProjects1.this);
        HashMap<String, String> profile=prefManagerMarketing.getUserDetails();
        MobileNumber=profile.get("mobilenumber");



        if (getIntent() != null) {
            //MobileNumber = getIntent().getStringExtra("MobileNumber");
            ID = getIntent().getStringExtra("ID");
            pdf = getIntent().getStringExtra("pdf");
            pdflist = (ArrayList<String>)getIntent().getSerializableExtra("pdflist");

            ProjectName=getIntent().getStringExtra("ProjectName");
            PlotSize=getIntent().getStringExtra("PlotSize");
            FastMoving=getIntent().getStringExtra("FastMoving");
            Cost=getIntent().getStringExtra("Cost");
            Googlelocation=getIntent().getStringExtra("Googlelocation");
            Comments=getIntent().getStringExtra("Comments");
            Acres=getIntent().getStringExtra("Acres");

            projectname.setText(ProjectName);
            plotsize.setText(PlotSize);
            fastmoving.setText(FastMoving);
            cost.setText(Cost);
            googlelocation.setText(Googlelocation);
            comments.setText(Comments);
            acres.setText(Acres);

        }

        rg.check(R.id.active);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(pdflist!=null){

            number=pdflist.size();
            recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter6 = new RecyclerAdapter6(NearbyProjects1.this,number,pdflist);
            recyclerView1.setAdapter(recyclerAdapter6);
            recyclerAdapter6.notifyDataSetChanged();
        }

        uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 if (ContextCompat.checkSelfPermission(NearbyProjects1.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(NearbyProjects1.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects1.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects1.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(NearbyProjects1.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }


                ProjectName = projectname.getText().toString();
                Acres = acres.getText().toString();
                PlotSize = plotsize.getText().toString();
                FastMoving = fastmoving.getText().toString();
                Cost = cost.getText().toString();
                Googlelocation = googlelocation.getText().toString();
                Comments = comments.getText().toString();


              /* Intent intent3 = new Intent(NearbyProjects1.this, PdfActivityProjects.class);
                intent3.putExtra("MobileNumber",MobileNumber);
                intent3.putExtra("ID",ID);

                intent3.putExtra("ProjectName",ProjectName);
                intent3.putExtra("Acres",Acres);
                intent3.putExtra("PlotSize",PlotSize);
                intent3.putExtra("FastMoving",FastMoving);
                intent3.putExtra("Cost",Cost);
                intent3.putExtra("Googlelocation",Googlelocation);
                intent3.putExtra("Comments",Comments);


                startActivity(intent3);*/

                Intent intent4 = new Intent(NearbyProjects1.this, NormalFilePickActivity.class);
                intent4.putExtra(Constant.MAX_NUMBER, 9);
                intent4.putExtra(IS_NEED_FOLDER_LIST, true);
                intent4.putExtra(NormalFilePickActivity.SUFFIX,
                        new String[] {"xlsx", "xls", "doc", "dOcX", "ppt", ".pptx", "pdf"});
                startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);


              /* camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        *//*Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);*//*

                   *//*     Intent intent = new Intent(NearbyProjects1.this, Gallery2.class);
                        // Set the title
                        intent.putExtra("title", "Select media");
                        // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
                        intent.putExtra("mode", 2);
                        //intent.putExtra("maxSelection", 3); // Optional
                        startActivityForResult(intent, OPEN_MEDIA_PICKER1);*//*

                        Intent intent = new Intent(NearbyProjects1.this, ImagesSelectorActivity.class);
                        // max number of images to be selected
                        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 5);
                        // min size of image which will be shown; to filter tiny images (mainly icons)
                        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
                        // show camera or not
                        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                        // pass current selected images as the initial value
                        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                        // start the selector
                        startActivityForResult(intent, REQUEST_CODE1);

                        dialog.dismiss();
                    }
                });*/
            }

        });





       uploadimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(NearbyProjects1.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(NearbyProjects1.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects1.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects1.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(NearbyProjects1.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }


                Intent intent1 = new Intent(NearbyProjects1.this, ImagePickActivity.class);
                intent1.putExtra(IS_NEED_CAMERA, true);
                intent1.putExtra(Constant.MAX_NUMBER, 9);
                intent1.putExtra(IS_NEED_FOLDER_LIST, true);
                startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_IMAGE);

              /*  LinearLayout camera, folder,folder1;

                dialog = new Dialog(NearbyProjects1.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);

                folder1.setVisibility(View.GONE);
                camera.setVisibility(View.GONE);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 102);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {*/
//                        *//*Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(i, 100);*//*

                       /* Intent intent = new Intent(NearbyProjects1.this, Gallery.class);
                        // Set the title
                        intent.putExtra("title", "Select media");
                        // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
                        intent.putExtra("mode", 2);
                        //intent.putExtra("maxSelection", 3); // Optional
                        startActivityForResult(intent, OPEN_MEDIA_PICKER);
*/




              /*  Intent intent = new Intent(NearbyProjects1.this, ImagesSelectorActivity.class);
                // max number of images to be selected
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 5);
                // min size of image which will be shown; to filter tiny images (mainly icons)
                intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
                // show camera or not
                intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                // pass current selected images as the initial value
                intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                // start the selector
                startActivityForResult(intent, REQUEST_CODE1);
*/



//                      Intent intent=new Intent(NearbyProjects1.this,MultiImageActivity.class);
//                      startActivity(intent);
                 /*   }
                });*/
            }

        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (active.isChecked()) {

                    Active = "1";


                } else if (inactive.isChecked()) {

                    Active = "0";

                } else {

                    Toast.makeText(NearbyProjects1.this, "Please select Active/InActive...", Toast.LENGTH_SHORT).show();
                }

                ProjectName = projectname.getText().toString();
                Acres = acres.getText().toString();
                PlotSize = plotsize.getText().toString();
                FastMoving = fastmoving.getText().toString();
                Cost = cost.getText().toString();
                Googlelocation = googlelocation.getText().toString();
                Comments = comments.getText().toString();

                addProject(ProjectName, Acres, PlotSize, FastMoving, Cost,Googlelocation,Active,Comments);

            }
        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else {

        }

        /*if (imagePic1 != null && !imagePic1.isEmpty() && !imagePic1.equals("null")) {

            Picasso.get().load(imagePic1).into(imageupload1);

            bitmap1 = ((BitmapDrawable) imageupload1.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap1);
            converetdImage1 = getResizedBitmap1(bitmap1, 500);

        } else {

        }
*/



        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


      /*  checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                STORAGE_PERMISSION_CODE);*/

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions

            if (permissionsToRequest.size() > 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                }
                Log.d(TAG, "Permission requests");
                canGetLocation = false;
            }


            // get location
            getLocation();
        }
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private Bitmap getResizedBitmap1(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    list1 = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    StringBuilder builder = new StringBuilder();
                    for (ImageFile file : list1) {
                        String path = file.getPath();
                        builder.append(path + "\n");
                        ImageList.add(path);
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapterImages(NearbyProjects1.this, ImageList,list1);
                    recyclerView.setAdapter(recyclerAdapter1);
                    recyclerAdapter1.notifyDataSetChanged();

                }
                break;
            case Constant.REQUEST_CODE_PICK_FILE:
            if (resultCode == RESULT_OK) {
                list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                StringBuilder builder = new StringBuilder();
                for (NormalFile file : list) {
                    String path = file.getPath();
                    builder.append(path + "\n");
                    PdfList.add(path);
                }

                number=PdfList.size();
                recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter6 = new RecyclerAdapter6(NearbyProjects1.this,number,PdfList);
                recyclerView1.setAdapter(recyclerAdapter6);
                recyclerAdapter6.notifyDataSetChanged();
            }
            break;
        }

/*
        if (requestCode == REQUEST_CODE1) {
            if (resultCode == RESULT_OK) {
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);


                       *//* selectedvideoImgList.add(mResults.get(index));

                        multi_image_path = mResults.get(index);
                        String PickedImgPath = mResults.get(index);
                        System.out.println("path" + PickedImgPath);*//*

                recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this, LinearLayoutManager.HORIZONTAL, false));
                recyclerAdapter1 = new RecyclerAdapter1(NearbyProjects1.this, selectedvideoImgList,mResults);
                recyclerView.setAdapter(recyclerAdapter1);
                recyclerAdapter1.notifyDataSetChanged();


            }


        }*/



       /* if (requestCode == REQUEST_CODE2) {
            if (resultCode == RESULT_OK) {
                mResults1 = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);


                recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this, LinearLayoutManager.HORIZONTAL, false));
                recyclerAdapter2 = new RecyclerAdapter2(NearbyProjects1.this, selectedvideoImgList1);
                recyclerView.setAdapter(recyclerAdapter2);
                recyclerAdapter2.notifyDataSetChanged();

              *//*  if (mResults1.size() > 0) {
                    for (int index = 0; index < mResults1.size(); index++) {

                        selectedvideoImgList1.add(mResults1.get(index));

                        multi_image_path1 = mResults1.get(index);
                        String PickedImgPath = mResults1.get(index);
                        System.out.println("path" + PickedImgPath);

                        File file = new File(mResults1.get(index));
                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array.add(MultipartBody.Part.createFormData("image[]", file.getName(), surveyBody));


                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter2 = new RecyclerAdapter2(NearbyProjects1.this, selectedvideoImgList1);
                    recyclerView.setAdapter(recyclerAdapter2);


                  *//**//*  assert mResults != null;

                    // show results in textview
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                    for (String result : mResults) {
                        sb.append(result).append("\n");
                    }
                    tvResults.setText(sb.toString());*//**//*
                }*//*
            }


        }*/

      /*  if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            selectedvideoImgList.clear();
            ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
            if(selectionResult!=null){
                if (selectionResult.size() > 0) {
                    for (int index = 0; index < selectionResult.size(); index++) {
                        selectedvideoImgList.add(selectionResult.get(index));

                        multi_image_path = selectionResult.get(index);
                        String PickedImgPath = selectionResult.get(index);
                        System.out.println("path" + PickedImgPath);

                        File file = new File(selectionResult.get(index));
                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array1.add(MultipartBody.Part.createFormData("brochure[]", file.getName(), surveyBody));
                    }

                    recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
                    recyclerAdapter1 = new RecyclerAdapter1(NearbyProjects1.this,selectedvideoImgList, mResults);
                    recyclerView1.setAdapter(recyclerAdapter1);
                    relative_image1.setVisibility(View.VISIBLE);
                    //selectedvideoImgList.clear();
                    txt_count1.setText(String.valueOf(selectedvideoImgList.size()));
//                txt_count1.setText(String.valueOf(myList.size()));
                }
            } else if (requestCode == CAMERA_CAPTURE) {
                if (resultCode == RESULT_OK) {
                    onCaptureImageResult(data);
                }
            }
            }




        if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            selectedvideoImgList1.clear();
            ArrayList<String> selectionResult1 = data.getStringArrayListExtra("result1");
            if(selectionResult1!=null){
                if (selectionResult1.size() > 0) {
                    for (int index = 0; index < selectionResult1.size(); index++) {



                        selectedvideoImgList1.add(selectionResult1.get(index));

                        multi_image_path = selectionResult1.get(index);
                        String PickedImgPath1 = selectionResult1.get(index);
                        System.out.println("path" + PickedImgPath1);

                        File file1 = new File(selectionResult1.get(index));
                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array.add(MultipartBody.Part.createFormData("image[]", file1.getName(), surveyBody));
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
                    recyclerAdapter2 = new RecyclerAdapter2(NearbyProjects1.this,selectedvideoImgList1);
                    recyclerView.setAdapter(recyclerAdapter2);
                    relative_image.setVisibility(View.VISIBLE);
                    txt_count.setText(String.valueOf(selectedvideoImgList1.size()));
//                txt_count1.setText(String.valueOf(myList.size()));
                }
            } else if (requestCode == CAMERA_CAPTURE) {
                if (resultCode == RESULT_OK) {
                    onCaptureImageResult(data);
                }
            }

        }


        if (resultCode == RESULT_OK) {
            if (requestCode == 111) {
                Uri uri = data.getData();
                docFilePath = getFileNameByUri(this, uri);
            }
        }
*/


        if (requestCode == PICK_IMAGE) {

            if (resultCode == RESULT_OK) {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                //  PickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
//                    pick_img_part.setImageBitmap(bm);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                c.close();
            }
        } else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
//                pickedDocPath = String.valueOf(filePath);
            pickedDocPath = GalleryUriToPath.getPath(this, filePath);
            document_file.add(new File(pickedDocPath));
            Log.e("path ", pickedDocPath);
            //   Toast.makeText(this, "document has  selected", Toast.LENGTH_SHORT).show();
            String filename = pickedDocPath.substring(pickedDocPath.lastIndexOf("/") + 1);
            //  text_doc_name.setText(filename);
            // text_doc_name.setText("document has  selected");

        }else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
//                pickedDocPath = String.valueOf(filePath);
            pickedDocPath = GalleryUriToPath.getPath(this, filePath);
            document_file1.add(new File(pickedDocPath));
            Log.e("path ", pickedDocPath);
            //   Toast.makeText(this, "document has  selected", Toast.LENGTH_SHORT).show();
            String filename = pickedDocPath.substring(pickedDocPath.lastIndexOf("/") + 1);
            //  text_doc_name.setText(filename);
            // text_doc_name.setText("document has  selected");

        }


      /*  if (requestCode == MULTIIMAGE && resultCode == RESULT_OK) {

            if (requestCode == 1100) {

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();

                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);


                        image = new File(imagesEncodedList.get(i));

                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        images_array1.add(MultipartBody.Part.createFormData("brochure[]", image.getName(), surveyBody));


                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }

                    recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(NearbyProjects1.this, imagesEncodedList, mResults);
                    recyclerView1.setAdapter(recyclerAdapter1);


                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        }

if (requestCode == MULTIIMAGE1 && resultCode == RESULT_OK) {

            if (requestCode == 1101) {

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();

                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);


                        image = new File(imagesEncodedList.get(i));

                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        images_array.add(MultipartBody.Part.createFormData("image[]", image.getName(), surveyBody));


                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(NearbyProjects1.this, imagesEncodedList, mResults);
                    recyclerView.setAdapter(recyclerAdapter1);


                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        }
*/


        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

          /*  imageupload.setVisibility(View.VISIBLE);

            imageupload.setImageBitmap(converetdImage);*/

            // imageupload.setVisibility(View.VISIBLE);
            ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
            bitmapArray.add(converetdImage); // Add a bitmap





            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);

            images_array.add(MultipartBody.Part.createFormData("image[]", image.getName(), surveyBody));

            recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter3 = new RecyclerAdapter3(NearbyProjects1.this,bitmapArray);
            recyclerView.setAdapter(recyclerAdapter3);



            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        if (requestCode == 102 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

          /*  imageupload.setVisibility(View.VISIBLE);

            imageupload.setImageBitmap(converetdImage);*/

            // imageupload.setVisibility(View.VISIBLE);
            ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
            bitmapArray.add(converetdImage); // Add a bitmap





            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);

            images_array.add(MultipartBody.Part.createFormData("image[]", image.getName(), surveyBody));

            recyclerView.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter3 = new RecyclerAdapter3(NearbyProjects1.this,bitmapArray);
            recyclerView.setAdapter(recyclerAdapter3);



            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }




    }

    private String getFileNameByUri(NearbyProjects1 nearbyProjects1, Uri uri) {

        String filepath = "";//default fileName
        // Uri filePathUri = uri;
        File file;
        if (uri.getScheme().toString().compareTo("content") == 0)
        {

            try {


                String[] proj = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
                cursor.moveToFirst();
                int column_index = cursor.getColumnIndex(proj[0]);
                image = new File(String.valueOf(column_index));
                // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                images_array1.add(MultipartBody.Part.createFormData("brochure[]", image.getName(), surveyBody));

                recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter3 = new RecyclerAdapter3(images_array,NearbyProjects1.this,"key2");
                recyclerView1.setAdapter(recyclerAdapter3);
                recyclerAdapter3.notifyDataSetChanged();

                return cursor.getString(column_index);

            } finally {
                {

                }
            }

        }
        else
        if (uri.getScheme().compareTo("file") == 0)
        {
            try
            {
                file = new File(new URI(uri.toString()));
                if (file.exists())
                    filepath = file.getAbsolutePath();

                image = new File(filepath);
                image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                images_array1.add(MultipartBody.Part.createFormData("brochure[]", image.getName(), surveyBody));


            }
            catch (URISyntaxException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {
            filepath = uri.getPath();
        }
        return filepath;


    }


       /* String fileName = getFileName(uri,image,images_array);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(wallpaperDirectory + File.separator + fileName);
            // create folder if not exists

            copy(this, uri, copyFile);
            image = new File(copyFile.getAbsolutePath());
           // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            images_array1.add(MultipartBody.Part.createFormData("brochure[]", image.getName(), surveyBody));

            recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter3 = new RecyclerAdapter3(images_array1,NearbyProjects1.this,"key2");
            recyclerView1.setAdapter(recyclerAdapter3);
            recyclerAdapter3.notifyDataSetChanged();

            return copyFile.getAbsolutePath();
        }
        return null;*/


   /* private void copy(NearbyProjects1 nearbyProjects1, Uri uri, File copyFile) {

        try {
            InputStream inputStream = this.getContentResolver().openInputStream(uri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(copyFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int copystream(InputStream inputStream, OutputStream outputStream) {

        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(inputStream, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(outputStream, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }

    public   String getFileName(Uri uri, File image, List<MultipartBody.Part> images_array) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();

  image = new File(path);

        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
        images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

        recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter3 = new RecyclerAdapter3(images_array,NearbyProjects1.this,"key2");
        recyclerView1.setAdapter(recyclerAdapter3);
        recyclerAdapter3.notifyDataSetChanged();



        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);

  image = new File((fileName));
            // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

            recyclerView1.setLayoutManager(new LinearLayoutManager(NearbyProjects1.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter3 = new RecyclerAdapter3(images_array,NearbyProjects1.this,"key2");
            recyclerView1.setAdapter(recyclerAdapter3);
            recyclerAdapter3.notifyDataSetChanged();

        }




        return fileName;

    }
*/

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context, Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    private void showDialog(String external_storage, final Context context, final String readExternalStorage) {

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(external_storage + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{readExternalStorage},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }


            @Override
            public void onBackPressed ()
            {
                super.onBackPressed();
            }

            private void addProject(String projectName, String acres, String plotSize, String fastMoving, String cost, String googlelocation, String active,String commentss){



                if(projectName.equals("")){

                    Toast.makeText(NearbyProjects1.this, "Please enter project name", Toast.LENGTH_SHORT).show();
                }else if(acres.equals("")){

                    Toast.makeText(NearbyProjects1.this, "Please enter acres", Toast.LENGTH_SHORT).show();
                }else if(googlelocation.equals("")){

                    Toast.makeText(NearbyProjects1.this, "Please select google location", Toast.LENGTH_SHORT).show();
                } else if(plotSize.equals("")){

                    Toast.makeText(NearbyProjects1.this, "Please enter plot size", Toast.LENGTH_SHORT).show();
                } else if(fastMoving.equals("")){

                    Toast.makeText(this, "Please enter fast moving", Toast.LENGTH_SHORT).show();
                }else if(cost.equals("")){

                    Toast.makeText(this, "Please enter cost", Toast.LENGTH_SHORT).show();
                }else if(PdfList.size()<0){
                    Toast.makeText(this, "Please select PDF", Toast.LENGTH_SHORT).show();
                }else if(ImageList.size()<0){

                    Toast.makeText(this, "Please select Image", Toast.LENGTH_SHORT).show();
                }

else {


                    final ProgressDialog progressDialog=new ProgressDialog(NearbyProjects1.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    MultipartBody.Part body = null;
                    MultipartBody.Part body1 = null;


                    if (image1 != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image1);
                        body1 = MultipartBody.Part.createFormData("image[]", image1.getName(), requestFile);

                    }



                    if(PdfList.size()>0){

                        for (int index = 0; index < PdfList.size(); index++) {

                            File file = new File(PdfList.get(index));
                            if (file != null) {
                                RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), file);
                                body = MultipartBody.Part.createFormData("brochure[]", file.getName(), requestBody);
                                //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                                images_array.add(MultipartBody.Part.createFormData("brochure[]", file.getName(), requestBody));

                            }else {

                                RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
                                body = MultipartBody.Part.createFormData("brochure[]", emptydocument, requestBody);
                                //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                                images_array.add(MultipartBody.Part.createFormData("brochure[]", emptydocument, requestBody));
                            }


                        }

                    } else {
                        RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
                        body = MultipartBody.Part.createFormData("brochure[]", emptydocument, requestBody);
                        //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                        images_array.add(MultipartBody.Part.createFormData("brochure[]", emptydocument, requestBody));
                    }


                    if (ImageList.size() > 0) {
                        for (int index = 0; index < ImageList.size(); index++) {

                            File file = new File(ImageList.get(index));

                            if (file != null) {

                                RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                                images_array1.add(MultipartBody.Part.createFormData("image[]", file.getName(), surveyBody));

                            }
                            else {
                                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
                                body = MultipartBody.Part.createFormData("images[]", emptydocument, requestBody);
                                //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                                images_array.add(MultipartBody.Part.createFormData("image[]", emptydocument, requestBody));
                            }
                        }

                    } else {

                        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
                        body = MultipartBody.Part.createFormData("images[]", emptydocument, requestBody);
                        //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                        images_array.add(MultipartBody.Part.createFormData("images[]", emptydocument, requestBody));
                    }
                    RequestBody ProjectName1 = RequestBody.create(MediaType.parse("multipart/form-data"), projectName);
                    RequestBody Acres1 = RequestBody.create(MediaType.parse("multipart/form-data"), acres);
                    RequestBody PlotSize1 = RequestBody.create(MediaType.parse("multipart/form-data"), plotSize);
                    RequestBody FastMoving1 = RequestBody.create(MediaType.parse("multipart/form-data"), fastMoving);
                    RequestBody Cost1 = RequestBody.create(MediaType.parse("multipart/form-data"), cost);
                    RequestBody GoogleLocation1 = RequestBody.create(MediaType.parse("multipart/form-data"), googlelocation);
                    RequestBody Active1 = RequestBody.create(MediaType.parse("multipart/form-data"), active);
                    RequestBody MobileNumber1 = RequestBody.create(MediaType.parse("multipart/form-data"), MobileNumber);
                    RequestBody LeadID1 = RequestBody.create(MediaType.parse("multipart/form-data"), ID);
                    RequestBody Comments1 = RequestBody.create(MediaType.parse("multipart/form-data"), commentss);



                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AddNearbyProjectResponse> call = apiInterface.adminNearbyProject(MobileNumber1,LeadID1,ProjectName1,Acres1,PlotSize1,FastMoving1,Cost1,GoogleLocation1,images_array,images_array1,Active1,Comments1);
                    call.enqueue(new Callback<AddNearbyProjectResponse>() {
                        @Override
                        public void onResponse(Call<AddNearbyProjectResponse> call, Response<AddNearbyProjectResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                AddNearbyProjectResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(NearbyProjects1.this, "Nearby Project's Added Successfully...", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(NearbyProjects1.this, NearByProjectsList.class);
                                intent.putExtra("MobileNumber",MobileNumber);
                                intent.putExtra("ID",ID);
                                startActivity(intent);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Converter<ResponseBody, APIError> converter =
                                        ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                APIError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    APIError.StatusBean status=error.getStatus();
                                    Toast.makeText(NearbyProjects1.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }

                            }

                        }


                        @Override
                        public void onFailure(Call<AddNearbyProjectResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(NearbyProjects1.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();

                        }
                    });


                }





            }



    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {

        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                }

                if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                    /*loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);*/
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (NearbyProjects1.this.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }


    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(NearbyProjects1.this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(NearbyProjects1.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(final Location loc) {


        Log.d(TAG, "updateUI");

        latitude = (loc.getLatitude());
        longitude = (loc.getLongitude());

        // txtAddress.setText(Double.toString(latitude));
        Geocoder geocoder = new Geocoder(NearbyProjects1.this, Locale.getDefault());

        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            googlelocation.setText(address + city_name + state + country + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }

    }

        }