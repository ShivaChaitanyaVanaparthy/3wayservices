package com.igrand.ThreeWaySolutions.Activities.SITEENGINEER;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddProjectEstimation;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerProjectEstimation;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProjectEstimationListResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class ProjectEstimation extends BaseActivity {

    ProjectEstimationListResponse.StatusBean statusBean;
    RecyclerProjectEstimation recyclerUser;
    String ID,SUM,Project,key,key1;
    ImageView back;
    RecyclerView recyclerView;

    ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_estimation);
        Button add;



        add=findViewById(R.id.add);
        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);

        if(getIntent()!=null){
            ID=getIntent().getStringExtra("ID");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProjectEstimation.this,SiteEngineerProjectDetails.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                intent.putExtra("keysite","keysite");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
              //finish();
            }
        });


        getProjectEstimation();



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProjectEstimation.this, AddProjectEstimation.class);
                intent.putExtra("ID",ID);
                intent.putExtra("Project",Project);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });

    }

    private void getProjectEstimation() {

        final ProgressDialog progressDialog = new ProgressDialog(ProjectEstimation.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProjectEstimationListResponse> call = apiInterface.projectEstimationList(ID);
        call.enqueue(new Callback<ProjectEstimationListResponse>() {
            @Override
            public void onResponse(Call<ProjectEstimationListResponse> call, Response<ProjectEstimationListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProjectEstimationListResponse.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(ProjectEstimation.this));
                    recyclerUser = new RecyclerProjectEstimation(ProjectEstimation.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(ProjectEstimation.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }


            @Override
            public void onFailure(Call<ProjectEstimationListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ProjectEstimation.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getProjectEstimation();
    }
}