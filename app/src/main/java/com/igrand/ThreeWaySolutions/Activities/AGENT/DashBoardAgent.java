package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NotificationsAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ChangePassword;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.NotificationCountResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

public class DashBoardAgent extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    ImageView toggle;
    RecyclerView recyclerView;
    RecyclerAdapterAgent recyclerAdapter;
    ImageView notification;
    LinearLayout add,users,logout1;
    Boolean exit=false;
    ApiInterface apiInterface;
    AgentLeadsListResponse.StatusBean statusBean;
    String MobileNumber,Name,AgentId,username,UserName;
    LinearLayout leads;
    TextView name,size1;
    PrefManagerAgent prefManagerAgent;
    TextView txt_count;
    NotificationCountResponse.StatusBean statusBean3;
    RelativeLayout relative_image;
    Handler handler = new Handler();
    Runnable refresh;
    SwipeRefreshLayout pullToRefresh;
    LinearLayout approved,pending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_agent);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        name = findViewById(R.id.name);
        navigationView.setNavigationItemSelectedListener(this);


        toggle=findViewById(R.id.toggle11);
        leads=findViewById(R.id.leads);
        recyclerView=findViewById(R.id.recyclerView);
        add=findViewById(R.id.add);
        notification=findViewById(R.id.notification);
        users=findViewById(R.id.users);
        logout1=findViewById(R.id.logout1);
        size1=findViewById(R.id.size);
        txt_count=findViewById(R.id.txt_count);
        relative_image=findViewById(R.id.relative_image);
         pullToRefresh = findViewById(R.id.pullToRefresh);
         approved = findViewById(R.id.approved);
         pending = findViewById(R.id.pending);
        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });





        approved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(DashBoardAgent.this,ApprovedLeadsAgent.class);
                intent.putExtra("MobileNumber",MobileNumber);
                startActivity(intent);
            }
        });

        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(DashBoardAgent.this,PendingLeadsAgent.class);
                 intent.putExtra("MobileNumber",MobileNumber);
                startActivity(intent);
            }
        });



        prefManagerAgent=new PrefManagerAgent(DashBoardAgent.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        UserName=profile.get("username");

        name.setText(UserName);




                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<NotificationCountResponse> call1 = apiInterface.notificationcountagent(MobileNumber);
                call1.enqueue(new Callback<NotificationCountResponse>() {
                    @Override
                    public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                        if (response.code() == 200) {
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;

                            //int x=statusBean3.getCount();

                            if(statusBean3.getCount()!=0){
                                relative_image.setVisibility(View.VISIBLE);
                                txt_count.setText(String.valueOf(statusBean3.getCount()));

                            }


                        } else if (response.code() != 200) {

                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardAgent.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
                    }
                });












        if(getIntent()!=null)
        {
            //MobileNumber=getIntent().getStringExtra("MobileNumber");
            //Name=getIntent().getStringExtra("Name");
            AgentId=getIntent().getStringExtra("AgentId");


        }


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerAgent.clearSession();
                Intent intent = new Intent(DashBoardAgent.this, Login.class);
                //intent.putExtra("Home",false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAgent.this,LeadsListAgent.class);
                startActivity(intent);
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(DashBoardAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AgentLeadsListResponse> call = apiInterface.agentLeadsList(MobileNumber);
        call.enqueue(new Callback<AgentLeadsListResponse>() {
            @Override
            public void onResponse(Call<AgentLeadsListResponse> call, Response<AgentLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AgentLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardAgent.this));
                    recyclerAdapter = new RecyclerAdapterAgent(DashBoardAgent.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(DashBoardAgent.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }
            }

            @Override
            public void onFailure(Call<AgentLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAgent.this, AddLeadsAgent.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("Name",Name);
                startActivity(intent);
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAgent.this, NotificationsAgent.class);
                startActivity(intent);
            }
        });



        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void refreshData() {


        prefManagerAgent=new PrefManagerAgent(DashBoardAgent.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        UserName=profile.get("username");

        name.setText(UserName);




        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationCountResponse> call1 = apiInterface.notificationcountagent(MobileNumber);
        call1.enqueue(new Callback<NotificationCountResponse>() {
            @Override
            public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                if (response.code() == 200) {
                    statusBean3 = response.body() != null ? response.body().getStatus() : null;

                    //int x=statusBean3.getCount();

                    if(statusBean3.getCount()!=0){
                        relative_image.setVisibility(View.VISIBLE);
                        txt_count.setText(String.valueOf(statusBean3.getCount()));

                    }


                } else if (response.code() != 200) {

                }
            }

            @Override
            public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardAgent.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
            }
        });












        if(getIntent()!=null)
        {
            //MobileNumber=getIntent().getStringExtra("MobileNumber");
            //Name=getIntent().getStringExtra("Name");
            AgentId=getIntent().getStringExtra("AgentId");


        }


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerAgent.clearSession();
                Intent intent = new Intent(DashBoardAgent.this, Login.class);
                //intent.putExtra("Home",false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAgent.this,LeadsListAgent.class);
                startActivity(intent);
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(DashBoardAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AgentLeadsListResponse> call = apiInterface.agentLeadsList(MobileNumber);
        call.enqueue(new Callback<AgentLeadsListResponse>() {
            @Override
            public void onResponse(Call<AgentLeadsListResponse> call, Response<AgentLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AgentLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardAgent.this));
                    recyclerAdapter = new RecyclerAdapterAgent(DashBoardAgent.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardAgent.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AgentLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAgent.this, AddLeadsAgent.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("Name",Name);
                startActivity(intent);
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAgent.this, NotificationsAgent.class);
                startActivity(intent);
            }
        });

    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardAgent.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        item.setChecked(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawers();

        if (id == R.id.nav_home) {


        } else if (id == R.id.nav_wallet) {

            Intent intent = new Intent(DashBoardAgent.this, LeadsListAgent.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);

        } else if (id == R.id.nav_users) {

            Intent intent = new Intent(DashBoardAgent.this, UsersListAgent.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);

        } else if (id == R.id.nav_digital) {

            Intent intent = new Intent(DashBoardAgent.this, ProfileAgent.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);


        } else if (id == R.id. nav_changepin) {

            Intent intent = new Intent(DashBoardAgent.this, ChangePasswordAgent.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);

        } else if (id == R.id.referral) {

            Intent intent = new Intent(DashBoardAgent.this, ReferralsListAgent.class);
            startActivity(intent);

        }  else if (id == R.id.earns) {

            Intent intent = new Intent(DashBoardAgent.this, EarnsListAgent.class);
            startActivity(intent);

        } /*else if (id == R.id. nav_appr) {

            Intent intent = new Intent(DashBoardAgent.this, ProcuremntLeadsListAgent.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);

        }*/ else if (id == R.id.nav_notifications) {

            Intent intent = new Intent(DashBoardAgent.this, NotificationsAgent.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);


        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
