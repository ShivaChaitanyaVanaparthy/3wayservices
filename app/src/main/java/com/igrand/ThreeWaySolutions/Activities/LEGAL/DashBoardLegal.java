package com.igrand.ThreeWaySolutions.Activities.LEGAL;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.ChangePasswordChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadsListChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.NotificationsChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.ProfileChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ChangePassword;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerChecking;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerLegal;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.DashBoardProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.NotificationsProcurement;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterChecking;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterLegal;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProcurement;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.CheckingDashBoardResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListLegalResponse;
import com.igrand.ThreeWaySolutions.Response.NotificationCountResponse;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardLegal extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    ImageView toggle;
    RecyclerView recyclerView;
    RecyclerAdapterLegal recyclerAdapterLegal;
    ImageView notification;
    LinearLayout add,logout1;
    ImageView edit;
    Boolean exit=false;
    ApiInterface apiInterface;
    CheckingDashBoardResponse.StatusBean statusBean;
    TextView total,active,inactive;
    LeadsListLegalResponse.StatusBean statusBean1;
    String MobileNumber,MobileNumber1,UserName;
    PrefManagerLegal prefManagerLegal;
    TextView name;

    TextView txt_count;
    NotificationCountResponse.StatusBean statusBean3;
    RelativeLayout relative_image;
    Handler handler = new Handler();
    Runnable refresh;
    SwipeRefreshLayout pullToRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_legal);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        toggle=findViewById(R.id.toggle11);
        recyclerView=findViewById(R.id.recyclerView);
        add=findViewById(R.id.add);
        notification=findViewById(R.id.notification);
        logout1=findViewById(R.id.logout1);
        total=findViewById(R.id.total);
        active=findViewById(R.id.active);
        inactive=findViewById(R.id.inactive);
        name=findViewById(R.id.name);
        //name=findViewById(R.id.name);
        txt_count=findViewById(R.id.txt_count);
        relative_image=findViewById(R.id.relative_image);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });




        prefManagerLegal=new PrefManagerLegal(DashBoardLegal.this);
        HashMap<String, String> profile=prefManagerLegal.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        UserName=profile.get("username");

        name.setText(UserName);

        if(getIntent()!=null){

            //MobileNumber=getIntent().getStringExtra("MobileNumber");
        }





                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<NotificationCountResponse> call2 = apiInterface.notificationcountlegal(MobileNumber);
                call2.enqueue(new Callback<NotificationCountResponse>() {
                    @Override
                    public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                        if (response.code() == 200) {
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;


                            if(statusBean3.getCount()!=0){
                                relative_image.setVisibility(View.VISIBLE);
                                txt_count.setText(String.valueOf(statusBean3.getCount()));

                            }


                        } else if (response.code() != 200) {

                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardChecking.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
                    }
                });





        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefManagerLegal.clearSession();
                Intent intent = new Intent(DashBoardLegal.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });




        final ProgressDialog progressDialog = new ProgressDialog(DashBoardLegal.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LeadsListLegalResponse> call1 = apiInterface.legalLeadsList();
        call1.enqueue(new Callback<LeadsListLegalResponse>() {
            @Override
            public void onResponse(Call<LeadsListLegalResponse> call, Response<LeadsListLegalResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    List<LeadsListLegalResponse.DataBean> dataBeans=response.body().getData();
                    //List<String> documents=dataBeans.get(0).getDocument();
                    Toast.makeText(DashBoardLegal.this, "Leads List...", Toast.LENGTH_SHORT).show();

                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardLegal.this));
                    recyclerAdapterLegal = new RecyclerAdapterLegal(DashBoardLegal.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapterLegal);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardLegal.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<LeadsListLegalResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardLegal.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardLegal.this, NotificationsLegal.class);
                startActivity(intent);
            }
        });

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });

    }

    private void refreshData() {


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationCountResponse> call2 = apiInterface.notificationcountlegal(MobileNumber);
        call2.enqueue(new Callback<NotificationCountResponse>() {
            @Override
            public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                if (response.code() == 200) {
                    statusBean3 = response.body() != null ? response.body().getStatus() : null;


                    if(statusBean3.getCount()!=0){
                        relative_image.setVisibility(View.VISIBLE);
                        txt_count.setText(String.valueOf(statusBean3.getCount()));

                    }


                } else if (response.code() != 200) {

                }
            }

            @Override
            public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardChecking.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
            }
        });



        final ProgressDialog progressDialog = new ProgressDialog(DashBoardLegal.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LeadsListLegalResponse> call1 = apiInterface.legalLeadsList();
        call1.enqueue(new Callback<LeadsListLegalResponse>() {
            @Override
            public void onResponse(Call<LeadsListLegalResponse> call, Response<LeadsListLegalResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    List<LeadsListLegalResponse.DataBean> dataBeans=response.body().getData();
                    //List<String> documents=dataBeans.get(0).getDocument();
                    Toast.makeText(DashBoardLegal.this, "Leads List...", Toast.LENGTH_SHORT).show();

                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardLegal.this));
                    recyclerAdapterLegal = new RecyclerAdapterLegal(DashBoardLegal.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapterLegal);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardLegal.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<LeadsListLegalResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardLegal.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardLegal.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        item.setChecked(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawers();

        if (id == R.id.nav_home) {



        }  else if (id == R.id.nav_digital) {

            Intent intent = new Intent(DashBoardLegal.this, ProfileLegal.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);


        }else if (id == R.id.nav_wallet) {

            Intent intent = new Intent(DashBoardLegal.this, LeadsListLegal.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);


        }
        else if (id == R.id. nav_changepin) {

            Intent intent = new Intent(DashBoardLegal.this, ChangePasswordLegal.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);

        }else if (id == R.id.nav_notifications) {

            Intent intent = new Intent(DashBoardLegal.this, NotificationsLegal.class);
            startActivity(intent);

        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}

