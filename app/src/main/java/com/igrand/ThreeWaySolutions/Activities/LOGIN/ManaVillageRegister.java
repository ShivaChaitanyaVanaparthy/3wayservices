package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.GamesList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ReportsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UsersListAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterDstActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterGameList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterGamesActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMandalActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterStateActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterVillageActiveList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddPlayerResponse;
import com.igrand.ThreeWaySolutions.Response.AddUserResponse;
import com.igrand.ThreeWaySolutions.Response.AdminActiveDistList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveMandalList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveVillageList;
import com.igrand.ThreeWaySolutions.Response.AdminAddDistrict;
import com.igrand.ThreeWaySolutions.Response.AdminAddVillage;
import com.igrand.ThreeWaySolutions.Response.AdminGameList;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ManaVillageRegister extends BaseActivity {

    TextView state, district, mandal,village,games,uploadimage,uploadimage1,already,dob,signin;
    EditText fname,lname,phone,anum,address,pin;
    ImageView imageupload,imageupload1;
    String Fname,Lname,Phone,Anum,Address,Pin,imagePic, picturePath;
    ImageView back;
    Button add;
    String Mandal;
    ApiInterface apiInterface;
    RecyclerAdapterStateActiveList recyclerAdapter;
    RecyclerAdapterDstActiveList recyclerAdapter1;
    AdminActiveStateList.StatusBean statusBean;
    AdminAddDistrict.StatusBean statusBean1;
    AdminActiveDistList.StatusBean statusBean2;
    AdminAddVillage.StatusBean statusBean3;
    AdminActiveMandalList.StatusBean statusBean4;
    AdminActiveVillageList.StatusBean statusBean5;
    AdminGameList.StatusBean statusBean6;
    String StateId, DistId,MandalId,VillageId,GameId,Village,StateName,DistrictName,MandalName,DOB;
    RecyclerAdapterMandalActiveList recyclerAdapter2;
    RecyclerAdapterVillageActiveList recyclerAdapter4;
    RecyclerAdapterGamesActiveList recyclerAdapter6;
    Dialog dialog;
    Bitmap converetdImage;
    private Bitmap bitmap;
    File image = null;
    Calendar myCalendar;
    File image1 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mana_village_register);
        add = findViewById(R.id.add);
        back = findViewById(R.id.back);
        district = findViewById(R.id.district);
        state = findViewById(R.id.state);
        mandal = findViewById(R.id.mandal);
        village = findViewById(R.id.village);
        games = findViewById(R.id.games);
        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        phone = findViewById(R.id.phone);
        anum = findViewById(R.id.anum);
        address = findViewById(R.id.address);
        pin = findViewById(R.id.pin);
        uploadimage = findViewById(R.id.uploadimage);
        imageupload = findViewById(R.id.imageupload);
        imageupload1 = findViewById(R.id.imageupload1);
        uploadimage1 = findViewById(R.id.uploadimage1);
        dob = findViewById(R.id.dob);
        already = findViewById(R.id.already);
        signin = findViewById(R.id.signin);


        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManaVillageRegister.this,Login.class);
                startActivity(intent);
            }
        });

        already.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ManaVillageRegister.this, PlayersListAdmin.class);
                startActivity(intent);

            }
        });

        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ManaVillageRegister.this, R.style.TimePickerTheme,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        
        
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                Fname=fname.getText().toString();
                Lname=lname.getText().toString();
                Phone=phone.getText().toString();
                Anum=anum.getText().toString();
                Address=address.getText().toString();
                Pin=pin.getText().toString();
                DOB=dob.getText().toString();



                addplayer(Fname,Lname,Phone,Anum,Address,Pin,DOB);
            }
        });




        uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LinearLayout camera, folder,folder1;

                dialog = new Dialog(ManaVillageRegister.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);


                camera.setVisibility(View.VISIBLE);
                folder1.setVisibility(View.GONE);

                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();


                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();



                    }
                });


            }
        });



        uploadimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LinearLayout camera, folder,folder1;

                dialog = new Dialog(ManaVillageRegister.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);

                camera.setVisibility(View.VISIBLE);
                folder1.setVisibility(View.GONE);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 102);
                        dialog.dismiss();


                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 103);
                        dialog.dismiss();



                    }
                });


            }
        });

        games.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(ManaVillageRegister.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);


                final ProgressDialog progressDialog = new ProgressDialog(ManaVillageRegister.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminGameList> call = apiInterface.adminGameList();
                call.enqueue(new Callback<AdminGameList>() {
                    @Override
                    public void onResponse(Call<AdminGameList> call, Response<AdminGameList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean6 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminGameList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(ManaVillageRegister.this));
                            recyclerAdapter6 = new RecyclerAdapterGamesActiveList(dataBeans,ManaVillageRegister.this, games, dialog, ManaVillageRegister.this);
                            recyclerView.setAdapter(recyclerAdapter6);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ManaVillageRegister.this, "No State's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminGameList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(ManaVillageRegister.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(ManaVillageRegister.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);


                final ProgressDialog progressDialog = new ProgressDialog(ManaVillageRegister.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminActiveStateList> call = apiInterface.adminstateActiveList();
                call.enqueue(new Callback<AdminActiveStateList>() {
                    @Override
                    public void onResponse(Call<AdminActiveStateList> call, Response<AdminActiveStateList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminActiveStateList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(ManaVillageRegister.this));
                            recyclerAdapter = new RecyclerAdapterStateActiveList(ManaVillageRegister.this, dataBeans, state, dialog, ManaVillageRegister.this);
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ManaVillageRegister.this, "No State's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminActiveStateList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(ManaVillageRegister.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });

        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else {

        }


    }

    private void updateLabel() {

        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dob.setText(sdf.format(myCalendar.getTime()));
        DOB=dob.getText().toString();
    }


    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();





        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (requestCode == 103 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image1 = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload1.setImageBitmap(converetdImage);
                imageupload1.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload.setImageBitmap(converetdImage);
            imageupload.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }else if (requestCode == 102 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload1.setImageBitmap(converetdImage);
            imageupload1.setVisibility(View.VISIBLE);
            image1 = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private File createImagefile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        picturePath = image.getAbsolutePath();
        return image;
    }

    public void getId(final String selectedworkid) {

        StateId = selectedworkid;


        district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(ManaVillageRegister.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);


                final ProgressDialog progressDialog = new ProgressDialog(ManaVillageRegister.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminActiveDistList> call = apiInterface.admindstActiveList(selectedworkid);
                call.enqueue(new Callback<AdminActiveDistList>() {
                    @Override
                    public void onResponse(Call<AdminActiveDistList> call, Response<AdminActiveDistList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean2 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminActiveDistList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(ManaVillageRegister.this));
                            recyclerAdapter1 = new RecyclerAdapterDstActiveList(ManaVillageRegister.this, dataBeans, dialog, district, ManaVillageRegister.this);
                            recyclerView.setAdapter(recyclerAdapter1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ManaVillageRegister.this, "No Dist's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminActiveDistList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(ManaVillageRegister.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });

    }

    public void getId1(final String selectedworkid) {

        DistId=selectedworkid;


        mandal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(ManaVillageRegister.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);


                final ProgressDialog progressDialog = new ProgressDialog(ManaVillageRegister.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminActiveMandalList> call = apiInterface.adminmandalActiveList(selectedworkid);
                call.enqueue(new Callback<AdminActiveMandalList>() {
                    @Override
                    public void onResponse(Call<AdminActiveMandalList> call, Response<AdminActiveMandalList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean4 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminActiveMandalList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(ManaVillageRegister.this));
                            recyclerAdapter2 = new RecyclerAdapterMandalActiveList(ManaVillageRegister.this, dataBeans,mandal, dialog, ManaVillageRegister.this);
                            recyclerView.setAdapter(recyclerAdapter2);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ManaVillageRegister.this, "No Dist's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminActiveMandalList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(ManaVillageRegister.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });




    }

    public void getId2(final String selectedworkid) {

        MandalId=selectedworkid;

        village.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final Dialog dialog = new Dialog(ManaVillageRegister.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);


                final ProgressDialog progressDialog = new ProgressDialog(ManaVillageRegister.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminActiveVillageList> call = apiInterface.adminvillageActiveList(selectedworkid);
                call.enqueue(new Callback<AdminActiveVillageList>() {
                    @Override
                    public void onResponse(Call<AdminActiveVillageList> call, Response<AdminActiveVillageList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean5 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminActiveVillageList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(ManaVillageRegister.this));
                            recyclerAdapter4 = new RecyclerAdapterVillageActiveList(ManaVillageRegister.this, dataBeans,village, dialog, ManaVillageRegister.this);
                            recyclerView.setAdapter(recyclerAdapter4);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ManaVillageRegister.this, "No Dist's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminActiveVillageList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(ManaVillageRegister.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });

    }

    public void getId3(String selectedworkid) {

       VillageId= selectedworkid;

    }

    public void getId5(String selectedworkid) {

        GameId= selectedworkid;

    }
    private void addplayer(String fname, String lname, String phone, String anum, String address, String pin, String DOB) {

        final ProgressDialog progressDialog = new ProgressDialog(ManaVillageRegister.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        MultipartBody.Part body = null;
        MultipartBody.Part body1 = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);

        } if(image1!=null) {

            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image1);
            body1 = MultipartBody.Part.createFormData("aadhar_image", image1.getName(), requestFile);
            // Toast.makeText(this, "Please upload image", Toast.LENGTH_SHORT).show();
        }

        RequestBody Rfname = RequestBody.create(MediaType.parse("multipart/form-data"), fname);
        RequestBody Rlname = RequestBody.create(MediaType.parse("multipart/form-data"), lname);
        RequestBody Rphone = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
        RequestBody Rnum = RequestBody.create(MediaType.parse("multipart/form-data"), anum);
        RequestBody Raddress = RequestBody.create(MediaType.parse("multipart/form-data"), address);
        RequestBody Rpin = RequestBody.create(MediaType.parse("multipart/form-data"), pin);
        RequestBody RDob = RequestBody.create(MediaType.parse("multipart/form-data"), DOB);
        RequestBody SId = RequestBody.create(MediaType.parse("multipart/form-data"), StateId);
        RequestBody DId = RequestBody.create(MediaType.parse("multipart/form-data"), DistId);
        RequestBody MId = RequestBody.create(MediaType.parse("multipart/form-data"), MandalId);
        RequestBody VId = RequestBody.create(MediaType.parse("multipart/form-data"), VillageId);
        RequestBody GId = RequestBody.create(MediaType.parse("multipart/form-data"), GameId);




        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddPlayerResponse> call = apiInterface.adminAddPlayer(Rfname, Rlname, Rphone, Rnum, Raddress,Rpin,RDob,SId,DId,MId,VId,GId,body,body1);
        call.enqueue(new Callback<AddPlayerResponse>() {
            @Override
            public void onResponse(Call<AddPlayerResponse> call, Response<AddPlayerResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    AddPlayerResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(ManaVillageRegister.this, "OTP Sent Successfully...", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ManaVillageRegister.this, OTPPlayer.class);
                    intent.putExtra("MobileNumber",Phone);
                    startActivity(intent);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ManaVillageRegister.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AddPlayerResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ManaVillageRegister.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

    }



}
