package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkReport1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkReport2;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.EngagerReportLIst;
import com.igrand.ThreeWaySolutions.Response.WorkReportLIst;

import java.util.List;

public class EngagerReports extends BaseActivity {

    RecyclerView recyclerView;
    ApiInterface apiInterface;
    EngagerReportLIst.StatusBean statusBean;
    RecyclerWorkReport2 recyclerUser;
    ImageView back;
    ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engager_reports);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


      /*  final ProgressDialog progressDialog = new ProgressDialog(EngagerReports.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EngagerReportLIst> call = apiInterface.EngagerReportLIst();
        call.enqueue(new Callback<EngagerReportLIst>() {
            @Override
            public void onResponse(Call<EngagerReportLIst> call, Response<EngagerReportLIst> response) {

                if (response.code() == 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<List<EngagerReportLIst.DataBean>> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(EngagerReports.this));
                    recyclerUser = new RecyclerWorkReport2(EngagerReports.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);





                } else if (response.code() != 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(EngagerReports.this, "No EngagerReports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<EngagerReportLIst> call, Throwable t) {
                //progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(EngagerReports.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });






    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
