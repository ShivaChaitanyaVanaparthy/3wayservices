package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ForgetPassword;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.OTP;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddProjects;
import com.igrand.ThreeWaySolutions.Response.AdminForgetPassword;

public class ProjectsAdmin extends BaseActivity {

    ImageView back;
    ApiInterface apiInterface;
    EditText projectname,villagename;
    RadioButton active,inactive;
    RadioGroup rg;
    AdminAddProjects.StatusBean statusBean;
    String mobileNumber,Project,Village,Status;
    Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects_admin);
        back=findViewById(R.id.back);

        projectname=findViewById(R.id.projectname);
        villagename=findViewById(R.id.villagename);
        active=findViewById(R.id.active);
        inactive=findViewById(R.id.inactive);
        rg=findViewById(R.id.rg);
        add=findViewById(R.id.add);

        rg.check(R.id.active);

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Project=projectname.getText().toString();
                Village=villagename.getText().toString();
                if(active.isChecked()){
                    Status="1";
                } else if(inactive.isChecked()){
                    Status="0";
                }


                final ProgressDialog progressDialog = new ProgressDialog(ProjectsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminAddProjects> call = apiInterface.adminAddProjects(mobileNumber,Project,Village,Status);
                call.enqueue(new Callback<AdminAddProjects>() {
                    @Override
                    public void onResponse(Call<AdminAddProjects> call, Response<AdminAddProjects> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(ProjectsAdmin.this, "Project Added Successfully......", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(ProjectsAdmin.this, ProjectsList.class);
                            intent.putExtra("MobileNumber",mobileNumber);
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ProjectsAdmin.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminAddProjects> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(ProjectsAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });
            }
        });

    }
}
