package com.igrand.ThreeWaySolutions.Activities.PROCUREMENT;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments2;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProcDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProcDocumentResponse;

import java.util.ArrayList;
import java.util.List;

public class ProcDocList extends BaseActivity {

    ApiInterface apiInterface;
    RecyclerView recyclerView;
    String ID;
   // ImageView back,img;
    TextView imgtxt;
    ImageView back;
    ProcDocumentResponse.StatusBean statusBean;
    String document,Document;
    List<String> strings;
    ArrayList<String> list=new ArrayList<>();
    RecyclerAdapterAgentDocuments2 recyclerAdapter1;
    RecyclerAdapterProcDocuments recyclerAdapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proc_doc_list);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
       // img=findViewById(R.id.img);
        //imgtxt=findViewById(R.id.imgtxt);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(getIntent()!=null){
            ID=getIntent().getStringExtra("ID");
        }


        final ProgressDialog progressDialog = new ProgressDialog(ProcDocList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProcDocumentResponse> call = apiInterface.ProcList(ID);
        call.enqueue(new Callback<ProcDocumentResponse>() {
            @Override
            public void onResponse(Call<ProcDocumentResponse> call, Response<ProcDocumentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                     statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(ProcDocList.this, "Document's List...", Toast.LENGTH_SHORT).show();



                    List<ProcDocumentResponse.ProposalDocumentsBean> dataBeans=response.body().getProposal_documents();


                    recyclerView.setLayoutManager(new LinearLayoutManager(ProcDocList.this));
                    recyclerAdapter2 = new RecyclerAdapterProcDocuments(ProcDocList.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter2);




                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ProcDocList.this, "No Document Found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<ProcDocumentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ProcDocList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }
}
