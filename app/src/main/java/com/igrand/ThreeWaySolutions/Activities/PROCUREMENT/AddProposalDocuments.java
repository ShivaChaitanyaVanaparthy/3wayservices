package com.igrand.ThreeWaySolutions.Activities.PROCUREMENT;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.AddDocumentChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.PdfActivityChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerProcurement;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter1;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddProposalDocumentsResponse;
import com.squareup.picasso.Picasso;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.vincent.filepicker.activity.BaseActivity.IS_NEED_FOLDER_LIST;
import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class AddProposalDocuments extends BaseActivity {

    TextView proposaldocument;
    EditText proposalname;
    Button add;
    ImageView imageupload;
    ApiInterface apiInterface;
    ImageView back;


    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    String imagePic, picturePath,ProposalName,Id,MobileNumber;
    Dialog dialog;


    private ArrayList<String> mResults = new ArrayList<>();
    private static final int REQUEST_CODE1 = 732;
    public static List<String> selectedvideoImgList = new ArrayList<>();
    String multi_image_path = "empty";
    List<MultipartBody.Part> images_array = new ArrayList<>();
    RecyclerAdapter1 recyclerAdapter1;
    RecyclerView recyclerView;
    PrefManagerProcurement prefManagerProcurement;
    LinearLayout folder;
    ImageView upload;
    String  Name,pdf,Property, Date, Village, Checking, Status, Comments, Image,MOBILE,Propertydesc,LegalStatus,LegalDate,SurveyStatus,SurveyDate,Acres,Survey,Mandal,District, Address, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate, Document,LesionDate,LesionStatus,Latitude,Longitude;
    ArrayList<String> pdflist=new ArrayList<>();
    EditText docdes;
    String DocDes;

    List<MultipartBody.Part> images_array1 = new ArrayList<>();
    ImageView pdf1;
    String emptydocument="";

    ArrayList<NormalFile> list;
    ArrayList<String> PdfList=new ArrayList<>();
    ArrayList<ImageFile> list1;
    ArrayList<String> ImageList=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_proposal_documents);

        imageupload=findViewById(R.id.imageupload);
        proposaldocument=findViewById(R.id.proposaldocument);
        proposalname=findViewById(R.id.proposalname);
        add=findViewById(R.id.add);
        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);
        folder=findViewById(R.id.folder);
        upload=findViewById(R.id.upload);
        docdes=findViewById(R.id.docdes);
        pdf1=findViewById(R.id.pdf);




        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (getIntent() != null) {

            Id = getIntent().getStringExtra("Id");
            Name = getIntent().getStringExtra("Name");
            pdf = getIntent().getStringExtra("pdf");
            pdflist = (ArrayList<String>)getIntent().getSerializableExtra("pdflist");

            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Image = getIntent().getStringExtra("Image");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");
            MOBILE = getIntent().getStringExtra("MOBILE");
            Propertydesc = getIntent().getStringExtra("Propertydesc");

            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");

            LegalStatus = getIntent().getStringExtra("LegalStatus");
            SurveyStatus = getIntent().getStringExtra("SurveyStatus");
            LegalDate = getIntent().getStringExtra("LegalDate");
            SurveyDate = getIntent().getStringExtra("SurveyDate");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");
           // MobileNumber = getIntent().getStringExtra("MobileNumber");
        }


        if(pdflist!=null){
            pdf1.setVisibility(View.VISIBLE);
            pdf1.setImageResource(R.drawable.imagepdf);
        }


        folder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent intent4 = new Intent(AddProposalDocuments.this, NormalFilePickActivity.class);
                intent4.putExtra(Constant.MAX_NUMBER, 1);
                intent4.putExtra(IS_NEED_FOLDER_LIST, true);
                intent4.putExtra(NormalFilePickActivity.SUFFIX,
                        new String[] {"xlsx", "xls", "doc", "dOcX", "ppt", ".pptx", "pdf"});
                startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);

                /*Intent intent = new Intent(AddProposalDocuments.this, PdfActivityProcurement.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("Id",Id);
                intent.putExtra("Property",Property);
                intent.putExtra("Date",Date);
                intent.putExtra("Village",Village);
                intent.putExtra("Checking",Checking);
                intent.putExtra("Document",Document);
                intent.putExtra("Image",Image);
                intent.putExtra("Status",Status);
                intent.putExtra("Comments",Comments);
                intent.putExtra("Latitude",Latitude);
                intent.putExtra("Longitude",Longitude);
                intent.putExtra("MOBILE",MobileNumber);
                intent.putExtra("MarketingStatus",MarketingStatus);
                intent.putExtra("MarketingDate",MarketingDate);
                intent.putExtra("CheckingDate",CheckingDate);
                intent.putExtra("ProcurementDate",ProcurementDate);
                intent.putExtra("ProcurementStatus",ProcurementStatus);
                intent.putExtra("Acres",Acres);
                intent.putExtra("Survey",Survey);
                intent.putExtra("Propertydesc",Propertydesc);
                intent.putExtra("Mandal",Mandal);
                intent.putExtra("District",District);


                startActivity(intent);*/
            }
        });
        prefManagerProcurement=new PrefManagerProcurement(AddProposalDocuments.this);
        HashMap<String, String> profile=prefManagerProcurement.getUserDetails();
        MobileNumber=profile.get("mobilenumber");

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProposalName=proposalname.getText().toString();
                DocDes=docdes.getText().toString();
                addProposal(Id,MobileNumber,ProposalName,DocDes);
            }
        });

        proposaldocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent1 = new Intent(AddProposalDocuments.this, ImagePickActivity.class);
                intent1.putExtra(IS_NEED_CAMERA, true);
                intent1.putExtra(Constant.MAX_NUMBER, 1);
                intent1.putExtra(IS_NEED_FOLDER_LIST, true);
                startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_IMAGE);



                /*LinearLayout camera, folder,folder1;

                dialog = new Dialog(AddProposalDocuments.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);

                folder1.setVisibility(View.GONE);
                camera.setVisibility(View.VISIBLE);






                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                               // STORAGE_PERMISSION_CODE);
                       *//* Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();*//*

                   *//*     Intent intent = new Intent(AddProposalDocuments.this, ImagesSelectorActivity.class);
                        // max number of images to be selected
                        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 5);
                        // min size of image which will be shown; to filter tiny images (mainly icons)
                        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
                        // show camera or not
                        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                        // pass current selected images as the initial value
                        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                        // start the selector
                        startActivityForResult(intent, REQUEST_CODE1);


                        dialog.dismiss();*//*

                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();


                    }
                });*/


            }
        });




        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }

    }


    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == RESULT_OK) {
                    list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    StringBuilder builder = new StringBuilder();
                    for (NormalFile file : list) {
                        String path = file.getPath();
                        builder.append(path + "\n");
                        PdfList.add(path);
                    }

                    pdf1.setVisibility(View.VISIBLE);
                    pdf1.setImageResource(R.drawable.imagepdf);

                }

                break;

            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    list1 = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    StringBuilder builder = new StringBuilder();
                    for (ImageFile file : list1) {
                        String path = file.getPath();
                        builder.append(path + "\n");
                        ImageList.add(path);
                    }
                    imageupload.setVisibility(View.VISIBLE);
                    File imgFile1 = new  File(ImageList.get(0));
                    Bitmap myBitmap1 = BitmapFactory.decodeFile(imgFile1.getAbsolutePath());
                    imageupload.setImageBitmap(myBitmap1);

                }
                break;
        }


        if (requestCode == REQUEST_CODE1) {
            if (resultCode == RESULT_OK) {
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);


                if (mResults.size() > 0) {
                    for (int index = 0; index < mResults.size(); index++) {

                        selectedvideoImgList.add(mResults.get(index));

                        multi_image_path = mResults.get(index);
                        String PickedImgPath = mResults.get(index);
                        System.out.println("path" + PickedImgPath);

                        File file = new File(mResults.get(index));
                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array.add(MultipartBody.Part.createFormData("proposal_document", file.getName(), surveyBody));


                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(AddProposalDocuments.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(AddProposalDocuments.this, selectedvideoImgList, mResults);
                    recyclerView.setAdapter(recyclerAdapter1);


                  /*  assert mResults != null;

                    // show results in textview
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                    for (String result : mResults) {
                        sb.append(result).append("\n");
                    }
                    tvResults.setText(sb.toString());*/
                }
            }


        }

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {

            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload.setImageBitmap(converetdImage);
            imageupload.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");

            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
            images_array.add(MultipartBody.Part.createFormData("proposal_document", image .getName(), surveyBody));


            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }



        }
    }

    private void addProposal(String id, String mobileNumber, String proposalName, String docDes) {





            final ProgressDialog progressDialog = new ProgressDialog(AddProposalDocuments.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();

            MultipartBody.Part body = null;
            MultipartBody.Part body1 = null;


        if(ImageList.size()>0){

            for (int index = 0; index < ImageList.size(); index++) {


                File file = new File(ImageList.get(index));
                if (file != null) {
                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    body = MultipartBody.Part.createFormData("proposal_document", file.getName(), requestBody);
                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


                }else {

                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
                    body = MultipartBody.Part.createFormData("proposal_document", emptydocument, requestBody);
                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

                }

            }

        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
            body = MultipartBody.Part.createFormData("proposal_document", emptydocument, requestBody);
            //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

        }



        if(PdfList.size() > 0){


            for (int index = 0; index < PdfList.size(); index++) {


                File file = new File(PdfList.get(index));
                if (file != null) {

                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), file);
                    body1 = MultipartBody.Part.createFormData("proposal_document_pdf", file.getName(), requestBody);
                    images_array1.add(MultipartBody.Part.createFormData("proposal_document_pdf", file.getName(), requestBody));
                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

                }else {

                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
                    body1 = MultipartBody.Part.createFormData("proposal_document_pdf", emptydocument, requestBody);

                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

                }



            }

        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
            body1 = MultipartBody.Part.createFormData("proposal_document_pdf", emptydocument, requestBody);
            //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

        }


          /*  if (image != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                body = MultipartBody.Part.createFormData("proposal_document", image.getName(), requestFile);
            }
            if(pdflist!=null){


                for (int index = 0; index < pdflist.size(); index++) {

                    File file = new File(pdflist.get(index));
                    // Parsing any Media type file
                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), file);
                    body1 = MultipartBody.Part.createFormData("proposal_document_pdf", file.getName(), requestBody);

                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                    images_array1.add(MultipartBody.Part.createFormData("proposal_document_pdf", file.getName(), requestBody));
                }

            }*/

                RequestBody LeadId = RequestBody.create(MediaType.parse("multipart/form-data"), id);
                RequestBody MobileNumber = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
                RequestBody ProposalNamee = RequestBody.create(MediaType.parse("multipart/form-data"), proposalName);
                RequestBody ProposalPDF = RequestBody.create(MediaType.parse("multipart/form-data"), docDes);

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddProposalDocumentsResponse> call = apiInterface.addproposaldocument(LeadId, MobileNumber, ProposalNamee, body,images_array1);
                call.enqueue(new Callback<AddProposalDocumentsResponse>() {
                    @Override
                    public void onResponse(Call<AddProposalDocumentsResponse> call, Response<AddProposalDocumentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            AddProposalDocumentsResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(AddProposalDocuments.this, "Proposal Documents Added Successfully...", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AddProposalDocuments.this, LeadDetailsProcurement.class);
                            intent.putExtra("MobileNumber",mobileNumber);
                            intent.putExtra("ID",Id);
                            intent.putExtra("Property",Property);
                            intent.putExtra("Date",Date);
                            intent.putExtra("Village",Village);
                            intent.putExtra("Checking",Checking);
                            intent.putExtra("Document",Document);
                            intent.putExtra("Image",Image);
                            intent.putExtra("Status",Status);
                            intent.putExtra("Comments",Comments);
                            intent.putExtra("Latitude",Latitude);
                            intent.putExtra("Longitude",Longitude);
                            intent.putExtra("MOBILE",mobileNumber);
                            intent.putExtra("MarketingStatus",MarketingStatus);
                            intent.putExtra("MarketingDate",MarketingDate);
                            intent.putExtra("CheckingDate",CheckingDate);
                            intent.putExtra("ProcurementDate",ProcurementDate);
                            intent.putExtra("ProcurementStatus",ProcurementStatus);
                            intent.putExtra("Acres",Acres);
                            intent.putExtra("Survey",Survey);
                            intent.putExtra("Propertydesc",Propertydesc);
                            intent.putExtra("Mandal",Mandal);
                            intent.putExtra("District",District);
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddProposalDocuments.this, "Error", Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<AddProposalDocumentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddProposalDocuments.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });




    }
}
