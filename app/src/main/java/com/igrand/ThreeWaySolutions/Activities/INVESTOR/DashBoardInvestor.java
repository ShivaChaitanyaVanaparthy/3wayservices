package com.igrand.ThreeWaySolutions.Activities.INVESTOR;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.R;

public class DashBoardInvestor extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_investor);
    }
}
