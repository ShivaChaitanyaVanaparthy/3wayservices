package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.CHECKING.AddDocumentChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerMarketing;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.AddDocumentMarketing;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.PdfActivity1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter3;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter4;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.TechnicalDocResponse;
import com.squareup.picasso.Picasso;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static com.vincent.filepicker.activity.BaseActivity.IS_NEED_FOLDER_LIST;
import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class AddDocumentMarketing extends BaseActivity {
    LinearLayout document,folder;
    Dialog dialog;
    EditText imgdesc,docdesc;
    String ImgDes,DocDes,Id;
    private ArrayList<String> mResults = new ArrayList<>();

    private static final int REQUEST_CODE1 = 732;

    ArrayList<String> pdflist=new ArrayList<>();
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    String imagePic, picturePath,Name;
    ImageView imageupload;
    Bitmap converetdImage;
    public static List<String> selectedvideoImgList = new ArrayList<>();
    String multi_image_path = "empty";
    List<MultipartBody.Part> images_array = new ArrayList<>();
    List<MultipartBody.Part> images_array1 = new ArrayList<>();
    RecyclerAdapter1 recyclerAdapter1;
    RecyclerView recyclerView,recyclerView1;
    int CAMERA_CAPTURE = 1;
    int OPEN_MEDIA_PICKER = 2;
    int PICK_IMAGE = 3;
    int PICK_FILE_REQUEST = 7;
    ArrayList<String> myList;
    String pickedDocPath = null;
    ArrayList<File> document_file = new ArrayList<>();
    public static final int MULTIIMAGE = 1100;

    String imageEncoded,pdf,MobileNumber;
    List<String> imagesEncodedList;
    RecyclerAdapter4 recyclerAdapter4;
    RecyclerAdapter3 recyclerAdapter3;
    private static final int BUFFER_SIZE =1024 * 2;
    private static final String IMAGE_DIRECTORY = "/demonuts_upload_gallery";
    PrefManagerMarketing prefManagerMarketing;
    Button submit;

    ApiInterface apiInterface;
    String emptydocument="";
    ImageView pdf1,back;
    String  Property, Date, Village, Checking, Status, Comments, Image,MOBILE,Propertydesc,LegalStatus,LegalDate,SurveyStatus,SurveyDate,Acres,Survey,Mandal,District, Address, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate, Document,LesionDate,LesionStatus,Latitude,Longitude;

    ArrayList<NormalFile> list;
    ArrayList<String> PdfList=new ArrayList<>();
    ArrayList<ImageFile> list1;
    ArrayList<String> ImageList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_document_marketing);

        document=findViewById(R.id.document);
        imageupload=findViewById(R.id.imageupload);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView1=findViewById(R.id.recyclerView1);
        folder=findViewById(R.id.folder);
        imgdesc=findViewById(R.id.imgdes);
        docdesc=findViewById(R.id.docdes);
        submit=findViewById(R.id.submit);
        pdf1=findViewById(R.id.pdf);
        back=findViewById(R.id.back);

        prefManagerMarketing=new PrefManagerMarketing(AddDocumentMarketing.this);
        HashMap<String, String> profile=prefManagerMarketing.getUserDetails();
        MobileNumber=profile.get("mobilenumber");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (getIntent() != null) {

            Id = getIntent().getStringExtra("Id");
            Name = getIntent().getStringExtra("Name");
            pdf = getIntent().getStringExtra("pdf");
            pdflist = (ArrayList<String>)getIntent().getSerializableExtra("pdflist");


            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Image = getIntent().getStringExtra("Image");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");
            MOBILE = getIntent().getStringExtra("MOBILE");
            Propertydesc = getIntent().getStringExtra("Propertydesc");

            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");

            LegalStatus = getIntent().getStringExtra("LegalStatus");
            SurveyStatus = getIntent().getStringExtra("SurveyStatus");
            LegalDate = getIntent().getStringExtra("LegalDate");
            SurveyDate = getIntent().getStringExtra("SurveyDate");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");
        }

        if(pdflist!=null){
            pdf1.setVisibility(View.VISIBLE);
            pdf1.setImageResource(R.drawable.imagepdf);
        }


        folder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent4 = new Intent(AddDocumentMarketing.this, NormalFilePickActivity.class);
                intent4.putExtra(Constant.MAX_NUMBER, 1);
                intent4.putExtra(IS_NEED_FOLDER_LIST, true);
                intent4.putExtra(NormalFilePickActivity.SUFFIX,
                        new String[] {"xlsx", "xls", "doc", "dOcX", "ppt", ".pptx", "pdf"});
                startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);

                       /* Intent intent = new Intent();
                        intent.setType("application/pdf");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select PDF"), 111);
                        dialog.dismiss();*/

/*

                Intent intent = new Intent(AddDocumentMarketing.this, PdfActivityMarketing.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("Id",Id);

                intent.putExtra("Property",Property);
                intent.putExtra("Date",Date);
                intent.putExtra("Village",Village);
                intent.putExtra("Checking",Checking);
                intent.putExtra("Document",Document);
                intent.putExtra("Image",Image);
                intent.putExtra("Status",Status);
                intent.putExtra("Comments",Comments);
                intent.putExtra("Latitude",Latitude);
                intent.putExtra("Longitude",Longitude);
                intent.putExtra("MOBILE",MobileNumber);
                intent.putExtra("MarketingStatus",MarketingStatus);
                intent.putExtra("MarketingDate",MarketingDate);
                intent.putExtra("CheckingDate",CheckingDate);
                intent.putExtra("ProcurementDate",ProcurementDate);
                intent.putExtra("ProcurementStatus",ProcurementStatus);
                intent.putExtra("Acres",Acres);
                intent.putExtra("Survey",Survey);
                intent.putExtra("Propertydesc",Propertydesc);
                intent.putExtra("Mandal",Mandal);
                intent.putExtra("District",District);

                startActivity(intent);
*/



            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImgDes=imgdesc.getText().toString();
                DocDes=docdesc.getText().toString();

                getImg(ImgDes,DocDes);
            }
        });

        document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(AddDocumentMarketing.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(AddDocumentMarketing.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddDocumentMarketing.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(AddDocumentMarketing.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(AddDocumentMarketing.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }

                Intent intent1 = new Intent(AddDocumentMarketing.this, ImagePickActivity.class);
                intent1.putExtra(IS_NEED_CAMERA, true);
                intent1.putExtra(Constant.MAX_NUMBER, 1);
                intent1.putExtra(IS_NEED_FOLDER_LIST, true);
                startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_IMAGE);


               /* LinearLayout camera, folder,folder1;

                dialog = new Dialog(AddDocumentMarketing.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);

                folder1.setVisibility(View.GONE);
                camera.setVisibility(View.VISIBLE);





                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();





                        dialog.dismiss();
                    }
                });*/
            }
        });

        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }

    }


    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == RESULT_OK) {
                    list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    StringBuilder builder = new StringBuilder();
                    for (NormalFile file : list) {
                        String path = file.getPath();
                        builder.append(path + "\n");
                        PdfList.add(path);
                    }

                    pdf1.setVisibility(View.VISIBLE);
                    pdf1.setImageResource(R.drawable.imagepdf);

                }

                break;

            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    list1 = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    StringBuilder builder = new StringBuilder();
                    for (ImageFile file : list1) {
                        String path = file.getPath();
                        builder.append(path + "\n");
                        ImageList.add(path);
                    }
                    imageupload.setVisibility(View.VISIBLE);
                    File imgFile1 = new  File(ImageList.get(0));
                    Bitmap myBitmap1 = BitmapFactory.decodeFile(imgFile1.getAbsolutePath());
                    imageupload.setImageBitmap(myBitmap1);

                }
                break;
        }

        if (requestCode == REQUEST_CODE1) {
            if (resultCode == RESULT_OK) {
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);


                if (mResults.size() > 0) {
                    for (int index = 0; index < mResults.size(); index++) {

                        selectedvideoImgList.add(mResults.get(index));

                        multi_image_path = mResults.get(index);
                        String PickedImgPath = mResults.get(index);
                        System.out.println("path" + PickedImgPath);

                        File file = new File(mResults.get(index));
                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array.add(MultipartBody.Part.createFormData("document[]", file.getName(), surveyBody));


                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(AddDocumentMarketing.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(AddDocumentMarketing.this, selectedvideoImgList, mResults);
                    recyclerView.setAdapter(recyclerAdapter1);


                  /*  assert mResults != null;

                    // show results in textview
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                    for (String result : mResults) {
                        sb.append(result).append("\n");
                    }
                    tvResults.setText(sb.toString());*/
                }
            }


        }










        if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
            if (selectionResult.size() > 0) {
                for (int index = 0; index < selectionResult.size(); index++) {

                    selectedvideoImgList.add(selectionResult.get(index));

                    multi_image_path = selectionResult.get(index);
                    String PickedImgPath = selectionResult.get(index);
                    System.out.println("path" + PickedImgPath);

                    File file = new File(selectionResult.get(index));
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                    images_array.add(MultipartBody.Part.createFormData("document[]", file.getName(), surveyBody));


                }

                recyclerView.setLayoutManager(new LinearLayoutManager(AddDocumentMarketing.this,LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter1 = new RecyclerAdapter1(AddDocumentMarketing.this,selectedvideoImgList, mResults);
                recyclerView.setAdapter(recyclerAdapter1);

//                txt_count1.setText(String.valueOf(myList.size()));
            }
        } else if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == 111) {

                Uri uri = data.getData();
                String uriString = uri.getPath();
                File myFile = new File(uriString);

                String path = getFilePathFromURI(AddDocumentMarketing.this,uri);
                // Log.d("ioooo",path);
                uploadPDF(path);



              /*  Uri uri = data.getData();
                docFilePath = getFileNameByUri(this, uri);

                */




               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                   // docFilePath=getPath(this,uri);
                }*/
            }
        }


        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                myList = (ArrayList<String>) getIntent().getSerializableExtra("List");



                if (myList.size() > 0) {
                    for (int index = 0; index < myList.size(); index++) {

                        selectedvideoImgList.add(myList.get(index));

                        multi_image_path = myList.get(index);
                        String PickedImgPath = myList.get(index);
                        System.out.println("path" + PickedImgPath);



                        File file = new File(myList.get(index));
                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        //String link=file.getName();
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array.add(MultipartBody.Part.createFormData("document[]", file.getName(), surveyBody));


                        /*recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                        recyclerAdapter1 = new RecyclerAdapter1(AddLeadsAgent.this,selectedvideoImgList);
                        recyclerView.setAdapter(recyclerAdapter1);
*/

                       /* recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                        recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this,selectedvideoImgList);
                        recyclerView.setAdapter(recyclerAdapter3);*/


                    /*   recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                        recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this,bitmapArray);
                        recyclerView.setAdapter(recyclerAdapter3);*/


                        //Picasso.get().load(file.getName()).into(imageupload);

                    }


                }
            }

        }


        if (requestCode == PICK_IMAGE) {

            if (resultCode == RESULT_OK) {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                //  PickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
//                    pick_img_part.setImageBitmap(bm);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                c.close();
            }
        } else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
//                pickedDocPath = String.valueOf(filePath);
            pickedDocPath = GalleryUriToPath.getPath(this, filePath);
            document_file.add(new File(pickedDocPath));
            Log.e("path ", pickedDocPath);
            //   Toast.makeText(this, "document has  selected", Toast.LENGTH_SHORT).show();
            String filename = pickedDocPath.substring(pickedDocPath.lastIndexOf("/") + 1);
            //  text_doc_name.setText(filename);
            // text_doc_name.setText("document has  selected");

        }


        if (requestCode == MULTIIMAGE && resultCode == RESULT_OK) {

            if (requestCode == 1100) {

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
             /*   if (data.getData() != null) {

                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();


                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded = cursor.getString(columnIndex);
                    imagesEncodedList.add(imageEncoded);

                    cursor.close();

                    image = new File(String.valueOf(columnIndex));

                    recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsSubAgent1.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(AddLeadsSubAgent1.this, imagesEncodedList);
                    recyclerView.setAdapter(recyclerAdapter1);



                    // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                    images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));





                } else {*/
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);


                        image = new File(imagesEncodedList.get(i));

                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(AddDocumentMarketing.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(AddDocumentMarketing.this, imagesEncodedList, mResults);
                    recyclerView.setAdapter(recyclerAdapter1);


                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);


            ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
            bitmapArray.add(converetdImage); // Add a bitmap




            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);

            images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

            recyclerView.setLayoutManager(new LinearLayoutManager(AddDocumentMarketing.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter4 = new RecyclerAdapter4(bitmapArray,AddDocumentMarketing.this);
            recyclerView.setAdapter(recyclerAdapter4);
            recyclerAdapter4.notifyDataSetChanged();





            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }



        }

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if(requestCode==102) {

            Uri selectedImage1 = data.getData();
            String[] filePathColumn1 = {MediaStore.Images.Media.DATA};
            Cursor cursor1 = getContentResolver().query(selectedImage1,
                    filePathColumn1, null, null, null);
            cursor1.moveToFirst();
            int columnIndex1 = cursor1.getColumnIndex(filePathColumn1[0]);
            picturePath = cursor1.getString(columnIndex1);
            cursor1.close();
            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage1);
                converetdImage = getResizedBitmap(bitmap1, 500);
                imageupload.setImageBitmap(converetdImage);

            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if(requestCode==103 && resultCode == Activity.RESULT_OK){



            Bitmap converetdImage1 = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();

            converetdImage1.compress(Bitmap.CompressFormat.JPEG, 90, bytes1);

            //imageupload.setImageBitmap(converetdImage1);

            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes1.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    private void uploadPDF(String path) {

        String pdfname = String.valueOf(Calendar.getInstance().getTimeInMillis());

        image = new File(path);

        image = new File(Environment.getExternalStorageDirectory(), path);
        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
        images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


        recyclerView1.setLayoutManager(new LinearLayoutManager(AddDocumentMarketing.this, LinearLayoutManager.HORIZONTAL, false));
        recyclerAdapter3 = new RecyclerAdapter3(AddDocumentMarketing.this, images_array, "key5");
        recyclerView1.setAdapter(recyclerAdapter3);
        recyclerAdapter3.notifyDataSetChanged();



    }

    private String getFilePathFromURI(AddDocumentMarketing context, Uri contentUri) {

        String fileName = getFileName(contentUri);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(wallpaperDirectory + File.separator + fileName);
            // create folder if not exists

            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    private String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int copystream(InputStream input, OutputStream output) throws Exception, IOException {
        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }



    private void getImg(String imgDes, String docDes) {



        MultipartBody.Part body = null;
        MultipartBody.Part body1 = null;


        if(ImageList.size()>0){

            for (int index = 0; index < ImageList.size(); index++) {


                File file = new File(ImageList.get(index));
                if (file != null) {
                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    body = MultipartBody.Part.createFormData("document_image", file.getName(), requestBody);
                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


                }else {

                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
                    body = MultipartBody.Part.createFormData("document_image", emptydocument, requestBody);
                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

                }

            }

        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
            body = MultipartBody.Part.createFormData("document_image", emptydocument, requestBody);
            //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

        }



        if(PdfList.size() > 0){


            for (int index = 0; index < PdfList.size(); index++) {


                File file = new File(PdfList.get(index));
                if (file != null) {

                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), file);
                    body1 = MultipartBody.Part.createFormData("document", file.getName(), requestBody);
                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

                }else {

                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
                    body1 = MultipartBody.Part.createFormData("document", emptydocument, requestBody);
                    //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

                }



            }

        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
            body1 = MultipartBody.Part.createFormData("document", emptydocument, requestBody);
            //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

        }




        if(ImageList.size()>0||PdfList.size()>0){

            final ProgressDialog progressDialog = new ProgressDialog(AddDocumentMarketing.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();

            RequestBody ID = RequestBody.create(MediaType.parse("multipart/form-data"), Id);
            RequestBody MOBILE = RequestBody.create(MediaType.parse("multipart/form-data"), MobileNumber);
            RequestBody IMGDES = RequestBody.create(MediaType.parse("multipart/form-data"), imgDes);
            RequestBody DOCDES = RequestBody.create(MediaType.parse("multipart/form-data"), docDes);


            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<TechnicalDocResponse> call = apiInterface.MarkeAddDoc(ID,MOBILE,IMGDES,DOCDES,body,body1);
            call.enqueue(new Callback<TechnicalDocResponse>() {
                @Override
                public void onResponse(Call<TechnicalDocResponse> call, Response<TechnicalDocResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        TechnicalDocResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(AddDocumentMarketing.this, "Image/Document Added Successfully...", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AddDocumentMarketing.this, LeadDetailsMarketing.class);
                        intent.putExtra("MobileNumber",MobileNumber);
                        intent.putExtra("ID",Id);
                        intent.putExtra("Property",Property);
                        intent.putExtra("Date",Date);
                        intent.putExtra("Village",Village);
                        intent.putExtra("Checking",Checking);
                        intent.putExtra("Document",Document);
                        intent.putExtra("Image",Image);
                        intent.putExtra("Status",Status);
                        intent.putExtra("Comments",Comments);
                        intent.putExtra("Latitude",Latitude);
                        intent.putExtra("Longitude",Longitude);
                        intent.putExtra("MOBILE",MobileNumber);
                        intent.putExtra("MarketingStatus",MarketingStatus);
                        intent.putExtra("MarketingDate",MarketingDate);
                        intent.putExtra("CheckingDate",CheckingDate);
                        intent.putExtra("ProcurementDate",ProcurementDate);
                        intent.putExtra("ProcurementStatus",ProcurementStatus);
                        intent.putExtra("Acres",Acres);
                        intent.putExtra("Survey",Survey);
                        intent.putExtra("Propertydesc",Propertydesc);
                        intent.putExtra("Mandal",Mandal);
                        intent.putExtra("District",District);
                        startActivity(intent);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(AddDocumentMarketing.this, "Error...", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<TechnicalDocResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(AddDocumentMarketing.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();

                }
            });



        } else {
            Toast.makeText(this, "Please select Image/PDF", Toast.LENGTH_SHORT).show();
        }




    }

}
