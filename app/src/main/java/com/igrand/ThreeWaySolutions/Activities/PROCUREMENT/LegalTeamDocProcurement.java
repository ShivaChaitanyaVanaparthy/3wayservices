package com.igrand.ThreeWaySolutions.Activities.PROCUREMENT;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddInvestment;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterInventory1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterLegalTeamDocProcurement;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryProjectResponse;
import com.igrand.ThreeWaySolutions.Response.LegalDocResponse;

import java.util.List;

public class LegalTeamDocProcurement extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterLegalTeamDocProcurement recyclerAdapter;
    ApiInterface apiInterface;
    LegalDocResponse.StatusBean statusBean;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal_team_doc_procurement);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(LegalTeamDocProcurement.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LegalDocResponse> call = apiInterface.addLegalDoc();
        call.enqueue(new Callback<LegalDocResponse>() {
            @Override
            public void onResponse(Call<LegalDocResponse> call, Response<LegalDocResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<LegalDocResponse.DocumentNameListBean> dataBeans = response.body().getDocument_name_list();


                           /* for(int i=0;i<=dataBeans.size();i++) {
                                project_name=dataBeans.get(0).getProject_name();
                                break;
                            }
*/
                    Toast.makeText(LegalTeamDocProcurement.this, "Project's List...", Toast.LENGTH_SHORT).show();

                    recyclerView.setLayoutManager(new LinearLayoutManager(LegalTeamDocProcurement.this));
                    recyclerAdapter = new RecyclerAdapterLegalTeamDocProcurement(LegalTeamDocProcurement.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(LegalTeamDocProcurement.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<LegalDocResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(LegalTeamDocProcurement.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



    }
}
