package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN1.UsersListAdmin1;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddUserResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class AddUserAdmin1 extends BaseActivity {

    Button button, submit;
    ImageView back, imageupload;
    EditText userName, phoneNumber, email;
    String UserType, UserName, PhoneNumber, Email, imagePic, picturePath, Active, UserType1, Gender;
    Integer Number;
    ApiInterface apiInterface;
    RadioGroup rg1;
    RadioButton male, female;
    TextView uploadimage;
    private Boolean exit = false;
    ImageView telephonebook;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    String MobileNumber;
    private String TAG = "mobile";
    Dialog dialog;
    int OPEN_MEDIA_PICKER = 2;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    String empty="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user1);


        back = findViewById(R.id.back);
        userName = findViewById(R.id.userName);
        phoneNumber = findViewById(R.id.phoneNumber);
        email = findViewById(R.id.email);
       /* rg = findViewById(R.id.rg);
        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);*/
        submit = findViewById(R.id.submit);
        uploadimage = findViewById(R.id.uploadimage);
        imageupload = findViewById(R.id.imageupload);
        telephonebook = findViewById(R.id.telephonebook);
        rg1 = findViewById(R.id.rg1);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (getIntent() != null) {
            MobileNumber = getIntent().getStringExtra("MobileNumber");
        }

        //rg.check(R.id.active);

        telephonebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(MobileRechargeActivity.this,ContactsActivity.class);
                startActivity(intent);*/

//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                startActivityForResult(intent, 1);

                Uri uri = Uri.parse("content://contacts");
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, 102);
            }
        });




        uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LinearLayout camera, folder;

                dialog = new Dialog(AddUserAdmin1.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                       Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();


                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();



                    }
                });


            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {








                UserName = userName.getText().toString();
                PhoneNumber = phoneNumber.getText().toString();
                Email = email.getText().toString();




                addUser("site_engineer", UserName, PhoneNumber, Email);


            }
        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else {

        }

    }



    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();


        if (requestCode == 102) {

            Uri uri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};

            Cursor cursor1 = getContentResolver().query(uri, projection,
                    null, null, null);
            cursor1.moveToFirst();

            int numberColumnIndex = cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            String number = cursor1.getString(numberColumnIndex);

           // phoneNumber.setText(number);


            if(number.startsWith("+"))
            {
                if(number.length()==13)
                {
                    String str_getMOBILE=number.substring(3);
                    phoneNumber.setText(str_getMOBILE);
                }
                else if(number.length()==14)
                {
                    String str_getMOBILE=number.substring(4);
                    phoneNumber.setText(str_getMOBILE);
                }


            }
            else
            {
                phoneNumber.setText(number);
            }





           /* Pattern complie = Pattern.compile(" ");
            String[] phonenUmber = complie.split(number);
            phoneNumber.setText(phonenUmber[1]);*/
            int nameColumnIndex = cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String name = cursor1.getString(nameColumnIndex);















         /*   public String phoeNumberWithOutCountryCode(String number) {
                Pattern complie = Pattern.compile(" ");
                String[] phonenUmber = complie.split(phoneNumberWithCountryCode);
                Log.e("number is", phonenUmber[1]);
                return phonenUmber[1];
            }
*/

        }


        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload.setImageBitmap(converetdImage);
            imageupload.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private File createImagefile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        picturePath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void addUser(String userType, String userName, String phoneNumber, String email) {


        Number = phoneNumber.length();

        if (Number != 10 || Number.equals("")) {

            Toast.makeText(this, "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (userType == null) {

            Toast.makeText(AddUserAdmin1.this, "Please Select User Type", Toast.LENGTH_SHORT).show();
        } else if (userName.equals("")) {

            Toast.makeText(AddUserAdmin1.this, "Please Select User Name", Toast.LENGTH_SHORT).show();
        } else if (email.equals("")) {

            Toast.makeText(AddUserAdmin1.this, "Please Select Email", Toast.LENGTH_SHORT).show();
        } /*else if(status.equals("")){

            Toast.makeText(AddUserAdmin.this, "Please Select Status", Toast.LENGTH_SHORT).show();
        } */ else if (imageupload.equals("")) {

            Toast.makeText(this, "Please upload image", Toast.LENGTH_SHORT).show();
        } else {


            final ProgressDialog progressDialog = new ProgressDialog(AddUserAdmin1.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();

            MultipartBody.Part body = null;
            if (image != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                body = MultipartBody.Part.createFormData("profile", image.getName(), requestFile);

            } else {

                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), empty);
                body = MultipartBody.Part.createFormData("profile", empty, requestFile);
               // Toast.makeText(this, "Please upload image", Toast.LENGTH_SHORT).show();
            }

            RequestBody UserType1 = RequestBody.create(MediaType.parse("multipart/form-data"), userType);
            RequestBody UserName1 = RequestBody.create(MediaType.parse("multipart/form-data"), userName);
            RequestBody PhoneNumber1 = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNumber);
            RequestBody Email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email);
            // RequestBody Status1 = RequestBody.create(MediaType.parse("multipart/form-data"), status);
            //RequestBody GENDER = RequestBody.create(MediaType.parse("multipart/form-data"), gender);


            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AddUserResponse> call = apiInterface.adminAddUser(UserType1, UserName1, PhoneNumber1, Email1, body);
            call.enqueue(new Callback<AddUserResponse>() {
                @Override
                public void onResponse(Call<AddUserResponse> call, Response<AddUserResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        AddUserResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(AddUserAdmin1.this, "User Added Successfully...", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AddUserAdmin1.this, UsersListAdmin1.class);
                        intent.putExtra("MobileNumber", MobileNumber);
                        startActivity(intent);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIError> converter =
                                ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                        APIError error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIError.StatusBean status=error.getStatus();
                            Toast.makeText(AddUserAdmin1.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }

                    }

                }



                @Override
                public void onFailure(Call<AddUserResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(AddUserAdmin1.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();

                }
            });

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(AddUserAdmin1.this,
                        "Camera Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(AddUserAdmin1.this,
                        "Camera Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(AddUserAdmin1.this,
                        "Storage Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(AddUserAdmin1.this,
                        "Storage Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
