package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.AGENT.NotificationsAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterNotifications;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.NotificationsAdminListResponse;
import com.igrand.ThreeWaySolutions.Response.NotificationsAdminResponse;

import java.util.List;

public class NotificationsAdmin extends BaseActivity {

    ImageView back;
    RecyclerView recyclerView;
    RecyclerAdapterNotifications recyclerAdapterNotifications;
    String mobileNumber,AgentId;
    ApiInterface apiInterface;
    NotificationsAdminListResponse.StatusBean statusBean;
    LinearLayout add;
    ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);
        add=findViewById(R.id.add);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent=new Intent(NotificationsAdmin.this,DashBoardAdmin.class);
               startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            AgentId=getIntent().getStringExtra("AgentId");
        }




        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               Intent intent=new Intent(NotificationsAdmin.this, AddNotificationsAdmin.class);
               startActivity(intent);
            }
        });


       /* final ProgressDialog progressDialog = new ProgressDialog(NotificationsAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationsAdminListResponse> call = apiInterface.notificationList();
        call.enqueue(new Callback<NotificationsAdminListResponse>() {
            @Override
            public void onResponse(Call<NotificationsAdminListResponse> call, Response<NotificationsAdminListResponse> response) {

                if (response.code() == 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;


                   List<NotificationsAdminListResponse.StatusBean.DataBean>  dataBeans1 = response.body().getStatus().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(NotificationsAdmin.this));
                    recyclerAdapterNotifications = new RecyclerAdapterNotifications(NotificationsAdmin.this,dataBeans1);
                    recyclerView.setAdapter(recyclerAdapterNotifications);

                   // Toast.makeText(NotificationsAdmin.this, "Notification's List...", Toast.LENGTH_SHORT).show();


                } else if (response.code() != 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    //Toast.makeText(AddNotificationsAdmin.this, "Please Check the Password and try again...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<NotificationsAdminListResponse> call, Throwable t) {
               // progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(NotificationsAdmin.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });






    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

}

