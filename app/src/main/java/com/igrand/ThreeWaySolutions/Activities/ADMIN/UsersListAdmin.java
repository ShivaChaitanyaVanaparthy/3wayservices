package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterUser;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.UserListAdminResponse;

import java.util.List;

public class UsersListAdmin extends BaseActivity {

    RecyclerAdapterUser recyclerUser;
    RecyclerView recyclerView;
    ImageView back;
   // LinearLayout add;
    Button add;
    String MobileNumber;
    ApiInterface apiInterface;
    UserListAdminResponse.StatusBean statusBean;
    ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);




        if(getIntent()!=null){

            MobileNumber=getIntent().getStringExtra("MobileNumber");
        }
/*
        final ProgressDialog progressDialog = new ProgressDialog(UsersListAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserListAdminResponse> call = apiInterface.adminUserList();
        call.enqueue(new Callback<UserListAdminResponse>() {
            @Override
            public void onResponse(Call<UserListAdminResponse> call, Response<UserListAdminResponse> response) {

                if (response.code() == 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<UserListAdminResponse.DataBean> dataBeans=response.body().getData();
                   // Toast.makeText(UsersListAdmin.this, "User's List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(UsersListAdmin.this));
                    recyclerUser = new RecyclerAdapterUser(UsersListAdmin.this,MobileNumber,dataBeans, "keyadmin");
                    recyclerView.setAdapter(recyclerUser);


                } else if (response.code() != 200) {
                   // progressDialog.dismiss();
                    Toast.makeText(UsersListAdmin.this, "Error while generating OTP...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<UserListAdminResponse> call, Throwable t) {
                //progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(UsersListAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(UsersListAdmin.this, DashBoardAdmin.class);
                intent.putExtra("MobileNumber",MobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(UsersListAdmin.this, AddUserAdmin.class);
                intent.putExtra("MobileNumber",MobileNumber);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(UsersListAdmin.this,DashBoardAdmin.class);
        intent.putExtra("MobileNumber",MobileNumber);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        return;
    }

}
