package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.DashBoardMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjects1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdminComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdminComment1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterNearByProjectList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdditinalInfoResponse;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.NearByProjectResponse;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearByProjectsList1 extends BaseActivity {

    RecyclerView recyclerView,recyclerView1;
    ImageView back;
    RecyclerAdapterNearByProjectList recyclerUser;
    ApiInterface apiInterface;
    NearByProjectResponse.StatusBean statusBean;
    AdditinalInfoResponse.StatusBean statusBean1;
    LinearLayout add;
    String mobileNumber,ID;
    TextView id,date,roadconnectivity,purchasevalue,salesvalue;
    LinearLayout card,linear;
    RelativeLayout relative;
    AgentLeadsCommentsResponse.StatusBean statusBean2;
    RecyclerAdapterAdminComment1 recyclerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by_projects_list1);


        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        //add=findViewById(R.id.add);
        back=findViewById(R.id.back);
        roadconnectivity=findViewById(R.id.roadconnectivity);
        purchasevalue=findViewById(R.id.purchasevalue);
        salesvalue=findViewById(R.id.salesvalue);
        date=findViewById(R.id.date);
        id=findViewById(R.id.id);
        card=findViewById(R.id.card);
        relative=findViewById(R.id.relative);
        recyclerView1=findViewById(R.id.text);
        linear=findViewById(R.id.linear);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            ID=getIntent().getStringExtra("ID");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog1 = new ProgressDialog(NearByProjectsList1.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AgentLeadsCommentsResponse> call1 = apiInterface.agentLeadsComments(ID);
        call1.enqueue(new Callback<AgentLeadsCommentsResponse>() {
            @Override
            public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                if (response.code() == 200) {
                    progressDialog1.dismiss();
                    relative.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    List<AgentLeadsCommentsResponse.DataBean> dataBeans = response.body().getData();
                    //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView1.setLayoutManager(new LinearLayoutManager(NearByProjectsList1.this));
                    recyclerAdapter = new RecyclerAdapterAdminComment1(NearByProjectsList1.this, dataBeans);
                    recyclerView1.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog1.dismiss();
                    Toast.makeText(NearByProjectsList1.this, "No Comments...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(NearByProjectsList1.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });


       /* final ProgressDialog progressDialog = new ProgressDialog(NearByProjectsList1.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdditinalInfoResponse> call = apiInterface.additinalinfo(ID);
        call.enqueue(new Callback<AdditinalInfoResponse>() {
            @Override
            public void onResponse(Call<AdditinalInfoResponse> call, Response<AdditinalInfoResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    relative.setVisibility(View.VISIBLE);
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(NearByProjectsList1.this, "Project List......", Toast.LENGTH_SHORT).show();

                    AdditinalInfoResponse.AdditionalInfoBean dataBeans=response.body().getAdditional_info();
                    if(dataBeans.getLead_id()!=null){
                        linear.setVisibility(View.VISIBLE);
                    }
                    id.setText(dataBeans.getLead_id());
                    date.setText(dataBeans.getDatetime());
                    roadconnectivity.setText(dataBeans.getRoad_connectivity());
                    purchasevalue.setText(dataBeans.getExpected_purchase_value());
                    salesvalue.setText(dataBeans.getExpected_sales_value());



                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(NearByProjectsList1.this, "No Additional Info...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdditinalInfoResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(NearByProjectsList1.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

*/
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });


        final ProgressDialog progressDialog2 = new ProgressDialog(NearByProjectsList1.this);
        progressDialog2.setMessage("Loading.....");
        progressDialog2.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NearByProjectResponse> call2 = apiInterface.nearByProjectList(ID);
        call2.enqueue(new Callback<NearByProjectResponse>() {
            @Override
            public void onResponse(Call<NearByProjectResponse> call, Response<NearByProjectResponse> response) {

                if (response.code() == 200) {
                    progressDialog2.dismiss();
                    relative.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(NearByProjectsList1.this, "Project List......", Toast.LENGTH_SHORT).show();
                    List<NearByProjectResponse.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(NearByProjectsList1.this));
                    recyclerUser = new RecyclerAdapterNearByProjectList(NearByProjectsList1.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog2.dismiss();
                    Toast.makeText(NearByProjectsList1.this, "No project's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<NearByProjectResponse> call, Throwable t) {
                progressDialog2.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(NearByProjectsList1.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }
}
