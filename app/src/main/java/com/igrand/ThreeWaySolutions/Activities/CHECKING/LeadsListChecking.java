package com.igrand.ThreeWaySolutions.Activities.CHECKING;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadsListAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadsListAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterChecking;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;

import java.util.List;

public class LeadsListChecking extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterChecking recyclerAdapter;
    ApiInterface apiInterface;
    ImageView back;
    RecyclerAdapterChecking recyclerAdapterChecking;
    LeadsListCheckingResponse.StatusBean statusBean1;
    String mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_list_checking);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        final ProgressDialog progressDialog = new ProgressDialog(LeadsListChecking.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LeadsListCheckingResponse> call1 = apiInterface.checkingLeadsList();
        call1.enqueue(new Callback<LeadsListCheckingResponse>() {
            @Override
            public void onResponse(Call<LeadsListCheckingResponse> call, Response<LeadsListCheckingResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    List<LeadsListCheckingResponse.DataBean> dataBeans=response.body().getData();
                    //List<String> documents=dataBeans.get(0).getDocument();
                    Toast.makeText(LeadsListChecking.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(LeadsListChecking.this));
                    recyclerAdapterChecking = new RecyclerAdapterChecking(LeadsListChecking.this,dataBeans,mobileNumber);
                    recyclerView.setAdapter(recyclerAdapterChecking);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(LeadsListChecking.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<LeadsListCheckingResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LeadsListChecking.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        /*recyclerView.setLayoutManager(new LinearLayoutManager(LeadsListChecking.this));
        recyclerAdapter = new RecyclerAdapterChecking(LeadsListChecking.this);
        recyclerView.setAdapter(recyclerAdapter);*/

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
