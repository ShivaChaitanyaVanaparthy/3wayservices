package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterUOMList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;

import java.util.List;

public class UomList extends BaseActivity {

    RecyclerView recyclerView;
    ImageView back;
    RecyclerAdapterUOMList recyclerUser;
    ApiInterface apiInterface;
    AdminUOMList.StatusBean statusBean;
    Button add;
    String mobileNumber,keytech;
    ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uom_list);


        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);



        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            keytech=getIntent().getStringExtra("keytech");
        }

        if(keytech!=null){
            if(keytech.equals("keytech")){
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(UomList.this, DashBoardTechnical.class);
                        intent.putExtra("MobileNumber",mobileNumber);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                    }
                });
            }
        } else {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(UomList.this,DashBoardAdmin.class);
                    intent.putExtra("MobileNumber",mobileNumber);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                }
            });
        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(UomList.this, AddUom.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
            }
        });



/*
        final ProgressDialog progressDialog = new ProgressDialog(UomList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminUOMList> call = apiInterface.uomList();
        call.enqueue(new Callback<AdminUOMList>() {
            @Override
            public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {

                if (response.code() == 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(UomList.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminUOMList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(UomList.this));
                    recyclerUser = new RecyclerAdapterUOMList(UomList.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(UomList.this, "No City's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminUOMList> call, Throwable t) {
               // progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(UomList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
