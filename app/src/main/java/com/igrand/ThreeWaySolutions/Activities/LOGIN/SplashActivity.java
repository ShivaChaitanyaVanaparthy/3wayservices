package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN1.DashBoardAdmin1;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.DashBoardSubAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.DashBoardLegal;
import com.igrand.ThreeWaySolutions.Activities.LESION.DashBoardLesion;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.DashBoardMarketing;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PrefManagerAgent;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin1;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerChecking;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerLegal;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerLesion;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerMarketing;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerProcurement;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerSubAgent;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerSurvey;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical1;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.DashBoardProcurement;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.DashBoardSiteEngineer;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.PrefManagerSiteEngineer;
import com.igrand.ThreeWaySolutions.Activities.SURVEY.DashBoardSurvey;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL1.DashBoardTechnical1;
import com.igrand.ThreeWaySolutions.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.os.Build.VERSION_CODES.M;

public class SplashActivity extends BaseActivity {

    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static int splashscreentimeout=2000;
    Dialog dialog;
    RadioGroup rg,rg1;
    RadioButton agent, checking,restuarent1,user1;
    Button registerbutton;
    PrefManagerAdmin prefManagerAdmin;
    PrefManagerAdmin1 prefManagerAdmin1;
    PrefManagerAgent prefManagerAgent;
    PrefManagerChecking prefManagerChecking;
    PrefManagerMarketing prefManagerMarketing;
    PrefManagerTechnical prefManagerTechnical;
    PrefManagerTechnical1 prefManagerTechnical1;
    PrefManagerProcurement prefManagerProcurement;
    PrefManagerSubAgent prefManagerSubAgent;
    PrefManagerLegal prefManagerLegal;
    PrefManagerSurvey prefManagerSurvey;
    PrefManagerLesion prefManagerLesion;
    PrefManagerSiteEngineer prefManagerSiteEngineer;
    private static final String TAG = "user1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        prefManagerAgent = new PrefManagerAgent(SplashActivity.this);
        prefManagerAdmin = new PrefManagerAdmin(SplashActivity.this);
        prefManagerAdmin1 = new PrefManagerAdmin1(SplashActivity.this);
        prefManagerChecking = new PrefManagerChecking(SplashActivity.this);
        prefManagerMarketing= new PrefManagerMarketing(SplashActivity.this);
        prefManagerTechnical= new PrefManagerTechnical(SplashActivity.this);
        prefManagerTechnical1= new PrefManagerTechnical1(SplashActivity.this);
        prefManagerProcurement= new PrefManagerProcurement(SplashActivity.this);
        prefManagerSubAgent= new PrefManagerSubAgent(SplashActivity.this);
        prefManagerLegal= new PrefManagerLegal(SplashActivity.this);
        prefManagerSurvey= new PrefManagerSurvey(SplashActivity.this);
        prefManagerLesion= new PrefManagerLesion(SplashActivity.this);
        prefManagerSiteEngineer= new PrefManagerSiteEngineer(SplashActivity.this);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

               /* if (M <= Build.VERSION.SDK_INT) {*/


                if (checkPermissions()) {

                    if (prefManagerAdmin.isLoggedIn()) {
                        //prefManager1.clearSession();
                        Log.e(TAG, "Admin" + prefManagerAdmin.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardAdmin.class);
                        startActivity(intent);

                    } else if (prefManagerAdmin1.isLoggedIn()) {
                        //prefManager1.clearSession();
                        Log.e(TAG, "Admin1" + prefManagerAdmin1.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardAdmin1.class);
                        startActivity(intent);

                    } else if (prefManagerAgent.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Agent" + prefManagerAgent.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardAgent.class);
                        startActivity(intent);
                    } else if (prefManagerChecking.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Checking" + prefManagerChecking.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardChecking.class);
                        startActivity(intent);
                    } else if (prefManagerMarketing.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Marketing" + prefManagerMarketing.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardMarketing.class);
                        startActivity(intent);
                    }else if (prefManagerTechnical.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Technical" + prefManagerTechnical.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardTechnical.class);
                        startActivity(intent);
                    }else if (prefManagerTechnical1.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Technical" + prefManagerTechnical1.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardTechnical1.class);
                        startActivity(intent);
                    }else if (prefManagerSiteEngineer.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Technical" + prefManagerSiteEngineer.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardSiteEngineer.class);
                        startActivity(intent);
                    } else if (prefManagerProcurement.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Procurement" + prefManagerProcurement.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardProcurement.class);
                        startActivity(intent);
                    } else if (prefManagerSubAgent.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "SubAgent" + prefManagerSubAgent.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardSubAgent.class);
                        startActivity(intent);
                    } else if (prefManagerLegal.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Legal" + prefManagerLegal.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardLegal.class);
                        startActivity(intent);
                    } else if (prefManagerSurvey.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Survey" + prefManagerSurvey.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardSurvey.class);
                        startActivity(intent);
                    } else if (prefManagerLesion.isLoggedIn()) {
                        //  prefManager.clearSession();
                        Log.e(TAG, "Lesion" + prefManagerLesion.isLoggedIn());
                        Intent intent = new Intent(SplashActivity.this, DashBoardLesion.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(SplashActivity.this, SelectModule.class);
                        //intent.putExtra("Home", false);
                        startActivity(intent);

                    }
                }

           /* }  else {

                    Toast.makeText(getApplicationContext(), "Please take the permissions...", Toast.LENGTH_SHORT).show();

                }*/

            }
            },splashscreentimeout);

    }

    private boolean checkPermissions() {
        final Context context = SplashActivity.this;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                //ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                //ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
               // ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED
        )
        {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST);
            return false;
        } else {
            return true;
        }
    }


    protected void onLeaveThisActivity() {
    }

    protected void onStartNewActivity() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
                int i = 0;
                if (grantResults != null && grantResults.length > 0) {
                    for (i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            break;
                        }
                    }
                    if (i == grantResults.length) {
                        Intent intent = new Intent(getApplication(), SelectModule.class);
                        //  intent.putExtra("array", itemList);
                        startActivity(intent);
                    } else {
                        checkPermissions();
                    }
                }
                break;
        }
    }

}


