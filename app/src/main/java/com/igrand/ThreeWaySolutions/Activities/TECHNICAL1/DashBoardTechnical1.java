package com.igrand.ThreeWaySolutions.Activities.TECHNICAL1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ChangePasswordAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.EngagerReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MaterialReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NotificationsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProfileAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ReportsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.SupplierReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkReports;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical1;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.ChangePasswordTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.ProfileTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL1.DashBoardTechnical1;
import com.igrand.ThreeWaySolutions.Activities.ADMIN1.UsersListAdmin1;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProjectList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterTechnicalProjectList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminDashBoardResponse;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

public class DashBoardTechnical1 extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    ImageView toggle, next, next1, next2, next3;
    RecyclerView recyclerView;
    RecyclerAdapterAdmin recyclerAdapter;
    ImageView notification;
    LinearLayout add, users, logout1;
    Boolean exit = false;
    String MobileNumber1, Name;
    LinearLayout leads, totalprojects;
    ApiInterface apiInterface;
    AdminProjectsList.StatusBean statusBean;
    AdminDashBoardResponse.StatusBean statusBean1;
    TextView workreports, engagerreports, supplierresports;
    PrefManagerTechnical1 prefManagerAdmin1;
    TextView name;
    DrawerLayout drawer;
    LinearLayout linear_expand, linear, linear1, linearr;
    RelativeLayout linear_home, linear_projects, linear_profile, linear_changepin, linear_notifications, siteengineer;
    TextView txt_users, txt_values, materialtype, legaldoc, city, worktype, subworktype, uom, machine, linear_reports, state, district, mandal, village, games, materialreport, measurementreport;
    SwipeRefreshLayout pullToRefresh;
    ShimmerFrameLayout mShimmerViewContainer;
    RecyclerAdapterTechnicalProjectList recyclerUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_technical1);

        drawer = findViewById(R.id.drawer_layout);
        toggle = findViewById(R.id.toggle11);
        recyclerView = findViewById(R.id.recyclerView);
        add = findViewById(R.id.add);
        linear_home = findViewById(R.id.linear_home);
        linear_projects = findViewById(R.id.linear_projects);
        city = findViewById(R.id.city);
        linear_changepin = findViewById(R.id.linear_changepin);
        linear_notifications = findViewById(R.id.linear_notifications);
        linear_profile = findViewById(R.id.linear_profile);
        linear_reports = findViewById(R.id.linear_reports);
        linear1 = findViewById(R.id.linear1);
        linearr = findViewById(R.id.linearr);
        notification = findViewById(R.id.notification);
        users = findViewById(R.id.users);
        logout1 = findViewById(R.id.logout1);
        leads = findViewById(R.id.leads);
        name = findViewById(R.id.name);
        next = findViewById(R.id.next);
        next1 = findViewById(R.id.next1);
        worktype = findViewById(R.id.worktype);
        subworktype = findViewById(R.id.subworktype);
        uom = findViewById(R.id.uom);
        machine = findViewById(R.id.machine);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        txt_users = findViewById(R.id.txt_users);
        txt_values = findViewById(R.id.txt_values);
        linear_expand = findViewById(R.id.linear_expand);
        linear = findViewById(R.id.linear);
        next2 = findViewById(R.id.next2);
        next3 = findViewById(R.id.next3);
        workreports = findViewById(R.id.workreports);
        engagerreports = findViewById(R.id.engagerreports);
        supplierresports = findViewById(R.id.supplierreports);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        materialreport = findViewById(R.id.materialreport);
        measurementreport = findViewById(R.id.measurementreport);
        siteengineer = findViewById(R.id.siteengineer);


        workreports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical1.this, WorkReports.class);
                startActivity(intent);
            }
        });


        siteengineer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical1.this, UsersListAdmin1.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });

        engagerreports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical1.this, EngagerReports.class);
                startActivity(intent);
            }
        });

        supplierresports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical1.this, SupplierReports.class);
                startActivity(intent);
            }
        });
        materialreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical1.this, MaterialReports.class);
                startActivity(intent);
            }
        });
        measurementreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical1.this, MeasurementReports.class);
                startActivity(intent);
                // Intent intent=new Intent(DashBoardTechnical1.this,SupplierReports.class);
                // startActivity(intent);
            }
        });


        linear_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.closeDrawers();
            }
        });


        linear_projects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical1.this, ProjectsList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });
        linear_reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical1.this, ReportsList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });


        linear_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical1.this, ProfileTech.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });
        linear_changepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(DashBoardTechnical1.this, ChangePasswordTech.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });


        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //Toast.makeText(DashBoardTechnical1.this, "ggggg", Toast.LENGTH_SHORT).show();
                linearr.setVisibility(linearr.isShown() ? View.GONE : View.VISIBLE);
                if (linearr.isShown()) {
                    linearr.setVisibility(View.VISIBLE);
                    next2.setVisibility(View.GONE);
                    next3.setVisibility(View.VISIBLE);
                } else {
                    next2.setVisibility(View.VISIBLE);
                    next3.setVisibility(View.GONE);
                }

            }
        });


        //expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);

        prefManagerAdmin1 = new PrefManagerTechnical1(DashBoardTechnical1.this);
        if (getIntent() != null) {

            // MobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        prefManagerAdmin1 = new PrefManagerTechnical1(DashBoardTechnical1.this);
        HashMap<String, String> profile = prefManagerAdmin1.getUserDetails();
        MobileNumber1 = profile.get("mobilenumber");
        Name = profile.get("username");


        name.setText(Name);


      /* final ProgressDialog progressDialog = new ProgressDialog(DashBoardTechnical1.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/

        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerAdmin1.clearSession();
                Intent intent = new Intent(DashBoardTechnical1.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });




       /* final ProgressDialog progressDialog1 = new ProgressDialog(DashBoardTechnical1.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminProjectsList> call = apiInterface.adminProjectsList();
        call.enqueue(new Callback<AdminProjectsList>() {
            @Override
            public void onResponse(Call<AdminProjectsList> call, Response<AdminProjectsList> response) {

                if (response.code() == 200) {
                    // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AdminProjectsList.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardTechnical1.this));
                    recyclerUser = new RecyclerAdapterTechnicalProjectList(getApplicationContext(), dataBeans, "key2");
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status = error.getStatus();
                        Toast.makeText(DashBoardTechnical1.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }


            @Override
            public void onFailure(Call<AdminProjectsList> call, Throwable t) {
                // progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(DashBoardTechnical1.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical1.this, NotificationsAdmin.class);
                startActivity(intent);
            }
        });

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    private void refreshData() {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminProjectsList> call = apiInterface.adminProjectsList();
        call.enqueue(new Callback<AdminProjectsList>() {
            @Override
            public void onResponse(Call<AdminProjectsList> call, Response<AdminProjectsList> response) {

                if (response.code() == 200) {
                    // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AdminProjectsList.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardTechnical1.this));
                    recyclerUser = new RecyclerAdapterTechnicalProjectList(getApplicationContext(), dataBeans, "key1");
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status = error.getStatus();
                        Toast.makeText(DashBoardTechnical1.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }


            @Override
            public void onFailure(Call<AdminProjectsList> call, Throwable t) {
                // progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(DashBoardTechnical1.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardTechnical1.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}