package com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdminComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LeadDetailsSubAgent extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1, approvedp;
    String Id, Property, Date, Village, Checking, Status, Comments, GoogleLocation, Address, IFRAME, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate,Address1,GoogleLocation1;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11, statusp, datep,mandal,district;
    Dialog dialog;
    LinearLayout remarks1;
    ApiInterface apiInterface;
    AgentLeadsCommentsResponse.StatusBean statusBean;
    RecyclerAdapterAdminComment recyclerAdapter;

    String Document,Latitude,Longitude,District,Mandal;
    String[] document2;
    RecyclerView recyclerView;
    RecyclerAdapterAgentDocuments recyclerAdapter1;
    List<String> strings = new ArrayList<>();
    List<String> strings1 = new ArrayList<>();
    String[] document3;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_sub_agent);

        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        //document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        date11 = findViewById(R.id.date11);
        remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);
        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        mandal = findViewById(R.id.mandal);
        district = findViewById(R.id.district);

        recyclerView = findViewById(R.id.recyclerView);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");
            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");


            address.setText(Address);

            mandal.setText(Mandal);
            district.setText(District);



        }

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });


        document2=Document.split(",");

        strings = Arrays.asList(Document.split(","));

        for(int i=0;i<strings.size();i++) {

            document3 = strings.get(i).split("\\.");

            if (document3[1].equals("pdf")) {


                recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsSubAgent.this,LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsSubAgent.this,document2,Document,  strings,"key1");
                recyclerView.setAdapter(recyclerAdapter2);

            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsSubAgent.this,LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter1 = new RecyclerAdapterAgentDocuments(LeadDetailsSubAgent.this,document2,Document,strings);
                recyclerView.setAdapter(recyclerAdapter1);

            }
        }



        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        address.setText(Address);

            }
        }

