package com.igrand.ThreeWaySolutions.Activities.TECHNICAL;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.CityList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.EngagerReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MachineList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MaterialReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NotificationsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ReportsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.SubWorkTypeList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.SupplierReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UomList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ValuesList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VendorsList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkReports;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.WorkTypeList;
import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ChangePasswordAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadsListAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.NotificationsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PrefManagerAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ProcuremntLeadsListAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.ProfileAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.UsersListAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.CheckingDashBoardResponse;
import com.igrand.ThreeWaySolutions.Response.NotificationCountResponse;
import com.igrand.ThreeWaySolutions.Response.TechLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.TechnicalDashBoardResponse;

import java.util.HashMap;
import java.util.List;

public class DashBoardTechnical extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    ImageView toggle,next2,next3,next,next1;
    RecyclerView recyclerView;
    RecyclerAdapterAgent recyclerAdapter;
    ImageView notification;
    LinearLayout add,users,logout1;
    Boolean exit=false;
    ApiInterface apiInterface;
    TechLeadsListResponse.StatusBean statusBean;
    String MobileNumber,Name,AgentId,username,UserName;
    LinearLayout approved,rejected;
    TextView name,size1;
    PrefManagerTechnical prefManagerAgent;
    TextView txt_count;
    NotificationCountResponse.StatusBean statusBean3;
    RelativeLayout relative_image;
    Handler handler = new Handler();
    Runnable refresh;
    SwipeRefreshLayout pullToRefresh;
    TechnicalDashBoardResponse.StatusBean statusBean1;
    TextView total,active,inactive,workreports,engagerreports,supplierresports,linear_reports,materialreport,measurementreport,materialtype,city,worktype,subworktype,uom,machine;
    RelativeLayout linear_home,linear_leads,linear_profile,linear_changepin,linear_notifications,linear_projects,linear_vendors;
    LinearLayout linear1,linearr,linear_txt,linear_expand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_technical);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        name = findViewById(R.id.name);
        navigationView.setNavigationItemSelectedListener(this);


        toggle=findViewById(R.id.toggle11);
        recyclerView=findViewById(R.id.recyclerView);
        notification=findViewById(R.id.notification);
        users=findViewById(R.id.users);
        logout1=findViewById(R.id.logout1);
        size1=findViewById(R.id.size);
        txt_count=findViewById(R.id.txt_count);
        relative_image=findViewById(R.id.relative_image);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        total=findViewById(R.id.total);
        active=findViewById(R.id.active);
        inactive=findViewById(R.id.inactive);
        approved=findViewById(R.id.approved);
        rejected=findViewById(R.id.rejected);
        linear_changepin=findViewById(R.id.linear_changepin);
        linear_home=findViewById(R.id.linear_home);
        linear_leads=findViewById(R.id.linear_leads);
        linear_notifications=findViewById(R.id.linear_notifications);
        linear_profile=findViewById(R.id.linear_profile);
        linear1=findViewById(R.id.linear1);
        linearr=findViewById(R.id.linearr);
        next2=findViewById(R.id.next2);
        next3=findViewById(R.id.next3);
        linear_reports=findViewById(R.id.linear_reports);
        workreports=findViewById(R.id.workreports);
        engagerreports=findViewById(R.id.engagerreports);
        supplierresports=findViewById(R.id.supplierreports);
        materialreport=findViewById(R.id.materialreport);
        measurementreport=findViewById(R.id.measurementreport);
        linear_txt=findViewById(R.id.linear_txt);
        linear_expand=findViewById(R.id.linear_expand);
        next=findViewById(R.id.next);
        next1=findViewById(R.id.next1);
        materialtype=findViewById(R.id.materialtype);
        city=findViewById(R.id.city);
        subworktype=findViewById(R.id.subworktype);
        worktype=findViewById(R.id.worktype);
        uom=findViewById(R.id.uom);
        machine=findViewById(R.id.machine);
        linear_projects=findViewById(R.id.linear_projects);
        linear_vendors=findViewById(R.id.linear_vendors);
        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });



        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //Toast.makeText(DashBoardAdmin.this, "ggggg", Toast.LENGTH_SHORT).show();
                linearr.setVisibility(linearr.isShown() ? View.GONE : View.VISIBLE);
                if (linearr.isShown()) {
                    linearr.setVisibility(View.VISIBLE);
                    next2.setVisibility(View.GONE);
                    next3.setVisibility(View.VISIBLE);
                } else {
                    next2.setVisibility(View.VISIBLE);
                    next3.setVisibility(View.GONE);
                }

            }
        });

        linear_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //Toast.makeText(DashBoardAdmin.this, "ggggg", Toast.LENGTH_SHORT).show();
                linear_expand.setVisibility(linear_expand.isShown() ? View.GONE : View.VISIBLE);
                if (linear_expand.isShown()) {
                    linear_expand.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                    next1.setVisibility(View.VISIBLE);
                } else {
                    next.setVisibility(View.VISIBLE);
                    next1.setVisibility(View.GONE);
                }

            }
        });


        linear_projects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical.this, ProjectsList.class);
                intent.putExtra("key","key");
                startActivity(intent);
            }
        });


        linear_vendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical.this, VendorsList.class);
                startActivity(intent);

            }
        });


        linear_reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical.this, ReportsList.class);
                startActivity(intent);
            }
        });

        workreports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, WorkReports.class);
                startActivity(intent);
            }
        });


        engagerreports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, EngagerReports.class);
                startActivity(intent);
            }
        });

        supplierresports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, SupplierReports.class);
                startActivity(intent);
            }
        });
        materialreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(DashBoardTechnical.this, MaterialReports.class);
                startActivity(intent);
            }
        });
        measurementreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(DashBoardTechnical.this, MeasurementReports.class);
                startActivity(intent);
                // Intent intent=new Intent(DashBoardAdmin.this,SupplierReports.class);
                // startActivity(intent);
            }
        });


        materialtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical.this, ValuesList.class);
                intent.putExtra("keytech","keytech");
                startActivity(intent);


            }
        });


        city.setVisibility(View.GONE);
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardTechnical.this, CityList.class);
                startActivity(intent);

            }
        });


        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, WorkTypeList.class);
                intent.putExtra("keytech","keytech");
                startActivity(intent);
            }
        });

        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, SubWorkTypeList.class);
                intent.putExtra("keytech","keytech");
                startActivity(intent);
            }
        });

        uom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, UomList.class);
                intent.putExtra("keytech","keytech");
                startActivity(intent);
            }
        });

        machine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, MachineList.class);
                intent.putExtra("keytech","keytech");
                startActivity(intent);
            }
        });












        linear_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawers();
            }
        }); linear_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical.this, ProfileTech.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("AgentId",AgentId);
                startActivity(intent);
            }
        }); linear_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical.this, NotificationsTech.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("AgentId",AgentId);
                startActivity(intent);
            }
        }); linear_leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical.this, LeadsListTech.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("AgentId",AgentId);
                startActivity(intent);
            }
        }); linear_changepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardTechnical.this, ChangePasswordTech.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("AgentId",AgentId);
                startActivity(intent);
            }
        });





        prefManagerAgent=new PrefManagerTechnical(DashBoardTechnical.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        UserName=profile.get("username");

        name.setText(UserName);


        approved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this,ApprovedLeadsTechnical.class);
                startActivity(intent);
            }
        });

        rejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this,RejectedLeadsTechnical.class);
                startActivity(intent);
            }
        });






        final ProgressDialog progressDialog1 = new ProgressDialog(DashBoardTechnical.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TechnicalDashBoardResponse> call2 = apiInterface.techDashBoard();
        call2.enqueue(new Callback<TechnicalDashBoardResponse>() {
            @Override
            public void onResponse(Call<TechnicalDashBoardResponse> call2, Response<TechnicalDashBoardResponse> response) {

                if (response.code() == 200) {
                    progressDialog1.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    List<TechnicalDashBoardResponse.DataBean> dataBeans1=response.body().getData();

                    active.setText(String.valueOf(dataBeans1.get(0).get_$ApprovedLeads11()));
                    inactive.setText(String.valueOf(dataBeans1.get(0).get_$OpenLeads32()));
                    total.setText(String.valueOf(dataBeans1.get(0).get_$TotalLeads190()));

                } else if (response.code() != 200) {
                    progressDialog1.dismiss();
                    Toast.makeText(DashBoardTechnical.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<TechnicalDashBoardResponse> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardTechnical.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });





        if(getIntent()!=null)
        {
            //MobileNumber=getIntent().getStringExtra("MobileNumber");
            //Name=getIntent().getStringExtra("Name");
            AgentId=getIntent().getStringExtra("AgentId");


        }


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerAgent.clearSession();
                Intent intent = new Intent(DashBoardTechnical.this, Login.class);
                //intent.putExtra("Home",false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });





        final ProgressDialog progressDialog = new ProgressDialog(DashBoardTechnical.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TechLeadsListResponse> call = apiInterface.techLeadsList();
        call.enqueue(new Callback<TechLeadsListResponse>() {
            @Override
            public void onResponse(Call<TechLeadsListResponse> call, Response<TechLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<TechLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardTechnical.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardTechnical.this));
                    recyclerAdapter = new RecyclerAdapterAgent(DashBoardTechnical.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardTechnical.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TechLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardTechnical.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });




        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, NotificationsTech.class);
                startActivity(intent);
            }
        });



        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void refreshData() {


        prefManagerAgent=new PrefManagerTechnical(DashBoardTechnical.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        UserName=profile.get("username");

        name.setText(UserName);




        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationCountResponse> call1 = apiInterface.notificationcounttech(MobileNumber);
        call1.enqueue(new Callback<NotificationCountResponse>() {
            @Override
            public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                if (response.code() == 200) {
                    statusBean3 = response.body() != null ? response.body().getStatus() : null;

                    //int x=statusBean3.getCount();

                    if(statusBean3.getCount()!=0){
                        relative_image.setVisibility(View.VISIBLE);
                        txt_count.setText(String.valueOf(statusBean3.getCount()));

                    }


                } else if (response.code() != 200) {

                }
            }

            @Override
            public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardAgent.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
            }
        });












        if(getIntent()!=null)
        {
            //MobileNumber=getIntent().getStringExtra("MobileNumber");
            //Name=getIntent().getStringExtra("Name");
            AgentId=getIntent().getStringExtra("AgentId");


        }


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerAgent.clearSession();
                Intent intent = new Intent(DashBoardTechnical.this, Login.class);
                //intent.putExtra("Home",false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });






        final ProgressDialog progressDialog = new ProgressDialog(DashBoardTechnical.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TechLeadsListResponse> call = apiInterface.techLeadsList();
        call.enqueue(new Callback<TechLeadsListResponse>() {
            @Override
            public void onResponse(Call<TechLeadsListResponse> call, Response<TechLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<TechLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(DashBoardTechnical.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardTechnical.this));
                    recyclerAdapter = new RecyclerAdapterAgent(DashBoardTechnical.this,dataBeans,MobileNumber);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(DashBoardTechnical.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TechLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardTechnical.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });



        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardTechnical.this, NotificationsTech.class);
                startActivity(intent);
            }
        });

    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardTechnical.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        item.setChecked(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawers();

        if (id == R.id.linear_home) {


        } else if (id == R.id.linear_leads) {

            Intent intent = new Intent(DashBoardTechnical.this, LeadsListTech.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);

        }   else if (id == R.id.linear_profile) {

            Intent intent = new Intent(DashBoardTechnical.this, ProfileTech.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);


        } else if (id == R.id. linear_changepin) {

            Intent intent = new Intent(DashBoardTechnical.this, ChangePasswordTech.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);


        } else if (id == R.id.linear_notifications) {

            Intent intent = new Intent(DashBoardTechnical.this, NotificationsTech.class);
            intent.putExtra("MobileNumber",MobileNumber);
            intent.putExtra("AgentId",AgentId);
            startActivity(intent);


        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}

