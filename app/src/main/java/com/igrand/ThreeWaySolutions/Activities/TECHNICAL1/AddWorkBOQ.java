package com.igrand.ThreeWaySolutions.Activities.TECHNICAL1;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterContractorProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSupplierProjectsList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddBoqResponse;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class AddWorkBOQ extends BaseActivity {

    TextView worktype,subworktype,uom;
    EditText description,no,length,width,depth,quantity;
    String Description,No,Length,Width,Depth,Quantity;
    Button submit;
    ApiInterface apiInterface;
    RecyclerAdapterContractorProjectsList recyclerAdapterContractorProjectsList;
    AdminWorkTypeList.StatusBean statusBean1;
    AdminSubWorkTypeListbyId.StatusBean statusBean2;
    AdminUOMList.StatusBean statusBean4;
    String WorkId,SubWorkId,UomId,ProjectId,key1,key;
    RecyclerAdapterSupplierProjectsList recyclerAdapterSupplierProjectsList1;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_work_b_o_q);

        worktype=findViewById(R.id.worktype);
        submit=findViewById(R.id.submit);
        subworktype=findViewById(R.id.subworktype);
        uom=findViewById(R.id.uom);
        description=findViewById(R.id.description);
        no=findViewById(R.id.no);
        length=findViewById(R.id.length);
        width=findViewById(R.id.width);
        depth=findViewById(R.id.depth);
        quantity=findViewById(R.id.quantity);
        back=findViewById(R.id.back);

        if(getIntent()!=null){
            ProjectId=getIntent().getStringExtra("ID");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddWorkBOQ.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddWorkBOQ.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddWorkBOQ.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(AddWorkBOQ.this, dataBeans, worktype, dialog, AddWorkBOQ.this,"work1");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddWorkBOQ.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddWorkBOQ.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSubWorkType();
            }
        });


        uom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddWorkBOQ.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddWorkBOQ.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminUOMList> call = apiInterface.uomList();
                call.enqueue(new Callback<AdminUOMList>() {
                    @Override
                    public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean4 = response.body() != null ? response.body().getStatus() : null;
                            // Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminUOMList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddWorkBOQ.this));
                            recyclerAdapterSupplierProjectsList1 = new RecyclerAdapterSupplierProjectsList(AddWorkBOQ.this,uom, dataBeans,dialog,"uom1");
                            recyclerView.setAdapter(recyclerAdapterSupplierProjectsList1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddWorkBOQ.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminUOMList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddWorkBOQ.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });






            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Description=description.getText().toString();
                No=no.getText().toString();
                Length=length.getText().toString();
                Width=width.getText().toString();
                Depth=depth.getText().toString();
                Quantity=quantity.getText().toString();


                if(WorkId==null){

                    Toast.makeText(AddWorkBOQ.this, "Please select WorkType", Toast.LENGTH_SHORT).show();
                }else if(SubWorkId==null){

                    Toast.makeText(AddWorkBOQ.this, "Please select SubWorkType", Toast.LENGTH_SHORT).show();
                }else if(UomId==null){

                    Toast.makeText(AddWorkBOQ.this, "Please select Units", Toast.LENGTH_SHORT).show();
                }else if(No.equals("")){

                    Toast.makeText(AddWorkBOQ.this, "Please enter No", Toast.LENGTH_SHORT).show();
                }else if(Quantity.equals("")){

                    Toast.makeText(AddWorkBOQ.this, "Please enter Quantity", Toast.LENGTH_SHORT).show();
                }else if(Length.equals("")){

                    Toast.makeText(AddWorkBOQ.this, "Please enter Length", Toast.LENGTH_SHORT).show();
                }else if(Width.equals("")){

                    Toast.makeText(AddWorkBOQ.this, "Please enter Width", Toast.LENGTH_SHORT).show();
                }else if(Depth.equals("")){

                    Toast.makeText(AddWorkBOQ.this, "Please enter Depth", Toast.LENGTH_SHORT).show();
                }

                else {

                    final ProgressDialog progressDialog = new ProgressDialog(AddWorkBOQ.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AddBoqResponse> call = apiInterface.addBoq(ProjectId,WorkId,SubWorkId,UomId,No,Length,Width,Depth,Quantity);
                    call.enqueue(new Callback<AddBoqResponse>() {
                        @Override
                        public void onResponse(Call<AddBoqResponse> call, Response<AddBoqResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                Toast.makeText(AddWorkBOQ.this, "BOQ added sucessfully...", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(AddWorkBOQ.this, BOQ.class);
                                intent.putExtra("ID",ProjectId);
                                intent.putExtra("key","key");
                                intent.putExtra("key1","key1");
                                startActivity(intent);


                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Converter<ResponseBody, APIError> converter =
                                        ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                APIError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    APIError.StatusBean status=error.getStatus();
                                    Toast.makeText(AddWorkBOQ.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }

                            }

                        }


                        @Override
                        public void onFailure(Call<AddBoqResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast = Toast.makeText(AddWorkBOQ.this,
                                    t.getMessage(), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });


                }




            }
        });



    }

    private void getSubWorkType() {


        final Dialog dialog = new Dialog(AddWorkBOQ.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog_work);

        dialog.show();

        final RecyclerView recyclerView;
        final RelativeLayout linear;
        final Button add;

        recyclerView = dialog.findViewById(R.id.recyclerView);
        linear = dialog.findViewById(R.id.linear);
        add = dialog.findViewById(R.id.add);


        final ProgressDialog progressDialog = new ProgressDialog(AddWorkBOQ.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminSubWorkTypeListbyId> call = apiInterface.adminFsubworkbyid(WorkId);
        call.enqueue(new Callback<AdminSubWorkTypeListbyId>() {
            @Override
            public void onResponse(Call<AdminSubWorkTypeListbyId> call, Response<AdminSubWorkTypeListbyId> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminSubWorkTypeListbyId.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddWorkBOQ.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(AddWorkBOQ.this, dataBeans, dialog, AddWorkBOQ.this, subworktype,"subwork");
                    recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AddWorkBOQ.this, "No SubWorks...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminSubWorkTypeListbyId> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(AddWorkBOQ.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }


    public void getId1(String selectedworkid1) {
        WorkId=selectedworkid1;
    }

    public void getId2(String selectedworkid2) {
        SubWorkId=selectedworkid2;
    }

    public void getId4(String selectedworkid4) {
        UomId=selectedworkid4;
    }
}