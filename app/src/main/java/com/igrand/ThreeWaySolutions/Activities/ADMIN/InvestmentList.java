package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterInvestmentList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterLegalList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminInvestmentList;
import com.igrand.ThreeWaySolutions.Response.AdminLegalList;

import java.util.List;

public class InvestmentList extends BaseActivity {

    Button add;
    ImageView back;
    String mobileNumber;
    ApiInterface apiInterface;
    AdminInvestmentList.StatusBean statusBean;
    RecyclerView recyclerView;
    RecyclerAdapterInvestmentList recyclerUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investment_admin);

        add=findViewById(R.id.add);
        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(InvestmentList.this,AddInvestment.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(InvestmentList.this,DashBoardAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });




        final ProgressDialog progressDialog = new ProgressDialog(InvestmentList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminInvestmentList> call = apiInterface.adminInvestmentList();
        call.enqueue(new Callback<AdminInvestmentList>() {
            @Override
            public void onResponse(Call<AdminInvestmentList> call, Response<AdminInvestmentList> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(InvestmentList.this, "Investments List......", Toast.LENGTH_SHORT).show();
                    List<AdminInvestmentList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(InvestmentList.this));
                    recyclerUser = new RecyclerAdapterInvestmentList(InvestmentList.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(InvestmentList.this, "No Investments...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminInvestmentList> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(InvestmentList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }
}
