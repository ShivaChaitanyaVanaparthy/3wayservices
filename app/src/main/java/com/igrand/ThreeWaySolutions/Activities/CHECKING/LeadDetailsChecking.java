package com.igrand.ThreeWaySolutions.Activities.CHECKING;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NearByProjectsList1;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.ProposalDocuments;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Geolocations;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerChecking;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.AddDocumentTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.TechDocList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterChecking;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsCheckingResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsChecking extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1,approvedp,approveds,approvedl,approvedle;
    String Id, Property, Date, Village, Checking, Status, Comments, GoogleLocation, Address, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, checkingcomments, status11, date11, statusp, datep,statusl,statuss,datel,dates,statusle,datele,acres,survey,district,mandal,add1,comments;
    Dialog dialog;
    Button edit,add;
    ApiInterface apiInterface;
    EditLeadsCheckingResponse.StatusBean statusBean;
    String MobileNumber, ct_status, ct_comments, CommentsChecking, MOBILE,District,Mandal,Propertydesc;
    UpdateLeadsCheckingResponse.StatusBean statusBean1;
    AgentLeadsCommentsResponse.StatusBean statusBean2;
    LinearLayout remarks1;
    RecyclerAdapterCheckingComment recyclerAdapter;
    LinearLayout DocList,imglist;

    EditText commentsonly;
    Button commentsbutton;
    LinearLayout mLayout;

    String LegalStatus,SurveyStatus,LegalDate,SurveyDate,Survey,Acres;
    String Document,LesionDate,LesionStatus,Latitude,Longitude;
    String[] document2;
    RecyclerView recyclerView;
    RecyclerAdapterCheckingDocuments1 recyclerAdapter1;
    LinearLayout marketing,procurement;

    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    private String TAG = "mobile";
    int OPEN_MEDIA_PICKER = 2;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    String empty="";
    ImageView imageupload;
    String imagePic, picturePath,MobileNumber1,pdf,Image;
    PrefManagerChecking prefManagerChecking;

    List<String> strings,stringsimg;
    String[] document3,imagedoc;
    RecyclerAdapterAgentImages recyclerAdapter3;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;
    RecyclerAdapterAgentImages recyclerAdapterAgentImages;
    RecyclerView recyclerVieww;





    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_checking);
        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        edit = findViewById(R.id.edit);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        //checking=findViewById(R.id.Checking);
        //document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);
        //comment11 = findViewById(R.id.comment);
        //checkingcomments = findViewById(R.id.checkingcomments);

        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        date11 = findViewById(R.id.date11);

        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);

        statuss = findViewById(R.id.statuss);
        dates = findViewById(R.id.dates);
        approveds = findViewById(R.id.approveds);
        statusl = findViewById(R.id.statusl);
        datel = findViewById(R.id.datel);
        approvedl = findViewById(R.id.approvedl);

        recyclerView = findViewById(R.id.recyclerView);
        acres = findViewById(R.id.acres);
        survey = findViewById(R.id.survey);
        add = findViewById(R.id.add);
        comments = findViewById(R.id.comments);


        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
        marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);
        district = findViewById(R.id.district);
        mandal = findViewById(R.id.mandal);
        add1 = findViewById(R.id.add1);
        recyclerVieww = findViewById(R.id.recyclerVieww);
        DocList = findViewById(R.id.doclist);
        imglist = findViewById(R.id.imglist);

        prefManagerChecking=new PrefManagerChecking(LeadDetailsChecking.this);
        HashMap<String, String> profile=prefManagerChecking.getUserDetails();
        MobileNumber1=profile.get("mobilenumber");


        if (getIntent() != null) {

            MobileNumber = getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadDetailsChecking.this, DashBoardChecking.class);
                intent.putExtra("MobileNumber", MobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

              // finish();

            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Image = getIntent().getStringExtra("Image");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            MOBILE = getIntent().getStringExtra("MOBILE");
            Propertydesc = getIntent().getStringExtra("Propertydesc");

            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");

            LegalStatus = getIntent().getStringExtra("LegalStatus");
            SurveyStatus = getIntent().getStringExtra("SurveyStatus");
            LegalDate = getIntent().getStringExtra("LegalDate");
            SurveyDate = getIntent().getStringExtra("SurveyDate");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            Status = getIntent().getStringExtra("Status");
            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");
        }

        acres.setText(Acres);
        survey.setText(Survey);
        district.setText(District);
        mandal.setText(Mandal);
        comments.setText(Propertydesc);


        marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(LeadDetailsChecking.this, NearByProjectsList1.class);
                intent.putExtra("ID", Id);
                startActivity(intent);

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsChecking.this, AddDocumentChecking.class);
                intent.putExtra("Id",Id);
                intent.putExtra("MobileNumber",MOBILE);
                intent.putExtra("Property",Property);
                intent.putExtra("Date",Date);
                intent.putExtra("Village",Village);
                intent.putExtra("Checking",Checking);
                intent.putExtra("Document",Document);
                intent.putExtra("Image",Image);
                intent.putExtra("Status",Status);
                intent.putExtra("Comments",Comments);
                intent.putExtra("Latitude",Latitude);
                intent.putExtra("Longitude",Longitude);
                intent.putExtra("MOBILE",MOBILE);
                intent.putExtra("MarketingStatus",MarketingStatus);
                intent.putExtra("MarketingDate",MarketingDate);
                intent.putExtra("CheckingDate",CheckingDate);
                intent.putExtra("ProcurementDate",ProcurementDate);
                intent.putExtra("ProcurementStatus",ProcurementStatus);
                intent.putExtra("Acres",Acres);
                intent.putExtra("Survey",Survey);
                intent.putExtra("Propertydesc",Propertydesc);
                intent.putExtra("Mandal",Mandal);
                intent.putExtra("District",District);
                startActivity(intent);
            }
        });

        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsChecking.this, CheckingDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });


        procurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadDetailsChecking.this, ProposalDocuments.class);
                intent.putExtra("ID", Id);
                startActivity(intent);

            }
        });

        if(Document.equals("")){

            DocList.setVisibility(View.GONE);

        }else {

            document2 = Document.split(",");

            strings = Arrays.asList(Document.split(","));

            for (int i = 0; i < strings.size(); i++) {


                document3 = strings.get(i).split("\\.");
                for (int j = 0; j < document2.length; j++) {

                    pdf = document2[j].substring(document2[j].length() - 3);
                    if (pdf.equals("pdf")) {

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsChecking.this, document2, Document, strings, "keychecking");
                        recyclerView.setAdapter(recyclerAdapter2);

                    } else {


                    }

                }

            }

        }

        if(Image.equals("")){
            imglist.setVisibility(View.GONE);
        }else {
            imagedoc=Image.split(",");
            stringsimg = Arrays.asList(Image.split(","));

            recyclerVieww.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter3 = new RecyclerAdapterAgentImages(LeadDetailsChecking.this,imagedoc,stringsimg,Image,"keychecking");
            recyclerVieww.setAdapter(recyclerAdapter3);
        }









       /* document2 = Document.split(",");

        recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsChecking.this, LinearLayoutManager.HORIZONTAL, false));
        recyclerAdapter1 = new RecyclerAdapterCheckingDocuments1(LeadDetailsChecking.this, document2, Document);
        recyclerView.setAdapter(recyclerAdapter1);
*/
        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });

        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //checking.setText(Checking);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        //remarks1.setText(Comments);


      /*  remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TextView text;
                final RecyclerView recyclerView;
                ImageView back;
                final RelativeLayout linear;

                dialog = new Dialog(LeadDetailsChecking.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialogboxremarks);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                recyclerView = dialog.findViewById(R.id.text);
                linear = dialog.findViewById(R.id.linear);
                // back = dialog.findViewById(R.id.back);


               *//* back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       dialog.dismiss();
                    }
                });*//*


                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsChecking.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AgentLeadsCommentsResponse> call = apiInterface.agentLeadsComments(Id);
                call.enqueue(new Callback<AgentLeadsCommentsResponse>() {
                    @Override
                    public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean2 = response.body() != null ? response.body().getStatus() : null;
                            List<AgentLeadsCommentsResponse.DataBean> dataBeans = response.body().getData();
                            //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                            recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsChecking.this));
                            recyclerAdapter = new RecyclerAdapterCheckingComment(LeadDetailsChecking.this, dataBeans, MobileNumber);
                            recyclerView.setAdapter(recyclerAdapter);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsChecking.this, "No Comments...", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsChecking.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                    }
                });
            }
        });*/

        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        // remarks1.setText(Comments);

        if (Checking != null) {

            if (Checking.equals("0")) {
                edit.setVisibility(View.GONE);
                status.setVisibility(View.VISIBLE);
                status.setText("Rejected");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.rejected);
                date1.setText(CheckingDate);

            } else if (Checking.equals("1")) {
                edit.setVisibility(View.GONE);
                status.setVisibility(View.VISIBLE);
                status.setText("Approved");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.approved);
                date1.setText(CheckingDate);
            } else if (Checking.equals("2")) {
                edit.setVisibility(View.VISIBLE);
                status.setVisibility(View.VISIBLE);
                status.setText("Pending");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.question);
                date1.setText(CheckingDate);
            }


            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }
            }

            if (ProcurementStatus != null) {

                if (ProcurementStatus.equals("4")) {

                    statusp.setText("Rejected");
                    approvedp.setImageResource(R.drawable.rejected);
                    datep.setText(ProcurementDate);

                } else if (ProcurementStatus.equals("5")) {
                    statusp.setText("Approved");
                    approvedp.setImageResource(R.drawable.approved);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("3")) {
                    statusp.setText("Processing");
                    approvedp.setImageResource(R.drawable.processing);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("2")) {
                    statusp.setText("Open");
                    approvedp.setImageResource(R.drawable.open);
                    datep.setText(ProcurementDate);
                }

            }
            if (LegalStatus != null) {

                if (LegalStatus.equals("0")) {

                    statusl.setText("Rejected");
                    approvedl.setImageResource(R.drawable.rejected);
                    datel.setText(LegalDate);

                } else if (LegalStatus.equals("1")) {
                    statusl.setText("Approved");
                    approvedl.setImageResource(R.drawable.approved);
                    datel.setText(LegalDate);
                } else if (LegalStatus.equals("2")) {
                    statusl.setText("Open");
                    approvedl.setImageResource(R.drawable.open);
                    datel.setText(LegalDate);
                }

            }
            if (SurveyStatus != null) {

                if (SurveyStatus.equals("0")) {

                    statuss.setText("Rejected");
                    approveds.setImageResource(R.drawable.rejected);
                    dates.setText(SurveyDate);

                } else if (SurveyStatus.equals("1")) {
                    statuss.setText("Approved");
                    approveds.setImageResource(R.drawable.approved);
                    dates.setText(SurveyDate);
                } else if (SurveyStatus.equals("2")) {
                    statuss.setText("Open");
                    approveds.setImageResource(R.drawable.open);
                    dates.setText(SurveyDate);
                }

            }


            if (LesionStatus != null) {

                if (LesionStatus.equals("0")) {

                    statusle.setText("Rejected");
                    approvedle.setImageResource(R.drawable.rejected);
                    datele.setText(LesionDate);

                } else if (LesionStatus.equals("1")) {
                    statusle.setText("Approved");
                    approvedle.setImageResource(R.drawable.approved);
                    datele.setText(LesionDate);
                } else if (LesionStatus.equals("2")) {
                    statusle.setText("Open");
                    approvedle.setImageResource(R.drawable.open);
                    datele.setText(LesionDate);
                }

            }


            if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

                Picasso.get().load(imagePic).into(imageupload);

                bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
                Log.e("bitmap", "" + bitmap);
                converetdImage = getResizedBitmap(bitmap, 500);

            } else {

            }

            getOutput();


        }
    }




    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload.setImageBitmap(converetdImage);
            imageupload.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private File createImagefile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        picturePath = image.getAbsolutePath();
        return image;
    }


    private void getOutput() {


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final RadioGroup rg;
                final RadioButton accept, reject;
                final EditText comment;
                Button submit;
                final LinearLayout linear;



                dialog = new Dialog(LeadDetailsChecking.this);
                dialog.setContentView(R.layout.dialogboxedit);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                rg = dialog.findViewById(R.id.rg);
                accept = dialog.findViewById(R.id.accept);
                reject = dialog.findViewById(R.id.reject);
                comment = dialog.findViewById(R.id.comments);
                submit = dialog.findViewById(R.id.submit);
                linear = dialog.findViewById(R.id.linear);
                imageupload = dialog.findViewById(R.id.imageupload);




                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsChecking.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<EditLeadsCheckingResponse> call1 = apiInterface.editLeadsChecking(Id, MOBILE);
                call1.enqueue(new Callback<EditLeadsCheckingResponse>() {
                    @Override
                    public void onResponse(Call<EditLeadsCheckingResponse> call, Response<EditLeadsCheckingResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<EditLeadsCheckingResponse.DataBean> dataBeans = response.body().getData();
                            if (dataBeans.get(0).get_$CheckingTeamStatus267().equals("0")) {

                                rg.check(R.id.reject);
                            } else if (dataBeans.get(0).get_$CheckingTeamStatus267().equals("1")) {
                                rg.check(R.id.accept);
                            }

                            // comment.setText(dataBeans.get(0).getCt_comments());


                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsChecking.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<EditLeadsCheckingResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsChecking.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (accept.isChecked()) {

                            ct_status = "1";


                        } else if (reject.isChecked()) {

                            ct_status = "0";

                        }

                        ct_comments = comment.getText().toString();



                        final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsChecking.this);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();
                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<UpdateLeadsCheckingResponse> call1 = apiInterface.updateLeadsChecking(Id, ct_status, ct_comments,MobileNumber1);
                        call1.enqueue(new Callback<UpdateLeadsCheckingResponse>() {
                            @Override
                            public void onResponse(Call<UpdateLeadsCheckingResponse> call, Response<UpdateLeadsCheckingResponse> response) {

                                if (response.code() == 200) {
                                    progressDialog.dismiss();
                                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                    Toast.makeText(LeadDetailsChecking.this, "Leads Updated Successfully", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplication(), DashBoardChecking.class);
                                    intent.putExtra("MobileNumber", MobileNumber);
                                    startActivity(intent);
                                    // comment11.setVisibility(View.VISIBLE);


                                } else if (response.code() != 200) {
                                    progressDialog.dismiss();
                                    Toast.makeText(LeadDetailsChecking.this, "Error...", Toast.LENGTH_SHORT).show();

                                }

                            }


                            @Override
                            public void onFailure(Call<UpdateLeadsCheckingResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                Toast toast = Toast.makeText(LeadDetailsChecking.this,
                                        t.getMessage(), Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();


                            }
                        });



                    }
                });


            }
        });


    }

}
