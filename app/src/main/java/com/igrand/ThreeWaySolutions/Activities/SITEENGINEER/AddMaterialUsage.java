package com.igrand.ThreeWaySolutions.Activities.SITEENGINEER;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddSupplierReport;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterContractorProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSupplierProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterWorkContractor;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddBoqResponse;
import com.igrand.ThreeWaySolutions.Response.AdminAddSupplier;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryMaterialResponse;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;
import com.igrand.ThreeWaySolutions.Response.VendorListResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddMaterialUsage extends BaseActivity {

    TextView worktype,subworktype,uom,name,date1,materialtype;
    EditText opening,received,consumption;
    String Bill,No,Length,Width,Depth,Quantity,SupplierId,Date,Opening,Received,Consumption,MaterialId;
    Button submit;
    ApiInterface apiInterface;
    RecyclerAdapterContractorProjectsList recyclerAdapterContractorProjectsList;
    AdminWorkTypeList.StatusBean statusBean1;
    AdminSubWorkTypeListbyId.StatusBean statusBean2;
    AdminUOMList.StatusBean statusBean4;
    String WorkId,SubWorkId,UomId,ProjectId,key,key1;
    RecyclerAdapterSupplierProjectsList recyclerAdapterSupplierProjectsList1;
    Calendar myCalendar;
    AdminAddSupplier.StatusBean statusBean;
    RecyclerAdapterSupplierProjectsList recyclerAdapterContractorProjectsList1;
    AdminInventoryMaterialResponse.StatusBean statusBean3;
    RecyclerAdapterSupplierProjectsList getRecyclerAdapterSupplierProjectsList1;
    ImageView back;
    RecyclerAdapterWorkContractor recyclerAdapterworkContractor;
    VendorListResponse.StatusBean statusBean6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_material_usage);


        worktype=findViewById(R.id.worktype);
        submit=findViewById(R.id.submit);
        subworktype=findViewById(R.id.subworktype);
        uom=findViewById(R.id.uom);
        name=findViewById(R.id.name);
        date1=findViewById(R.id.date);
        opening=findViewById(R.id.openingstock);
        received=findViewById(R.id.received);
        consumption=findViewById(R.id.consumption);
        materialtype=findViewById(R.id.material);
        back=findViewById(R.id.back);

        if(getIntent()!=null){
            ProjectId=getIntent().getStringExtra("ID");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        String date_n = new SimpleDateFormat("mm/dd/yyyy", Locale.getDefault()).format(new Date());

        date1.setText(date_n);


        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddMaterialUsage.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddMaterialUsage.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddMaterialUsage.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(AddMaterialUsage.this, dataBeans, worktype, dialog, AddMaterialUsage.this,"work3");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddMaterialUsage.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddMaterialUsage.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSubWorkType();
            }
        });


        materialtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final Dialog dialog = new Dialog(AddMaterialUsage.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddMaterialUsage.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminInventoryMaterialResponse> call = apiInterface.adminInventoryMaterial();
                call.enqueue(new Callback<AdminInventoryMaterialResponse>() {
                    @Override
                    public void onResponse(Call<AdminInventoryMaterialResponse> call, Response<AdminInventoryMaterialResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminInventoryMaterialResponse.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddMaterialUsage.this));
                            recyclerAdapterContractorProjectsList1 = new RecyclerAdapterSupplierProjectsList(AddMaterialUsage.this, dataBeans,dialog, materialtype,"material3");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddMaterialUsage.this, "No Materials...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminInventoryMaterialResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddMaterialUsage.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });



        uom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddMaterialUsage.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddMaterialUsage.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminUOMList> call = apiInterface.uomList();
                call.enqueue(new Callback<AdminUOMList>() {
                    @Override
                    public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean4 = response.body() != null ? response.body().getStatus() : null;
                            // Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminUOMList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddMaterialUsage.this));
                            recyclerAdapterSupplierProjectsList1 = new RecyclerAdapterSupplierProjectsList(AddMaterialUsage.this,uom, dataBeans,dialog,"uom3");
                            recyclerView.setAdapter(recyclerAdapterSupplierProjectsList1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddMaterialUsage.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminUOMList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddMaterialUsage.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });






            }
        });

        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {

                String myFormat = "MM/dd/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                date1.setText(sdf.format(myCalendar.getTime()));
            }

        };



        date1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddMaterialUsage.this, R.style.TimePickerTheme,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(AddMaterialUsage.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();


                final ProgressDialog progressDialog = new ProgressDialog(AddMaterialUsage.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<VendorListResponse> call1 = apiInterface.adminVendorList(ProjectId,"2");
                call1.enqueue(new Callback<VendorListResponse>() {
                    @Override
                    public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean6 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<VendorListResponse.StatusBean.VendorsBean> dataBeans=response.body().getStatus().getVendors();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddMaterialUsage.this));
                            recyclerAdapterworkContractor = new RecyclerAdapterWorkContractor(AddMaterialUsage.this, dataBeans, name, dialog,add);
                            recyclerView.setAdapter(recyclerAdapterworkContractor);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddMaterialUsage.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<VendorListResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddMaterialUsage.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Opening=opening.getText().toString();
                        Received=received.getText().toString();
                        Consumption=consumption.getText().toString();
                        Date=date1.getText().toString();



                final ProgressDialog progressDialog = new ProgressDialog(AddMaterialUsage.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddBoqResponse> call = apiInterface.addmaterial(ProjectId,WorkId,SubWorkId,UomId,MaterialId,Opening,Received,Consumption,SupplierId,Date);
                call.enqueue(new Callback<AddBoqResponse>() {
                    @Override
                    public void onResponse(Call<AddBoqResponse> call, Response<AddBoqResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddMaterialUsage.this, "MaterailUsage added sucessfully...", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(AddMaterialUsage.this,MaterialUsage.class);
                            intent.putExtra("ID",ProjectId);
                            intent.putExtra("key",key);
                            intent.putExtra("key1",key1);
                            startActivity(intent);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddMaterialUsage.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                        }

                    }


                    @Override
                    public void onFailure(Call<AddBoqResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddMaterialUsage.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });





            }
        });

    }


    private void getSubWorkType() {


        final Dialog dialog = new Dialog(AddMaterialUsage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog_work);

        dialog.show();

        final RecyclerView recyclerView;
        final RelativeLayout linear;
        final Button add;

        recyclerView = dialog.findViewById(R.id.recyclerView);
        linear = dialog.findViewById(R.id.linear);
        add = dialog.findViewById(R.id.add);


        final ProgressDialog progressDialog = new ProgressDialog(AddMaterialUsage.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminSubWorkTypeListbyId> call = apiInterface.adminFsubworkbyid(WorkId);
        call.enqueue(new Callback<AdminSubWorkTypeListbyId>() {
            @Override
            public void onResponse(Call<AdminSubWorkTypeListbyId> call, Response<AdminSubWorkTypeListbyId> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminSubWorkTypeListbyId.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddMaterialUsage.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(AddMaterialUsage.this, dataBeans, dialog, AddMaterialUsage.this, subworktype,"subwork3");
                    recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AddMaterialUsage.this, "No SubWorks...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminSubWorkTypeListbyId> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(AddMaterialUsage.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }

    public void getId0(String selectedworkid) {

        SupplierId=selectedworkid;
    }

    public void getId1(String selectedworkid1) {
        WorkId=selectedworkid1;
    }

    public void getId2(String selectedworkid2) {
        SubWorkId=selectedworkid2;
    }

    public void getId4(String selectedworkid4) {
        UomId=selectedworkid4;
    }

    public void getId3(String selectedworkid3) {
        MaterialId=selectedworkid3;
    }
}