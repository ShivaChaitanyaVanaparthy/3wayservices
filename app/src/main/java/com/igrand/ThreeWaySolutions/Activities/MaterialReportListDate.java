package com.igrand.ThreeWaySolutions.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.MeasurementReportListDate;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMaterial;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMaterialReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMeasurementReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.MaterialListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementListResponse;

import java.util.List;

public class MaterialReportListDate extends BaseActivity {

    ApiInterface apiInterface;
    RecyclerView recyclerView3;
    RecyclerAdapterMaterialReport recyclerAdapterContractorProjectsList;
    MaterialListResponse.StatusBean statusBean;
    String ID,Date;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_report_list_date);

        recyclerView3 = findViewById(R.id.recyclerView3);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Date=getIntent().getStringExtra("Date");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(MaterialReportListDate.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MaterialListResponse> call = apiInterface.adminMaterialReportList(ID, Date);
        call.enqueue(new Callback<MaterialListResponse>() {
            @Override
            public void onResponse(Call<MaterialListResponse> call, Response<MaterialListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<MaterialListResponse.DataBean> dataBeans = response.body().getData();
                    recyclerView3.setLayoutManager(new LinearLayoutManager(MaterialReportListDate.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterMaterialReport(MaterialReportListDate.this, dataBeans);
                    recyclerView3.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(MaterialReportListDate.this, "No Material Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<MaterialListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(MaterialReportListDate.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });
    }
}