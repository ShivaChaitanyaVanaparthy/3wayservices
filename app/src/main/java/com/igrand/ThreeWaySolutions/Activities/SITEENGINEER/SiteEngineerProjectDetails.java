package com.igrand.ThreeWaySolutions.Activities.SITEENGINEER;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.VendorsList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL1.BOQ;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddVendors;

public class SiteEngineerProjectDetails extends BaseActivity {

    TextView addVendor,workReport,engagerReport,supplierReport,boq,measutement,materal;
    String ID,key,key1,keysite;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_engineer_project_details);

        if(getIntent()!=null){
            ID=getIntent().getStringExtra("ID");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
            keysite=getIntent().getStringExtra("keysite");
        }

        back=findViewById(R.id.back);
        workReport=findViewById(R.id.workReport);
        engagerReport=findViewById(R.id.engagerReport);
        supplierReport=findViewById(R.id.supplierReport);
        boq=findViewById(R.id.boq);
        measutement=findViewById(R.id.measurement);
        materal=findViewById(R.id.material);


        if(keysite!=null){
            if(keysite.equals("keysite")){
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finish();

                        Intent intent=new Intent(SiteEngineerProjectDetails.this, DashBoardSiteEngineer.class);
                        intent.putExtra("key",key);
                        intent.putExtra("key1",key1);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                    }
                });

            }
        }



      // back.setOnClickListener(view -> finish());




        addVendor=findViewById(R.id.addVendor);

        addVendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SiteEngineerProjectDetails.this, VendorsList.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });

        workReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SiteEngineerProjectDetails.this,WorkReport.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });

        engagerReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SiteEngineerProjectDetails.this,EngagerReport.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });

        supplierReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SiteEngineerProjectDetails.this,SupplierReport.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });

        boq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SiteEngineerProjectDetails.this, BOQ.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });
        measutement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SiteEngineerProjectDetails.this,MeasurementSheet.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });

        materal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SiteEngineerProjectDetails.this,MaterialUsage.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                startActivity(intent);
            }
        });
    }
}