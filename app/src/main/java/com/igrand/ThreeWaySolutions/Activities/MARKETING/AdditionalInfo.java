package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.CityAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.CityList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ChangePassword;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.OTP;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdditionalInfoResponse;
import com.igrand.ThreeWaySolutions.Response.AdminAddCity;

public class AdditionalInfo extends BaseActivity {

    EditText roadconnectivity,purchasevalue,salesvalue;
    ApiInterface apiInterface;
    String ID,MobileNumber,RoadConnectivity,PurchaseValue,SalesValue,Active;
    AdditionalInfoResponse.StatusBean statusBean;
    Button submit;
    RadioGroup rg,rg1;
    RadioButton active, inactive;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_info);

        roadconnectivity=findViewById(R.id.roadconnectivity);
        purchasevalue=findViewById(R.id.purchasevalue);
        salesvalue=findViewById(R.id.salesvalue);
        submit=findViewById(R.id.submit);
        rg = findViewById(R.id.rg);
        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        back = findViewById(R.id.back);

        rg.check(R.id.active);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent()!=null){

            MobileNumber=getIntent().getStringExtra("MobileNumber");
            ID=getIntent().getStringExtra("ID");
        }




        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                 RoadConnectivity=roadconnectivity.getText().toString();
                 PurchaseValue=purchasevalue.getText().toString();
                 SalesValue=salesvalue.getText().toString();



                if (active.isChecked()) {

                    Active = "1";


                } else if (inactive.isChecked()) {

                   Active = "0";

                } else {

                    Toast.makeText(AdditionalInfo.this, "Please select Active/InActive...", Toast.LENGTH_SHORT).show();
                }



                if(RoadConnectivity.equals("")){

                    Toast.makeText(AdditionalInfo.this, "Please enter road connectivity", Toast.LENGTH_SHORT).show();
                }else if(PurchaseValue.equals("")){

                    Toast.makeText(AdditionalInfo.this, "Please enter purchase valus", Toast.LENGTH_SHORT).show();
                }else if(SalesValue.equals("")){

                    Toast.makeText(AdditionalInfo.this, "Please enter sales value", Toast.LENGTH_SHORT).show();
                }

                else {

                    final ProgressDialog progressDialog = new ProgressDialog(AdditionalInfo.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AdditionalInfoResponse> call = apiInterface.additionalInfo(ID,MobileNumber,RoadConnectivity,PurchaseValue,SalesValue,Active);
                    call.enqueue(new Callback<AdditionalInfoResponse>() {
                        @Override
                        public void onResponse(Call<AdditionalInfoResponse> call, Response<AdditionalInfoResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(AdditionalInfo.this, "Additional Info Added Successfully......", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(AdditionalInfo.this, DashBoardMarketing.class);
                    intent.putExtra("MobileNumber",MobileNumber);
                    startActivity(intent);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(AdditionalInfo.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<AdditionalInfoResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(AdditionalInfo.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });


                }

            }
        });




    }
}
