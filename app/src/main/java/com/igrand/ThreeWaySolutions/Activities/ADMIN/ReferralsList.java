package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEarns;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterReferrals;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.EarnsListResponse;
import com.igrand.ThreeWaySolutions.Response.ReferralListResponse;

import java.util.List;

public class ReferralsList extends BaseActivity {

    ImageView back;
    RecyclerView recyclerView;
    RecyclerAdapterReferrals recyclerAdapter;
    ApiInterface apiInterface;
    ShimmerFrameLayout mShimmerViewContainer;
    ReferralListResponse.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referrals_list);

        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ReferralListResponse> call = apiInterface.referrallist();
        call.enqueue(new Callback<ReferralListResponse>() {
            @Override
            public void onResponse(Call<ReferralListResponse> call, Response<ReferralListResponse> response) {

                if (response.code() == 200) {
                    // progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ReferralListResponse.DataBean> dataBeans = response.body().getData();
                    //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(ReferralsList.this));
                    recyclerAdapter=new RecyclerAdapterReferrals(ReferralsList.this,dataBeans,"key");
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    //progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(ReferralsList.this, "No Referral's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<ReferralListResponse> call, Throwable t) {
                // progressDialog1.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ReferralsList.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

}
