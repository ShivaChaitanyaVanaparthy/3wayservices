package com.igrand.ThreeWaySolutions.Activities.SURVEY;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.NearByProjectsList1;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.DashBoardLegal;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.LeadDetailsLegal;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.ProposalDocuments;
import com.igrand.ThreeWaySolutions.Activities.LESION.LeadDetailsLesion;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Geolocations;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage1;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments7;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterLegalComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSurveyComment;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsLegalResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsSurveyResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsLegalResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsSurveyResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class LeadDetailsSurvey extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1, approvedp, approvedl, approveds,approvedle;
    String Id, Property, Date, Village, Checking, Status, Comments, GoogleLocation, Address, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate,Mandal,District;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, checkingcomments, status11, date11, statusp, datep, statusl, datel, dates, statuss,statusle,datele,acres,survey,mandal,district;
    Dialog dialog;
    Button edit;
    ApiInterface apiInterface;
    EditLeadsSurveyResponse.StatusBean statusBean;
    String MobileNumber, ct_status, ct_comments, CommentsChecking, MOBILE, LegalStatus, legalDate,SurveyStatus,SurveyDate;
    UpdateLeadsSurveyResponse.StatusBean statusBean1;
    AgentLeadsCommentsResponse.StatusBean statusBean2;
    LinearLayout remarks1;
    RecyclerAdapterSurveyComment recyclerAdapter;

    EditText commentsonly;
    Button commentsbutton;
    LinearLayout mLayout;


    String Document,LesionDate,LesionStatus,Latitude,Longitude,Survey,Acres;
    String[] document2;
    RecyclerView recyclerView;
    RecyclerAdapterCheckingDocuments7 recyclerAdapter1;
    LinearLayout marketing,procurement;

    ImageView image;
    TextView comments;
    String documentt,COMMENTS,IMAGE;
    ArrayList<String> list=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_survey);
        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        edit = findViewById(R.id.edit);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        //checking=findViewById(R.id.Checking);
        //document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);
        //comment11 = findViewById(R.id.comment);
        //checkingcomments = findViewById(R.id.checkingcomments);

        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        date11 = findViewById(R.id.date11);

        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);

        approvedl = findViewById(R.id.approvedl);
        statusl = findViewById(R.id.statusl);
        datel = findViewById(R.id.datel);

        dates = findViewById(R.id.dates);
        statuss = findViewById(R.id.statuss);
        approveds = findViewById(R.id.approveds);

        recyclerView = findViewById(R.id.recyclerView);
        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
        marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);
        acres = findViewById(R.id.acres);
        survey = findViewById(R.id.survey);
        district = findViewById(R.id.district);
        mandal = findViewById(R.id.mandal);
        image = findViewById(R.id.image);
        comments = findViewById(R.id.comments);



        if (getIntent() != null) {

            MobileNumber = getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadDetailsSurvey.this, DashBoardSurvey.class);
                intent.putExtra("MobileNumber", MobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");
            MOBILE = getIntent().getStringExtra("MOBILE");

            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            LegalStatus = getIntent().getStringExtra("LegalStatus");
            legalDate = getIntent().getStringExtra("legalDate");

            SurveyStatus = getIntent().getStringExtra("SurveyStatus");
            SurveyDate = getIntent().getStringExtra("SurveyDate");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");

            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");

            COMMENTS = getIntent().getStringExtra("COMMENTS");
            IMAGE = getIntent().getStringExtra("IMAGE");
        }


        acres.setText(Acres);
        survey.setText(Survey);
        district.setText(District);
        mandal.setText(Mandal);

        comments.setText(COMMENTS);


        documentt="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+ IMAGE;

        Picasso.get().load(documentt).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(image);


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // document1="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/documents/"+IMAGE;

                list.add(documentt);

                Intent intent = new Intent(LeadDetailsSurvey.this, ProfileImage1.class);
                intent.putExtra("Image",documentt);
                intent.putExtra("List",list);
                startActivity(intent);


            }
        });

        marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent intent=new Intent(LeadDetailsSurvey.this, NearByProjectsList1.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });


        procurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsSurvey.this, ProposalDocuments.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });
        document2=Document.split(",");

        recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsSurvey.this,LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter1 = new RecyclerAdapterCheckingDocuments7(LeadDetailsSurvey.this,document2,Document);
        recyclerView.setAdapter(recyclerAdapter1);


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });


        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //checking.setText(Checking);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        //remarks1.setText(Comments);

        remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TextView text;
                final RecyclerView recyclerView;

                dialog = new Dialog(LeadDetailsSurvey.this);
                dialog.setContentView(R.layout.dialogboxremarks);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                recyclerView = dialog.findViewById(R.id.text);


                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsSurvey.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AgentLeadsCommentsResponse> call = apiInterface.agentLeadsComments(Id);
                call.enqueue(new Callback<AgentLeadsCommentsResponse>() {
                    @Override
                    public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean2 = response.body() != null ? response.body().getStatus() : null;
                            List<AgentLeadsCommentsResponse.DataBean> dataBeans = response.body().getData();
                            //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                            recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsSurvey.this));
                            recyclerAdapter = new RecyclerAdapterSurveyComment(LeadDetailsSurvey.this, dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsSurvey.this, "No Comments...", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsSurvey.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                    }
                });
            }
        });

        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        // remarks1.setText(Comments);


        dates.setText(SurveyDate);
        datel.setText(legalDate);
        if (Checking != null) {

            if (Checking.equals("0")) {

                status.setVisibility(View.VISIBLE);
                status.setText("Rejected");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.rejected);
                date1.setText(CheckingDate);

            } else if (Checking.equals("1")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Approved");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.approved);
                date1.setText(CheckingDate);
            } else if (Checking.equals("2")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Pending");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.question);
                date1.setText(CheckingDate);
            }


            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }
            }

            if (ProcurementStatus != null) {

                if (ProcurementStatus.equals("4")) {

                    statusp.setText("Rejected");
                    approvedp.setImageResource(R.drawable.rejected);
                    datep.setText(ProcurementDate);

                } else if (ProcurementStatus.equals("5")) {
                    statusp.setText("Approved");
                    approvedp.setImageResource(R.drawable.approved);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("3")) {
                    statusp.setText("Processing");
                    approvedp.setImageResource(R.drawable.processing);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("2")) {
                    statusp.setText("Open");
                    approvedp.setImageResource(R.drawable.open);
                    datep.setText(ProcurementDate);
                }

            }

            if (LegalStatus != null) {

                if (LegalStatus.equals("0")) {

                    statusl.setVisibility(View.VISIBLE);
                    statusl.setText("Rejected");
                    approvedl.setVisibility(View.VISIBLE);
                    approvedl.setImageResource(R.drawable.rejected);
                    datel.setText(legalDate);

                } else if (LegalStatus.equals("1")) {
                    statusl.setVisibility(View.VISIBLE);
                    statusl.setText("Approved");
                    approvedl.setVisibility(View.VISIBLE);
                    approvedl.setImageResource(R.drawable.approved);
                    datel.setText(legalDate);
                } else if (LegalStatus.equals("2")) {
                    statusl.setVisibility(View.VISIBLE);
                    statusl.setText("Open");
                    approvedl.setVisibility(View.VISIBLE);
                    approvedl.setImageResource(R.drawable.question);
                    datel.setText(legalDate);
                }


                if (SurveyStatus != null) {

                    if (SurveyStatus.equals("0")) {
                        edit.setVisibility(View.GONE);
                        statuss.setVisibility(View.VISIBLE);
                        statuss.setText("Rejected");
                        approveds.setVisibility(View.VISIBLE);
                        approveds.setImageResource(R.drawable.rejected);
                        dates.setText(SurveyDate);

                    } else if (SurveyStatus.equals("1")) {
                        edit.setVisibility(View.GONE);
                        statuss.setVisibility(View.VISIBLE);
                        statuss.setText("Approved");
                        approveds.setVisibility(View.VISIBLE);
                        approveds.setImageResource(R.drawable.approved);
                        dates.setText(SurveyDate);
                    } else if (SurveyStatus.equals("2")) {
                        edit.setVisibility(View.VISIBLE);
                        statuss.setVisibility(View.VISIBLE);
                        statuss.setText("Open");
                        approveds.setVisibility(View.VISIBLE);
                        approveds.setImageResource(R.drawable.question);
                        dates.setText(SurveyDate);
                    }

                    if (LesionStatus != null) {

                        if (LesionStatus.equals("0")) {

                            statusle.setText("Rejected");
                            approvedle.setImageResource(R.drawable.rejected);
                            datele.setText(LesionDate);

                        } else if (LesionStatus.equals("1")) {
                            statusle.setText("Approved");
                            approvedle.setImageResource(R.drawable.approved);
                            datele.setText(LesionDate);
                        }  else if (LesionStatus.equals("2")) {
                            statusle.setText("Open");
                            approvedle.setImageResource(R.drawable.open);
                            datele.setText(LesionDate);
                        }

                    }




                   /* Picasso.get().load(Document).error(R.drawable.profilepic).into(document);

                    document.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            document.buildDrawingCache();
                            Bitmap bitmap = document.getDrawingCache();
                            Intent intent = new Intent(LeadDetailsSurvey.this, ProfileImage.class);
                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                            intent.putExtra("byteArray", _bs.toByteArray());
                            startActivity(intent);
                        }
                    });
*/

                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            final RadioGroup rg;
                            final RadioButton accept, reject;
                            final EditText comment;
                            Button submit;
                            final LinearLayout linear;

                            dialog = new Dialog(LeadDetailsSurvey.this);
                            dialog.setContentView(R.layout.dialogboxedit);
                            dialog.show();
                            dialog.setCancelable(true);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            Window window = dialog.getWindow();
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            rg = dialog.findViewById(R.id.rg);
                            accept = dialog.findViewById(R.id.accept);
                            reject = dialog.findViewById(R.id.reject);
                            comment = dialog.findViewById(R.id.comments);
                            submit = dialog.findViewById(R.id.submit);
                            linear = dialog.findViewById(R.id.linear);


                            final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsSurvey.this);
                            progressDialog.setMessage("Loading.....");
                            progressDialog.show();
                            apiInterface = ApiClient.getClient().create(ApiInterface.class);
                            Call<EditLeadsSurveyResponse> call1 = apiInterface.editLeadsSurvey(Id);
                            call1.enqueue(new Callback<EditLeadsSurveyResponse>() {
                                @Override
                                public void onResponse(Call<EditLeadsSurveyResponse> call, Response<EditLeadsSurveyResponse> response) {

                                    if (response.code() == 200) {
                                        progressDialog.dismiss();
                                        linear.setVisibility(View.VISIBLE);
                                        statusBean = response.body() != null ? response.body().getStatus() : null;
                                        List<EditLeadsSurveyResponse.DataBean> dataBeans = response.body().getData();
                                        if (dataBeans.get(0).get_$SurveyTeamStatus229().equals("0")) {

                                            rg.check(R.id.reject);
                                        } else if (dataBeans.get(0).get_$SurveyTeamStatus229().equals("1")) {
                                            rg.check(R.id.accept);
                                        }

                                        // comment.setText(dataBeans.get(0).getCt_comments());


                                    } else if (response.code() != 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(LeadDetailsSurvey.this, "Error...", Toast.LENGTH_SHORT).show();

                                    }

                                }


                                @Override
                                public void onFailure(Call<EditLeadsSurveyResponse> call, Throwable t) {
                                    progressDialog.dismiss();
                                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                    Toast toast = Toast.makeText(LeadDetailsSurvey.this,
                                            t.getMessage(), Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                    toast.show();


                                }
                            });


                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    if (accept.isChecked()) {

                                        ct_status = "1";


                                    } else if (reject.isChecked()) {

                                        ct_status = "0";

                                    }

                                    ct_comments = comment.getText().toString();


                                    final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsSurvey.this);
                                    progressDialog.setMessage("Loading.....");
                                    progressDialog.show();
                                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                    Call<UpdateLeadsSurveyResponse> call1 = apiInterface.updateLeadsSurvey(Id, ct_status, ct_comments);
                                    call1.enqueue(new Callback<UpdateLeadsSurveyResponse>() {
                                        @Override
                                        public void onResponse(Call<UpdateLeadsSurveyResponse> call, Response<UpdateLeadsSurveyResponse> response) {

                                            if (response.code() == 200) {
                                                progressDialog.dismiss();
                                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                                Toast.makeText(LeadDetailsSurvey.this, "Leads Updated Successfully", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplication(), DashBoardSurvey.class);
                                                intent.putExtra("MobileNumber", MobileNumber);
                                                startActivity(intent);
                                                // comment11.setVisibility(View.VISIBLE);


                                            } else if (response.code() != 200) {
                                                progressDialog.dismiss();
                                                Toast.makeText(LeadDetailsSurvey.this, "Error...", Toast.LENGTH_SHORT).show();

                                            }

                                        }


                                        @Override
                                        public void onFailure(Call<UpdateLeadsSurveyResponse> call, Throwable t) {
                                            progressDialog.dismiss();
                                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                            Toast toast = Toast.makeText(LeadDetailsSurvey.this,
                                                    t.getMessage(), Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                            toast.show();


                                        }
                                    });


                                }
                            });


                        }
                    });


                }


            }
        }
    }
}
