package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NearByProjectsList1;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.CheckingDocList;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.ProposalDocuments;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Geolocations;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerMarketing;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.AddDocumentTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.TechDocList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterChecking;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments4;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMarketingComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsMarketingResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsMarketingResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsMarketing extends BaseActivity {

    ImageView back,img1,img2,img3,img4,document,approved,approved11,approvedp,approvedm,approvedc,approvedl,approveds,approvedle;
    String Id,Property,Date,Village,Comments,GoogleLocation,Address,CheckingStatus,MarketingStatus,ProcurementStatus, ProcurementDate;
    TextView id,property,date,village,checking,google,address,remarks,date11,checkingcomments,date00,status11,statusp, datep,statuss,statusl,dates,datel,statusle,datele,acres,survey,mandal,district,add,add1;
    Dialog dialog;
    Button edit;
    ApiInterface apiInterface;
    EditLeadsMarketingResponse.StatusBean statusBean;
    String MobileNumber,ct_status,ct_comments,CommentsChecking,MOBILE,CheckingDate,MarketingDate,Mandal,District;
    UpdateLeadsMarketingResponse.StatusBean statusBean1;
    TextView datec,statusc,datem,statusm;
    String LegalStatus,LegalDate,SurveyStatus,SurveyDate,Survey,Acres;

    EditText commentsonly;
    String Roadconnectivity,SalesValue;
    Button commentsbutton,nearbyprojects;
    LinearLayout mLayout,additionalinfo,remarks1;
    AgentLeadsCommentsResponse.StatusBean statusBean2;
    RecyclerAdapterMarketingComment recyclerAdapter;

    String Document,LesionDate,LesionStatus,Latitude,Longitude,pdf,Image;
    String[] document2,imagedoc;
    RecyclerView recyclerView,recyclerVieww;
    RecyclerAdapterCheckingDocuments4 recyclerAdapter1;
    LinearLayout marketing,procurement;

    List<String> strings,stringsimg;
    String[] document3;
    RecyclerAdapterAgentImages recyclerAdapter3;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;
    LinearLayout DocList,imglist;
    TextView comments;
    String Propertydesc,Checking,Status,MobileNumber1;
    PrefManagerMarketing prefManagerMarketing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_marketing);



        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        datec = findViewById(R.id.datec);
        statusc = findViewById(R.id.statusc);
        datem = findViewById(R.id.datem);
        statusm = findViewById(R.id.statusm);
        approvedm = findViewById(R.id.approvedm);
        approvedc = findViewById(R.id.approvedc);
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        recyclerView = findViewById(R.id.recyclerView);

        approvedl = findViewById(R.id.approvedl);
        approveds = findViewById(R.id.approveds);
        dates = findViewById(R.id.dates);
        datel = findViewById(R.id.datel);
        statuss = findViewById(R.id.statuss);
        statusl = findViewById(R.id.statusl);

        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
        marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);

        acres = findViewById(R.id.acres);
        survey = findViewById(R.id.survey);
        add = findViewById(R.id.add);
        add1 = findViewById(R.id.add1);
        recyclerVieww = findViewById(R.id.recyclerVieww);
        DocList = findViewById(R.id.doclist);
        imglist = findViewById(R.id.imglist);
        comments = findViewById(R.id.comments);

        id=findViewById(R.id.Id);
        edit=findViewById(R.id.edit);
        property=findViewById(R.id.Property);
        date=findViewById(R.id.date);
        village=findViewById(R.id.Village);
        //checking=findViewById(R.id.Checking);
        //document=findViewById(R.id.document);
        google=findViewById(R.id.google);
        address=findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        //status=findViewById(R.id.status);
        //date11=findViewById(R.id.date11);
        remarks1=findViewById(R.id.remarks1);
        additionalinfo=findViewById(R.id.linear);
        nearbyprojects=findViewById(R.id.nearbyprojects);
        district = findViewById(R.id.district);
        mandal = findViewById(R.id.mandal);




        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        prefManagerMarketing=new PrefManagerMarketing(LeadDetailsMarketing.this);
        HashMap<String, String> profile=prefManagerMarketing.getUserDetails();
        MobileNumber1=profile.get("mobilenumber");




        //approved=findViewById(R.id.approved);
       // comment11=findViewById(R.id.comment);
       // checkingcomments=findViewById(R.id.checkingcomments);





        if(getIntent()!=null){

            MobileNumber=getIntent().getStringExtra("MobileNumber");

        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsMarketing.this, DashBoardMarketing.class);
                intent.putExtra("MobileNumber",MobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            }
        });



        if(getIntent()!=null){
            Id=getIntent().getStringExtra("ID");
            Property=getIntent().getStringExtra("Property");
            Date=getIntent().getStringExtra("Date");
            Village=getIntent().getStringExtra("Village");
            CheckingStatus=getIntent().getStringExtra("CheckingStatus");
            Document=getIntent().getStringExtra("Document");
            Image = getIntent().getStringExtra("Image");
            Comments=getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address=getIntent().getStringExtra("Address");
            MarketingStatus=getIntent().getStringExtra("MarketingStatus");
            MOBILE=getIntent().getStringExtra("MOBILE");
            MarketingDate=getIntent().getStringExtra("MarketingDate");
            CheckingDate=getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            Propertydesc = getIntent().getStringExtra("Propertydesc");
            LegalStatus = getIntent().getStringExtra("LegalStatus");
            LegalDate = getIntent().getStringExtra("LegalDate");
            SurveyStatus = getIntent().getStringExtra("SurveyStatus");
            SurveyDate = getIntent().getStringExtra("SurveyDate");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            datec.setText(CheckingDate);
            datem.setText(MarketingDate);

            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");
            Checking = getIntent().getStringExtra("Checking");
            Status = getIntent().getStringExtra("Status");


        }

        acres.setText(Acres);
        survey.setText(Survey);
        district.setText(District);
        mandal.setText(Mandal);
        comments.setText(Propertydesc);

        marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent=new Intent(LeadDetailsMarketing.this, NearByProjectsList1.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });


        procurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsMarketing.this, ProposalDocuments.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsMarketing.this, AddDocumentMarketing.class);
                intent.putExtra("Id",Id);
                intent.putExtra("MobileNumber",MOBILE);
                intent.putExtra("Property",Property);
                intent.putExtra("Date",Date);
                intent.putExtra("Village",Village);
                intent.putExtra("Checking",Checking);
                intent.putExtra("Document",Document);
                intent.putExtra("Image",Image);
                intent.putExtra("Status",Status);
                intent.putExtra("Comments",Comments);
                intent.putExtra("Latitude",Latitude);
                intent.putExtra("Longitude",Longitude);
                intent.putExtra("MOBILE",MOBILE);
                intent.putExtra("MarketingStatus",MarketingStatus);
                intent.putExtra("MarketingDate",MarketingDate);
                intent.putExtra("CheckingDate",CheckingDate);
                intent.putExtra("ProcurementDate",ProcurementDate);
                intent.putExtra("ProcurementStatus",ProcurementStatus);
                intent.putExtra("Acres",Acres);
                intent.putExtra("Survey",Survey);
                intent.putExtra("Propertydesc",Propertydesc);
                intent.putExtra("Mandal",Mandal);
                intent.putExtra("District",District);
                startActivity(intent);
            }
        });

        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsMarketing.this, MarketingDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });


        if(Document.equals("")){
            DocList.setVisibility(View.GONE);
        }else {

            document2 = Document.split(",");

            strings = Arrays.asList(Document.split(","));

            for (int i = 0; i < strings.size(); i++) {


                document3 = strings.get(i).split("\\.");
                for (int j = 0; j < document2.length; j++) {

                    pdf = document2[j].substring(document2[j].length() - 3);
                    if (pdf.equals("pdf")) {

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsMarketing.this, document2, Document, strings, "keymarketing");
                        recyclerView.setAdapter(recyclerAdapter2);

                    } else {


                    }

                }

            }

        }
        if(Image.equals("")){

            imglist.setVisibility(View.GONE);

        } else {
            imagedoc=Image.split(",");

            stringsimg = Arrays.asList(Image.split(","));

            recyclerVieww.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter3 = new RecyclerAdapterAgentImages(LeadDetailsMarketing.this,imagedoc,stringsimg,Image,"keymarketing");
            recyclerVieww.setAdapter(recyclerAdapter3);
        }



        /*document2=Document.split(",");

        recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsMarketing.this,LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter1 = new RecyclerAdapterCheckingDocuments4(LeadDetailsMarketing.this,document2,Document);
        recyclerView.setAdapter(recyclerAdapter1);
*/


        additionalinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsMarketing.this,AdditionalInfo.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("ID",Id);

                startActivity(intent);
            }
        });

        nearbyprojects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsMarketing.this, NearByProjectsList.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });


        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //checking.setText(Checking);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);

        //remarks1.setText(Comments);

        remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                Intent intent=new Intent(LeadDetailsMarketing.this, CheckingDocList.class);
                intent.putExtra("ID",Id);
                intent.putExtra("Key","Key");
                startActivity(intent);


               /* final RecyclerView recyclerView;
                LinearLayout linear;

                dialog = new Dialog(LeadDetailsMarketing.this);
                dialog.setContentView(R.layout.dialogboxremarks);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                recyclerView = dialog.findViewById(R.id.text);
                linear = dialog.findViewById(R.id.linear);




                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsMarketing.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AgentLeadsCommentsResponse> call = apiInterface.agentLeadsComments(Id);
                call.enqueue(new Callback<AgentLeadsCommentsResponse>() {
                    @Override
                    public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean2 = response.body() != null ? response.body().getStatus() : null;
                            List<AgentLeadsCommentsResponse.DataBean> dataBeans=response.body().getData();
                            //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                            recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsMarketing.this));
                            recyclerAdapter = new RecyclerAdapterMarketingComment(LeadDetailsMarketing.this,dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsMarketing.this, "No Comments...", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(LeadDetailsMarketing.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                    }
                });
*/

                //text.setText(Comments);
            }
        });

        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        //date1.setText(Date);
        // remarks1.setText(Comments);


        if(CheckingStatus!=null){

            if(CheckingStatus.equals("0")){

                statusc.setText("Rejected");
                approvedc.setImageResource(R.drawable.rejected);
                datec.setText(CheckingDate);

            } else if (CheckingStatus.equals("1")) {
                statusc.setText("Approved");
                approvedc.setImageResource(R.drawable.approved);
                datec.setText(CheckingDate);
            }

        }


        if(MarketingStatus!=null){

            if(MarketingStatus.equals("0")){
                edit.setVisibility(View.GONE);
                statusm.setText("Rejected");
                approvedm.setImageResource(R.drawable.rejected);
                datem.setText(MarketingDate);

            } else if (MarketingStatus.equals("1")) {
                edit.setVisibility(View.GONE);
                statusm.setText("Approved");
                approvedm.setImageResource(R.drawable.approved);
                datem.setText(MarketingDate);
            }

            else if (MarketingStatus.equals("2")) {
                edit.setVisibility(View.VISIBLE);
                statusm.setText("Processing");
                approvedm.setImageResource(R.drawable.processing);
                datem.setText(MarketingDate);
            }
            else if (MarketingStatus.equals("3")) {
                edit.setVisibility(View.VISIBLE);
                statusm.setText("Open");
                approvedm.setImageResource(R.drawable.open);
                datem.setText(MarketingDate);
            }

        }

        if (ProcurementStatus != null) {

            if (ProcurementStatus.equals("4")) {

                statusp.setText("Rejected");
                approvedp.setImageResource(R.drawable.rejected);
                datep.setText(ProcurementDate);

            } else if (ProcurementStatus.equals("5")) {
                statusp.setText("Approved");
                approvedp.setImageResource(R.drawable.approved);
                datep.setText(ProcurementDate);
            } else if (ProcurementStatus.equals("3")) {
                statusp.setText("Processing");
                approvedp.setImageResource(R.drawable.processing);
                datep.setText(ProcurementDate);
            } else if (ProcurementStatus.equals("2")) {
                statusp.setText("Open");
                approvedp.setImageResource(R.drawable.open);
                datep.setText(ProcurementDate);
            }

        }

if (LegalStatus != null) {

            if (LegalStatus.equals("0")) {

                statusl.setText("Rejected");
                approvedl.setImageResource(R.drawable.rejected);
                datel.setText(LegalDate);

            } else if (LegalStatus.equals("1")) {
                statusl.setText("Approved");
                approvedl.setImageResource(R.drawable.approved);
                datel.setText(LegalDate);
            } else if (LegalStatus.equals("2")) {
                statusl.setText("Open");
                approvedl.setImageResource(R.drawable.open);
                datel.setText(LegalDate);
            }

        }

if (SurveyStatus != null) {

            if (SurveyStatus.equals("0")) {

                statuss.setText("Rejected");
                approveds.setImageResource(R.drawable.rejected);
                dates.setText(SurveyDate);

            } else if (SurveyStatus.equals("1")) {
                statuss.setText("Approved");
                approveds.setImageResource(R.drawable.approved);
                dates.setText(SurveyDate);
            } else if (SurveyStatus.equals("2")) {
                statuss.setText("Open");
                approveds.setImageResource(R.drawable.open);
                dates.setText(SurveyDate);
            }

        }

        if (LesionStatus != null) {

            if (LesionStatus.equals("0")) {

                statusle.setText("Rejected");
                approvedle.setImageResource(R.drawable.rejected);
                datele.setText(LesionDate);

            } else if (LesionStatus.equals("1")) {
                statusle.setText("Approved");
                approvedle.setImageResource(R.drawable.approved);
                datele.setText(LesionDate);
            }  else if (LesionStatus.equals("2")) {
                statusle.setText("Open");
                approvedle.setImageResource(R.drawable.open);
                datele.setText(LesionDate);
            }

        }



       /* Picasso.get().load(Document).error(R.drawable.profilepic).into(document);

        document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                document.buildDrawingCache();
                Bitmap bitmap = document.getDrawingCache();
                Intent intent=new Intent(LeadDetailsMarketing.this, ProfileImage.class);
                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                startActivity(intent);
            }
        });*/



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final RadioGroup rg;
                final RadioButton accept,reject,open,processing;
                final EditText comment,roadconnectivity,salesvalue;
                Button submit;
                final LinearLayout linear;


                dialog = new Dialog(LeadDetailsMarketing.this);
                dialog.setContentView(R.layout.dialogboxeditmarketing);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                rg = dialog.findViewById(R.id.rg);
                accept = dialog.findViewById(R.id.accept);
                reject = dialog.findViewById(R.id.reject);
                open = dialog.findViewById(R.id.open);
                processing = dialog.findViewById(R.id.processing);
                comment = dialog.findViewById(R.id.comments);
                submit = dialog.findViewById(R.id.submit);
                linear = dialog.findViewById(R.id.linear);
                roadconnectivity = dialog.findViewById(R.id.roadconnectivity);
                salesvalue = dialog.findViewById(R.id.salesvalue);




                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsMarketing.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<EditLeadsMarketingResponse> call1 = apiInterface.editLeadsMarketing(Id,MOBILE);
                call1.enqueue(new Callback<EditLeadsMarketingResponse>() {
                    @Override
                    public void onResponse(Call<EditLeadsMarketingResponse> call, Response<EditLeadsMarketingResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<EditLeadsMarketingResponse.DataBean> dataBeans=response.body().getData();

                            if(dataBeans.get(0).getMt_status().equals("0")) {

                                rg.check(R.id.reject);
                            } else if(dataBeans.get(0).getMt_status().equals("1")){
                                rg.check(R.id.accept);
                            }else if(dataBeans.get(0).getMt_status().equals("2")){
                                rg.check(R.id.processing);
                            }else if(dataBeans.get(0).getMt_status().equals("3")){
                                rg.check(R.id.open);
                            }

                            // comment.setText(dataBeans.get(0).getCt_comments());




                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsMarketing.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<EditLeadsMarketingResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(LeadDetailsMarketing.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (accept.isChecked()) {

                            ct_status = "1";


                        } else if (reject.isChecked()) {

                            ct_status = "0";

                        }else if (processing.isChecked()) {

                            ct_status = "2";

                        }else if (open.isChecked()) {

                            ct_status = "3";

                        }

                        ct_comments=comment.getText().toString();

                        Roadconnectivity=roadconnectivity.getText().toString();
                        SalesValue=salesvalue.getText().toString();



                        final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsMarketing.this);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();
                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<UpdateLeadsMarketingResponse> call1 = apiInterface.updateLeadsMarketing(Id,MobileNumber1,ct_status,ct_comments,Roadconnectivity,SalesValue);
                        call1.enqueue(new Callback<UpdateLeadsMarketingResponse>() {
                            @Override
                            public void onResponse(Call<UpdateLeadsMarketingResponse> call, Response<UpdateLeadsMarketingResponse> response) {

                                if (response.code() == 200) {
                                    progressDialog.dismiss();
                                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                    Toast.makeText(LeadDetailsMarketing.this, "Leads Updated Successfully", Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(getApplication(),DashBoardMarketing.class);
                                    intent.putExtra("MobileNumber",MobileNumber);
                                    startActivity(intent);
//                                    comment11.setVisibility(View.VISIBLE);




                                } else if (response.code() != 200) {
                                    progressDialog.dismiss();
                                    Toast.makeText(LeadDetailsMarketing.this, "Error...", Toast.LENGTH_SHORT).show();

                                }

                            }


                            @Override
                            public void onFailure(Call<UpdateLeadsMarketingResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                Toast toast= Toast.makeText(LeadDetailsMarketing.this,
                                        t.getMessage() , Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();


                            }
                        });


                    }
                });

            }
        });
    }
}
