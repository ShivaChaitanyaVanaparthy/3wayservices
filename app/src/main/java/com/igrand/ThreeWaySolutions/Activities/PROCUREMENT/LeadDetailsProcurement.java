package com.igrand.ThreeWaySolutions.Activities.PROCUREMENT;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.NearByProjectsList1;
import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.CheckingDocList;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.ProposalDocuments;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.DashBoardMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.LeadDetailsMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.MarketingDocList;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadDetailsTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.TechDocList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments5;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsMarketingResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsProcurementResponse;
import com.igrand.ThreeWaySolutions.Response.LeadDetailResponse;
import com.igrand.ThreeWaySolutions.Response.LeadIdResponse;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsMarketingResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsProcurementResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class LeadDetailsProcurement extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved00, approved1, approvedp,approveds,approvedl,approvedle,approvedt;
    String Id, Checking, StatusProperty, Date, Village, Comments, GoogleLocation, Address, CheckingStatus, MarketingStatus;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11, statusp, datep,statusl,statuss,datel,dates,statusle,datele,mandal,district,statust,datet,add1;
    Dialog dialog;
    Button edit, comment11;
    ApiInterface apiInterface;
    String MobileNumber, ct_status, ct_comments, CommentsChecking, MobileNumber1, Property, MOBILE, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate,Mandal,District;
    LeadDetailResponse.StatusBean statusBean;
    NestedScrollView scroll;
    EditText commentsonly;
    Button commentsbutton,add;
    LinearLayout mLayout, additionalinfo;
    String LegalStatus,SurveyStatus,LegalDate,SurveyDate;
    String ProposalName;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    String imagePic, picturePath,Name;
    Boolean number;
    String Status,Acres,Survey,Document,ProposalNotes,LesionDate,LesionStatus,Latitude,Longitude,TechnicalStatus,TechnicalDate,key,pdf,Image;
    String[] document2;
    RecyclerView recyclerView,recyclerVieww;
    RecyclerAdapterCheckingDocuments5 recyclerAdapter1;
    LeadIdResponse.StatusBean statusBean2;
    LinearLayout marketing,procurement,tech,remarks1;

    List<String> strings,stringsimg;
    String[] document3,imagedoc;
    RecyclerAdapterAgentImages recyclerAdapter3;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;
    TextView comments,road,sales;
    String Propertydesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_procurement);

        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        // document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        date11 = findViewById(R.id.date11);
        approved = findViewById(R.id.approved);
        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        recyclerView = findViewById(R.id.recyclerView);
        add = findViewById(R.id.add);

        statuss = findViewById(R.id.statuss);
        dates = findViewById(R.id.dates);
        approveds = findViewById(R.id.approveds);
        statusl = findViewById(R.id.statusl);
        datel = findViewById(R.id.datel);
        approvedl = findViewById(R.id.approvedl);
        edit = findViewById(R.id.edit);
        scroll = findViewById(R.id.scroll);
        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
        marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);
        district = findViewById(R.id.district);
        mandal = findViewById(R.id.mandal);
        statust = findViewById(R.id.statust);
        datet = findViewById(R.id.datet);
        approvedt = findViewById(R.id.approvedt);
        tech = findViewById(R.id.tech);
        add1 = findViewById(R.id.add1);
        recyclerVieww = findViewById(R.id.recyclerVieww);
        remarks1 = findViewById(R.id.remarks1);
        comments = findViewById(R.id.comments);
        road = findViewById(R.id.road);
        sales = findViewById(R.id.sales);


        if (getIntent() != null) {


            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            Image = getIntent().getStringExtra("Image");
            //Status = getIntent().getStringExtra("Status");
            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            TechnicalStatus = getIntent().getStringExtra("TechnicalStatus");
            TechnicalDate = getIntent().getStringExtra("TechnicalDate");
            MobileNumber = getIntent().getStringExtra("MobileNumber");
            MobileNumber1 = getIntent().getStringExtra("MobileNumber1");
            ProposalNotes = getIntent().getStringExtra("ProposalNotes");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");
            key = getIntent().getStringExtra("key");
            Propertydesc = getIntent().getStringExtra("Propertydesc");
            Status = getIntent().getStringExtra("Status");
            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            date1.setText(MarketingDate);
            district.setText(District);
            mandal.setText(Mandal);
            comments.setText(Propertydesc);


        }



        if(Document.equals("")){

        }else {

            document2 = Document.split(",");

            strings = Arrays.asList(Document.split(","));

            for (int i = 0; i < strings.size(); i++) {


                document3 = strings.get(i).split("\\.");
                for (int j = 0; j < document2.length; j++) {

                    pdf = document2[j].substring(document2[j].length() - 3);
                    if (pdf.equals("pdf")) {

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsProcurement.this, document2, Document, strings, "keypp");
                        recyclerView.setAdapter(recyclerAdapter2);

                    }

                }

            }

        }
        imagedoc=Image.split(",");

        stringsimg = Arrays.asList(Image.split(","));

        recyclerVieww.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter3 = new RecyclerAdapterAgentImages(LeadDetailsProcurement.this,imagedoc,stringsimg,Image,"keyprocurement");
        recyclerVieww.setAdapter(recyclerAdapter3);


        if(key!=null){
            if(key.equals("key")){
                add1.setVisibility(View.GONE);
                add.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);

                procurement.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    /*Intent intent=new Intent(LeadDetailsProcurement.this, ProposalDocuments.class);
                    intent.putExtra("ID",Id);
                    startActivity(intent);*/

                        Intent intent=new Intent(LeadDetailsProcurement.this,ProcDocList.class);
                        intent.putExtra("ID",Id);
                        startActivity(intent);

                    }
                });
            } else {

                add.setVisibility(View.VISIBLE);
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(LeadDetailsProcurement.this,AddProposalDocuments.class);
                        intent.putExtra("Id",Id);
                        intent.putExtra("MobileNumber",MOBILE);
                        intent.putExtra("Property",Property);
                        intent.putExtra("Date",Date);
                        intent.putExtra("Village",Village);
                        intent.putExtra("Checking",Checking);
                        intent.putExtra("Document",Document);
                        intent.putExtra("Image",Image);
                        intent.putExtra("Status",Status);
                        intent.putExtra("Comments",Comments);
                        intent.putExtra("Latitude",Latitude);
                        intent.putExtra("Longitude",Longitude);
                        intent.putExtra("MOBILE",MOBILE);
                        intent.putExtra("MarketingStatus",MarketingStatus);
                        intent.putExtra("MarketingDate",MarketingDate);
                        intent.putExtra("CheckingDate",CheckingDate);
                        intent.putExtra("ProcurementDate",ProcurementDate);
                        intent.putExtra("ProcurementStatus",ProcurementStatus);
                        intent.putExtra("Acres",Acres);
                        intent.putExtra("Survey",Survey);
                        intent.putExtra("Propertydesc",Propertydesc);
                        intent.putExtra("Mandal",Mandal);
                        intent.putExtra("District",District);
                        startActivity(intent);

                    }
                });

            }
        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement.this,AddProposalDocuments.class);
                intent.putExtra("Id",Id);
                intent.putExtra("MobileNumber",MOBILE);
                intent.putExtra("Property",Property);
                intent.putExtra("Date",Date);
                intent.putExtra("Village",Village);
                intent.putExtra("Checking",Checking);
                intent.putExtra("Document",Document);
                intent.putExtra("Image",Image);
                intent.putExtra("Status",Status);
                intent.putExtra("Comments",Comments);
                intent.putExtra("Latitude",Latitude);
                intent.putExtra("Longitude",Longitude);
                intent.putExtra("MOBILE",MOBILE);
                intent.putExtra("MarketingStatus",MarketingStatus);
                intent.putExtra("MarketingDate",MarketingDate);
                intent.putExtra("CheckingDate",CheckingDate);
                intent.putExtra("ProcurementDate",ProcurementDate);
                intent.putExtra("ProcurementStatus",ProcurementStatus);
                intent.putExtra("Acres",Acres);
                intent.putExtra("Survey",Survey);
                intent.putExtra("Propertydesc",Propertydesc);
                intent.putExtra("Mandal",Mandal);
                intent.putExtra("District",District);
                startActivity(intent);

            }
        });
        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement.this,ProcDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });

        marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement.this, MarketingDocList.class);
                intent.putExtra("ID",Id);
                intent.putExtra("Key","Key");
                startActivity(intent);
            }
        });

        remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement.this, CheckingDocList.class);
                intent.putExtra("ID",Id);
                intent.putExtra("Key","Key");
                startActivity(intent);
            }
        });




        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });



        tech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsProcurement.this, TechDocList.class);
                intent.putExtra("ID",Id);
                intent.putExtra("Key","Key");
                startActivity(intent);
            }
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadDetailsProcurement.this, DashBoardProcurement.class);
                intent.putExtra("MobileNumber", MobileNumber);
                intent.putExtra("Id", Id);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            }
        });



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent intent=new Intent(LeadDetailsProcurement.this,EditProcurement.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("ID",Id);
                startActivity(intent);


            }
        });



        final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsProcurement.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProcurementLeadResponse> call = apiInterface.LeadDetailProc(Id);
        call.enqueue(new Callback<ProcurementLeadResponse>() {
            @Override
            public void onResponse(Call<ProcurementLeadResponse> call, Response<ProcurementLeadResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    ProcurementLeadResponse.StatusBean statusBean = response.body() != null ? response.body().getStatus() : null;

                    ProcurementLeadResponse.DataBean dataBeans=response.body().getData();


                    road.setText(dataBeans.getRoad_connectivity());
                    sales.setText(dataBeans.getExpected_sales_value());




                } else if(response.code()==404)
                {

                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(LeadDetailsProcurement.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }


            @Override
            public void onFailure(Call<ProcurementLeadResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LeadDetailsProcurement.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });







        if (Checking.equals("0")) {

            status.setVisibility(View.VISIBLE);
            status.setText("Rejected");
            approved.setVisibility(View.VISIBLE);
            approved.setImageResource(R.drawable.rejected);
            date1.setText(CheckingDate);

        } else if (Checking.equals("1")) {
            status.setVisibility(View.VISIBLE);
            status.setText("Approved");
            approved.setVisibility(View.VISIBLE);
            approved.setImageResource(R.drawable.approved);
            date1.setText(CheckingDate);
        } else if (Checking.equals("2")) {
            status.setVisibility(View.VISIBLE);
            status.setText("Pending");
            approved.setVisibility(View.VISIBLE);
            approved.setImageResource(R.drawable.question);
            date1.setText(CheckingDate);
        }


        if (MarketingStatus != null) {

            if (MarketingStatus.equals("0")) {

                status11.setVisibility(View.VISIBLE);
                status11.setText("Rejected");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.rejected);
                date11.setText(MarketingDate);

            } else if (MarketingStatus.equals("1")) {
                status11.setVisibility(View.VISIBLE);
                status11.setText("Approved");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.approved);
                date11.setText(MarketingDate);
            } else if (MarketingStatus.equals("2")) {
                status11.setVisibility(View.VISIBLE);
                status11.setText("Processing");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.question);
                date11.setText(MarketingDate);
            } else if (MarketingStatus.equals("3")) {
                status11.setVisibility(View.VISIBLE);
                status11.setText("Open");
                approved1.setVisibility(View.VISIBLE);
                approved1.setImageResource(R.drawable.open);
                date11.setText(MarketingDate);
            }

            if (TechnicalStatus != null) {

                if (TechnicalStatus.equals("2")) {

                    statust.setVisibility(View.VISIBLE);
                    statust.setText("Open");
                    approvedt.setVisibility(View.VISIBLE);
                    approvedt.setImageResource(R.drawable.rejected);
                    datet.setText(TechnicalDate);

                } else if (TechnicalStatus.equals("1")) {
                    statust.setVisibility(View.VISIBLE);
                    statust.setText("Approved");
                    approvedt.setVisibility(View.VISIBLE);
                    approvedt.setImageResource(R.drawable.approved);
                    datet.setText(TechnicalDate);
                    edit.setVisibility(View.GONE);
                } else if (TechnicalStatus.equals("0")) {
                    statust.setVisibility(View.VISIBLE);
                    statust.setText("Reject");
                    approvedt.setVisibility(View.VISIBLE);
                    approvedt.setImageResource(R.drawable.question);
                    datet.setText(TechnicalDate);
                    edit.setVisibility(View.GONE);
                }
            }


            if (ProcurementStatus != null) {

                if (ProcurementStatus.equals("4")) {
                    edit.setVisibility(View.GONE);
                    add.setVisibility(View.GONE);
                    statusp.setText("Rejected");
                    approvedp.setImageResource(R.drawable.rejected);
                    datep.setText(ProcurementDate);

                } else if (ProcurementStatus.equals("5")) {
                    edit.setVisibility(View.GONE);
                    add.setVisibility(View.VISIBLE);
                    statusp.setText("Approved");
                    approvedp.setImageResource(R.drawable.approved);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("3")) {
                    edit.setVisibility(View.VISIBLE);
                    add.setVisibility(View.GONE);
                    statusp.setText("Processing");
                    approvedp.setImageResource(R.drawable.processing);
                    datep.setText(ProcurementDate);
                } else if (ProcurementStatus.equals("2")) {
                    edit.setVisibility(View.VISIBLE);
                    add.setVisibility(View.VISIBLE);
                    statusp.setText("Open");
                    approvedp.setImageResource(R.drawable.open);
                    datep.setText(ProcurementDate);
                }



            }




                final ProgressDialog progressDialog1 = new ProgressDialog(LeadDetailsProcurement.this);
                progressDialog1.setMessage("Loading.....");
                progressDialog1.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<LeadDetailResponse> call1 = apiInterface.leadDetailprocurement(MobileNumber1, Id);
                call1.enqueue(new Callback<LeadDetailResponse>() {
                    @Override
                    public void onResponse(Call<LeadDetailResponse> call, Response<LeadDetailResponse> response) {

                        if (response.code() == 200) {
                            scroll.setVisibility(View.VISIBLE);
                            progressDialog1.dismiss();

                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            LeadDetailResponse.DataBean dataBeans = response.body().getData();


                            id.setText(dataBeans.getId());
                            property.setText(dataBeans.getProperty_name());
                            date.setText(dataBeans.getDatetime());
                            village.setText(dataBeans.getVillage_name());
                            //google.setText(GoogleLocation);
                            address.setText(dataBeans.getAddress());
                            //remarks.setText(Comments);

                            // remarks1.setText(Comments);


                            if (response.code() != 200) {
                                progressDialog1.dismiss();
                                Toast.makeText(LeadDetailsProcurement.this, "Error...", Toast.LENGTH_SHORT).show();

                            }

                        }
                    }


                    @Override
                    public void onFailure(Call<LeadDetailResponse> call, Throwable t) {
                        progressDialog1.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsProcurement.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });
            }
        }
    }

