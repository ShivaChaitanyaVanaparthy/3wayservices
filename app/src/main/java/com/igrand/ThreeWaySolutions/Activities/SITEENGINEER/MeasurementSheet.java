package com.igrand.ThreeWaySolutions.Activities.SITEENGINEER;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerBoqList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerMachineryReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerMeasurementList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkName;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminMachineryList;
import com.igrand.ThreeWaySolutions.Response.BoqListResponse;
import com.igrand.ThreeWaySolutions.Response.MeasurementSheetResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class MeasurementSheet extends BaseActivity {

    Button add;
    RecyclerView recyclerView;
    //TextView userType;
    RecyclerMeasurementList recyclerUser;
    ApiInterface apiInterface;
    String ID,Project,key,key1;
    MeasurementSheetResponse.StatusBean statusBean;
    ImageView back;
    TextView contractor;
    String PersonId;
    Button submit;
    RecyclerWorkName recyclerUser1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement_sheet);
        add=findViewById(R.id.add);
        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        contractor = findViewById(R.id.contractor);
        submit = findViewById(R.id.submit);

        //userType=v.findViewById(R.id.userType);





        if(getIntent()!=null){

            ID = getIntent().getStringExtra("ID");
            Project = getIntent().getStringExtra("Project");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
            //SUM = args.getString("SUM");
        }





        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MeasurementSheet.this,SiteEngineerProjectDetails.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                intent.putExtra("keysite","keysite");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                //finish();
            }
        });



        getMeasurement();


        contractor.setOnClickListener(view -> {


            final Dialog dialog = new Dialog(MeasurementSheet.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.radiobutton_dialog_work);


            final RecyclerView recyclerView;
            final RelativeLayout linear;
            final Button add;

            recyclerView = dialog.findViewById(R.id.recyclerView);
            linear = dialog.findViewById(R.id.linear);
            add = dialog.findViewById(R.id.add);

            dialog.show();

            final ProgressDialog progressDialog = new ProgressDialog(MeasurementSheet.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<MeasurementSheetResponse> call = apiInterface.measurementList(ID);
            call.enqueue(new Callback<MeasurementSheetResponse>() {
                @Override
                public void onResponse(Call<MeasurementSheetResponse> call, Response<MeasurementSheetResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        linear.setVisibility(View.VISIBLE);
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<MeasurementSheetResponse.DataBean> dataBeans = response.body().getData();
                        recyclerView.setLayoutManager(new LinearLayoutManager(MeasurementSheet.this));
                        recyclerUser1 = new RecyclerWorkName(MeasurementSheet.this, dataBeans,contractor, dialog,add);
                        recyclerView.setAdapter(recyclerUser1);


                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIError> converter =
                                ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                        APIError error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIError.StatusBean status = error.getStatus();
                            Toast.makeText(MeasurementSheet.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                }


                @Override
                public void onFailure(Call<MeasurementSheetResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(MeasurementSheet.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });
        });

        submit.setOnClickListener(view -> {

            final ProgressDialog progressDialog = new ProgressDialog(MeasurementSheet.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<MeasurementSheetResponse> call = apiInterface.vendorwiseMeasurement(ID, PersonId);
            call.enqueue(new Callback<MeasurementSheetResponse>() {
                @Override
                public void onResponse(Call<MeasurementSheetResponse> call, Response<MeasurementSheetResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<MeasurementSheetResponse.DataBean> dataBeans = response.body().getData();
                        recyclerView.setLayoutManager(new LinearLayoutManager(MeasurementSheet.this));
                        recyclerUser = new RecyclerMeasurementList(MeasurementSheet.this, dataBeans);
                        recyclerView.setAdapter(recyclerUser);


                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIError> converter =
                                ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                        APIError error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIError.StatusBean status = error.getStatus();
                            Toast.makeText(MeasurementSheet.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                }


                @Override
                public void onFailure(Call<MeasurementSheetResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(MeasurementSheet.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });


        });






        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MeasurementSheet.this, AddMeasurementSheet.class);
                intent.putExtra("ID",ID);
                intent.putExtra("Project",Project);
                intent.putExtra("key","key");
                intent.putExtra("key1","key1");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

    }

    private void getMeasurement() {

        final ProgressDialog progressDialog = new ProgressDialog(MeasurementSheet.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MeasurementSheetResponse> call = apiInterface.measurementList(ID);
        call.enqueue(new Callback<MeasurementSheetResponse>() {
            @Override
            public void onResponse(Call<MeasurementSheetResponse> call, Response<MeasurementSheetResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<MeasurementSheetResponse.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(MeasurementSheet.this));
                    recyclerUser = new RecyclerMeasurementList(MeasurementSheet.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);





                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(MeasurementSheet.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }


            @Override
            public void onFailure(Call<MeasurementSheetResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(MeasurementSheet.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMeasurement();
    }

    public void getId0(String workid) {
        PersonId=workid;
    }
}