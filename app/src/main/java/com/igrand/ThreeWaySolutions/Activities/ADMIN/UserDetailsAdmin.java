package com.igrand.ThreeWaySolutions.Activities.ADMIN;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ForgetPassword;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.OTP;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterUserLeads;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminForgetPassword;
import com.igrand.ThreeWaySolutions.Response.LeadsDetailResponse;
import com.igrand.ThreeWaySolutions.Response.UserDetailsResponse;
import com.igrand.ThreeWaySolutions.Response.UserListAdminResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserDetailsAdmin extends BaseActivity {
    RecyclerView recyclerView;
    RecyclerAdapterUserLeads recyclerAdapter;
    ImageView back;
    ApiInterface apiInterface;
    UserDetailsResponse.StatusBean statusBean;
    ImageView image;
    TextView name,email,phone,leads,list;
    String MobileNumber,Profile,Email,Name;
    Integer sizee;
    ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        image=findViewById(R.id.image);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        phone=findViewById(R.id.phone);
        leads=findViewById(R.id.leads);
        list=findViewById(R.id.list);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        if(getIntent()!=null){

            MobileNumber=getIntent().getStringExtra("MobileNumber");
            Profile=getIntent().getStringExtra("Profile");
            Email=getIntent().getStringExtra("Email");
            Name=getIntent().getStringExtra("Name");

            email.setText(Email);
            name.setText(Name);
            Picasso.get().load(Profile).error(R.drawable.profilepic).into(image);
            leads.setText("0");
            phone.setText(MobileNumber);

        }

       /* final ProgressDialog progressDialog = new ProgressDialog(UserDetailsAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserDetailsResponse> call = apiInterface.adminLeadsDetails(MobileNumber);
        call.enqueue(new Callback<UserDetailsResponse>() {
            @Override
            public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {

                if (response.code() == 200) {
                    //progressDialog.dismiss();
                    list.setVisibility(View.VISIBLE);
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<UserDetailsResponse.DataBean> dataBeans=response.body().getData();
                    List<UserDetailsResponse.DataBean.LeadsBean> leadsBeans=dataBeans.get(0).getLeads();

                    Picasso.get().load(dataBeans.get(0).getProfile()).placeholder(R.drawable.loading).error(R.drawable.profilepic).into(image);
                    name.setText(dataBeans.get(0).getUsername());
                    phone.setText(dataBeans.get(0).getMobile());
                    email.setText(dataBeans.get(0).getEmail());
                    leads.setText(String.valueOf(dataBeans.get(0).getLeadscount()));
                    sizee=dataBeans.get(0).getLeadscount();


                    recyclerView.setLayoutManager(new LinearLayoutManager(UserDetailsAdmin.this));
                    recyclerAdapter = new RecyclerAdapterUserLeads(UserDetailsAdmin.this,dataBeans,sizee,leadsBeans);
                    recyclerView.setAdapter(recyclerAdapter);

                } else if (response.code() != 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(UserDetailsAdmin.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                //progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(UserDetailsAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
