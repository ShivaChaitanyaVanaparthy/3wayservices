package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEngagorReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSupplierReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerList;
import com.igrand.ThreeWaySolutions.Response.SupplierReportResponse;

import java.util.List;

public class SupplierReportListDate extends BaseActivity {


    ApiInterface apiInterface;
    RecyclerView recyclerView3;
    RecyclerAdapterSupplierReport recyclerAdapterContractorProjectsList;
    SupplierReportResponse.StatusBean statusBean;
    String ID,Date;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_report_list_date);

        recyclerView3 = findViewById(R.id.recyclerView3);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Date=getIntent().getStringExtra("Date");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(SupplierReportListDate.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SupplierReportResponse> call = apiInterface.adminMachinertReportList1(ID, Date);
        call.enqueue(new Callback<SupplierReportResponse>() {
            @Override
            public void onResponse(Call<SupplierReportResponse> call, Response<SupplierReportResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<SupplierReportResponse.DataBean> dataBeans = response.body().getData();
                    recyclerView3.setLayoutManager(new LinearLayoutManager(SupplierReportListDate.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterSupplierReport(SupplierReportListDate.this, dataBeans);
                    recyclerView3.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(SupplierReportListDate.this, "No Supplier Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<SupplierReportResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(SupplierReportListDate.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });

    }
}
