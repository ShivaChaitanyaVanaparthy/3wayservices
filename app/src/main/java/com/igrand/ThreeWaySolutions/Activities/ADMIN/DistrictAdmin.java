package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterStateActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubworkWorkList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminAddDistrict;
import com.igrand.ThreeWaySolutions.Response.AdminSubAddWorkType;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.List;

public class DistrictAdmin extends BaseActivity {

    TextView state;
    EditText district;
    ImageView back;
    Button add;
    String District,StateName;
    ApiInterface apiInterface;
    RecyclerAdapterStateActiveList recyclerAdapter;
    AdminActiveStateList.StatusBean statusBean;
    AdminAddDistrict.StatusBean statusBean1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_district_admin);

        add=findViewById(R.id.add);
        back=findViewById(R.id.back);
        district=findViewById(R.id.district);
        state=findViewById(R.id.state);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(DistrictAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);
                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);


                final ProgressDialog progressDialog = new ProgressDialog(DistrictAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminActiveStateList> call = apiInterface.adminstateActiveList();
                call.enqueue(new Callback<AdminActiveStateList>() {
                    @Override
                    public void onResponse(Call<AdminActiveStateList> call, Response<AdminActiveStateList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminActiveStateList.DataBean> dataBeans=response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(DistrictAdmin.this));
                            recyclerAdapter = new RecyclerAdapterStateActiveList(DistrictAdmin.this,dataBeans,state,dialog,DistrictAdmin.this);
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(DistrictAdmin.this, "No State's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminActiveStateList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(DistrictAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });

    }

    public void getId(final String selectedworkid) {


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                District=district.getText().toString();
                StateName=state.getText().toString();


                if(District.equals("")){

                    Toast.makeText(DistrictAdmin.this, "Please enter District", Toast.LENGTH_SHORT).show();

                } else if(StateName.equals("")){
                    Toast.makeText(DistrictAdmin.this, "Please select State", Toast.LENGTH_SHORT).show();

                } else{

                    final ProgressDialog progressDialog = new ProgressDialog(DistrictAdmin.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AdminAddDistrict> call = apiInterface.adminAddDistrict(District,selectedworkid);
                    call.enqueue(new Callback<AdminAddDistrict>() {
                        @Override
                        public void onResponse(Call<AdminAddDistrict> call, Response<AdminAddDistrict> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(DistrictAdmin.this, "District Added Successfully......", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(DistrictAdmin.this, DistrictList.class);
                                //intent.putExtra("MobileNumber",mobileNumber);
                                startActivity(intent);
                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(DistrictAdmin.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<AdminAddDistrict> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(DistrictAdmin.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();
                        }
                    });

                }
            }
        });
    }
}
