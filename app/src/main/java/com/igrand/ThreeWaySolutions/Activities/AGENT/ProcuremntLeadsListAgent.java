package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProcurementLeadsList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProcurementAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProcurementAgent;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadsListAdminResponse;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadsListAgentResponse;

import java.util.HashMap;
import java.util.List;

public class ProcuremntLeadsListAgent extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterProcurementAgent recyclerAdapter;
    ProcurementLeadsListAgentResponse.StatusBean statusBean;
    ApiInterface apiInterface;
    ImageView back;
    String mobileNumber,AgentId;
    PrefManagerAgent prefManagerAgent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procuremnt_leads_list_agent);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        prefManagerAgent=new PrefManagerAgent(ProcuremntLeadsListAgent.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        AgentId=profile.get("agentid");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProcuremntLeadsListAgent.this, DashBoardAgent.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(ProcuremntLeadsListAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProcurementLeadsListAgentResponse> call = apiInterface.agentProcurementLeadsList(AgentId);
        call.enqueue(new Callback<ProcurementLeadsListAgentResponse>() {
            @Override
            public void onResponse(Call<ProcurementLeadsListAgentResponse> call, Response<ProcurementLeadsListAgentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProcurementLeadsListAgentResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(ProcuremntLeadsListAgent.this, "Leads List", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(ProcuremntLeadsListAgent.this));
                    recyclerAdapter = new RecyclerAdapterProcurementAgent(ProcuremntLeadsListAgent.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ProcuremntLeadsListAgent.this, "No Data Found...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<ProcurementLeadsListAgentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ProcuremntLeadsListAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

    }
}
