package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProfileImage extends AppCompatActivity {

    ImageView profileimage,profileback;
    String Image;
    ArrayList<String> list=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_image);

        profileimage=findViewById(R.id.profileimage);
        profileback=findViewById(R.id.profileback);



        if(getIntent()!=null){

            Image=getIntent().getStringExtra("Image");
            list=getIntent().getStringArrayListExtra("List");
            Picasso.get().load(Image).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(profileimage);
        }




        profileback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(getIntent().hasExtra("byteArray")) {

            profileimage=findViewById(R.id.profileimage);


          /*  Bitmap _bitmap = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("byteArray"),0,getIntent().getByteArrayExtra("byteArray").length);
            profileimage.setImageBitmap(_bitmap);

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
            profileimage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            profileimage.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            profileimage.setAdjustViewBounds(false);
            profileimage.setScaleType(ImageView.ScaleType.FIT_XY);*/
        }


    }
}
