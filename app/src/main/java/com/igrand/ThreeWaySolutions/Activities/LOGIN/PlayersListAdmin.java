package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DistrictAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.VillageAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterDstActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterMandalActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterPlayerList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterStateActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterVillageActiveList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveDistList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveMandalList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveVillageList;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.PlayersListResponse;

import java.util.List;

public class PlayersListAdmin extends BaseActivity {

    ApiInterface apiInterface;
    PlayersListResponse.StatusBean statusBean;
    AdminActiveStateList.StatusBean statusBean1;
    AdminActiveDistList.StatusBean statusBean2;
    AdminActiveMandalList.StatusBean statusBean3;
    AdminActiveVillageList.StatusBean statusBean4;
    RecyclerAdapterPlayerList recyclerAdapter;
    RecyclerAdapterStateActiveList recyclerAdapter1;
    RecyclerAdapterDstActiveList recyclerAdapter2;
    RecyclerAdapterMandalActiveList recyclerAdapter3;
    RecyclerAdapterVillageActiveList recyclerAdapter4;
    RecyclerView recyclerView;
    LinearLayout filter;
    String StateId,DistrictId,MandalId,VillageId;
    ImageView back;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_list_admin);

        recyclerView=findViewById(R.id.recyclerView);
        filter=findViewById(R.id.filter);
        back=findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(PlayersListAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_state);

                final RadioButton state1,district1,mandal1,village1;
                RadioGroup radiogroup;
                ImageView back;
                Button button;
                final TextView state,district,mandal,village;

                state1=dialog.findViewById(R.id.state1);
                district1=dialog.findViewById(R.id.district1);
                mandal1=dialog.findViewById(R.id.mandal1);
                village1=dialog.findViewById(R.id.village1);
                back=dialog.findViewById(R.id.back);
                button = dialog.findViewById(R.id.button);
                state = dialog.findViewById(R.id.state);
                radiogroup = dialog.findViewById(R.id.radiogroup);
                district = dialog.findViewById(R.id.district);
                mandal = dialog.findViewById(R.id.mandal);
                village = dialog.findViewById(R.id.village);
                dialog.show();


                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if(checkedId == R.id.state1){
                            state.setVisibility(View.VISIBLE);
                            district.setVisibility(View.GONE);
                            mandal.setVisibility(View.GONE);
                            village.setVisibility(View.GONE);

                            state.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {



                                    final Dialog dialog = new Dialog(PlayersListAdmin.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.radiobutton_dialog_work);

                                    dialog.show();

                                    final RecyclerView recyclerView;
                                    final RelativeLayout linear;
                                    final Button add;

                                    recyclerView = dialog.findViewById(R.id.recyclerView);
                                    linear = dialog.findViewById(R.id.linear);
                                    add = dialog.findViewById(R.id.add);



                                    final ProgressDialog progressDialog = new ProgressDialog(PlayersListAdmin.this);
                                    progressDialog.setMessage("Loading.....");
                                    progressDialog.show();
                                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                    Call<AdminActiveStateList> call = apiInterface.adminstateActiveList();
                                    call.enqueue(new Callback<AdminActiveStateList>() {
                                        @Override
                                        public void onResponse(Call<AdminActiveStateList> call, Response<AdminActiveStateList> response) {

                                            if (response.code() == 200) {
                                                progressDialog.dismiss();
                                                linear.setVisibility(View.VISIBLE);
                                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                                //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                                                List<AdminActiveStateList.DataBean> dataBeans=response.body().getData();
                                                recyclerView.setLayoutManager(new LinearLayoutManager(PlayersListAdmin.this));
                                                recyclerAdapter1 = new RecyclerAdapterStateActiveList(PlayersListAdmin.this,dataBeans,state,dialog,PlayersListAdmin.this);
                                                recyclerView.setAdapter(recyclerAdapter1);

                                            } else if (response.code() != 200) {
                                                progressDialog.dismiss();
                                                Toast.makeText(PlayersListAdmin.this, "No State's...", Toast.LENGTH_SHORT).show();

                                            }

                                        }


                                        @Override
                                        public void onFailure(Call<AdminActiveStateList> call, Throwable t) {
                                            progressDialog.dismiss();
                                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                            Toast toast= Toast.makeText(PlayersListAdmin.this,
                                                    t.getMessage() , Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                                            toast.show();


                                        }
                                    });



                                }
                            });




                        }

                        else  if(checkedId == R.id.district1){

                            state.setVisibility(View.GONE);
                            district.setVisibility(View.VISIBLE);
                            mandal.setVisibility(View.GONE);
                            village.setVisibility(View.GONE);

                            district.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    final Dialog dialog = new Dialog(PlayersListAdmin.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.radiobutton_dialog_work);

                                    dialog.show();

                                    final RecyclerView recyclerView;
                                    final RelativeLayout linear;
                                    final Button add;

                                    recyclerView = dialog.findViewById(R.id.recyclerView);
                                    linear = dialog.findViewById(R.id.linear);
                                    add = dialog.findViewById(R.id.add);


                                    final ProgressDialog progressDialog = new ProgressDialog(PlayersListAdmin.this);
                                    progressDialog.setMessage("Loading.....");
                                    progressDialog.show();
                                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                    Call<AdminActiveDistList> call = apiInterface.activedistlist();
                                    call.enqueue(new Callback<AdminActiveDistList>() {
                                        @Override
                                        public void onResponse(Call<AdminActiveDistList> call, Response<AdminActiveDistList> response) {

                                            if (response.code() == 200) {
                                                progressDialog.dismiss();
                                                linear.setVisibility(View.VISIBLE);
                                                statusBean2 = response.body() != null ? response.body().getStatus() : null;
                                                //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                                                List<AdminActiveDistList.DataBean> dataBeans = response.body().getData();
                                                recyclerView.setLayoutManager(new LinearLayoutManager(PlayersListAdmin.this));
                                                recyclerAdapter2 = new RecyclerAdapterDstActiveList(PlayersListAdmin.this, dataBeans, dialog, district, PlayersListAdmin.this);
                                                recyclerView.setAdapter(recyclerAdapter2);

                                            } else if (response.code() != 200) {
                                                progressDialog.dismiss();
                                                Toast.makeText(PlayersListAdmin.this, "No Dist's...", Toast.LENGTH_SHORT).show();

                                            }

                                        }


                                        @Override
                                        public void onFailure(Call<AdminActiveDistList> call, Throwable t) {
                                            progressDialog.dismiss();
                                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                            Toast toast = Toast.makeText(PlayersListAdmin.this,
                                                    t.getMessage(), Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                            toast.show();


                                        }
                                    });

                                }
                            });

                        }

                        else  if(checkedId == R.id.mandal1){

                            mandal.setVisibility(View.VISIBLE);
                            state.setVisibility(View.GONE);
                            district.setVisibility(View.GONE);
                            village.setVisibility(View.GONE);

                            mandal.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {



                                    final Dialog dialog = new Dialog(PlayersListAdmin.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.radiobutton_dialog_work);

                                    dialog.show();

                                    final RecyclerView recyclerView;
                                    final RelativeLayout linear;
                                    final Button add;

                                    recyclerView = dialog.findViewById(R.id.recyclerView);
                                    linear = dialog.findViewById(R.id.linear);
                                    add = dialog.findViewById(R.id.add);


                                    final ProgressDialog progressDialog = new ProgressDialog(PlayersListAdmin.this);
                                    progressDialog.setMessage("Loading.....");
                                    progressDialog.show();
                                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                    Call<AdminActiveMandalList> call = apiInterface.activemandallist();
                                    call.enqueue(new Callback<AdminActiveMandalList>() {
                                        @Override
                                        public void onResponse(Call<AdminActiveMandalList> call, Response<AdminActiveMandalList> response) {

                                            if (response.code() == 200) {
                                                progressDialog.dismiss();
                                                linear.setVisibility(View.VISIBLE);
                                                statusBean3 = response.body() != null ? response.body().getStatus() : null;
                                                //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                                                List<AdminActiveMandalList.DataBean> dataBeans = response.body().getData();
                                                recyclerView.setLayoutManager(new LinearLayoutManager(PlayersListAdmin.this));
                                                recyclerAdapter3 = new RecyclerAdapterMandalActiveList(PlayersListAdmin.this, dataBeans,mandal, dialog, PlayersListAdmin.this);
                                                recyclerView.setAdapter(recyclerAdapter3);

                                            } else if (response.code() != 200) {
                                                progressDialog.dismiss();
                                                Toast.makeText(PlayersListAdmin.this, "No Dist's...", Toast.LENGTH_SHORT).show();

                                            }

                                        }


                                        @Override
                                        public void onFailure(Call<AdminActiveMandalList> call, Throwable t) {
                                            progressDialog.dismiss();
                                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                            Toast toast = Toast.makeText(PlayersListAdmin.this,
                                                    t.getMessage(), Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                            toast.show();


                                        }
                                    });







                                }
                            });



                        }

                        else  if(checkedId == R.id.village1) {

                            state.setVisibility(View.GONE);
                            district.setVisibility(View.GONE);
                            mandal.setVisibility(View.GONE);
                            village.setVisibility(View.VISIBLE);

                            village.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    final Dialog dialog = new Dialog(PlayersListAdmin.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.radiobutton_dialog_work);

                                    dialog.show();

                                    final RecyclerView recyclerView;
                                    final RelativeLayout linear;
                                    final Button add;

                                    recyclerView = dialog.findViewById(R.id.recyclerView);
                                    linear = dialog.findViewById(R.id.linear);
                                    add = dialog.findViewById(R.id.add);


                                    final ProgressDialog progressDialog = new ProgressDialog(PlayersListAdmin.this);
                                    progressDialog.setMessage("Loading.....");
                                    progressDialog.show();
                                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                    Call<AdminActiveVillageList> call = apiInterface.activevillagelist();
                                    call.enqueue(new Callback<AdminActiveVillageList>() {
                                        @Override
                                        public void onResponse(Call<AdminActiveVillageList> call, Response<AdminActiveVillageList> response) {

                                            if (response.code() == 200) {
                                                progressDialog.dismiss();
                                                linear.setVisibility(View.VISIBLE);
                                                statusBean4 = response.body() != null ? response.body().getStatus() : null;
                                                //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                                                List<AdminActiveVillageList.DataBean> dataBeans = response.body().getData();
                                                recyclerView.setLayoutManager(new LinearLayoutManager(PlayersListAdmin.this));
                                                recyclerAdapter4 = new RecyclerAdapterVillageActiveList(PlayersListAdmin.this, dataBeans,village, dialog, PlayersListAdmin.this);
                                                recyclerView.setAdapter(recyclerAdapter4);

                                            } else if (response.code() != 200) {
                                                progressDialog.dismiss();
                                                Toast.makeText(PlayersListAdmin.this, "No Villages's...", Toast.LENGTH_SHORT).show();

                                            }

                                        }


                                        @Override
                                        public void onFailure(Call<AdminActiveVillageList> call, Throwable t) {
                                            progressDialog.dismiss();
                                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                            Toast toast = Toast.makeText(PlayersListAdmin.this,
                                                    t.getMessage(), Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                            toast.show();


                                        }
                                    });
                                }
                            });


                        }
                    }



                });


                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent=new Intent(PlayersListAdmin.this,PlayersFilter.class);
                        intent.putExtra("StateId",StateId);
                        intent.putExtra("DistrictId",DistrictId);
                        intent.putExtra("MandalId",MandalId);
                        intent.putExtra("VillageId",VillageId);
                        startActivity(intent);
                    }
                });







            }
        });

        final ProgressDialog progressDialog1 = new ProgressDialog(PlayersListAdmin.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PlayersListResponse> call1 = apiInterface.adminPlayersList();
        call1.enqueue(new Callback<PlayersListResponse>() {
            @Override
            public void onResponse(Call<PlayersListResponse> call, Response<PlayersListResponse> response) {

                if (response.code() == 200) {
                     progressDialog1.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<PlayersListResponse.DataBean> dataBeans = response.body().getData();

                    //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(PlayersListAdmin.this));
                    recyclerAdapter = new RecyclerAdapterPlayerList(PlayersListAdmin.this, dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog1.dismiss();
                    Toast.makeText(PlayersListAdmin.this, "No Players...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<PlayersListResponse> call, Throwable t) {
                 progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(PlayersListAdmin.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }

    public void getId(String selectedworkid) {

        StateId=selectedworkid;

    }

    public void getId1(String selectedworkid) {

        DistrictId=selectedworkid;
    }

    public void getId2(String selectedworkid) {

        MandalId=selectedworkid;
    }

    public void getId3(String selectedworkid) {

        VillageId=selectedworkid;
    }
}
