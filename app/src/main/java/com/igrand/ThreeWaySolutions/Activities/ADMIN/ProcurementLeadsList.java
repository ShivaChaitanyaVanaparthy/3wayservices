package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProcurementAdmin;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadsListAdminResponse;

import java.util.HashMap;
import java.util.List;

public class ProcurementLeadsList extends BaseActivity {


    RecyclerView recyclerView;
    RecyclerAdapterProcurementAdmin recyclerAdapter;
    ProcurementLeadsListAdminResponse.StatusBean statusBean;
    ApiInterface apiInterface;
    ImageView back;
    String mobileNumber;
    PrefManagerAdmin prefManagerAdmin;
    String MobileNumber1,Name;
    ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procurement_leads_list);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        prefManagerAdmin=new PrefManagerAdmin(ProcurementLeadsList.this);
        HashMap<String, String> profile=prefManagerAdmin.getUserDetails();
        MobileNumber1=profile.get("mobilenumber");
        Name=profile.get("username");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProcurementLeadsList.this,DashBoardAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });




       /* final ProgressDialog progressDialog = new ProgressDialog(ProcurementLeadsList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProcurementLeadsListAdminResponse> call = apiInterface.adminProcurementLeadsList();
        call.enqueue(new Callback<ProcurementLeadsListAdminResponse>() {
            @Override
            public void onResponse(Call<ProcurementLeadsListAdminResponse> call, Response<ProcurementLeadsListAdminResponse> response) {

                if (response.code() == 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProcurementLeadsListAdminResponse.DataBean> dataBeans=response.body().getData();
                    //Toast.makeText(ProcurementLeadsList.this, "Leads List", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(ProcurementLeadsList.this));
                    recyclerAdapter = new RecyclerAdapterProcurementAdmin(ProcurementLeadsList.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(ProcurementLeadsList.this, "No Lead's...", Toast.LENGTH_SHORT).show();

                }

            }



            @Override
            public void onFailure(Call<ProcurementLeadsListAdminResponse> call, Throwable t) {
               // progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ProcurementLeadsList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

}
