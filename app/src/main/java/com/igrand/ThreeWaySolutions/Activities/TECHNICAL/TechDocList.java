package com.igrand.ThreeWaySolutions.Activities.TECHNICAL;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.Comments;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.MarketingDocList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterTechnicalDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentUpdateSubAgentLeadResponse;
import com.igrand.ThreeWaySolutions.Response.TechDocResponse;

import java.util.List;

public class TechDocList extends BaseActivity {

    ApiInterface apiInterface;
    RecyclerView recyclerView;
    TechDocResponse.StatusBean statusBean;
    RecyclerAdapterTechnicalDocuments recyclerAdapter2;
    String ID,Key;
    ImageView back;
    TextView comments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tech_doc_list);


        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        comments=findViewById(R.id.comments);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(getIntent()!=null){
            ID=getIntent().getStringExtra("ID");
            Key=getIntent().getStringExtra("Key");
        }

        if(Key!=null){
            if(Key.equals("Key")){
                comments.setVisibility(View.VISIBLE);
            }
        }


        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(TechDocList.this, Comments.class);
                intent.putExtra("ID",ID);
                intent.putExtra("Key",Key);
                startActivity(intent);
            }
        });



        final ProgressDialog progressDialog = new ProgressDialog(TechDocList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TechDocResponse> call = apiInterface.TechList(ID);
        call.enqueue(new Callback<TechDocResponse>() {
            @Override
            public void onResponse(Call<TechDocResponse> call, Response<TechDocResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(TechDocList.this, "Document's List...", Toast.LENGTH_SHORT).show();
                    List<TechDocResponse.DataBean> dataBeans=response.body().getData();



                        recyclerView.setLayoutManager(new LinearLayoutManager(TechDocList.this));
                        recyclerAdapter2 = new RecyclerAdapterTechnicalDocuments(TechDocList.this,dataBeans,"technical1");
                        recyclerView.setAdapter(recyclerAdapter2);






                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(TechDocList.this, "No Document Found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<TechDocResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(TechDocList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }
}
