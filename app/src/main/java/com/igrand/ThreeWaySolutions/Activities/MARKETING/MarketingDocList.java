package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerMarketing;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.TechDocList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterTechnicalDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.TechDocResponse;

import java.util.HashMap;
import java.util.List;

public class MarketingDocList extends BaseActivity {

    ApiInterface apiInterface;
    RecyclerView recyclerView;
    TechDocResponse.StatusBean statusBean;
    RecyclerAdapterTechnicalDocuments recyclerAdapter2;
    String ID,Key,MobileNumber;
    ImageView back;
    TextView nearbyprojects,comments;
    PrefManagerMarketing prefManagerMarketing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_doc_list);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        nearbyprojects=findViewById(R.id.nearbyprojects);
        comments=findViewById(R.id.comments);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        prefManagerMarketing=new PrefManagerMarketing(MarketingDocList.this);
        HashMap<String, String> profile=prefManagerMarketing.getUserDetails();
        MobileNumber=profile.get("mobilenumber");


        if(getIntent()!=null){
            ID=getIntent().getStringExtra("ID");
            Key=getIntent().getStringExtra("Key");

        }

        if(Key!=null){
            if(Key.equals("Key")){
                nearbyprojects.setVisibility(View.VISIBLE);
                comments.setVisibility(View.VISIBLE);
            }
        }


        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MarketingDocList.this, Comments.class);
                intent.putExtra("ID",ID);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("Key",Key);

                startActivity(intent);
            }
        });

        nearbyprojects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MarketingDocList.this, NearByProjectsList.class);
                intent.putExtra("ID",ID);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("Key",Key);

                startActivity(intent);
            }
        });

        final ProgressDialog progressDialog = new ProgressDialog(MarketingDocList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TechDocResponse> call = apiInterface.MarkList(ID);
        call.enqueue(new Callback<TechDocResponse>() {
            @Override
            public void onResponse(Call<TechDocResponse> call, Response<TechDocResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(MarketingDocList.this, "Document's List...", Toast.LENGTH_SHORT).show();
                    List<TechDocResponse.DataBean> dataBeans=response.body().getData();



                    recyclerView.setLayoutManager(new LinearLayoutManager(MarketingDocList.this));
                    recyclerAdapter2 = new RecyclerAdapterTechnicalDocuments(MarketingDocList.this,dataBeans,"Marketing");
                    recyclerView.setAdapter(recyclerAdapter2);






                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(MarketingDocList.this, "No Document Found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<TechDocResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(MarketingDocList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }
}
