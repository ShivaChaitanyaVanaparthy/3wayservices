package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.igrand.ThreeWaySolutions.Adapters.SlidingImage_Adapter;
import com.igrand.ThreeWaySolutions.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class ProfileImage1 extends AppCompatActivity {

    ImageView profileimage,profileback;
    String Image;
    ArrayList<String> IMAGES=new ArrayList<>();

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<String> ImagesArray = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_image1);

        if(getIntent()!=null){

            IMAGES=getIntent().getStringArrayListExtra("List");

        }


        init(IMAGES);
    }

    private void init(ArrayList<String> IMAGES) {
        for(int i = 0; i<this.IMAGES.size(); i++){

            ImagesArray.add(IMAGES.get(i));

        }


        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(ProfileImage1.this,ImagesArray));

/*
        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);*/

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
       /* indicator.setRadius(5 * density);*/

        NUM_PAGES = this.IMAGES.size();

        // Auto start of viewpager
       /* final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*/
        // Pager listener over indicator
        /*indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });*/


    }
}
