package com.igrand.ThreeWaySolutions.Activities.TECHNICAL;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.ApprovedLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentApprovedLeadResponse;

import java.util.List;

public class ApprovedLeadsTechnical extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterAgent recyclerAdapter;
    AgentApprovedLeadResponse.StatusBean statusBean;
    ApiInterface apiInterface;
    ImageView back;
    String mobileNumber,AgentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approved_leads_technical);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });







        final ProgressDialog progressDialog = new ProgressDialog(ApprovedLeadsTechnical.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AgentApprovedLeadResponse> call = apiInterface.approvedLeadsTechnical();
        call.enqueue(new Callback<AgentApprovedLeadResponse>() {
            @Override
            public void onResponse(Call<AgentApprovedLeadResponse> call, Response<AgentApprovedLeadResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AgentApprovedLeadResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(ApprovedLeadsTechnical.this, "Approved Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(ApprovedLeadsTechnical.this));
                    recyclerAdapter = new RecyclerAdapterAgent(ApprovedLeadsTechnical.this,dataBeans, mobileNumber);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ApprovedLeadsTechnical.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AgentApprovedLeadResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ApprovedLeadsTechnical.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });

    }
}
