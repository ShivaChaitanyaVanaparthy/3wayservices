package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterInventory;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterInventory1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterInventory2;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddInvestmentResponse;
import com.igrand.ThreeWaySolutions.Response.AddUserResponse;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryProjectResponse;
import com.igrand.ThreeWaySolutions.Response.AdminInvestementResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddInvestment extends BaseActivity {

    ImageView back, imageupload;
    TextView selectproject, investorname;
    ApiInterface apiInterface;
    AdminInventoryProjectResponse.StatusBean statusBean;
    RecyclerAdapterInventory1 recyclerAdapter;
    AdminInvestementResponse.StatusBean statusBean1;
    RecyclerAdapterInventory2 recyclerAdapter1;
    String ProjectID, InvestorId;
    EditText investoramount, investdate, maturedate, duration, matureamount, uploadimage;
    RadioGroup rg;
    RadioButton active, inactive;
    Button submit;
    String MobileNumber, Active, PrID, InID, InvestAmount, InvestDate, MatureDate, Duration, MatureAmount, picturePath,imagePic;


    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    private String TAG = "mobile";
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_investment);

        back = findViewById(R.id.back);
        selectproject = findViewById(R.id.selectproject);
        investorname = findViewById(R.id.investorname);
        imageupload = findViewById(R.id.imageupload);

        investoramount = findViewById(R.id.investoramount);
        investdate = findViewById(R.id.investdate);
        maturedate = findViewById(R.id.maturedate);
        duration = findViewById(R.id.duration);
        matureamount = findViewById(R.id.matureamount);
        rg = findViewById(R.id.rg);
        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        submit = findViewById(R.id.submit);
        uploadimage = findViewById(R.id.uploadimage);

        rg.check(R.id.active);


        if (getIntent() != null) {
            MobileNumber = getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        selectproject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(AddInvestment.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.recyclerview_inventor);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.show();
                final RecyclerView recyclerView;
                final Button submit;
                final LinearLayout linearLayout;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                submit = dialog.findViewById(R.id.submit);
                linearLayout = dialog.findViewById(R.id.linear);


                final ProgressDialog progressDialog = new ProgressDialog(AddInvestment.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminInventoryProjectResponse> call = apiInterface.adminInventoryProjetcs();
                call.enqueue(new Callback<AdminInventoryProjectResponse>() {
                    @Override
                    public void onResponse(Call<AdminInventoryProjectResponse> call, Response<AdminInventoryProjectResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linearLayout.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AdminInventoryProjectResponse.DataBean> dataBeans = response.body().getData();


                           /* for(int i=0;i<=dataBeans.size();i++) {
                                project_name=dataBeans.get(0).getProject_name();
                                break;
                            }
*/
                            Toast.makeText(AddInvestment.this, "Project's List...", Toast.LENGTH_SHORT).show();

                            recyclerView.setLayoutManager(new LinearLayoutManager(AddInvestment.this));
                            recyclerAdapter = new RecyclerAdapterInventory1(AddInvestment.this, dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddInvestment.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminInventoryProjectResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddInvestment.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        //project.setText(project_name);

                    }
                });

            }
        });


        investorname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddInvestment.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.recyclerview_inventor);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.show();
                final RecyclerView recyclerView;
                final Button submit;
                final LinearLayout linearLayout;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                submit = dialog.findViewById(R.id.submit);
                linearLayout = dialog.findViewById(R.id.linear);


                final ProgressDialog progressDialog = new ProgressDialog(AddInvestment.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminInvestementResponse> call = apiInterface.adminInvestments();
                call.enqueue(new Callback<AdminInvestementResponse>() {
                    @Override
                    public void onResponse(Call<AdminInvestementResponse> call, Response<AdminInvestementResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linearLayout.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            List<AdminInvestementResponse.DataBean> dataBeans = response.body().getData();


                           /* for(int i=0;i<=dataBeans.size();i++) {
                                project_name=dataBeans.get(0).getProject_name();
                                break;
                            }
*/
                            Toast.makeText(AddInvestment.this, "Investor's Name List...", Toast.LENGTH_SHORT).show();

                            recyclerView.setLayoutManager(new LinearLayoutManager(AddInvestment.this));
                            recyclerAdapter1 = new RecyclerAdapterInventory2(AddInvestment.this, dataBeans);
                            recyclerView.setAdapter(recyclerAdapter1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddInvestment.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminInvestementResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddInvestment.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        //project.setText(project_name);

                    }
                });

            }
        });


        uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(AddInvestment.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddInvestment.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(AddInvestment.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(AddInvestment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(AddInvestment.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddInvestment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(AddInvestment.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(AddInvestment.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }

                LinearLayout camera, folder;

                dialog = new Dialog(AddInvestment.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();
                    }
                });

            }

        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                submitResponse();


            }


        });







     if(imagePic !=null&&!imagePic.isEmpty()&&!imagePic.equals("null"))

    {

        Picasso.get().load(imagePic).into(imageupload);

        bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
        Log.e("bitmap", "" + bitmap);
        converetdImage = getResizedBitmap(bitmap, 500);

    } else

    {

    }

}


    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();



        if (requestCode == 100) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);



                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP


            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {



            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload.setImageBitmap(converetdImage);
            image = new File(Environment.getExternalStorageDirectory(),"temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // new uploadFileToServerTask().execute(destination.getAbsolutePath());






           /* Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            converetdImage = getResizedBitmap(bitmap, 500);
            imageupload.setImageBitmap(converetdImage);*/
            /*try {
                createImagefile();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

        }
    }

    private File createImagefile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        picturePath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    public void TextgetProject(String selection, String id) {
        selectproject.setText(selection);
        ProjectID=id;

    }

    public void TextgetProject1(String selection, String id) {

        investorname.setText(selection);
        InvestorId=id;
    }

    private void submitResponse() {



        InvestAmount=investoramount.getText().toString();
        InvestDate=investdate.getText().toString();
        MatureDate=maturedate.getText().toString();
        Duration=duration.getText().toString();
        MatureAmount=matureamount.getText().toString();


        if (active.isChecked()) {

            Active = "1";


        } else if (inactive.isChecked()) {

            Active = "0";

        } else {

            Toast.makeText(AddInvestment.this, "Please select Active/InActive...", Toast.LENGTH_SHORT).show();
        }


        addInvestment(MobileNumber,ProjectID,InvestorId,InvestAmount,InvestDate,MatureDate,Duration,MatureAmount,Active);




    }

    private void addInvestment(String mobileNumber, String projectID, String investorId, String investAmount, String investDate, String matureDate, String duration, String matureAmount, String active) {



        final ProgressDialog progressDialog=new ProgressDialog(AddInvestment.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("document[]", image.getName(), requestFile);

        }

        RequestBody Mobile = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
        RequestBody Project = RequestBody.create(MediaType.parse("multipart/form-data"), projectID);
        RequestBody Investor = RequestBody.create(MediaType.parse("multipart/form-data"), investorId);
        RequestBody InvestAm = RequestBody.create(MediaType.parse("multipart/form-data"), investAmount);
        RequestBody InvestDa = RequestBody.create(MediaType.parse("multipart/form-data"), investDate);
        RequestBody MatureDa = RequestBody.create(MediaType.parse("multipart/form-data"), matureDate);
        RequestBody Durat = RequestBody.create(MediaType.parse("multipart/form-data"), duration);
        RequestBody MatureAm = RequestBody.create(MediaType.parse("multipart/form-data"), matureAmount);
        RequestBody Statuss = RequestBody.create(MediaType.parse("multipart/form-data"), active);


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddInvestmentResponse> call = apiInterface.adminAddInvestment(Mobile,Project,Investor,InvestAm,InvestDa,MatureDa,Durat,MatureAm,Statuss,body);
        call.enqueue(new Callback<AddInvestmentResponse>() {
            @Override
            public void onResponse(Call<AddInvestmentResponse> call, Response<AddInvestmentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    AddInvestmentResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(AddInvestment.this, "Investment Added Successfully...", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(AddInvestment.this, InvestmentList.class);
                    intent.putExtra("MobileNumber",MobileNumber);
                    startActivity(intent);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AddInvestment.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AddInvestmentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(AddInvestment.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });






    }

}
