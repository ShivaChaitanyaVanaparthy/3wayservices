package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.WorkReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterContractorProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEstimationProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterVendorList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterWorkContractor;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddVendors;
import com.igrand.ThreeWaySolutions.Response.AdminContractorList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReport;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;
import com.igrand.ThreeWaySolutions.Response.VendorListResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class AddWorkReport extends BaseActivity {

    ImageView back;
    Button submit;
    TextView userType,worktype,subworktype,date1,amountt;
    ApiInterface apiInterface;
    RecyclerAdapterEstimationProjectsList recyclerAdapter;
    AdminWorkTypeList.StatusBean statusBean1;
    VendorListResponse.StatusBean statusBean;
    AdminSubWorkTypeListbyId.StatusBean statusBean2;
    AdminWorkReport.StatusBean statusBean3;
    RecyclerAdapterContractorProjectsList recyclerAdapterContractorProjectsList;
    String WorkId,Amountt;
    EditText description,transportcharge,skilled,skilledrate,male,malerate,female,femalerate,amount;
    String Description,Transportcharge,key,key1,Skilled,Skilledrate,Male,Malerate,Female,Femalerate,Date1,Amount,MobileNumber,ID,ContractorId,SubWOrkId,Project;

    PrefManagerAdmin prefManagerAdmin;
    Calendar myCalendar;
    VendorListResponse.StatusBean statusBean4;
    RecyclerAdapterVendorList recyclerUser;
    RecyclerAdapterWorkContractor recyclerAdapterworkContractor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_work_report);

        back=findViewById(R.id.back);
        submit=findViewById(R.id.submit);
        userType=findViewById(R.id.userType);
        worktype=findViewById(R.id.worktype);
        subworktype=findViewById(R.id.subworktype);
        description=findViewById(R.id.description);
        skilled=findViewById(R.id.skilled);
        skilledrate=findViewById(R.id.skilledrate);
        malerate=findViewById(R.id.malerate);
        male=findViewById(R.id.male);
        female=findViewById(R.id.female);
        femalerate=findViewById(R.id.femalerate);
        date1=findViewById(R.id.date);
        amount=findViewById(R.id.amount);
        amountt=findViewById(R.id.amountt);
        transportcharge=findViewById(R.id.transportcharge);

        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Project=getIntent().getStringExtra("Project");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
        }





        String date_n = new SimpleDateFormat("mm/dd/yyyy", Locale.getDefault()).format(new Date());

        date1.setText(date_n);


        femalerate.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0) {

                    int i=Integer.parseInt(skilled.getText().toString());
                    int i1=Integer.parseInt(skilledrate.getText().toString());
                    int i2=Integer.parseInt(male.getText().toString());
                    int i3=Integer.parseInt(malerate.getText().toString());
                    int i4=Integer.parseInt(female.getText().toString());
                    int i5=Integer.parseInt(femalerate.getText().toString());



                    int skilledadd=i*i1;
                    int maleadd=i2*i3;
                    int femaledadd=i4*i5;



                    int amounttotal=skilledadd+maleadd+femaledadd;

                    amountt.setText(String.valueOf(amounttotal));


                }



            }
        });







         myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };


        date1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddWorkReport.this,R.style.TimePickerTheme,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        prefManagerAdmin = new PrefManagerAdmin(AddWorkReport.this);
        HashMap<String, String> profile = prefManagerAdmin.getUserDetails();
        MobileNumber = profile.get("mobilenumber");


        userType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(AddWorkReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();


                final ProgressDialog progressDialog = new ProgressDialog(AddWorkReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
               Call<VendorListResponse> call1 = apiInterface.adminVendorList(ID,"1");
                call1.enqueue(new Callback<VendorListResponse>() {
                    @Override
                    public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<VendorListResponse.StatusBean.VendorsBean> dataBeans=response.body().getStatus().getVendors();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddWorkReport.this));
                            recyclerAdapterworkContractor = new RecyclerAdapterWorkContractor(AddWorkReport.this, dataBeans, userType, dialog,add);
                            recyclerView.setAdapter(recyclerAdapterworkContractor);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddWorkReport.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<VendorListResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddWorkReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });




            }
        });



        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddWorkReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddWorkReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddWorkReport.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(AddWorkReport.this, dataBeans, worktype, dialog, AddWorkReport.this,"work");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddWorkReport.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddWorkReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                getSubWorkType();



            }
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Description=description.getText().toString();
                Skilled=skilled.getText().toString();
                Skilledrate=skilledrate.getText().toString();
                Male=male.getText().toString();
                Malerate=malerate.getText().toString();
                Female=female.getText().toString();
                Femalerate=femalerate.getText().toString();
                Date1=date1.getText().toString();
                Amount=amount.getText().toString();
                Amountt=amountt.getText().toString();
                Transportcharge=transportcharge.getText().toString();


               /* int i=Integer.parseInt(Skilled);
                int i1=Integer.parseInt(Skilledrate);
                int i2=Integer.parseInt(Male);
                int i3=Integer.parseInt(Malerate);
                int i4=Integer.parseInt(Female);
                int i5=Integer.parseInt(Femalerate);



                int skilledadd=i*i1;
                int maleadd=i2*i3;
                int femaledadd=i4*i5;



                int amounttotal=skilledadd+maleadd+femaledadd;

                amountt.setText(String.valueOf(amounttotal));*/





                if(ContractorId==null){

                    Toast.makeText(AddWorkReport.this, "Please select ContractorType", Toast.LENGTH_SHORT).show();
                }else if(WorkId==null){

                    Toast.makeText(AddWorkReport.this, "Please select WorkType", Toast.LENGTH_SHORT).show();
                }else if(SubWOrkId==null){

                    Toast.makeText(AddWorkReport.this, "Please select SubWorkType", Toast.LENGTH_SHORT).show();
                }  else if(Skilled.equals("")){

                    Toast.makeText(AddWorkReport.this, "Please enter Skilled", Toast.LENGTH_SHORT).show();
                }else if(Male.equals("")){

                    Toast.makeText(AddWorkReport.this, "Please enter Male/Female", Toast.LENGTH_SHORT).show();
                }else if(Female.equals("")){

                    Toast.makeText(AddWorkReport.this, "Please enter Male/Female", Toast.LENGTH_SHORT).show();
                }else if(Date1.equals("")){

                    Toast.makeText(AddWorkReport.this, "Please enter Date", Toast.LENGTH_SHORT).show();
                }

else {


                    final ProgressDialog progressDialog = new ProgressDialog(AddWorkReport.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AdminWorkReport> call = apiInterface.adminWorkReport(MobileNumber,ID,ContractorId,WorkId,SubWOrkId,Description,Skilled,Male,Female,Date1,Transportcharge);
                    call.enqueue(new Callback<AdminWorkReport>() {
                        @Override
                        public void onResponse(Call<AdminWorkReport> call, Response<AdminWorkReport> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean3 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(AddWorkReport.this, "Added Successfully......", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(AddWorkReport.this, WorkReport.class);
                                intent.putExtra("ID",ID);
                                intent.putExtra("Project",Project);
                                intent.putExtra("key",key);
                                intent.putExtra("key1",key1);
                                startActivity(intent);





                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Converter<ResponseBody, APIError> converter =
                                        ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                APIError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    APIError.StatusBean status=error.getStatus();
                                    Toast.makeText(AddWorkReport.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }

                            }

                        }


                        @Override
                        public void onFailure(Call<AdminWorkReport> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast = Toast.makeText(AddWorkReport.this,
                                    t.getMessage(), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });




                }



            }
        });
    }

    private void updateLabel() {

        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        date1.setText(sdf.format(myCalendar.getTime()));
    }


    public void getId0(String selectedworkid) {

        ContractorId=selectedworkid;


    }

    public void getId1(String selectedworkid1) {

        WorkId=selectedworkid1;

    }

    public void getId2(String selectedworkid2) {
        SubWOrkId=selectedworkid2;

    }


    private void getSubWorkType() {


        final Dialog dialog = new Dialog(AddWorkReport.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog_work);

        dialog.show();

        final RecyclerView recyclerView;
        final RelativeLayout linear;
        final Button add;

        recyclerView = dialog.findViewById(R.id.recyclerView);
        linear = dialog.findViewById(R.id.linear);
        add = dialog.findViewById(R.id.add);


        final ProgressDialog progressDialog = new ProgressDialog(AddWorkReport.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminSubWorkTypeListbyId> call = apiInterface.adminFsubworkbyid(WorkId);
        call.enqueue(new Callback<AdminSubWorkTypeListbyId>() {
            @Override
            public void onResponse(Call<AdminSubWorkTypeListbyId> call, Response<AdminSubWorkTypeListbyId> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminSubWorkTypeListbyId.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddWorkReport.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterContractorProjectsList(AddWorkReport.this, dataBeans, dialog, AddWorkReport.this, subworktype,"sub");
                    recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AddWorkReport.this, "No SubWorks...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminSubWorkTypeListbyId> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(AddWorkReport.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }


}
