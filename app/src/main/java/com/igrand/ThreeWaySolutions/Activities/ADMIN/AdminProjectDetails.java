package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.FRAGMENTS.ProjectEngagerFragment;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.FRAGMENTS.ProjectWorkreportFragment;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.FRAGMENTS.ProjectSupplierFragment;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.FRAGMENTS.ProjectEstimationFragment;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectCost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdminProjectDetails extends BaseActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapterNotifications pagerAdapterNotifications;
    ImageView back;
    String date,village,project,status,Id;
    TextView projectname,villagename,status1,estimatedcost;
    String ProjectId,MobileNumber;
    ApiInterface apiInterface;
    AdminProjectCost.StatusBean statusBean;
    PrefManagerAdmin prefManagerAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_project_details);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        back = findViewById(R.id.back);
        projectname = findViewById(R.id.projectname);
        villagename = findViewById(R.id.villagename);
        status1 = findViewById(R.id.status);
        estimatedcost = findViewById(R.id.estimatedcost);


        if(getIntent()!=null){

            date=getIntent().getStringExtra("Date");
            village=getIntent().getStringExtra("Village");
            project=getIntent().getStringExtra("Project");
            status=getIntent().getStringExtra("Status");
            Id=getIntent().getStringExtra("ID");

            projectname.setText(project);
            villagename.setText(village);

        }


        prefManagerAdmin = new PrefManagerAdmin(AdminProjectDetails.this);
        HashMap<String, String> profile = prefManagerAdmin.getUserDetails();
        MobileNumber = profile.get("mobilenumber");



        final ProgressDialog progressDialog = new ProgressDialog(AdminProjectDetails.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminProjectCost> call = apiInterface.adminProjectCost(Id);
        call.enqueue(new Callback<AdminProjectCost>() {
            @Override
            public void onResponse(Call<AdminProjectCost> call, Response<AdminProjectCost> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;


                    estimatedcost.setText(String.valueOf(response.body().getData()));




                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    //Toast.makeText(AdminProjectDetails.this, "No WorkReports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminProjectCost> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(AdminProjectDetails.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(AdminProjectDetails.this,ProjectsList.class);
                intent.putExtra("MobileNumber",MobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#000000"));
        tabLayout.setTabTextColors(Color.parseColor("#80000000"), Color.parseColor("#000000"));


        Bundle bundle = new Bundle();
        bundle.putString("Id",Id);
        bundle.putString("Project",project);


        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getSupportFragmentManager(),bundle,bundle);
        pagerAdapterNotifications.addFragment(new ProjectEstimationFragment(), "PROJECT ESTIMATION");
        pagerAdapterNotifications.addFragment(new ProjectWorkreportFragment(), "WORK REPORT");
        pagerAdapterNotifications.addFragment(new ProjectEngagerFragment(), "ENGAGER REPORT");
        pagerAdapterNotifications.addFragment(new ProjectSupplierFragment(), "SUPPLIER REPORT");

        viewPager.setAdapter(pagerAdapterNotifications);
        tabLayout.setupWithViewPager(viewPager);

    }

    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private final Bundle projectId;
        private final Bundle projectName;



        public ViewPagerAdapterNotifications(FragmentManager supportFragmentManager, Bundle id,Bundle name) {
            super(supportFragmentManager);
            projectId=id;
            projectName=name;
        }

        @Override
        public Fragment getItem(int position) {
            /*final ProjectEstimationFragment f = new ProjectEstimationFragment();
            f.setArguments(this.projectId);
            return f;*/
            //return mFragmentList.get(position);
            //return ViewFragment.newInstance(i);


            Fragment fragment = null;
            if (position == 0) {
                fragment = new ProjectEstimationFragment();
                fragment.setArguments(this.projectId);
                fragment.setArguments(this.projectName);
            } else if (position == 1) {
                fragment = new ProjectWorkreportFragment();
                fragment.setArguments(this.projectId);
                fragment.setArguments(this.projectName);
            }else if (position == 2) {
                fragment = new ProjectEngagerFragment();
                fragment.setArguments(this.projectId);
                fragment.setArguments(this.projectName);
            }else if (position == 3) {
                fragment = new ProjectSupplierFragment();
                fragment.setArguments(this.projectId);
                fragment.setArguments(this.projectName);
            }
            return fragment;

        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }
    }
}
