package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.ProjectEstimation;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.SupplierReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSupplierProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterWorkContractor;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddSupplier;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerReport;
import com.igrand.ThreeWaySolutions.Response.AdminInventoryMaterialResponse;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminSupplierReport;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;
import com.igrand.ThreeWaySolutions.Response.VendorListResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class AddSupplierReport extends BaseActivity {

    ImageView back;
    Button submit;
    TextView suppliername,worktype,subworktype,materialtype,uom,date1;
    EditText ts,description,quantity,unloading,transport;
    String Date, Ts,Description,Quantity,Unloading,Transport;
    ApiInterface apiInterface;
    AdminAddSupplier.StatusBean statusBean;
    AdminWorkTypeList.StatusBean statusBean1;
    AdminSubWorkTypeListbyId.StatusBean statusBean2;
    AdminInventoryMaterialResponse.StatusBean statusBean3;
    AdminUOMList.StatusBean statusBean4;
    AdminSupplierReport.StatusBean statusBean5;
    RecyclerAdapterSupplierProjectsList recyclerAdapterContractorProjectsList;
    String SupplierId,key,key1,WorkId,SubWorkId,MaterialId,UomId,MobileNumber,ID,Project;
    PrefManagerAdmin prefManagerAdmin;
    Calendar myCalendar;
    VendorListResponse.StatusBean statusBean6;
    RecyclerAdapterWorkContractor recyclerAdapterworkContractor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_machinery_report);

        back=findViewById(R.id.back);
        submit=findViewById(R.id.submit);
        suppliername=findViewById(R.id.suppliername);
        worktype=findViewById(R.id.worktype);
        subworktype=findViewById(R.id.subworktype);
        materialtype=findViewById(R.id.materialtype);
        uom=findViewById(R.id.uom);
        ts=findViewById(R.id.ts);
        description=findViewById(R.id.description);
        quantity=findViewById(R.id.quantity);
        date1=findViewById(R.id.date);
        unloading=findViewById(R.id.unloading);
        transport=findViewById(R.id.transport);


        prefManagerAdmin = new PrefManagerAdmin(AddSupplierReport.this);
        HashMap<String, String> profile = prefManagerAdmin.getUserDetails();
        MobileNumber = profile.get("mobilenumber");


        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Project=getIntent().getStringExtra("Project");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
        }


        myCalendar = Calendar.getInstance();

        String date_n = new SimpleDateFormat("mm/dd/yyyy", Locale.getDefault()).format(new Date());

        date1.setText(date_n);


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        date1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddSupplierReport.this, R.style.TimePickerTheme,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        suppliername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(AddSupplierReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();


                final ProgressDialog progressDialog = new ProgressDialog(AddSupplierReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<VendorListResponse> call1 = apiInterface.adminVendorList(ID,"3");
                call1.enqueue(new Callback<VendorListResponse>() {
                    @Override
                    public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean6 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<VendorListResponse.StatusBean.VendorsBean> dataBeans=response.body().getStatus().getVendors();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddSupplierReport.this));
                            recyclerAdapterworkContractor = new RecyclerAdapterWorkContractor(AddSupplierReport.this, dataBeans, suppliername, dialog,add);
                            recyclerView.setAdapter(recyclerAdapterworkContractor);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddSupplierReport.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<VendorListResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddSupplierReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });


        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddSupplierReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddSupplierReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddSupplierReport.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterSupplierProjectsList(AddSupplierReport.this, dataBeans, worktype, dialog, AddSupplierReport.this,"work");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddSupplierReport.this, "No WorkTypes...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddSupplierReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                getSubWorkType();



            }
        });

        materialtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final Dialog dialog = new Dialog(AddSupplierReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddSupplierReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminInventoryMaterialResponse> call = apiInterface.adminInventoryMaterial();
                call.enqueue(new Callback<AdminInventoryMaterialResponse>() {
                    @Override
                    public void onResponse(Call<AdminInventoryMaterialResponse> call, Response<AdminInventoryMaterialResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminInventoryMaterialResponse.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddSupplierReport.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterSupplierProjectsList(AddSupplierReport.this, dataBeans,dialog, materialtype,"material");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddSupplierReport.this, "No Materials...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminInventoryMaterialResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddSupplierReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        uom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddSupplierReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddSupplierReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminUOMList> call = apiInterface.uomList();
                call.enqueue(new Callback<AdminUOMList>() {
                    @Override
                    public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean4 = response.body() != null ? response.body().getStatus() : null;
                           // Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminUOMList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddSupplierReport.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterSupplierProjectsList(AddSupplierReport.this,uom, dataBeans,dialog,"uom");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddSupplierReport.this, "No Units", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminUOMList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddSupplierReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });






            }
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSupplierReport();

            }
        });
    }

    private void updateLabel() {

        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        date1.setText(sdf.format(myCalendar.getTime()));
    }


    public void getId0(String selectedworkid) {

        SupplierId=selectedworkid;
    }

    public void getId1(String selectedworkid1) {

        WorkId=selectedworkid1;
    }

    private void getSubWorkType() {

        final Dialog dialog = new Dialog(AddSupplierReport.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog_work);

        dialog.show();

        final RecyclerView recyclerView;
        final RelativeLayout linear;
        final Button add;

        recyclerView = dialog.findViewById(R.id.recyclerView);
        linear = dialog.findViewById(R.id.linear);
        add = dialog.findViewById(R.id.add);


        final ProgressDialog progressDialog = new ProgressDialog(AddSupplierReport.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminSubWorkTypeListbyId> call = apiInterface.adminFsubworkbyid(WorkId);
        call.enqueue(new Callback<AdminSubWorkTypeListbyId>() {
            @Override
            public void onResponse(Call<AdminSubWorkTypeListbyId> call, Response<AdminSubWorkTypeListbyId> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddSupplierReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminSubWorkTypeListbyId.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddSupplierReport.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterSupplierProjectsList(AddSupplierReport.this, dataBeans, dialog, AddSupplierReport.this, subworktype,"sub");
                    recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AddSupplierReport.this, "No SubWorkTypes...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminSubWorkTypeListbyId> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(AddSupplierReport.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }

    public void getId2(String selectedworkid2) {

        SubWorkId=selectedworkid2;
    }

    public void getId3(String selectedworkid3) {

        MaterialId=selectedworkid3;
    }

    public void getId4(String selectedworkid4) {

        UomId=selectedworkid4;
    }


    private void addSupplierReport() {

        Ts=ts.getText().toString();
        Description=description.getText().toString();
        Quantity=quantity.getText().toString();
        Date=date1.getText().toString();
        Unloading=unloading.getText().toString();
        Transport=transport.getText().toString();


        if(WorkId==null){

            Toast.makeText(AddSupplierReport.this, "Please select WorkType", Toast.LENGTH_SHORT).show();
        }else if(SubWorkId==null){

            Toast.makeText(AddSupplierReport.this, "Please select SubWorkType", Toast.LENGTH_SHORT).show();
        }else if(SupplierId==null){

            Toast.makeText(AddSupplierReport.this, "Please select Supplier", Toast.LENGTH_SHORT).show();
        }  else if(MaterialId==null){

            Toast.makeText(AddSupplierReport.this, "Please select MaterialId", Toast.LENGTH_SHORT).show();
        }else if(UomId==null){

            Toast.makeText(AddSupplierReport.this, "Please select Units", Toast.LENGTH_SHORT).show();
        }else if(Ts.equals("")){

            Toast.makeText(AddSupplierReport.this, "Please enter Ts", Toast.LENGTH_SHORT).show();
        }else if(Quantity.equals("")){

            Toast.makeText(AddSupplierReport.this, "Please enter Quantity", Toast.LENGTH_SHORT).show();
        }else if(Date.equals("")){

            Toast.makeText(AddSupplierReport.this, "Please enter Date", Toast.LENGTH_SHORT).show();
        }

        else {


            final ProgressDialog progressDialog = new ProgressDialog(AddSupplierReport.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AdminSupplierReport> call = apiInterface.adminAddSupplier(MobileNumber,ID,WorkId,SubWorkId,SupplierId,MaterialId,UomId,Ts,Description,Quantity,Date,Unloading,Transport);
            call.enqueue(new Callback<AdminSupplierReport>() {
                @Override
                public void onResponse(Call<AdminSupplierReport> call, Response<AdminSupplierReport> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean5 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(AddSupplierReport.this, "Added Successfully...", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(AddSupplierReport.this, SupplierReport.class);
                        intent.putExtra("ID",ID);
                        intent.putExtra("Project",Project);
                        intent.putExtra("key",key);
                        intent.putExtra("key1",key1);
                        startActivity(intent);






                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(AddSupplierReport.this, "No WorkReports...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<AdminSupplierReport> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(AddSupplierReport.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });




        }


    }
}
