package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.ProfileSubAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.ProfileMarketingResponse;
import com.igrand.ThreeWaySolutions.Response.ProfileProcurementResponse;
import com.igrand.ThreeWaySolutions.Response.ProfileSubAgentResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProfileMarketing extends BaseActivity {

    ImageView back,image;
    ApiInterface apiInterface;
    ProfileMarketingResponse.StatusBean statusBean;
    String mobileNumber,AgentId;
    TextView name,email,mobile;
    FrameLayout userprofile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_marketing);
        back=findViewById(R.id.back);
        image=findViewById(R.id.image);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        mobile=findViewById(R.id.mobile);
        userprofile=findViewById(R.id.userprofile);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            //AgentId=getIntent().getStringExtra("AgentId");
        }


        final ProgressDialog progressDialog = new ProgressDialog(ProfileMarketing.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProfileMarketingResponse> call = apiInterface.marketingProfile(mobileNumber);
        call.enqueue(new Callback<ProfileMarketingResponse>() {
            @Override
            public void onResponse(Call<ProfileMarketingResponse> call, Response<ProfileMarketingResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    userprofile.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<ProfileMarketingResponse.DataBean> dataBeans=response.body().getData();
                    Picasso.get().load(dataBeans.get(0).getProfile()).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(image);
                    name.setText(dataBeans.get(0).getUsername());
                    email.setText(dataBeans.get(0).getEmail());
                    //mobile.setText(dataBeans.get(0).());


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ProfileMarketing.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<ProfileMarketingResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ProfileMarketing.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }
}

