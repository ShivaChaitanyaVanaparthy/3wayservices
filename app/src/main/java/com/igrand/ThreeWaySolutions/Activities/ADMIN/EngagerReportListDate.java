package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEngagorReport;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;

import java.util.List;

public class EngagerReportListDate extends BaseActivity {

    ApiInterface apiInterface;
    RecyclerView recyclerView3;
    RecyclerAdapterEngagorReport recyclerAdapterContractorProjectsList1;
    AdminEngagerList.StatusBean statusBean1;
    String ID,Date;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engager_report_list_date);

        recyclerView3 = findViewById(R.id.recyclerView3);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Date=getIntent().getStringExtra("Date");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog1 = new ProgressDialog(EngagerReportListDate.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminEngagerList> call1 = apiInterface.adminEngagerReportList1(ID, Date);
        call1.enqueue(new Callback<AdminEngagerList>() {
            @Override
            public void onResponse(Call<AdminEngagerList> call, Response<AdminEngagerList> response) {

                if (response.code() == 200) {
                    progressDialog1.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminEngagerList.DataBean> dataBeans = response.body().getData();
                    recyclerView3.setLayoutManager(new LinearLayoutManager(EngagerReportListDate.this));
                    recyclerAdapterContractorProjectsList1 = new RecyclerAdapterEngagorReport(EngagerReportListDate.this, dataBeans);
                    recyclerView3.setAdapter(recyclerAdapterContractorProjectsList1);

                } else if (response.code() != 200) {
                    progressDialog1.dismiss();
                    Toast.makeText(EngagerReportListDate.this, "No Engagor Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminEngagerList> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(EngagerReportListDate.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });

    }
}
