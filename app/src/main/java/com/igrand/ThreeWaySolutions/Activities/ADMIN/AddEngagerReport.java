package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.EngagerReport;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.ProjectEstimation;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterContractorProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEngagerProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterWorkContractor;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddEngager;
import com.igrand.ThreeWaySolutions.Response.AdminContractorList;
import com.igrand.ThreeWaySolutions.Response.AdminEngagerReport;
import com.igrand.ThreeWaySolutions.Response.AdminMachineList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReport;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;
import com.igrand.ThreeWaySolutions.Response.VendorListResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class AddEngagerReport extends BaseActivity {

    ImageView back;
    Button submit;
    TextView starttime,endtime,starttime1,endtime1,date1;
    Calendar myCalendar;
    TextView engagername,worktype,subworktype,machinetype;
    EditText vehicle,description;
    ApiInterface apiInterface;
    AdminAddEngager.StatusBean statusBean;
    AdminWorkTypeList.StatusBean statusBean1;
    AdminSubWorkTypeListbyId.StatusBean statusBean2;
    AdminMachineList.StatusBean statusBean3;
    AdminEngagerReport.StatusBean statusBean4;
    RecyclerAdapterEngagerProjectsList recyclerAdapterContractorProjectsList;
    String EngagerId,key,key1,WorkId,SubWorkId,MachineryId,Vehicle,Description,Date,StartTime,EndTime,MobileNumber,ID,Project,StartTime1,EndTime1;
    PrefManagerAdmin prefManagerAdmin;
    VendorListResponse.StatusBean statusBean5;
    RecyclerAdapterWorkContractor recyclerAdapterworkContractor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_engager_report);

        back=findViewById(R.id.back);
        submit=findViewById(R.id.submit);
        starttime=findViewById(R.id.starttime);
        endtime=findViewById(R.id.endtime);
        date1=findViewById(R.id.date);
        engagername=findViewById(R.id.engagername);
        worktype=findViewById(R.id.worktype);
        subworktype=findViewById(R.id.subworktype);
        machinetype=findViewById(R.id.machinetype);
        vehicle=findViewById(R.id.vehicle);
        description=findViewById(R.id.description);
        starttime1=findViewById(R.id.starttime1);
        endtime1=findViewById(R.id.endtime1);

        prefManagerAdmin = new PrefManagerAdmin(AddEngagerReport.this);
        HashMap<String, String> profile = prefManagerAdmin.getUserDetails();
        MobileNumber = profile.get("mobilenumber");


        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Project=getIntent().getStringExtra("Project");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
        }

        String date_n = new SimpleDateFormat("mm/dd/yyyy", Locale.getDefault()).format(new Date());

        date1.setText(date_n);

        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        date1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddEngagerReport.this, R.style.TimePickerTheme,date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        starttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddEngagerReport.this, R.style.TimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker,int selectedHour, int selectedMinute) {
                        starttime.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        endtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddEngagerReport.this,  R.style.TimePickerTheme,new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        endtime.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
 starttime1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddEngagerReport.this, R.style.TimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker,int selectedHour, int selectedMinute) {
                        starttime1.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        endtime1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddEngagerReport.this,  R.style.TimePickerTheme,new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        endtime1.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        engagername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddEngagerReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddEngagerReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<VendorListResponse> call1 = apiInterface.adminVendorList(ID,"4");
                call1.enqueue(new Callback<VendorListResponse>() {
                    @Override
                    public void onResponse(Call<VendorListResponse> call, Response<VendorListResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean5 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddWorkReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<VendorListResponse.StatusBean.VendorsBean> dataBeans=response.body().getStatus().getVendors();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddEngagerReport.this));
                            recyclerAdapterworkContractor = new RecyclerAdapterWorkContractor(AddEngagerReport.this, dataBeans, engagername, dialog,add);
                            recyclerView.setAdapter(recyclerAdapterworkContractor);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(AddEngagerReport.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }

                    }


                    @Override
                    public void onFailure(Call<VendorListResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddEngagerReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });



        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddEngagerReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddEngagerReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddEngagerReport.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterEngagerProjectsList(AddEngagerReport.this, dataBeans, worktype, dialog, AddEngagerReport.this,"work");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddEngagerReport.this, "No WorkTypes...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddEngagerReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubWorkType();


            }
        });



        machinetype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddEngagerReport.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(AddEngagerReport.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminMachineList> call = apiInterface.machineList();
                call.enqueue(new Callback<AdminMachineList>() {
                    @Override
                    public void onResponse(Call<AdminMachineList> call, Response<AdminMachineList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;
                           // Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminMachineList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddEngagerReport.this));
                            recyclerAdapterContractorProjectsList = new RecyclerAdapterEngagerProjectsList(AddEngagerReport.this, dataBeans,dialog, machinetype,"machine");
                            recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddEngagerReport.this, "No MachineTypes...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminMachineList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddEngagerReport.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addReport();
            }
        });
    }



    private void updateLabel() {

        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        date1.setText(sdf.format(myCalendar.getTime()));
    }

    public void getId0(String selectedworkid) {
        EngagerId=selectedworkid;
    }

    public void getId1(String selectedworkid1) {

        WorkId=selectedworkid1;
    }

    private void getSubWorkType() {

        final Dialog dialog = new Dialog(AddEngagerReport.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog_work);

        dialog.show();

        final RecyclerView recyclerView;
        final RelativeLayout linear;
        final Button add;

        recyclerView = dialog.findViewById(R.id.recyclerView);
        linear = dialog.findViewById(R.id.linear);
        add = dialog.findViewById(R.id.add);


        final ProgressDialog progressDialog = new ProgressDialog(AddEngagerReport.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminSubWorkTypeListbyId> call = apiInterface.adminFsubworkbyid(WorkId);
        call.enqueue(new Callback<AdminSubWorkTypeListbyId>() {
            @Override
            public void onResponse(Call<AdminSubWorkTypeListbyId> call, Response<AdminSubWorkTypeListbyId> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminSubWorkTypeListbyId.DataBean> dataBeans = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddEngagerReport.this));
                    recyclerAdapterContractorProjectsList = new RecyclerAdapterEngagerProjectsList(AddEngagerReport.this, dataBeans, dialog, AddEngagerReport.this, subworktype,"sub");
                    recyclerView.setAdapter(recyclerAdapterContractorProjectsList);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(AddEngagerReport.this, "No SubWorktypes...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminSubWorkTypeListbyId> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(AddEngagerReport.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }

    public void getId2(String selectedworkid2) {

        SubWorkId=selectedworkid2;
    }

    public void getId3(String selectedworkid3) {
        MachineryId=selectedworkid3;
    }


    private void addReport() {


        Vehicle=vehicle.getText().toString();
        Description=description.getText().toString();
        Date=date1.getText().toString();
        StartTime=starttime.getText().toString();
        EndTime=endtime.getText().toString();
        StartTime1=starttime1.getText().toString();
        EndTime1=endtime1.getText().toString();


        if(WorkId==null){

            Toast.makeText(AddEngagerReport.this, "Please select WorkType", Toast.LENGTH_SHORT).show();
        }else if(SubWorkId==null){

            Toast.makeText(AddEngagerReport.this, "Please select SubWorkType", Toast.LENGTH_SHORT).show();
        }else if(EngagerId==null){

            Toast.makeText(AddEngagerReport.this, "Please select Engager", Toast.LENGTH_SHORT).show();
        }  else if(MachineryId.equals("")){

            Toast.makeText(AddEngagerReport.this, "Please enter MachineType", Toast.LENGTH_SHORT).show();
        }else if(Vehicle.equals("")){

            Toast.makeText(AddEngagerReport.this, "Please enter VehicleNumber", Toast.LENGTH_SHORT).show();
        }else if(Date.equals("")){

            Toast.makeText(AddEngagerReport.this, "Please enter Date", Toast.LENGTH_SHORT).show();
        }else if(StartTime.equals("")){

            Toast.makeText(AddEngagerReport.this, "Please enter StartTime", Toast.LENGTH_SHORT).show();
        }else if(EndTime.equals("")){

            Toast.makeText(AddEngagerReport.this, "Please enter EndTime", Toast.LENGTH_SHORT).show();
        }else if(StartTime1.equals("")){

            Toast.makeText(AddEngagerReport.this, "Please enter StartTime", Toast.LENGTH_SHORT).show();
        }else if(EndTime1.equals("")){

            Toast.makeText(AddEngagerReport.this, "Please enter EndTime", Toast.LENGTH_SHORT).show();
        }

        else {

            final ProgressDialog progressDialog = new ProgressDialog(AddEngagerReport.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AdminEngagerReport> call = apiInterface.adminAddEngager(MobileNumber,ID,WorkId,SubWorkId,EngagerId,MachineryId,Vehicle,Description,Date,StartTime,EndTime,StartTime1,EndTime1);
            call.enqueue(new Callback<AdminEngagerReport>() {
                @Override
                public void onResponse(Call<AdminEngagerReport> call, Response<AdminEngagerReport> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean4 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(AddEngagerReport.this, "Added Successfully...", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(AddEngagerReport.this, EngagerReport.class);
                        intent.putExtra("ID",ID);
                        intent.putExtra("Project",Project);
                        intent.putExtra("key",key);
                        intent.putExtra("key1",key1);
                        startActivity(intent);


                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(AddEngagerReport.this, "Error while Adding...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<AdminEngagerReport> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(AddEngagerReport.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();

                }
            });

        }

    }

}
