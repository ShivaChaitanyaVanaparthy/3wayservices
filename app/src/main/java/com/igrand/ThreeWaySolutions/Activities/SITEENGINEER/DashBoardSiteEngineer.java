package com.igrand.ThreeWaySolutions.Activities.SITEENGINEER;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.ApprovedLeadsTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.ChangePasswordTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadsListTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.NotificationsTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.ProfileTech;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.RejectedLeadsTechnical;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProjectList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSiteEngineerList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.NotificationCountResponse;
import com.igrand.ThreeWaySolutions.Response.TechLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.TechnicalDashBoardResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class DashBoardSiteEngineer extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    TextView name;
    String Name;
    PrefManagerSiteEngineer prefManagerSiteEngineer;
    RecyclerView recyclerView;
    RecyclerAdapterSiteEngineerList recyclerUser;
    ApiInterface apiInterface;
    AdminProjectsList.StatusBean statusBean;
    Button add;
    String mobileNumber,MobileNumber;
    ShimmerFrameLayout mShimmerViewContainer;
    ImageView toggle;
    private AppBarConfiguration mAppBarConfiguration;
    Boolean exit=false;
    ImageView notification;
    LinearLayout logout1;
    SwipeRefreshLayout pullToRefresh;
    NotificationCountResponse.StatusBean statusBean3;
    RelativeLayout relative_image;
    TextView txt_count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_siteengineer);

        prefManagerSiteEngineer=new PrefManagerSiteEngineer(DashBoardSiteEngineer.this);
        HashMap<String, String> profile=prefManagerSiteEngineer.getUserDetails();
        Name=profile.get("username");
        MobileNumber=profile.get("mobilenumber");


        name=findViewById(R.id.name);
        toggle=findViewById(R.id.toggle11);
        notification=findViewById(R.id.notification);
        logout1=findViewById(R.id.logout1);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        relative_image = findViewById(R.id.relative_image);
        txt_count = findViewById(R.id.txt_count);
        name.setText(Name);

        recyclerView=findViewById(R.id.recyclerView);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }



        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });



        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProjects();
                pullToRefresh.setRefreshing(false);
            }
        });


        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardSiteEngineer.this, NotificationsSiteEngineer.class);
                startActivity(intent);
            }
        });


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefManagerSiteEngineer.clearSession();
                Intent intent = new Intent(DashBoardSiteEngineer.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });



        getProjects();



    }

    private void getProjects() {


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationCountResponse> call1 = apiInterface.notificationcountsite(MobileNumber);
        call1.enqueue(new Callback<NotificationCountResponse>() {
            @Override
            public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                if (response.code() == 200) {
                    statusBean3 = response.body() != null ? response.body().getStatus() : null;

                    //int x=statusBean3.getCount();

                    if(statusBean3.getCount()!=0){
                        relative_image.setVisibility(View.VISIBLE);
                        txt_count.setText(String.valueOf(statusBean3.getCount()));

                    }


                } else if (response.code() != 200) {

                }
            }

            @Override
            public void onFailure(Call<NotificationCountResponse> call, Throwable t) {

                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       /* Toast toast= Toast.makeText(DashBoardAgent.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();*/
            }
        });





        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminProjectsList> call = apiInterface.adminProjectsList();
        call.enqueue(new Callback<AdminProjectsList>() {
            @Override
            public void onResponse(Call<AdminProjectsList> call, Response<AdminProjectsList> response) {

                if (response.code() == 200) {
                    // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(DashBoardSiteEngineer.this, "Project List......", Toast.LENGTH_SHORT).show();
                    List<AdminProjectsList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardSiteEngineer.this));
                    recyclerUser = new RecyclerAdapterSiteEngineerList(DashBoardSiteEngineer.this,dataBeans,"key");
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(DashBoardSiteEngineer.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<AdminProjectsList> call, Throwable t) {
                // progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(DashBoardSiteEngineer.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        getProjects();

    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        item.setChecked(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawers();

        if (id == R.id.nav_home) {

            drawer.closeDrawers();


        }   else if (id == R.id.nav_profile) {

            Intent intent = new Intent(DashBoardSiteEngineer.this, ProfileSiteEngineer.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);


        } else if (id == R.id. nav_changepin) {

            Intent intent = new Intent(DashBoardSiteEngineer.this, ChangePasswordSiteEngineer.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);


        } else if (id == R.id.nav_notifications) {

            Intent intent = new Intent(DashBoardSiteEngineer.this, NotificationsSiteEngineer.class);
            intent.putExtra("MobileNumber",MobileNumber);
            startActivity(intent);

        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardSiteEngineer.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }




}

