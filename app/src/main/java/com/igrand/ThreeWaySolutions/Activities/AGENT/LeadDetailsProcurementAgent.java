package com.igrand.ThreeWaySolutions.Activities.AGENT;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UsersListAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Geolocations;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdminComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments9;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments1;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddUserResponse;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLead1Response;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsProcurementAgent extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1, approvedp, documentp;
    String Id, Property, Date, Village, Checking, Document, Status, Comments, GoogleLocation, Address, IFRAME, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate, ProposalDocument, ProposalNotes, Documents, AdminStatus;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11, statusp, datep, proposalnotes;
    Dialog dialog;
    LinearLayout remarks1;
    ApiInterface apiInterface;
    AgentLeadsCommentsResponse.StatusBean statusBean;
    UpdateLeadResponse.StatusBean statusBean2;
    RecyclerAdapterAdminComment recyclerAdapter;
    Button update;
    RecyclerView recyclerView;
    RecyclerAdapterCheckingDocuments9 recyclerAdapter2;
    String[] document2;
    RecyclerAdapterSubAgentDocuments1 recyclerAdapter4;
    RecyclerAdapterAgentDocuments1 recyclerAdapter3;
    List<String> strings;
    String[] document3;
    UpdateLead1Response.StatusBean statusBean3;
    EditText comment;
    String Comment1, imagePic,picturePath;
    LinearLayout document1;
    ImageView imageupload;
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    String empty="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_agent1);

        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        //document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        date11 = findViewById(R.id.date11);
        remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);
        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        documentp = findViewById(R.id.documentp);
        proposalnotes = findViewById(R.id.proposalnotes);
        update = findViewById(R.id.update);
        recyclerView = findViewById(R.id.recyclerView);
        comment = findViewById(R.id.comment);
        document1 = findViewById(R.id.document);
        imageupload = findViewById(R.id.imageupload);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Comments = getIntent().getStringExtra("Comments");
            GoogleLocation = getIntent().getStringExtra("GoogleLocation");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");

            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            ProposalDocument = getIntent().getStringExtra("ProposalDocument");
            ProposalNotes = getIntent().getStringExtra("ProposalNotes");
            AdminStatus = getIntent().getStringExtra("AdminStatus");


        }


        document1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LinearLayout camera, folder,folder1;

                dialog = new Dialog(LeadDetailsProcurementAgent.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);


                camera.setVisibility(View.VISIBLE);
                folder1.setVisibility(View.GONE);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();


                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();


                    }
                });


            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Comment1 = comment.getText().toString();

                updateLead(Comment1,Id);
            }
        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else {

        }


        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        //date1.setText(Date);

        proposalnotes.setText(ProposalNotes);


        document2 = Document.split(",");

        strings = Arrays.asList(Document.split(","));

        for (int i = 0; i < strings.size(); i++) {

            document3 = strings.get(i).split(strings.get(i).split("\\.")[0]);

            if (document3[1].equals(".pdf")) {


                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                recyclerAdapter4 = new RecyclerAdapterSubAgentDocuments1(LeadDetailsProcurementAgent.this, Document, document2, strings, "keyp");
                recyclerView.setAdapter(recyclerAdapter4);

            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                recyclerAdapter3 = new RecyclerAdapterAgentDocuments1(LeadDetailsProcurementAgent.this, document2, Document, strings);
                recyclerView.setAdapter(recyclerAdapter3);

            }
        }

       /* document2= Document.split(",");

        recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsProcurementAdmin.this,LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter2 = new RecyclerAdapterCheckingDocuments9(LeadDetailsProcurementAdmin.this,document2,Document);
        recyclerView.setAdapter(recyclerAdapter2);*/

        Documents = "http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/proposal_documents/" + ProposalDocument;

        Picasso.get().load(Documents).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(documentp);


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadDetailsProcurementAgent.this, Geolocations.class);
                intent.putExtra("Iframe", GoogleLocation);
                startActivity(intent);
            }
        });

        remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final RecyclerView recyclerView;

                dialog = new Dialog(LeadDetailsProcurementAgent.this);
                dialog.setContentView(R.layout.dialogboxremarks);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                recyclerView = dialog.findViewById(R.id.text);


                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsProcurementAgent.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AgentLeadsCommentsResponse> call = apiInterface.agentLeadsComments(Id);
                call.enqueue(new Callback<AgentLeadsCommentsResponse>() {
                    @Override
                    public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AgentLeadsCommentsResponse.DataBean> dataBeans = response.body().getData();
                            //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                            recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsProcurementAgent.this));
                            recyclerAdapter = new RecyclerAdapterAdminComment(LeadDetailsProcurementAgent.this, dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsProcurementAgent.this, "No Comments...", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsProcurementAgent.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                    }
                });


            }
        });


        if (Checking != null) {

            if (Checking.equals("0")) {

                status.setVisibility(View.VISIBLE);
                status.setText("Rejected");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.rejected);
                date1.setText(CheckingDate);

            } else if (Checking.equals("1")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Approved");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.approved);
                date1.setText(CheckingDate);
            } else if (Checking.equals("2")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Pending");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.question);
                date1.setText(CheckingDate);
            }


            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }


                if (ProcurementStatus != null) {

                    if (ProcurementStatus.equals("4")) {

                        statusp.setText("Rejected");
                        approvedp.setImageResource(R.drawable.rejected);

                    } else if (ProcurementStatus.equals("5")) {
                        statusp.setText("Approved");
                        approvedp.setImageResource(R.drawable.approved);
                    } else if (ProcurementStatus.equals("3")) {
                        statusp.setText("Processing");
                        approvedp.setImageResource(R.drawable.processing);
                    } else if (ProcurementStatus.equals("2")) {
                        statusp.setText("Open");
                        approvedp.setImageResource(R.drawable.open);
                    }

                }


            }


        }
    }



    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();




        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            imageupload.setImageBitmap(converetdImage);
            imageupload.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private File createImagefile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        picturePath = image.getAbsolutePath();
        return image;
    }

    private void updateLead(String comment1, String id) {

        MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("ad_image", image.getName(), requestFile);

        } else {

            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), empty);
            body = MultipartBody.Part.createFormData("ad_image", empty, requestFile);
            // Toast.makeText(this, "Please upload image", Toast.LENGTH_SHORT).show();
        }



        RequestBody COMMENT = RequestBody.create(MediaType.parse("multipart/form-data"), comment1);
        RequestBody ID1 = RequestBody.create(MediaType.parse("multipart/form-data"), id);


        final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsProcurementAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UpdateLead1Response> call = apiInterface.agentupdates(ID1,COMMENT,body);

        call.enqueue(new Callback<UpdateLead1Response>() {
            @Override
            public void onResponse(Call<UpdateLead1Response> call, Response<UpdateLead1Response> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    UpdateLead1Response.StatusBean statusBean3 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(LeadDetailsProcurementAgent.this, "User Added Successfully...", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LeadDetailsProcurementAgent.this, DashBoardAgent.class);
                    startActivity(intent);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(LeadDetailsProcurementAgent.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<UpdateLead1Response> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(LeadDetailsProcurementAgent.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });



    }

}