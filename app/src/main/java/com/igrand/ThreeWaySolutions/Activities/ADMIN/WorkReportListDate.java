package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerWorkReport;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminWorkReportList;

import java.util.List;

public class WorkReportListDate extends BaseActivity {

    ApiInterface apiInterface;
    RecyclerView recyclerView3;
    RecyclerWorkReport recyclerAdapterContractorProjectsList2;
    AdminWorkReportList.StatusBean statusBean2;
    String ID,Date;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_report_list_date);

        recyclerView3=findViewById(R.id.recyclerView3);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            ID=getIntent().getStringExtra("ID");
            Date=getIntent().getStringExtra("Date");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final ProgressDialog progressDialog2 = new ProgressDialog(WorkReportListDate.this);
        progressDialog2.setMessage("Loading.....");
        progressDialog2.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminWorkReportList> call2 = apiInterface.adminWorkReportList2(ID, Date);
        call2.enqueue(new Callback<AdminWorkReportList>() {
            @Override
            public void onResponse(Call<AdminWorkReportList> call, Response<AdminWorkReportList> response) {

                if (response.code() == 200) {
                    progressDialog2.dismiss();
                    statusBean2 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(AddEngagerReport.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminWorkReportList.DataBean> dataBeans = response.body().getData();
                    recyclerView3.setLayoutManager(new LinearLayoutManager(WorkReportListDate.this));
                    recyclerAdapterContractorProjectsList2 = new RecyclerWorkReport(WorkReportListDate.this, dataBeans);
                    recyclerView3.setAdapter(recyclerAdapterContractorProjectsList2);

                } else if (response.code() != 200) {
                    progressDialog2.dismiss();
                    Toast.makeText(WorkReportListDate.this, "No Work Reports...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminWorkReportList> call, Throwable t) {
                progressDialog2.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(WorkReportListDate.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }

        });
    }
}
