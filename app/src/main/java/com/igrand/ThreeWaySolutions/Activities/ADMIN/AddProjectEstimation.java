package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.ProjectEstimation;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEstimationProjectsList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProjectList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubworkWorkList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterUOMList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectEstimation;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeList;
import com.igrand.ThreeWaySolutions.Response.AdminSubWorkTypeListbyId;
import com.igrand.ThreeWaySolutions.Response.AdminUOMList;
import com.igrand.ThreeWaySolutions.Response.AdminWorkTypeList;

import java.util.HashMap;
import java.util.List;

public class AddProjectEstimation extends BaseActivity {

    ImageView back;
    Button submit;
    PrefManagerAdmin prefManagerAdmin;
    TextView projectname, worktype, subworktype, uom, nou, rate, amount, description;
    String Nou, Rate, Amount, Description, Mobilenumber;
    ApiInterface apiInterface;
    AdminProjectsList.StatusBean statusBean1;
    RecyclerAdapterEstimationProjectsList recyclerUser;
    AdminWorkTypeList.StatusBean statusBean;
    RecyclerAdapterEstimationProjectsList recyclerAdapter;
    AdminSubWorkTypeListbyId.StatusBean statusBean2;
    AdminUOMList.StatusBean statusBean3;
    AdminProjectEstimation.StatusBean statusBean4;
    String ProjectId, WorkId, SubWorkId, UomId,id,Project,key,key1;
    Double NouI,RateI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project_estimation);

        back = findViewById(R.id.back);
        submit = findViewById(R.id.submit);
        projectname = findViewById(R.id.projectname);
        worktype = findViewById(R.id.worktype);
        subworktype = findViewById(R.id.subworktype);
        uom = findViewById(R.id.uom);
        nou = findViewById(R.id.nou);
        rate = findViewById(R.id.rate);
        amount = findViewById(R.id.amount);
        description = findViewById(R.id.description);



        rate.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                    NouI=Double.parseDouble(nou.getText().toString());

                if(rate!=null){
                    RateI=Double.parseDouble(rate.getText().toString());

                    amount.setText(String.valueOf(NouI*RateI));
                }

            }
        });





        if(getIntent()!=null){

            id=getIntent().getStringExtra("ID");
            Project = getIntent().getStringExtra("Project");
            key = getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
        }



        prefManagerAdmin = new PrefManagerAdmin(AddProjectEstimation.this);
        HashMap<String, String> profile = prefManagerAdmin.getUserDetails();
        Mobilenumber = profile.get("mobilenumber");


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        projectname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(AddProjectEstimation.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();


                final ProgressDialog progressDialog = new ProgressDialog(AddProjectEstimation.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminProjectsList> call = apiInterface.adminProjectsList();
                call.enqueue(new Callback<AdminProjectsList>() {
                    @Override
                    public void onResponse(Call<AdminProjectsList> call, Response<AdminProjectsList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(AddProjectEstimation.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminProjectsList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddProjectEstimation.this));
                            recyclerUser = new RecyclerAdapterEstimationProjectsList(AddProjectEstimation.this, dataBeans, AddProjectEstimation.this, projectname, dialog);
                            recyclerView.setAdapter(recyclerUser);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddProjectEstimation.this, "No City's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminProjectsList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddProjectEstimation.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddProjectEstimation.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();





                final ProgressDialog progressDialog = new ProgressDialog(AddProjectEstimation.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminWorkTypeList> call = apiInterface.adminWorkList();
                call.enqueue(new Callback<AdminWorkTypeList>() {
                    @Override
                    public void onResponse(Call<AdminWorkTypeList> call, Response<AdminWorkTypeList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddProjectEstimation.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminWorkTypeList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddProjectEstimation.this));
                            recyclerAdapter = new RecyclerAdapterEstimationProjectsList(AddProjectEstimation.this, dataBeans, worktype, dialog, AddProjectEstimation.this,"key");
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddProjectEstimation.this, "No Work Types...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminWorkTypeList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddProjectEstimation.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });


        uom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddProjectEstimation.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();


                final ProgressDialog progressDialog = new ProgressDialog(AddProjectEstimation.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminUOMList> call = apiInterface.uomList();
                call.enqueue(new Callback<AdminUOMList>() {
                    @Override
                    public void onResponse(Call<AdminUOMList> call, Response<AdminUOMList> response) {


                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean3 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddProjectEstimation.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminUOMList.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddProjectEstimation.this));
                            recyclerAdapter = new RecyclerAdapterEstimationProjectsList(AddProjectEstimation.this, dataBeans, uom, AddProjectEstimation.this, dialog,"uom");
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddProjectEstimation.this, "No Units...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminUOMList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddProjectEstimation.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

            }
        });



        getSubwork();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getOputpu();
            }
        });




    }


    public void getId0(String selectedworkid) {

        ProjectId = selectedworkid;
    }

    public void getId1(String selectedworkid1) {

        WorkId = selectedworkid1;
    }

    public void getId2(String selectedworkid2) {

        SubWorkId = selectedworkid2;
    }

    public void getId3(String selectedworkid3) {

        UomId = selectedworkid3;
    }

    private void getSubwork() {


        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(AddProjectEstimation.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);


                final ProgressDialog progressDialog = new ProgressDialog(AddProjectEstimation.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminSubWorkTypeListbyId> call = apiInterface.adminFsubworkbyid(WorkId);
                call.enqueue(new Callback<AdminSubWorkTypeListbyId>() {
                    @Override
                    public void onResponse(Call<AdminSubWorkTypeListbyId> call, Response<AdminSubWorkTypeListbyId> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean2 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddProjectEstimation.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminSubWorkTypeListbyId.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(AddProjectEstimation.this));
                            recyclerAdapter = new RecyclerAdapterEstimationProjectsList(AddProjectEstimation.this, dataBeans, dialog, AddProjectEstimation.this, subworktype,"sub");
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(AddProjectEstimation.this, "No Sub-Work Types...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminSubWorkTypeListbyId> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(AddProjectEstimation.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });



    }


    private void getOputpu() {


                Nou = nou.getText().toString();
                Rate = rate.getText().toString();
                Amount = amount.getText().toString();
                Description = description.getText().toString();



        if(WorkId==null){

            Toast.makeText(AddProjectEstimation.this, "Please select WorkType", Toast.LENGTH_SHORT).show();
        }else if(SubWorkId==null){

            Toast.makeText(AddProjectEstimation.this, "Please select SubWorkType", Toast.LENGTH_SHORT).show();
        }else if(UomId==null){

            Toast.makeText(AddProjectEstimation.this, "Please select Measurement Unit", Toast.LENGTH_SHORT).show();
        }  else if(Nou.equals("")){

            Toast.makeText(AddProjectEstimation.this, "Please enter units", Toast.LENGTH_SHORT).show();
        }else if(Rate.equals("")){

            Toast.makeText(AddProjectEstimation.this, "Please enter rate", Toast.LENGTH_SHORT).show();
        }
        else {

            final ProgressDialog progressDialog = new ProgressDialog(AddProjectEstimation.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AdminProjectEstimation> call = apiInterface.adminprojectestimation(Mobilenumber, id, WorkId, SubWorkId, Description, Nou, Rate, UomId, Amount);
            call.enqueue(new Callback<AdminProjectEstimation>() {
                @Override
                public void onResponse(Call<AdminProjectEstimation> call, Response<AdminProjectEstimation> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean4 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(AddProjectEstimation.this, "Added Successfully......", Toast.LENGTH_SHORT).show();


                            Intent intent = new Intent(AddProjectEstimation.this, ProjectEstimation.class);
                            intent.putExtra("ID",id);
                            intent.putExtra("Project",Project);
                            intent.putExtra("key",key);
                            intent.putExtra("key1",key1);
                            startActivity(intent);




                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(AddProjectEstimation.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<AdminProjectEstimation> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(AddProjectEstimation.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });

        }




    }



}