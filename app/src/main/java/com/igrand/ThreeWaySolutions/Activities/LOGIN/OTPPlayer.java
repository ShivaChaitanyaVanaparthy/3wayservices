package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.OTPResponse;
import com.mukesh.OtpView;

public class OTPPlayer extends BaseActivity {

    ImageView back;
    Button next;
    OtpView otpView;
    String otp,mobileNumber;
    ApiInterface apiInterface;
    OTPResponse.StatusBean statusBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpplayer);

        back=findViewById(R.id.back);
        next=findViewById(R.id.next);
        otpView = findViewById(R.id.otp_view);

        if(getIntent()!=null){
            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                otp=otpView.getText().toString();


                final ProgressDialog progressDialog = new ProgressDialog(OTPPlayer.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<OTPResponse> call = apiInterface.playerOtp(mobileNumber,otp);
                call.enqueue(new Callback<OTPResponse>() {
                    @Override
                    public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(OTPPlayer.this, "Player Registered Successfully...", Toast.LENGTH_SHORT).show();
                           /* Intent intent=new Intent(OTPPlayer.this, ManaVillageRegister.class);
                            intent.putExtra("Number",mobileNumber);
                            startActivity(intent);*/

                            final Dialog dialog = new Dialog(OTPPlayer.this);
                            dialog.setContentView(R.layout.couponapproved);
                            dialog.show();
                            Button done;
                            done=dialog.findViewById(R.id.done);
                            //dialog.setCancelable(false);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            Window window = dialog.getWindow();
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            done.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Intent intent=new Intent(OTPPlayer.this, SelectModule.class);
                                    intent.putExtra("Number",mobileNumber);
                                    startActivity(intent);

                                }
                            });



                        }

                        else {
                            progressDialog.dismiss();
                            Toast.makeText(OTPPlayer.this, "Please Enter Valid OTP...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<OTPResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(OTPPlayer.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });






            }
        });
    }
}
