package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentApprovedLeadResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class PendingLeadsAgent extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterAgent recyclerAdapter;
    AgentApprovedLeadResponse.StatusBean statusBean;
    ApiInterface apiInterface;
    ImageView back;
    String mobileNumber,AgentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_leads_agent);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            AgentId=getIntent().getStringExtra("AgentId");


        }




        final ProgressDialog progressDialog = new ProgressDialog(PendingLeadsAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AgentApprovedLeadResponse> call = apiInterface.agentRejectedLeadsList(mobileNumber);
        call.enqueue(new Callback<AgentApprovedLeadResponse>() {
            @Override
            public void onResponse(Call<AgentApprovedLeadResponse> call, Response<AgentApprovedLeadResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AgentApprovedLeadResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(PendingLeadsAgent.this, "Rejected Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(PendingLeadsAgent.this));
                    recyclerAdapter = new RecyclerAdapterAgent(PendingLeadsAgent.this,dataBeans, mobileNumber);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(PendingLeadsAgent.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }
            }

            @Override
            public void onFailure(Call<AgentApprovedLeadResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(PendingLeadsAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });
    }
}
