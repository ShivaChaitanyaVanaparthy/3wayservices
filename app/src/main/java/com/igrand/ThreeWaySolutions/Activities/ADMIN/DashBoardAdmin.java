package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.navigation.NavigationView;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Login;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ManaVillageRegister;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.PlayersListAdmin;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminDashBoardResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;

import java.util.HashMap;
import java.util.List;

public class DashBoardAdmin extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    ImageView toggle,next,next1,next2,next3;
    RecyclerView recyclerView;
    RecyclerAdapterAdmin recyclerAdapter;
    ImageView notification;
    LinearLayout add,users,logout1;
    Boolean exit=false;
    String MobileNumber1,Name;
    LinearLayout leads,totalprojects;
    ApiInterface apiInterface;
    LeadsListResponse.StatusBean statusBean;
    AdminDashBoardResponse.StatusBean statusBean1;
    TextView leads11,users11;
    PrefManagerAdmin prefManagerAdmin;
    TextView name;
    DrawerLayout drawer;
    LinearLayout linear_expand,linear,linear_txt,linearr;
    RelativeLayout linear_home,linear_leads,linear_vendors,linear_invent,linear_proleads,linear_profile,linear_changepin,linear_notifications,players,earns,referral;
    TextView txt_users,txt_values,materialtype,legaldoc,city,worktype,subworktype,uom,machine,state,district,mandal,village,games;
    SwipeRefreshLayout pullToRefresh;
    ShimmerFrameLayout mShimmerViewContainer;


    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel, List<String>> listDataChild;
    RelativeLayout linear_values;
    Handler handler = new Handler();
    Runnable refresh;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_admin);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        drawer = findViewById(R.id.drawer_layout);
        toggle = findViewById(R.id.toggle11);
        recyclerView = findViewById(R.id.recyclerView);
        add = findViewById(R.id.add);
        linear_home = findViewById(R.id.linear_home);
        linear_leads = findViewById(R.id.linear_leads);
        linear_vendors = findViewById(R.id.linear_vendors);
        linear_invent = findViewById(R.id.linear_invent);
        materialtype = findViewById(R.id.materialtype);
        legaldoc = findViewById(R.id.legaldoc);
        city = findViewById(R.id.city);
        linear_proleads = findViewById(R.id.linear_proleads);
        linear_changepin = findViewById(R.id.linear_changepin);
        linear_notifications = findViewById(R.id.linear_notifications);
        linear_profile = findViewById(R.id.linear_profile);
        linearr = findViewById(R.id.linearr);

        notification = findViewById(R.id.notification);
        users = findViewById(R.id.users);
        logout1 = findViewById(R.id.logout1);
        leads = findViewById(R.id.leads);
        leads11 = findViewById(R.id.leads11);
        users11 = findViewById(R.id.users11);
        name = findViewById(R.id.name);
        next = findViewById(R.id.next);
        next1 = findViewById(R.id.next1);
        worktype = findViewById(R.id.worktype);
        subworktype = findViewById(R.id.subworktype);
        uom = findViewById(R.id.uom);
        machine = findViewById(R.id.machine);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        txt_users = findViewById(R.id.txt_users);
        txt_values = findViewById(R.id.txt_values);
        linear_expand = findViewById(R.id.linear_expand);
        linear = findViewById(R.id.linear);
        linear_txt = findViewById(R.id.linear_txt);
        next2 = findViewById(R.id.next2);
        next3 = findViewById(R.id.next3);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        state = findViewById(R.id.state);
        district = findViewById(R.id.district);
        mandal = findViewById(R.id.mandal);
        village = findViewById(R.id.village);
        games = findViewById(R.id.games);
        players = findViewById(R.id.players);
        earns = findViewById(R.id.earns);
        referral = findViewById(R.id.referral);



        worktype.setVisibility(View.GONE);
        subworktype.setVisibility(View.GONE);
        uom.setVisibility(View.GONE);
        machine.setVisibility(View.GONE);
        materialtype.setVisibility(View.GONE);
        players.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardAdmin.this, PlayersListAdmin.class);
                startActivity(intent);
            }
        });


        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,StateList.class);
                startActivity(intent);
            }
        });

        district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,DistrictList.class);
                startActivity(intent);
            }
        });

        mandal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,MandalList.class);
                startActivity(intent);
            }
        });

        village.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,VillageList.class);
                startActivity(intent);
            }
        });

        games.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,GamesList.class);
                startActivity(intent);
            }
        });



        earns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,EarnsList.class);
                startActivity(intent);
            }
        });

        referral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,ReferralsList.class);
                startActivity(intent);
            }
        });


        linear_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, UsersListAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });


        linear_leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, LeadsListAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });



        linear_vendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, VendorsList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });

        linear_invent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, InventoryList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });

        materialtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, ValuesList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);


            }
        });

        legaldoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, LegalTeamDocumentsList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });

        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, CityList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });


        worktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,WorkTypeList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });

        subworktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,SubWorkTypeList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });

        uom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,UomList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });

        machine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoardAdmin.this,MachineList.class);
                intent.putExtra("MobileNumber", MobileNumber1);

                startActivity(intent);
            }
        });

        linear_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, NotificationsAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });

        linear_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, ProfileAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);

            }
        });
        linear_changepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(DashBoardAdmin.this, ChangePasswordAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });

        linear_proleads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(DashBoardAdmin.this, ProcurementLeadsList.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });


        linear_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //Toast.makeText(DashBoardAdmin.this, "ggggg", Toast.LENGTH_SHORT).show();
                linear_expand.setVisibility(linear_expand.isShown() ? View.GONE : View.VISIBLE);
                if (linear_expand.isShown()) {
                    linear_expand.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                    next1.setVisibility(View.VISIBLE);
                } else {
                    next.setVisibility(View.VISIBLE);
                    next1.setVisibility(View.GONE);
                }

            }
        });




        //expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);

        prefManagerAdmin = new PrefManagerAdmin(DashBoardAdmin.this);
        if (getIntent() != null) {

            // MobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        prefManagerAdmin = new PrefManagerAdmin(DashBoardAdmin.this);
        HashMap<String, String> profile = prefManagerAdmin.getUserDetails();
        MobileNumber1 = profile.get("mobilenumber");
        Name = profile.get("username");


        name.setText(Name);


      /* final ProgressDialog progressDialog = new ProgressDialog(DashBoardAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminDashBoardResponse> call = apiInterface.adminDashBoard();
        call.enqueue(new Callback<AdminDashBoardResponse>() {
            @Override
            public void onResponse(Call<AdminDashBoardResponse> call, Response<AdminDashBoardResponse> response) {

                if (response.code() == 200) {
                    //progressDialog.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    AdminDashBoardResponse.DataBean dataBeans1 = response.body().getData();

                    leads11.setText(String.valueOf(dataBeans1.get_$LeadsCount147()));
                    users11.setText(String.valueOf(dataBeans1.get_$UserCount53()));

                } else if (response.code() != 200) {
                    //progressDialog.dismiss();
                    Toast.makeText(DashBoardAdmin.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<AdminDashBoardResponse> call, Throwable t) {
                //progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(DashBoardAdmin.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManagerAdmin.clearSession();
                Intent intent = new Intent(DashBoardAdmin.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        toggle.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });


        users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardAdmin.this, UsersListAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });

        leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardAdmin.this, LeadsListAdmin.class);
                intent.putExtra("MobileNumber", MobileNumber1);
                startActivity(intent);
            }
        });


       /* final ProgressDialog progressDialog1 = new ProgressDialog(DashBoardAdmin.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LeadsListResponse> call1 = apiInterface.adminLeadsList();
        call1.enqueue(new Callback<LeadsListResponse>() {
            @Override
            public void onResponse(Call<LeadsListResponse> call, Response<LeadsListResponse> response) {

                if (response.code() == 200) {
                   // progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<LeadsListResponse.DataBean> dataBeans = response.body().getData();
                    String documents = dataBeans.get(0).getDocument();
                    //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardAdmin.this));
                    recyclerAdapter = new RecyclerAdapterAdmin(DashBoardAdmin.this, dataBeans, documents);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    //progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(DashBoardAdmin.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<LeadsListResponse> call, Throwable t) {
               // progressDialog1.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(DashBoardAdmin.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardAdmin.this, NotificationsAdmin.class);
                startActivity(intent);
            }
        });

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    private void refreshData() {

       /* final ProgressDialog progressDialog = new ProgressDialog(DashBoardAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminDashBoardResponse> call = apiInterface.adminDashBoard();
        call.enqueue(new Callback<AdminDashBoardResponse>() {
            @Override
            public void onResponse(Call<AdminDashBoardResponse> call, Response<AdminDashBoardResponse> response) {

                if (response.code() == 200) {
                   // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    AdminDashBoardResponse.DataBean dataBeans1 = response.body().getData();

                    leads11.setText(String.valueOf(dataBeans1.get_$LeadsCount147()));
                    users11.setText(String.valueOf(dataBeans1.get_$UserCount53()));

                } else if (response.code() != 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(DashBoardAdmin.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<AdminDashBoardResponse> call, Throwable t) {
                //progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(DashBoardAdmin.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



        /*final ProgressDialog progressDialog1 = new ProgressDialog(DashBoardAdmin.this);
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LeadsListResponse> call1 = apiInterface.adminLeadsList();
        call1.enqueue(new Callback<LeadsListResponse>() {
            @Override
            public void onResponse(Call<LeadsListResponse> call, Response<LeadsListResponse> response) {

                if (response.code() == 200) {
                   // progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<LeadsListResponse.DataBean> dataBeans = response.body().getData();
                    String documents = dataBeans.get(0).getDocument();
                    //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(DashBoardAdmin.this));
                    recyclerAdapter = new RecyclerAdapterAdmin(DashBoardAdmin.this, dataBeans, documents);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    //progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(DashBoardAdmin.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<LeadsListResponse> call, Throwable t) {
               // progressDialog1.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(DashBoardAdmin.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });
    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
                this.exit = true;
                Toast.makeText(DashBoardAdmin.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}