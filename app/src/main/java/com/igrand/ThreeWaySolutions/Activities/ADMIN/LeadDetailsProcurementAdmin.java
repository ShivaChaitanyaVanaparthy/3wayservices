package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Geolocations;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdminComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments9;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsProcurementAdmin extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1, approvedp,documentp;
    String Id, Property, Date, Village, Checking, Document, Status, Comments, GoogleLocation, Address, IFRAME, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate,ProposalDocument,ProposalNotes,Documents,AdminStatus;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11, statusp, datep,proposalnotes;
    Dialog dialog;
    LinearLayout remarks1;
    ApiInterface apiInterface;
    AgentLeadsCommentsResponse.StatusBean statusBean;
    UpdateLeadResponse.StatusBean statusBean2;
    RecyclerAdapterAdminComment recyclerAdapter;
    Button update;
    RecyclerView recyclerView;
    RecyclerAdapterCheckingDocuments9 recyclerAdapter2;
    String[] document2;
    RecyclerAdapterSubAgentDocuments recyclerAdapter4;
    RecyclerAdapterAgentDocuments recyclerAdapter3;
    List<String> strings;
    String[] document3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_admin1);

        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        //document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        date11 = findViewById(R.id.date11);
        remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);
        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        documentp = findViewById(R.id.documentp);
        proposalnotes = findViewById(R.id.proposalnotes);
        update = findViewById(R.id.update);
        recyclerView = findViewById(R.id.recyclerView);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Comments = getIntent().getStringExtra("Comments");
            GoogleLocation = getIntent().getStringExtra("GoogleLocation");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");

            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            ProposalDocument = getIntent().getStringExtra("ProposalDocument");
            ProposalNotes = getIntent().getStringExtra("ProposalNotes");
            AdminStatus = getIntent().getStringExtra("AdminStatus");


        }


        if(AdminStatus.equals("0")){


           // update.setVisibility(View.VISIBLE);

            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsProcurementAdmin.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<UpdateLeadResponse> call = apiInterface.adminupdates(Id);
                    call.enqueue(new Callback<UpdateLeadResponse>() {
                        @Override
                        public void onResponse(Call<UpdateLeadResponse> call, Response<UpdateLeadResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean2 = response.body() != null ? response.body().getStatus() : null;
                                Intent intent=new Intent(LeadDetailsProcurementAdmin.this,DashBoardAdmin.class);
                                startActivity(intent);
                                Toast.makeText(LeadDetailsProcurementAdmin.this, "Lead Activated for Agent", Toast.LENGTH_SHORT).show();

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(LeadDetailsProcurementAdmin.this, "Not Activated...", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<UpdateLeadResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast = Toast.makeText(LeadDetailsProcurementAdmin.this,
                                    t.getMessage(), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();
                        }
                    });




                }
            });


        }



        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        //date1.setText(Date);

        proposalnotes.setText(ProposalNotes);




        document2=Document.split(",");

        strings = Arrays.asList(Document.split(","));

        for(int i=0;i<strings.size();i++) {

            document3 = strings.get(i).split(strings.get(i).split("\\.")[0]);

            if (document3[1].equals(".pdf")) {


                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter4 = new RecyclerAdapterSubAgentDocuments(LeadDetailsProcurementAdmin.this,Document,document2,  strings,"keyp");
                recyclerView.setAdapter(recyclerAdapter4);

            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter3 = new RecyclerAdapterAgentDocuments(LeadDetailsProcurementAdmin.this,document2,Document,strings);
                recyclerView.setAdapter(recyclerAdapter3);

            }
        }

       /* document2= Document.split(",");

        recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsProcurementAdmin.this,LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter2 = new RecyclerAdapterCheckingDocuments9(LeadDetailsProcurementAdmin.this,document2,Document);
        recyclerView.setAdapter(recyclerAdapter2);*/

        Documents="http://www.igranddeveloper.xyz/3way-services/admin_assets/uploads/leads/proposal_documents/"+ProposalDocument;

        Picasso.get().load(Documents).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(documentp);


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadDetailsProcurementAdmin.this, Geolocations.class);
                intent.putExtra("Iframe", GoogleLocation);
                startActivity(intent);
            }
        });

        remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final RecyclerView recyclerView;

                dialog = new Dialog(LeadDetailsProcurementAdmin.this);
                dialog.setContentView(R.layout.dialogboxremarks);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                recyclerView = dialog.findViewById(R.id.text);


                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsProcurementAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AgentLeadsCommentsResponse> call = apiInterface.agentLeadsComments(Id);
                call.enqueue(new Callback<AgentLeadsCommentsResponse>() {
                    @Override
                    public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AgentLeadsCommentsResponse.DataBean> dataBeans = response.body().getData();
                            //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                            recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsProcurementAdmin.this));
                            recyclerAdapter = new RecyclerAdapterAdminComment(LeadDetailsProcurementAdmin.this, dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsProcurementAdmin.this, "No Comments...", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsProcurementAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                    }
                });


            }
        });


        if (Checking != null) {

            if (Checking.equals("0")) {

                status.setVisibility(View.VISIBLE);
                status.setText("Rejected");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.rejected);
                date1.setText(CheckingDate);

            } else if (Checking.equals("1")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Approved");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.approved);
                date1.setText(CheckingDate);
            } else if (Checking.equals("2")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Pending");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.question);
                date1.setText(CheckingDate);
            }


            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }


                if (ProcurementStatus != null) {

                    if (ProcurementStatus.equals("4")) {

                        statusp.setText("Rejected");
                        approvedp.setImageResource(R.drawable.rejected);

                    } else if (ProcurementStatus.equals("5")) {
                        statusp.setText("Approved");
                        approvedp.setImageResource(R.drawable.approved);
                    } else if (ProcurementStatus.equals("3")) {
                        statusp.setText("Processing");
                        approvedp.setImageResource(R.drawable.processing);
                    } else if (ProcurementStatus.equals("2")) {
                        statusp.setText("Open");
                        approvedp.setImageResource(R.drawable.open);
                    }

                }


             /*   Picasso.get().load(Document).error(R.drawable.profilepic).into(document);

                document.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        document.buildDrawingCache();
                        Bitmap bitmap = document.getDrawingCache();
                        Intent intent = new Intent(LeadDetailsProcurementAdmin.this, ProfileImage.class);
                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        startActivity(intent);
                    }
                });*/

            }
        }
    }
}
