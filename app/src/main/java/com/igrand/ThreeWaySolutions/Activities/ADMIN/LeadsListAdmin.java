package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;

import java.util.List;

public class LeadsListAdmin extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterAdmin recyclerAdapter;
    LeadsListResponse.StatusBean statusBean;
    ApiInterface apiInterface;
    ImageView back;
    String mobileNumber;
    ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_list_admin);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadsListAdmin.this,DashBoardAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


      /*  final ProgressDialog progressDialog = new ProgressDialog(LeadsListAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LeadsListResponse> call = apiInterface.adminLeadsList();
        call.enqueue(new Callback<LeadsListResponse>() {
            @Override
            public void onResponse(Call<LeadsListResponse> call, Response<LeadsListResponse> response) {

                if (response.code() == 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<LeadsListResponse.DataBean> dataBeans=response.body().getData();
                    String documents=dataBeans.get(0).getDocument();
                    //Toast.makeText(LeadsListAdmin.this, "Leads List", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(LeadsListAdmin.this));
                   /* skeletonScreen = Skeleton.bind(recyclerView)
                            .adapter(recyclerAdapter)
                            .load(R.layout.row_card)
                            .shimmer(true)
                            .angle(30)//shimmer tilt angle
// .color(R.color.colorAccent)//shimmer color
                            .frozen(true)//true means that the RecyclerView cannot be slid when the skeleton screen is displayed, otherwise it can be slid
                            .duration(1200)
                            .count(10)
                            .show();

                    recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            skeletonScreen.hide();
                        }
                    }, 10000);*/
                    recyclerAdapter = new RecyclerAdapterAdmin(LeadsListAdmin.this,dataBeans, documents);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(LeadsListAdmin.this, "Error...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LeadsListResponse> call, Throwable t) {
                //progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LeadsListAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

}
