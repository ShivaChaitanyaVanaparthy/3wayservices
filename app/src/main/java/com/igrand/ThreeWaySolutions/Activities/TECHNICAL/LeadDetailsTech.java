package com.igrand.ThreeWaySolutions.Activities.TECHNICAL;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.CheckingDocList;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.LeadDetailsChecking;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.MarketingDocList;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubTechDoc;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterTechDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.AgentUpdateSubAgentLeadResponse;
import com.igrand.ThreeWaySolutions.Response.AgentUpdateSubAgentRejectLeadResponse;
import com.igrand.ThreeWaySolutions.Response.EditLeadsCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.ProcurementLeadResponse;
import com.igrand.ThreeWaySolutions.Response.TechnicalLeadResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsCheckingResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateLeadsTechResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

public class LeadDetailsTech extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1,approvedp,approveds,approvedl,approvedle;
    String Id, Property, Date, Village, Checking, Status, Comments, GoogleLocation, Address, MarketingStatus, MarketingDate, CheckingDate,mobileNumber,UserType,ProcurementStatus, ProcurementDate;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11,statusp, datep,statusl,statuss,datel,dates,statusle,datele,acres,survey,mandal,district,add1;
    Dialog dialog;
    LinearLayout checking1,marketing;
    ApiInterface apiInterface;
    AgentLeadsCommentsResponse.StatusBean statusBean;
    RecyclerAdapterAgentComment recyclerAdapter;
    Button update,reject,edit,add;
    AgentUpdateSubAgentLeadResponse.StatusBean statusBean1;
    AgentUpdateSubAgentRejectLeadResponse.StatusBean statusBean2;
    LinearLayout linear;

    String Document,LesionDate,LesionStatus,Survey,Acres,District,Mandal,Image;
    String[] document2;
    RecyclerView recyclerView,recyclerVieww;
    RecyclerAdapterAgentDocuments recyclerAdapter1;
    String LegalStatus,SurveyStatus,LegalDate,SurveyDate,Latitude,Longitude,pdf;
    List<String> strings,stringsimg;
    String[] document3,imagedoc;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;
    String ct_status,ct_comments,TechDate,TechStatus;
    UpdateLeadsTechResponse.StatusBean statusBean3;
    RecyclerAdapterAgentImages recyclerAdapter3;
    TextView comments,road,sales;
    String Propertydesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_tech);
        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        //checking=findViewById(R.id.Checking);
        //document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        // remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);

        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        date11 = findViewById(R.id.date11);
        update = findViewById(R.id.update);

        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        recyclerView = findViewById(R.id.recyclerView);

        statuss = findViewById(R.id.statuss);
        dates = findViewById(R.id.dates);
        approveds = findViewById(R.id.approveds);
        statusl = findViewById(R.id.statusl);
        datel = findViewById(R.id.datel);
        approvedl = findViewById(R.id.approvedl);
        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
       /* marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);*/
        linear = findViewById(R.id.linear);
        reject = findViewById(R.id.reject);
        acres = findViewById(R.id.acres);
        survey = findViewById(R.id.survey);
        mandal = findViewById(R.id.mandal);
        district = findViewById(R.id.district);
        edit = findViewById(R.id.edit);
        add = findViewById(R.id.add);
        add1 = findViewById(R.id.add1);
        recyclerVieww = findViewById(R.id.recyclerVieww);
        checking1 = findViewById(R.id.checking);
        marketing = findViewById(R.id.marketing);
        comments = findViewById(R.id.comments);
        road = findViewById(R.id.road);
        sales = findViewById(R.id.sales);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsTech.this, DashBoardTechnical.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Image = getIntent().getStringExtra("Image");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");
            Propertydesc = getIntent().getStringExtra("Propertydesc");
            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            mobileNumber = getIntent().getStringExtra("MobileNumber");
            UserType = getIntent().getStringExtra("UserType");
            TechStatus = getIntent().getStringExtra("TechStatus");
            TechDate = getIntent().getStringExtra("TechDate");

            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");
        }

        acres.setText(Acres);
        survey.setText(Survey);
        mandal.setText(Mandal);
        district.setText(District);
        comments.setText(Propertydesc);

        final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsTech.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProcurementLeadResponse> call = apiInterface.LeadDetailTech(Id);
        call.enqueue(new Callback<ProcurementLeadResponse>() {
            @Override
            public void onResponse(Call<ProcurementLeadResponse> call, Response<ProcurementLeadResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    ProcurementLeadResponse.StatusBean statusBean = response.body() != null ? response.body().getStatus() : null;

                    ProcurementLeadResponse.DataBean dataBeans=response.body().getData();


                    road.setText(dataBeans.getRoad_connectivity());
                    sales.setText(dataBeans.getExpected_sales_value());




                } else if(response.code()==404)
                {

                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(LeadDetailsTech.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }


            @Override
            public void onFailure(Call<ProcurementLeadResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LeadDetailsTech.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


       /* final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsTech.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TechnicalLeadResponse> call = apiInterface.LeadDetailTech(Id);
        call.enqueue(new Callback<TechnicalLeadResponse>() {
            @Override
            public void onResponse(Call<TechnicalLeadResponse> call, Response<TechnicalLeadResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    TechnicalLeadResponse.StatusBean statusBean = response.body() != null ? response.body().getStatus() : null;

                   List<TechnicalLeadResponse.DataBean> dataBeans=response.body().getData();







                } else if(response.code()==404)
                {

                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(LeadDetailsTech.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }


            @Override
            public void onFailure(Call<TechnicalLeadResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LeadDetailsTech.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });*/


        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsTech.this,TechDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });


        checking1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsTech.this, CheckingDocList.class);
                intent.putExtra("ID",Id);
                intent.putExtra("Key","Key");
                startActivity(intent);
            }
        });

        marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsTech.this, MarketingDocList.class);
                intent.putExtra("ID",Id);
                intent.putExtra("Key","Key");
                startActivity(intent);
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                        final RadioGroup rg;
                        final RadioButton accept, reject;
                        final EditText comment;
                        Button submit;
                        final LinearLayout linear;
                        final TextView uploadimage,uploadimage1;


                        dialog = new Dialog(LeadDetailsTech.this);
                        dialog.setContentView(R.layout.dialogboxedit);
                        dialog.show();
                        dialog.setCancelable(true);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        Window window = dialog.getWindow();
                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                        rg = dialog.findViewById(R.id.rg);
                        accept = dialog.findViewById(R.id.accept);
                        reject = dialog.findViewById(R.id.reject);
                        comment = dialog.findViewById(R.id.comments);
                        submit = dialog.findViewById(R.id.submit);
                        linear = dialog.findViewById(R.id.linear);
                       /* uploadimage = dialog.findViewById(R.id.uploadimage);
                        uploadimage1 = dialog.findViewById(R.id.uploadimage1);

                        uploadimage.setVisibility(View.GONE);
                        uploadimage1.setVisibility(View.GONE);
*/







                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                                if (accept.isChecked()) {

                                    ct_status = "1";


                                } else if (reject.isChecked()) {

                                    ct_status = "0";

                                }

                                ct_comments = comment.getText().toString();



                                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsTech.this);
                                progressDialog.setMessage("Loading.....");
                                progressDialog.show();
                                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                Call<UpdateLeadsTechResponse> call1 = apiInterface.techUpdateLead(Id, ct_status, ct_comments,mobileNumber);
                                call1.enqueue(new Callback<UpdateLeadsTechResponse>() {

                                    @Override
                                    public void onResponse(Call<UpdateLeadsTechResponse> call, Response<UpdateLeadsTechResponse> response) {
                                        if (response.code() == 200) {
                                            progressDialog.dismiss();
                                            statusBean3 = response.body() != null ? response.body().getStatus() : null;
                                            Toast.makeText(LeadDetailsTech.this, "Leads Updated Successfully", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplication(), DashBoardTechnical.class);
                                            intent.putExtra("MobileNumber", mobileNumber);
                                            startActivity(intent);
                                            // comment11.setVisibility(View.VISIBLE);


                                        } else if (response.code() != 200) {
                                            progressDialog.dismiss();
                                            Toast.makeText(LeadDetailsTech.this, "Error...", Toast.LENGTH_SHORT).show();

                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<UpdateLeadsTechResponse> call, Throwable t) {
                                        progressDialog.dismiss();
                                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                        Toast toast = Toast.makeText(LeadDetailsTech.this,
                                                t.getMessage(), Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                        toast.show();


                                    }
                                });



                            }
                        });


                    }
                });






        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsTech.this,AddDocumentTech.class);
                intent.putExtra("Id",Id);
                intent.putExtra("MobileNumber",mobileNumber);
                intent.putExtra("Property",Property);
                intent.putExtra("Date",Date);
                intent.putExtra("Village",Village);
                intent.putExtra("Checking",Checking);
                intent.putExtra("Document",Document);
                intent.putExtra("Image",Image);
                intent.putExtra("Status",Status);
                intent.putExtra("Comments",Comments);
                intent.putExtra("Latitude",Latitude);
                intent.putExtra("Longitude",Longitude);
                intent.putExtra("MOBILE",mobileNumber);
                intent.putExtra("MarketingStatus",MarketingStatus);
                intent.putExtra("MarketingDate",MarketingDate);
                intent.putExtra("CheckingDate",CheckingDate);
                intent.putExtra("ProcurementDate",ProcurementDate);
                intent.putExtra("ProcurementStatus",ProcurementStatus);
                intent.putExtra("Acres",Acres);
                intent.putExtra("Survey",Survey);
                intent.putExtra("Propertydesc",Propertydesc);
                intent.putExtra("Mandal",Mandal);
                intent.putExtra("District",District);
                startActivity(intent);
            }
        });






    /*    document2=Document.split(",");

        strings = Arrays.asList(Document.split(","));

        for(int i=0;i<strings.size();i++) {



            document3 = strings.get(i).split("\\.");

            if (document3[1].equals("pdf")) {

                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter2 = new RecyclerAdapterSubTechDoc(LeadDetailsTech.this,document2,Document,  strings,"key");
                recyclerView.setAdapter(recyclerAdapter2);

            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter1 = new RecyclerAdapterTechDocuments(LeadDetailsTech.this,document2,strings,Document);
                recyclerView.setAdapter(recyclerAdapter1);

            }

        }
*/


        if(Document.equals("")){

        }else {

            document2 = Document.split(",");

            strings = Arrays.asList(Document.split(","));

            for (int i = 0; i < strings.size(); i++) {


                document3 = strings.get(i).split("\\.");
                for (int j = 0; j < document2.length; j++) {

                    pdf = document2[j].substring(document2[j].length() - 3);
                    if (pdf.equals("pdf")) {

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsTech.this, document2, Document, strings, "keyt");
                        recyclerView.setAdapter(recyclerAdapter2);

                    }

                }

            }

        }
        imagedoc=Image.split(",");

        stringsimg = Arrays.asList(Image.split(","));

        recyclerVieww.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter3 = new RecyclerAdapterAgentImages(LeadDetailsTech.this,imagedoc,stringsimg,Image,"keytechnical");
        recyclerVieww.setAdapter(recyclerAdapter3);



        if(Status.equals("0")){
            linear.setVisibility(View.VISIBLE);


            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsTech.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AgentUpdateSubAgentLeadResponse> call = apiInterface.agentupdatelead(Id);
                    call.enqueue(new Callback<AgentUpdateSubAgentLeadResponse>() {
                        @Override
                        public void onResponse(Call<AgentUpdateSubAgentLeadResponse> call, Response<AgentUpdateSubAgentLeadResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(LeadDetailsTech.this, "Lead Activated successfully...", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(LeadDetailsTech.this, DashBoardAgent.class);
                                intent.putExtra("MobileNumber",mobileNumber);
                                startActivity(intent);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(LeadDetailsTech.this, "Error while updating lead...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<AgentUpdateSubAgentLeadResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(LeadDetailsTech.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });

                }
            });


            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsTech.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AgentUpdateSubAgentRejectLeadResponse> call = apiInterface.agentupdateleadreject(Id);
                    call.enqueue(new Callback<AgentUpdateSubAgentRejectLeadResponse>() {
                        @Override
                        public void onResponse(Call<AgentUpdateSubAgentRejectLeadResponse> call, Response<AgentUpdateSubAgentRejectLeadResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean2 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(LeadDetailsTech.this, "Lead Deactivated successfully...", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(LeadDetailsTech.this, DashBoardAgent.class);
                                intent.putExtra("MobileNumber",mobileNumber);
                                startActivity(intent);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(LeadDetailsTech.this, "Error while updating lead...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<AgentUpdateSubAgentRejectLeadResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(LeadDetailsTech.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });




                }
            });



        }


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });

        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //checking.setText(Checking);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        //remarks1.setText(Comments);


        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        // remarks1.setText(Comments);


        if (Checking != null) {

            if (Checking.equals("0")) {

                status.setVisibility(View.VISIBLE);
                status.setText("Rejected");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.rejected);
                date1.setText(CheckingDate);

            } else if (Checking.equals("1")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Approved");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.approved);
                date1.setText(CheckingDate);
            } else if (Checking.equals("2")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Pending");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.question);
                date1.setText(CheckingDate);
            }


            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                }else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }




            }

            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                }else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }
            }

            if (TechStatus != null) {

                if (TechStatus.equals("2")) {

                    statusl.setVisibility(View.VISIBLE);
                    statusl.setText("Open");
                    approvedl.setVisibility(View.VISIBLE);
                    approvedl.setImageResource(R.drawable.open);
                    datel.setText(MarketingDate);

                } else if (TechStatus.equals("1")) {
                    statusl.setVisibility(View.VISIBLE);
                    statusl.setText("Approved");
                    approvedl.setVisibility(View.VISIBLE);
                    approvedl.setImageResource(R.drawable.approved);
                    datel.setText(MarketingDate);
                    edit.setVisibility(View.GONE);
                } else if (TechStatus.equals("0")) {
                    statusl.setVisibility(View.VISIBLE);
                    statusl.setText("Reject");
                    approvedl.setVisibility(View.VISIBLE);
                    approvedl.setImageResource(R.drawable.rejected);
                    datel.setText(MarketingDate);
                    edit.setVisibility(View.GONE);
                }
            }

        }
    }

}
