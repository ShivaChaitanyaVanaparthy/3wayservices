package com.igrand.ThreeWaySolutions.Activities.TECHNICAL1;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.APIError;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.SiteEngineerProjectDetails;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerBoqList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.BoqListResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class BOQ extends BaseActivity {

    Button add;
    RecyclerView recyclerView;
    //TextView userType;
    RecyclerBoqList recyclerUser;
    ApiInterface apiInterface;
    String ID,Project,key1,key;
    BoqListResponse.StatusBean statusBean;
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b_o_q);
        add=findViewById(R.id.add);
        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);

        //userType=v.findViewById(R.id.userType);





        if(getIntent()!=null){

            ID = getIntent().getStringExtra("ID");
            Project = getIntent().getStringExtra("Project");
            key=getIntent().getStringExtra("key");
            key1=getIntent().getStringExtra("key1");
            //SUM = args.getString("SUM");
        }




        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BOQ.this, DashBoardTechnical1.class);
                intent.putExtra("ID",ID);
                intent.putExtra("key",key);
                intent.putExtra("key1",key1);
                intent.putExtra("keysite","keysite");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            }
        });



        getBoq();







        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BOQ.this, AddWorkBOQ.class);
                intent.putExtra("ID",ID);
                intent.putExtra("Project",Project);
                intent.putExtra("key","key");
                intent.putExtra("key1","key1");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

    }

    private void getBoq() {


        final ProgressDialog progressDialog = new ProgressDialog(BOQ.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BoqListResponse> call = apiInterface.adminBoqList(ID);
        call.enqueue(new Callback<BoqListResponse>() {
            @Override
            public void onResponse(Call<BoqListResponse> call, Response<BoqListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<BoqListResponse.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(BOQ.this));
                    recyclerUser = new RecyclerBoqList(BOQ.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);





                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(BOQ.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }


            @Override
            public void onFailure(Call<BoqListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(BOQ.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBoq();
    }
}