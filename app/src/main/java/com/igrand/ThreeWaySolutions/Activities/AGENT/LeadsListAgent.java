package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.LeadsListTech;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgent;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsListResponse;
import com.igrand.ThreeWaySolutions.Response.TechLeadsListResponse;

import java.util.List;

public class LeadsListAgent extends BaseActivity {

    RecyclerView recyclerView;
    RecyclerAdapterAgent recyclerAdapter;
    AgentLeadsListResponse.StatusBean statusBean;
    ApiInterface apiInterface;
    ImageView back;
    String mobileNumber,AgentId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_list_agent);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            AgentId=getIntent().getStringExtra("AgentId");


        }




        final ProgressDialog progressDialog = new ProgressDialog(LeadsListAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AgentLeadsListResponse> call = apiInterface.agentLeadsList(mobileNumber);
        call.enqueue(new Callback<AgentLeadsListResponse>() {
            @Override
            public void onResponse(Call<AgentLeadsListResponse> call, Response<AgentLeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<AgentLeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(LeadsListAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(LeadsListAgent.this));
                    recyclerAdapter = new RecyclerAdapterAgent(LeadsListAgent.this,dataBeans, mobileNumber);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(LeadsListAgent.this, "No Leads...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AgentLeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LeadsListAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });






        /*final ProgressDialog progressDialog = new ProgressDialog(LeadsListAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LeadsListResponse> call = apiInterface.adminLeadsList();
        call.enqueue(new Callback<LeadsListResponse>() {
            @Override
            public void onResponse(Call<LeadsListResponse> call, Response<LeadsListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<LeadsListResponse.DataBean> dataBeans=response.body().getData();
                    Toast.makeText(LeadsListAgent.this, "Leads List", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(LeadsListAgent.this));
                    recyclerAdapter = new RecyclerAdapterAgent(LeadsListAgent.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(LeadsListAgent.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }




            @Override
            public void onFailure(Call<LeadsListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(LeadsListAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });*/




    }
}
