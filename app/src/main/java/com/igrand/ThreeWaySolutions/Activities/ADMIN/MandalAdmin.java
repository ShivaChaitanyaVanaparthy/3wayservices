package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterDstActiveList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterStateActiveList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminActiveDistList;
import com.igrand.ThreeWaySolutions.Response.AdminActiveStateList;
import com.igrand.ThreeWaySolutions.Response.AdminAddDistrict;
import com.igrand.ThreeWaySolutions.Response.AdminAddMandal;

import java.util.List;

public class MandalAdmin extends BaseActivity {

    TextView state,district;
    EditText mandal;
    ImageView back;
    Button add;
    String Mandal,StateName,DistrictName;
    ApiInterface apiInterface;
    RecyclerAdapterStateActiveList recyclerAdapter;
    RecyclerAdapterDstActiveList recyclerAdapter1;
    AdminActiveStateList.StatusBean statusBean;
    AdminAddDistrict.StatusBean statusBean1;
    AdminActiveDistList.StatusBean statusBean2;
    AdminAddMandal.StatusBean statusBean3;
    String StateId,DistId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mandal_admin);

        add=findViewById(R.id.add);
        back=findViewById(R.id.back);
        district=findViewById(R.id.district);
        state=findViewById(R.id.state);
        mandal=findViewById(R.id.mandal);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMandal();
            }
        });

        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(MandalAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);



                final ProgressDialog progressDialog = new ProgressDialog(MandalAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminActiveStateList> call = apiInterface.adminstateActiveList();
                call.enqueue(new Callback<AdminActiveStateList>() {
                    @Override
                    public void onResponse(Call<AdminActiveStateList> call, Response<AdminActiveStateList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminActiveStateList.DataBean> dataBeans=response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(MandalAdmin.this));
                            recyclerAdapter = new RecyclerAdapterStateActiveList(MandalAdmin.this,dataBeans,state,dialog,MandalAdmin.this);
                            recyclerView.setAdapter(recyclerAdapter);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(MandalAdmin.this, "No State's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminActiveStateList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(MandalAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });




            }
        });


    }



    public void getId(final String selectedworkid) {


        StateId=selectedworkid;


        district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final Dialog dialog = new Dialog(MandalAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_work);

                dialog.show();

                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);



                final ProgressDialog progressDialog = new ProgressDialog(MandalAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AdminActiveDistList> call = apiInterface.admindstActiveList(selectedworkid);
                call.enqueue(new Callback<AdminActiveDistList>() {
                    @Override
                    public void onResponse(Call<AdminActiveDistList> call, Response<AdminActiveDistList> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean2 = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(AddSubWorkTypeAdmin.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<AdminActiveDistList.DataBean> dataBeans=response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(MandalAdmin.this));
                            recyclerAdapter1 = new RecyclerAdapterDstActiveList(MandalAdmin.this,dataBeans,dialog,district,MandalAdmin.this);
                            recyclerView.setAdapter(recyclerAdapter1);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(MandalAdmin.this, "No Dist's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AdminActiveDistList> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(MandalAdmin.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });



            }
        });

    }

    public void getId1(String selectedworkid) {

        DistId=selectedworkid;

    }


    private void addMandal() {

        Mandal = mandal.getText().toString();
        StateName = state.getText().toString();
        DistrictName = district.getText().toString();


        if(Mandal.equals("")){

            Toast.makeText(MandalAdmin.this, "Please enter Mandal", Toast.LENGTH_SHORT).show();

        } else if(StateName.equals("")){
            Toast.makeText(MandalAdmin.this, "Please select State", Toast.LENGTH_SHORT).show();

        }else if(DistrictName.equals("")){
            Toast.makeText(MandalAdmin.this, "Please select District", Toast.LENGTH_SHORT).show();
        } else {

            final ProgressDialog progressDialog = new ProgressDialog(MandalAdmin.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AdminAddMandal> call = apiInterface.adminAddMandal(Mandal, StateId, DistId);
            call.enqueue(new Callback<AdminAddMandal>() {
                @Override
                public void onResponse(Call<AdminAddMandal> call, Response<AdminAddMandal> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean3 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(MandalAdmin.this, "Mandal Added Successfully......", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MandalAdmin.this, MandalList.class);
                        //intent.putExtra("MobileNumber",mobileNumber);
                        startActivity(intent);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(MandalAdmin.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<AdminAddMandal> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(MandalAdmin.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });
        }


    }

}

