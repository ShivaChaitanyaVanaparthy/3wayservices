package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.UsersListAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.AddLeadsAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddNearbyProjectResponse;
import com.igrand.ThreeWaySolutions.Response.AddUserResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NearbyProjects extends BaseActivity  {

    EditText projectname, acres, plotsize, fastmoving, cost, googlelocation, uploadimage, uploadimage1;
    ApiInterface apiInterface;
    RadioGroup rg;
    RadioButton active, inactive;
    ImageView back, imageupload, imageupload1;
    String ProjectName, Acres, PlotSize, FastMoving, Cost, Googlelocation, Uploadimage, Uploadimage1,picturePath, ID,Active,imagePic,imagePic1,picturePath1;
    Button submit;

    private Boolean exit = false;
    ImageView telephonebook;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    File image1 = null;
    private Bitmap bitmap,bitmap1;
    Bitmap converetdImage,converetdImage1;
    String MobileNumber;
    private String TAG = "mobile";
    Dialog dialog;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_projects);


       /* projectname = findViewById(R.id.projectname);
        acres = findViewById(R.id.acres);
        plotsize = findViewById(R.id.plotsize);
        fastmoving = findViewById(R.id.fastmoving);
        cost = findViewById(R.id.cost);
        googlelocation = findViewById(R.id.googlelocation);
        uploadimage = findViewById(R.id.uploadimage);
        uploadimage1 = findViewById(R.id.uploadimage1);
        rg = findViewById(R.id.rg);
        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        back = findViewById(R.id.back);
        imageupload = findViewById(R.id.imageupload);
        imageupload1 = findViewById(R.id.imageupload1);
        submit = findViewById(R.id.submit);


        if (getIntent() != null) {
            MobileNumber = getIntent().getStringExtra("MobileNumber");
            ID = getIntent().getStringExtra("ID");
        }

        rg.check(R.id.active);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(NearbyProjects.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(NearbyProjects.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(NearbyProjects.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(NearbyProjects.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(NearbyProjects.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }

                LinearLayout camera, folder;

                dialog = new Dialog(NearbyProjects.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();
                    }
                });
            }

        });


        uploadimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(NearbyProjects.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects.this,
                            Manifest.permission.CAMERA)) {
                    } else {
                        ActivityCompat.requestPermissions(NearbyProjects.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(NearbyProjects.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(NearbyProjects.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(NearbyProjects.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    } else {
                        ActivityCompat.requestPermissions(NearbyProjects.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }

                LinearLayout camera, folder;

                dialog = new Dialog(NearbyProjects.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);


                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 102);
                        dialog.dismiss();

                    }
                });

                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 103);
                        dialog.dismiss();
                    }
                });
            }

        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (active.isChecked()) {

                    Active = "1";


                } else if (inactive.isChecked()) {

                    Active = "0";

                } else {

                    Toast.makeText(NearbyProjects.this, "Please select Active/InActive...", Toast.LENGTH_SHORT).show();
                }

                ProjectName = projectname.getText().toString();
                Acres = acres.getText().toString();
                PlotSize = plotsize.getText().toString();
                FastMoving = fastmoving.getText().toString();
                Cost = cost.getText().toString();
                Googlelocation = googlelocation.getText().toString();

                addProject(MobileNumber,ID,ProjectName, Acres, PlotSize, FastMoving, Cost,Googlelocation,Active);

            }
        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else {

        }

        if (imagePic1 != null && !imagePic1.isEmpty() && !imagePic1.equals("null")) {

            Picasso.get().load(imagePic1).into(imageupload1);

            bitmap1 = ((BitmapDrawable) imageupload1.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap1);
            converetdImage1 = getResizedBitmap1(bitmap1, 500);

        } else {

        }
*/
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private Bitmap getResizedBitmap1(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {

            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);

            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

            picturePath = cursor.getString(columnIndex);

            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }


                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        converetdImage = getResizedBitmap(bitmap, 500);
                        imageupload.setImageBitmap(converetdImage);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            imageupload.setImageBitmap(converetdImage);

            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");

            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        else if(requestCode==102) {

            Uri selectedImage1 = data.getData();
            String[] filePathColumn1 = {MediaStore.Images.Media.DATA};
            Cursor cursor1 = getContentResolver().query(selectedImage1,
                    filePathColumn1, null, null, null);
            cursor1.moveToFirst();
            int columnIndex1 = cursor1.getColumnIndex(filePathColumn1[0]);
            picturePath1 = cursor1.getString(columnIndex1);
            cursor1.close();
            if (picturePath1 != null && !picturePath1.equals("")) {
                image1 = new File(picturePath1);
            }

            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage1);
                converetdImage1 = getResizedBitmap(bitmap1, 500);
                imageupload1.setImageBitmap(converetdImage1);


            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if(requestCode==103 && resultCode == Activity.RESULT_OK){



            Bitmap converetdImage1 = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();

            converetdImage1.compress(Bitmap.CompressFormat.JPEG, 90, bytes1);

            imageupload1.setImageBitmap(converetdImage1);

            image1 = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image1);
                fo.write(bytes1.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
            }


            @Override
            public void onBackPressed ()
            {
                super.onBackPressed();
            }

            private void addProject(String mobileNumber, String ID, String projectName, String acres, String plotSize, String fastMoving, String cost, String googlelocation, String active){




                final ProgressDialog progressDialog=new ProgressDialog(NearbyProjects.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();

                MultipartBody.Part body = null;
                MultipartBody.Part body1 = null;
                if (image != null) {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                    body = MultipartBody.Part.createFormData("brochure[]", image.getName(), requestFile);

                }if (image1 != null) {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image1);
                    body1 = MultipartBody.Part.createFormData("image[]", image1.getName(), requestFile);

                }

                RequestBody ProjectName1 = RequestBody.create(MediaType.parse("multipart/form-data"), projectName);
                RequestBody Acres1 = RequestBody.create(MediaType.parse("multipart/form-data"), acres);
                RequestBody PlotSize1 = RequestBody.create(MediaType.parse("multipart/form-data"), plotSize);
                RequestBody FastMoving1 = RequestBody.create(MediaType.parse("multipart/form-data"), fastMoving);
                RequestBody Cost1 = RequestBody.create(MediaType.parse("multipart/form-data"), cost);
                RequestBody GoogleLocation1 = RequestBody.create(MediaType.parse("multipart/form-data"), googlelocation);
                RequestBody Active1 = RequestBody.create(MediaType.parse("multipart/form-data"), active);
                RequestBody MobileNumber1 = RequestBody.create(MediaType.parse("multipart/form-data"), MobileNumber);
                RequestBody LeadID1 = RequestBody.create(MediaType.parse("multipart/form-data"), this.ID);



               /* apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddNearbyProjectResponse> call = apiInterface.adminNearbyProject(MobileNumber1,LeadID1,ProjectName1,Acres1,PlotSize1,FastMoving1,Cost1,GoogleLocation1,body1,Active1);
                call.enqueue(new Callback<AddNearbyProjectResponse>() {
                    @Override
                    public void onResponse(Call<AddNearbyProjectResponse> call, Response<AddNearbyProjectResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            AddNearbyProjectResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(NearbyProjects.this, "Nearby Project's Added Successfully...", Toast.LENGTH_SHORT).show();
                            *//*Intent intent=new Intent(NearbyProjects.this, UsersListAdmin.class);
                            intent.putExtra("MobileNumber",MobileNumber);
                            startActivity(intent);*//*

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(NearbyProjects.this, "Error...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<AddNearbyProjectResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(NearbyProjects.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });
*/
            }


}