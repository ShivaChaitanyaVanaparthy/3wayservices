package com.igrand.ThreeWaySolutions.Activities.AGENT;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.GalleryUriToPath;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.NearbyProjects1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter1;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter3;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter4;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter5;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapter6;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterImages;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.UpdateUserResponse;
import com.squareup.picasso.Picasso;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.BaseActivity.IS_NEED_FOLDER_LIST;
import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;


public class AddLeadsAgent extends BaseActivity implements LocationListener {

    private static final int BUFFER_SIZE =1024 * 2;
    private static final String IMAGE_DIRECTORY = "/demonuts_upload_gallery";

    Button submit;
    ImageView back, imageupload;
    RadioGroup rg;
    RadioButton active, inactive;
    EditText village_name, property_name, acres, comments,survey,address,mandal,district;
    TextView google_location;
    LinearLayout document,imagee;
    String Village, Property, Acres, Google, Comments, Document, Active, MobileNumber,Survey,Address,Mandal,District;
    ApiInterface apiInterface;
    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    Double latitude, longitude;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    String imagePic, picturePath,Name;
    RecyclerAdapter1 recyclerAdapter1;
    RecyclerAdapterImages recyclerAdapterImages;
    RecyclerView recyclerView,recyclervieww;
    RecyclerAdapter3 recyclerAdapter3;
    ArrayList<Bitmap> bitmapArray  = new ArrayList<>();
    ArrayList<Bitmap> bmp_images1 = new ArrayList<Bitmap>();
    RecyclerAdapter4 recyclerAdapter4;
    RecyclerAdapter5 recyclerAdapter5;
    String docFilePath;
    String imageFilePath;

    Dialog dialog;
    public static List<String> selectedvideoImgList = new ArrayList<>();
    String multi_image_path = "empty";
    List<MultipartBody.Part> images_array = new ArrayList<>();
    List<MultipartBody.Part> images_array1= new ArrayList<>();
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    int CAMERA_CAPTURE = 1;
    int OPEN_MEDIA_PICKER = 2;
    int PICK_IMAGE = 3;
    int PICK_FILE_REQUEST = 7;
    String pickedDocPath = null;
    ArrayList<File> document_file = new ArrayList<>();
    RelativeLayout relative_image,relative_image1;
    TextView txt_count,txt_count1;
    ArrayList<String> myList;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    public static final int MULTIIMAGE = 1100;

    String imageEncoded,pdf;
    List<String> imagesEncodedList;
    List<String> stringList=new ArrayList<>();

    ArrayList<String> mResults = new ArrayList<>();

    private static final int REQUEST_CODE1 = 732;

    ArrayList<String> pdflist=new ArrayList<>();
    int number;
    RecyclerAdapter6 recyclerAdapter6;
    String emptydocument="";


    ArrayList<NormalFile> list;
    ArrayList<String> PdfList=new ArrayList<>();
    ArrayList<ImageFile> list1;
    ArrayList<String> ImageList=new ArrayList<>();








    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_leads);



        submit = findViewById(R.id.submit);
        back = findViewById(R.id.back);
        rg = findViewById(R.id.rg);
        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        imageupload = findViewById(R.id.imageupload);
        village_name = findViewById(R.id.village_name);
        property_name = findViewById(R.id.property_name);
        acres = findViewById(R.id.acres);
        google_location = findViewById(R.id.google_location);
        comments = findViewById(R.id.comments);
        document = findViewById(R.id.document);
        survey = findViewById(R.id.survey);
        address = findViewById(R.id.address);

        relative_image=findViewById(R.id.relative_image);
        txt_count=findViewById(R.id.txt_count);
        mandal=findViewById(R.id.mandal);
        district=findViewById(R.id.district);
        imagee=findViewById(R.id.imagee);
        recyclerView=findViewById(R.id.recyclerView);
        recyclervieww=findViewById(R.id.recyclerVieww);

        rg.check(R.id.active);


        if (getIntent() != null) {
            MobileNumber = getIntent().getStringExtra("MobileNumber");
            Name = getIntent().getStringExtra("Name");
            pdf = getIntent().getStringExtra("pdf");
            pdflist = (ArrayList<String>)getIntent().getSerializableExtra("pdflist");

            Village=getIntent().getStringExtra("Village");
            Property=getIntent().getStringExtra("Property");
            Acres=getIntent().getStringExtra("Acres");
            Google=getIntent().getStringExtra("Google");
            Comments=getIntent().getStringExtra("Comments");
            Survey=getIntent().getStringExtra("Survey");
            Mandal=getIntent().getStringExtra("Mandal");
            Address=getIntent().getStringExtra("Address");
            District=getIntent().getStringExtra("District");

            village_name.setText(Village);
            property_name.setText(Property);
            acres.setText(Acres);
            google_location.setText(Google);
            comments.setText(Comments);
            survey.setText(Survey);
            mandal.setText(Mandal);
            address.setText(Address);
            district.setText(District);
        }





        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* Intent intent=new Intent(AddLeadsAgent.this,DashBoardAgent.class);
                startActivity(intent);*/
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Village = village_name.getText().toString();
                Property = property_name.getText().toString();
                Acres = acres.getText().toString();
                Google = google_location.getText().toString();
                Comments = comments.getText().toString();
                Survey = survey.getText().toString();
                Address = address.getText().toString();
                Mandal = mandal.getText().toString();
                District = district.getText().toString();


                if (active.isChecked()) {

                    Active = "1";


                } else if (inactive.isChecked()) {

                    Active = "0";

                }

                addLeads(Village, Property, Acres, Google, Comments, Active,Survey,MobileNumber,Address,Mandal,District);


            }
        });


        if(pdflist!=null){

            number=pdflist.size();
            recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter6 = new RecyclerAdapter6(AddLeadsAgent.this,number,pdflist);
            recyclerView.setAdapter(recyclerAdapter6);
            recyclerAdapter6.notifyDataSetChanged();
        }





        document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(AddLeadsAgent.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(AddLeadsAgent.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddLeadsAgent.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(AddLeadsAgent.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(AddLeadsAgent.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }

              /*  LinearLayout camera, folder,folder1;

                dialog = new Dialog(AddLeadsAgent.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);


                camera.setVisibility(View.GONE);
                folder.setVisibility(View.VISIBLE);
                folder1.setVisibility(View.GONE);*/


               /* folder1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {*/


                        Village = village_name.getText().toString();
                        Property = property_name.getText().toString();
                        Acres = acres.getText().toString();
                        Google = google_location.getText().toString();
                        Comments = comments.getText().toString();
                        Survey = survey.getText().toString();
                        Address = address.getText().toString();
                        Mandal = mandal.getText().toString();
                        District = district.getText().toString();


                       /* Intent intent3 = new Intent(AddLeadsAgent.this, PdfActivity.class);
                        intent3.putExtra("MobileNumber",MobileNumber);
                        intent3.putExtra("Village",Village);
                        intent3.putExtra("Property",Property);
                        intent3.putExtra("Acres",Acres);
                        intent3.putExtra("Google",Google);
                        intent3.putExtra("Comments",Comments);
                        intent3.putExtra("Survey",Survey);
                        intent3.putExtra("Mandal",Mandal);
                        intent3.putExtra("Address",Address);
                        intent3.putExtra("District",District);

                        startActivity(intent3);*/

                Intent intent4 = new Intent(AddLeadsAgent.this, NormalFilePickActivity.class);
                intent4.putExtra(Constant.MAX_NUMBER, 9);
                intent4.putExtra(IS_NEED_FOLDER_LIST, true);
                intent4.putExtra(NormalFilePickActivity.SUFFIX,
                        new String[] {"xlsx", "xls", "doc", "dOcX", "ppt", ".pptx", "pdf"});
                startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);




                  /*  }
                });*/

            }

        });

        imagee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(AddLeadsAgent.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(AddLeadsAgent.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddLeadsAgent.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(AddLeadsAgent.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user1 *asynchronously* -- don't block
                        // this thread waiting for the user1's response! After the user1
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(AddLeadsAgent.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }

                LinearLayout camera, folder,folder1;

                dialog = new Dialog(AddLeadsAgent.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder1 = dialog.findViewById(R.id.folder1);

               // folder1.setVisibility(View.GONE);

                camera.setVisibility(View.GONE);
                folder.setVisibility(View.VISIBLE);
                folder1.setVisibility(View.GONE);



                //camera.setVisibility(View.VISIBLE);
             /*   folder1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                       *//* Intent intent = new Intent();
                        intent.setType("application/pdf");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select PDF"), 111);
                        dialog.dismiss();*//*

                        Village = village_name.getText().toString();
                        Property = property_name.getText().toString();
                        Acres = acres.getText().toString();
                        Google = google_location.getText().toString();
                        Comments = comments.getText().toString();
                        Survey = survey.getText().toString();
                        Address = address.getText().toString();
                        Mandal = mandal.getText().toString();
                        District = district.getText().toString();


                        Intent intent3 = new Intent(AddLeadsAgent.this, PdfActivity.class);
                        intent3.putExtra("MobileNumber",MobileNumber);
                        intent3.putExtra("Village",Village);
                        intent3.putExtra("Property",Property);
                        intent3.putExtra("Acres",Acres);
                        intent3.putExtra("Google",Google);
                        intent3.putExtra("Comments",Comments);
                        intent3.putExtra("Survey",Survey);
                        intent3.putExtra("Mandal",Mandal);
                        intent3.putExtra("Address",Address);
                        intent3.putExtra("District",District);

                        startActivity(intent3);



                    }
                });*/


              /*  camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       // checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                       Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();

                    }
                });
*/
              /*  folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
*/

                        /*Intent intent = new Intent(AddLeadsAgent.this, ImagesSelectorActivity.class);
                        // max number of images to be selected
                        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 5);
                        // min size of image which will be shown; to filter tiny images (mainly icons)
                        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
                        // show camera or not
                        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                        // pass current selected images as the initial value
                        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                        // start the selector
                        startActivityForResult(intent, REQUEST_CODE1);
*/

                Intent intent1 = new Intent(AddLeadsAgent.this, ImagePickActivity.class);
                intent1.putExtra(IS_NEED_CAMERA, true);
                intent1.putExtra(Constant.MAX_NUMBER, 9);
                intent1.putExtra(IS_NEED_FOLDER_LIST, true);
                startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_IMAGE);

                        dialog.dismiss();
                  /*  }
                });*/
            }

        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }


        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


      /*  checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                STORAGE_PERMISSION_CODE);*/

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions

                if (permissionsToRequest.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                                ALL_PERMISSIONS_RESULT);
                    }
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }


            // get location
            getLocation();
        }

    }

    /*private void checkPermission(String permission, int requestCode) {

        if (ContextCompat.checkSelfPermission(AddLeadsAgent.this, permission)
                == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(AddLeadsAgent.this,
                    new String[]{permission},
                    requestCode);
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, 101);
            dialog.dismiss();
            Toast.makeText(AddLeadsAgent.this,
                    "Permission already granted",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }*/

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    list1 = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    StringBuilder builder = new StringBuilder();
                    for (ImageFile file : list1) {
                        String path = file.getPath();
                        builder.append(path + "\n");
                        ImageList.add(path);
                    }

                    recyclervieww.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapterImages = new RecyclerAdapterImages(AddLeadsAgent.this, ImageList,list1);
                    recyclervieww.setAdapter(recyclerAdapterImages);
                    recyclerAdapterImages.notifyDataSetChanged();

                }
                break;
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == RESULT_OK) {
                    list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    StringBuilder builder = new StringBuilder();
                    for (NormalFile file : list) {
                        String path = file.getPath();
                        builder.append(path + "\n");
                        PdfList.add(path);
                    }

                    number=PdfList.size();
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                    recyclerAdapter6 = new RecyclerAdapter6(AddLeadsAgent.this,number,PdfList);
                    recyclerView.setAdapter(recyclerAdapter6);
                    recyclerAdapter6.notifyDataSetChanged();
                }
                break;
        }



        if (requestCode == REQUEST_CODE1) {
            if (resultCode == RESULT_OK) {
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);


                       /* selectedvideoImgList.add(mResults.get(index));

                        multi_image_path = mResults.get(index);
                        String PickedImgPath = mResults.get(index);
                        System.out.println("path" + PickedImgPath);*/

                        recyclervieww.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this, LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter1 = new RecyclerAdapter1(AddLeadsAgent.this, selectedvideoImgList,mResults);
                        recyclervieww.setAdapter(recyclerAdapter1);
                        recyclerAdapter1.notifyDataSetChanged();


            }


    }










        if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
            if (selectionResult.size() > 0) {
                for (int index = 0; index < selectionResult.size(); index++) {

                    selectedvideoImgList.add(selectionResult.get(index));

                    multi_image_path = selectionResult.get(index);
                    String PickedImgPath = selectionResult.get(index);
                    System.out.println("path" + PickedImgPath);

                    File file = new File(selectionResult.get(index));
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                    images_array.add(MultipartBody.Part.createFormData("document[]", file.getName(), surveyBody));


                }

                recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter1 = new RecyclerAdapter1(AddLeadsAgent.this,selectedvideoImgList, mResults);
                recyclerView.setAdapter(recyclerAdapter1);
                relative_image.setVisibility(View.VISIBLE);
                recyclerAdapter1.notifyDataSetChanged();
                txt_count.setText(String.valueOf(selectedvideoImgList.size()));
//                txt_count1.setText(String.valueOf(myList.size()));
            }
        } else if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }

               if (resultCode == RESULT_OK) {
            if (requestCode == 111) {

                Uri uri = data.getData();
                String uriString = uri.getPath();
                File myFile = new File(uriString);

                String path = getFilePathFromURI(AddLeadsAgent.this,uri);
               // Log.d("ioooo",path);
                uploadPDF(path);



              /*  Uri uri = data.getData();
                docFilePath = getFileNameByUri(this, uri);

                */




               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                   // docFilePath=getPath(this,uri);
                }*/
            }
        }


        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                myList = (ArrayList<String>) getIntent().getSerializableExtra("List");

                relative_image1.setVisibility(View.VISIBLE);
                txt_count1.setText(String.valueOf(myList.size()));

                if (myList.size() > 0) {
                    for (int index = 0; index < myList.size(); index++) {

                        selectedvideoImgList.add(myList.get(index));

                        multi_image_path = myList.get(index);
                        String PickedImgPath = myList.get(index);
                        System.out.println("path" + PickedImgPath);



                        File file = new File(myList.get(index));
                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        //String link=file.getName();
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array.add(MultipartBody.Part.createFormData("document[]", file.getName(), surveyBody));


                        /*recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                        recyclerAdapter1 = new RecyclerAdapter1(AddLeadsAgent.this,selectedvideoImgList);
                        recyclerView.setAdapter(recyclerAdapter1);
*/

                       /* recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                        recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this,selectedvideoImgList);
                        recyclerView.setAdapter(recyclerAdapter3);*/


                    /*   recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
                        recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this,bitmapArray);
                        recyclerView.setAdapter(recyclerAdapter3);*/


                        //Picasso.get().load(file.getName()).into(imageupload);

                    }


                }
            }

        }


        if (requestCode == PICK_IMAGE) {

            if (resultCode == RESULT_OK) {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                //  PickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
//                    pick_img_part.setImageBitmap(bm);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                c.close();
            }
        } else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
//                pickedDocPath = String.valueOf(filePath);
            pickedDocPath = GalleryUriToPath.getPath(this, filePath);
            document_file.add(new File(pickedDocPath));
            Log.e("path ", pickedDocPath);
            //   Toast.makeText(this, "document has  selected", Toast.LENGTH_SHORT).show();
            String filename = pickedDocPath.substring(pickedDocPath.lastIndexOf("/") + 1);
            //  text_doc_name.setText(filename);
            // text_doc_name.setText("document has  selected");

        }


        if (requestCode == MULTIIMAGE && resultCode == RESULT_OK) {

            if (requestCode == 1100) {

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
             /*   if (data.getData() != null) {

                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();


                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded = cursor.getString(columnIndex);
                    imagesEncodedList.add(imageEncoded);

                    cursor.close();

                    image = new File(String.valueOf(columnIndex));

                    recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsSubAgent1.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(AddLeadsSubAgent1.this, imagesEncodedList);
                    recyclerView.setAdapter(recyclerAdapter1);



                    // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                    images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));





                } else {*/
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);


                        image = new File(imagesEncodedList.get(i));

                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerAdapter1 = new RecyclerAdapter1(AddLeadsAgent.this, imagesEncodedList, mResults);
                    recyclerView.setAdapter(recyclerAdapter1);


                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);


            ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
            bitmapArray.add(converetdImage); // Add a bitmap




            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);

            images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));

            recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this,LinearLayoutManager.HORIZONTAL,false));
            recyclerAdapter4 = new RecyclerAdapter4(bitmapArray,AddLeadsAgent.this);
            recyclerView.setAdapter(recyclerAdapter4);
            recyclerAdapter4.notifyDataSetChanged();





            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }



        }

        else if(requestCode==102) {

            Uri selectedImage1 = data.getData();
            String[] filePathColumn1 = {MediaStore.Images.Media.DATA};
            Cursor cursor1 = getContentResolver().query(selectedImage1,
                    filePathColumn1, null, null, null);
            cursor1.moveToFirst();
            int columnIndex1 = cursor1.getColumnIndex(filePathColumn1[0]);
            picturePath = cursor1.getString(columnIndex1);
            cursor1.close();
            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage1);
                converetdImage = getResizedBitmap(bitmap1, 500);
                imageupload.setImageBitmap(converetdImage);

            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if(requestCode==103 && resultCode == Activity.RESULT_OK){



            Bitmap converetdImage1 = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();

            converetdImage1.compress(Bitmap.CompressFormat.JPEG, 90, bytes1);

            //imageupload.setImageBitmap(converetdImage1);

            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes1.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void uploadPDF(String path) {

        String pdfname = String.valueOf(Calendar.getInstance().getTimeInMillis());

         image = new File(path);

         image = new File(Environment.getExternalStorageDirectory(), path);
        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
        images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


        recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this, LinearLayoutManager.HORIZONTAL, false));
        recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this, images_array, "key1");
        recyclerView.setAdapter(recyclerAdapter3);
        recyclerAdapter3.notifyDataSetChanged();



    }

    private String getFilePathFromURI(AddLeadsAgent context, Uri contentUri) {

        String fileName = getFileName(contentUri);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(wallpaperDirectory + File.separator + fileName);
            // create folder if not exists

            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    private String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int copystream(InputStream input, OutputStream output) throws Exception, IOException {
        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }

    private String getFileNameByUri(AddLeadsAgent addLeadsAgent, Uri uri) {

        String filepath = "";//default fileName
        // Uri filePathUri = uri;

        File file;
        if (uri.getScheme().toString().compareTo("content") == 0) {


            try {
                

                String[] proj = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
                int column_index = cursor.getColumnIndex(proj[0]);
                cursor.moveToFirst();
                image = new File(String.valueOf(column_index));
                // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


                recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this, LinearLayoutManager.HORIZONTAL, false));
                recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this, images_array, "key1");
                recyclerView.setAdapter(recyclerAdapter3);
                recyclerAdapter3.notifyDataSetChanged();

                return cursor.getString(column_index);
            } finally {
               /*if (cursor != null) {
                    cursor.close();
                }*/
          /*  Cursor cursor = addLeadsAgent.getContentResolver().query(uri, new String[] { android.provider.MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.ORIENTATION }, null, null, null);
            cursor.moveToFirst();

            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);



               String mImagePath = cursor.getString(column_index);
                cursor.close();
                filepath = mImagePath;
                image = new File(filepath);
                // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


                recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this, LinearLayoutManager.HORIZONTAL, false));
                recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this, images_array, "key1");
                recyclerView.setAdapter(recyclerAdapter3);
                recyclerAdapter3.notifyDataSetChanged();
*/
            }
        }
        else
        if (uri.getScheme().compareTo("file") == 0)
        {
            try
            {
                file = new File(new URI(uri.toString()));
                if (file.exists())
                    filepath = file.getAbsolutePath();

                image = new File(filepath);
                image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


            }
            catch (URISyntaxException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {
            filepath = uri.getPath();
        }
        return filepath;
    }




   /* @SuppressLint("NewApi")
    private String getFileNameByUri1(AddLeadsAgent context, Uri uri){


        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

              final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/my_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    *//**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     *//*
    public String getDataColumn(Context context, Uri uri, String selection,
                                String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
       // String[] filePathColumn = {MediaStore.Images.Media.DATA};

        try {

                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);

            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndex(column);


                image = new File(String.valueOf(column_index));
                // image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));


                recyclerView.setLayoutManager(new LinearLayoutManager(AddLeadsAgent.this, LinearLayoutManager.HORIZONTAL, false));
                recyclerAdapter3 = new RecyclerAdapter3(AddLeadsAgent.this, images_array, "key1");
                recyclerView.setAdapter(recyclerAdapter3);
                recyclerAdapter3.notifyDataSetChanged();

                return cursor.getString(column_index);

            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    *//**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     *//*
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    *//**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     *//*
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    *//**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     *//*
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
*/

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context, Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    private void showDialog(String external_storage, final Context context, final String readExternalStorage) {

        androidx.appcompat.app.AlertDialog.Builder alertBuilder = new androidx.appcompat.app.AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(external_storage + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{readExternalStorage},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        androidx.appcompat.app.AlertDialog alert = alertBuilder.create();
        alert.show();
    }


    @Override
    public void onBackPressed ()
    {
        super.onBackPressed();
    }






    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                }  if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                    /*loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);*/
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (AddLeadsAgent.this.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }


    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddLeadsAgent.this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(AddLeadsAgent.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(final Location loc) {


        Log.d(TAG, "updateUI");

        latitude = (loc.getLatitude());
        longitude = (loc.getLongitude());

        // txtAddress.setText(Double.toString(latitude));
        Geocoder geocoder = new Geocoder(AddLeadsAgent.this, Locale.getDefault());

        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            google_location.setText(address + city_name + state + country + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }

    }



    private void addLeads(String village, String property, String acres, String google, String comments, String active, String survey, String mobileNumber, String address,String mandall,String districtt) {




        if(village.equals("")){

            Toast.makeText(AddLeadsAgent.this, "Please enter Village Name", Toast.LENGTH_SHORT).show();
        }else if(property.equals("")){

            Toast.makeText(AddLeadsAgent.this, "Please enter Property Name", Toast.LENGTH_SHORT).show();
        }else if(acres.equals("")){

            Toast.makeText(AddLeadsAgent.this, "Please enter acres", Toast.LENGTH_SHORT).show();
        } else if(survey.equals("")){

            Toast.makeText(AddLeadsAgent.this, "Please enter survey number", Toast.LENGTH_SHORT).show();
        }else if(google.equals("")){

            Toast.makeText(this, "Please select google location", Toast.LENGTH_SHORT).show();
        }else if(mandall.equals("")){

            Toast.makeText(this, "Please enter Mandal", Toast.LENGTH_SHORT).show();
        }else if(districtt.equals("")){

            Toast.makeText(this, "Please enter district", Toast.LENGTH_SHORT).show();
        }

        else {


            final ProgressDialog progressDialog = new ProgressDialog(AddLeadsAgent.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();

            MultipartBody.Part body = null;
            if (image != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                body = MultipartBody.Part.createFormData("document[]", image.getName(), requestFile);

            }

            if(PdfList!=null){


                for (int index = 0; index < PdfList.size(); index++) {


                    File file = new File(PdfList.get(index));
                    if (file != null) {

                        RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), file);
                        body = MultipartBody.Part.createFormData("document[]", file.getName(), requestBody);
                        //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                        images_array.add(MultipartBody.Part.createFormData("document[]", file.getName(), requestBody));

                    }else {

                        RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
                        body = MultipartBody.Part.createFormData("document[]", emptydocument, requestBody);
                        //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                        images_array.add(MultipartBody.Part.createFormData("document[]", emptydocument, requestBody));
                    }



                }

            } else {
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/pdf"), emptydocument);
                body = MultipartBody.Part.createFormData("document[]", emptydocument, requestBody);
                //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                images_array.add(MultipartBody.Part.createFormData("document[]", emptydocument, requestBody));
            }

            if (ImageList.size() > 0) {
                for (int index = 0; index < ImageList.size(); index++) {

                    File file = new File(ImageList.get(index));

                    if (file != null) {

                        RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                        images_array1.add(MultipartBody.Part.createFormData("images[]", file.getName(), surveyBody));

                    }
                    /*else {
                        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
                        body = MultipartBody.Part.createFormData("images[]", emptydocument, requestBody);
                        //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                        images_array.add(MultipartBody.Part.createFormData("images[]", emptydocument, requestBody));
                    }*/
                }

            } else {

                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
                body = MultipartBody.Part.createFormData("images[]", emptydocument, requestBody);
                //images_array.add(MultipartBody.Part.createFormData("document[]", image.getName(), surveyBody));
                images_array.add(MultipartBody.Part.createFormData("images[]", emptydocument, requestBody));
            }





            String Geolocatin=String.valueOf(latitude);
            String Geolocatin1=String.valueOf(longitude);

            RequestBody Village = RequestBody.create(MediaType.parse("multipart/form-data"), village);
            RequestBody Property = RequestBody.create(MediaType.parse("multipart/form-data"), property);
            RequestBody Acres = RequestBody.create(MediaType.parse("multipart/form-data"), acres);
            RequestBody GoogleLoc = RequestBody.create(MediaType.parse("multipart/form-data"), google);
            RequestBody Comment = RequestBody.create(MediaType.parse("multipart/form-data"), comments);
            RequestBody Activee = RequestBody.create(MediaType.parse("multipart/form-data"), active);
            RequestBody SurveyNo = RequestBody.create(MediaType.parse("multipart/form-data"), survey);
            RequestBody Mobile = RequestBody.create(MediaType.parse("multipart/form-data"), mobileNumber);
            RequestBody Addrss = RequestBody.create(MediaType.parse("multipart/form-data"), address);
            RequestBody GeoLoc = RequestBody.create(MediaType.parse("multipart/form-data"), Geolocatin);
            RequestBody GeoLoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), Geolocatin1);
            RequestBody Mandal1 = RequestBody.create(MediaType.parse("multipart/form-data"), mandall);
            RequestBody District1 = RequestBody.create(MediaType.parse("multipart/form-data"), districtt);



            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<UpdateUserResponse> call = apiInterface.agentAddLead(Village, Property, Acres, GeoLoc,GeoLoc1, Comment, Activee,SurveyNo,Mobile, images_array,images_array1,Mandal1,District1);
            call.enqueue(new Callback<UpdateUserResponse>() {
                @Override
                public void onResponse(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        UpdateUserResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                        Toast.makeText(AddLeadsAgent.this, "Lead Added Successfully...", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AddLeadsAgent.this, DashBoardAgent.class);
                        intent.putExtra("MobileNumber",mobileNumber);
                        intent.putExtra("Name",Name);
                        startActivity(intent);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(AddLeadsAgent.this, "Error...", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<UpdateUserResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(AddLeadsAgent.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();

                }
            });

        }

    }

    /*public void getimage(ArrayList<Bitmap> bitmapArray1) {



    }*/
}


