package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterGameList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminGameList;

import java.util.List;

public class GamesList extends BaseActivity {

    RecyclerView recyclerView;
    ImageView back;
    RecyclerAdapterGameList recyclerUser;
    ApiInterface apiInterface;
    AdminGameList.StatusBean statusBean;
    Button add;
    String mobileNumber;
    ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games_list);
        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(GamesList.this,DashBoardAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(GamesList.this, GameAdmin.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
            }
        });




    /*    final ProgressDialog progressDialog = new ProgressDialog(StateList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();*/
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AdminGameList> call = apiInterface.adminGameList();
        call.enqueue(new Callback<AdminGameList>() {
            @Override
            public void onResponse(Call<AdminGameList> call, Response<AdminGameList> response) {

                if (response.code() == 200) {
                    //progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    // Toast.makeText(CityList.this, "City's List......", Toast.LENGTH_SHORT).show();
                    List<AdminGameList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(GamesList.this));
                    recyclerUser = new RecyclerAdapterGameList(GamesList.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    // progressDialog.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(GamesList.this, "No Game's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AdminGameList> call, Throwable t) {
                //progressDialog.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(GamesList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}


