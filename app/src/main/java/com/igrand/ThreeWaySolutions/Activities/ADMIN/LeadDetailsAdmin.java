package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.LeadDetailsAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.CheckingDocList;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.LeadDetailsLegal;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.ProposalDocuments;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.Geolocations;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ProfileImage;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.MarketingDocList;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.LeadDetailsProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.ProcDocList;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.TechDocList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdminComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdminDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsAdmin extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1, approvedp,approveds,approvedl,approvedle,approvedt;
    String Id, Property, Date, Village, Checking, Status, Comments, GoogleLocation, Address, IFRAME, MarketingStatus, MarketingDate, CheckingDate, ProcurementStatus, ProcurementDate;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11, statusp, datep,statusl,statuss,datel,dates,statusle,datele,acres,survey,statust,datet;
    Dialog dialog;
    LinearLayout remarks1;
    ApiInterface apiInterface;
    AgentLeadsCommentsResponse.StatusBean statusBean;
    RecyclerAdapterAdminComment recyclerAdapter;
    RecyclerView recyclerView;
    RecyclerAdapterAgentDocuments recyclerAdapter1;
    String LegalStatus,SurveyStatus,LegalDate,SurveyDate,Survey,Acres;

    String Document,LesionDate,LesionStatus,Longitude,Latitude,TechnicalStatus,TechnicalDate,pdf,Image;
    String[] document2,imagedoc;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;

    LinearLayout marketing,procurement,tech;
    List<String> strings,stringsimg;
    String[] document3;
    RecyclerAdapterSubAgentDocuments recyclerAdapter4;
    RecyclerAdapterAgentImages recyclerAdapter3;
    RecyclerView recyclerVieww;
    TextView comments;
    String Propertydesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_admin);


        back = findViewById(R.id.back);
        comments = findViewById(R.id.comments);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        // document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
        date11 = findViewById(R.id.date11);
        remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);
        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        recyclerView = findViewById(R.id.recyclerView);

        statuss = findViewById(R.id.statuss);
        dates = findViewById(R.id.dates);
        approveds = findViewById(R.id.approveds);
        statusl = findViewById(R.id.statusl);
        datel = findViewById(R.id.datel);
        approvedl = findViewById(R.id.approvedl);
        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
        marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);
        acres = findViewById(R.id.acres);
        survey = findViewById(R.id.survey);
        statust = findViewById(R.id.statust);
        datet = findViewById(R.id.datet);
        approvedt = findViewById(R.id.approvedt);
        tech = findViewById(R.id.tech);
        recyclerVieww = findViewById(R.id.recyclerVieww);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Propertydesc = getIntent().getStringExtra("Propertydesc");
            Comments = getIntent().getStringExtra("Comments");
            //GoogleLocation = getIntent().getStringExtra("GoogleLocation");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");
            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");
            Image = getIntent().getStringExtra("Image");

            LegalStatus = getIntent().getStringExtra("LegalStatus");
            SurveyStatus = getIntent().getStringExtra("SurveyStatus");
            LegalDate = getIntent().getStringExtra("LegalDate");
            SurveyDate = getIntent().getStringExtra("SurveyDate");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");
            Longitude = getIntent().getStringExtra("Longitude");
            Latitude = getIntent().getStringExtra("Latitude");
            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            TechnicalDate = getIntent().getStringExtra("TechnicalDate");
            TechnicalStatus = getIntent().getStringExtra("TechnicalStatus");
        }


        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        //date1.setText(Date);

        acres.setText(Acres);
        survey.setText(Survey);
        comments.setText(Propertydesc);




        if(Document.equals("")){

        }else {

            document2 = Document.split(",");

            strings = Arrays.asList(Document.split(","));

            for (int i = 0; i < strings.size(); i++) {


                document3 = strings.get(i).split("\\.");
                for (int j = 0; j < document2.length; j++) {

                    pdf = document2[j].substring(document2[j].length() - 3);
                    if (pdf.equals("pdf")) {

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsAdmin.this, document2, Document, strings, "keyAdmin");
                        recyclerView.setAdapter(recyclerAdapter2);

                    } else {
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerAdapter1 = new RecyclerAdapterAgentDocuments(LeadDetailsAdmin.this, document2, strings, Document);
                        recyclerView.setAdapter(recyclerAdapter1);

                    }

                }

            }

        }
        imagedoc=Image.split(",");

        stringsimg = Arrays.asList(Image.split(","));

        recyclerVieww.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerAdapter3 = new RecyclerAdapterAgentImages(LeadDetailsAdmin.this,imagedoc,stringsimg,Image,"keyadmin");
        recyclerVieww.setAdapter(recyclerAdapter3);








       // Picasso.get().load(Document).error(R.drawable.profilepic).placeholder(R.drawable.loading).into(document);

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });


        marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(LeadDetailsAdmin.this, MarketingDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });

        tech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsAdmin.this, TechDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);
            }
        });

        procurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsAdmin.this, ProcDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });

        remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(LeadDetailsAdmin.this, CheckingDocList.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

              /*  final RecyclerView recyclerView;
                final RelativeLayout linear;
                ImageView back;
                dialog = new Dialog(LeadDetailsAdmin.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialogboxremarks);
                dialog.show();
                //dialog.setCancelable(true);


                recyclerView = dialog.findViewById(R.id.text);
                linear = dialog.findViewById(R.id.linear);

               *//* back = dialog.findViewById(R.id.back);


                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });*//*



                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsAdmin.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AgentLeadsCommentsResponse> call = apiInterface.agentLeadsComments(Id);
                call.enqueue(new Callback<AgentLeadsCommentsResponse>() {
                    @Override
                    public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AgentLeadsCommentsResponse.DataBean> dataBeans = response.body().getData();
                            //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                            recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsAdmin.this));
                            recyclerAdapter = new RecyclerAdapterAdminComment(LeadDetailsAdmin.this, dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsAdmin.this, "No Comments...", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LeadDetailsAdmin.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                    }
                });

*/
            }
        });


        if (Checking != null) {

            if (Checking.equals("0")) {

                status.setVisibility(View.VISIBLE);
                status.setText("Rejected");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.rejected);
                date1.setText(CheckingDate);

            } else if (Checking.equals("1")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Approved");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.approved);
                date1.setText(CheckingDate);
            } else if (Checking.equals("2")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Pending");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.question);
                date1.setText(CheckingDate);
            }


            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }


                if (ProcurementStatus != null) {

                    if (ProcurementStatus.equals("4")) {

                        statusp.setText("Rejected");
                        approvedp.setImageResource(R.drawable.rejected);
                        datep.setText(ProcurementDate);

                    } else if (ProcurementStatus.equals("5")) {
                        statusp.setText("Approved");
                        approvedp.setImageResource(R.drawable.approved);
                        datep.setText(ProcurementDate);
                    } else if (ProcurementStatus.equals("3")) {
                        statusp.setText("Processing");
                        approvedp.setImageResource(R.drawable.processing);
                        datep.setText(ProcurementDate);
                    } else if (ProcurementStatus.equals("2")) {
                        statusp.setText("Open");
                        approvedp.setImageResource(R.drawable.open);
                        datep.setText(ProcurementDate);
                    }

                }

                if (TechnicalStatus != null) {

                    if (TechnicalStatus.equals("2")) {

                        statust.setVisibility(View.VISIBLE);
                        statust.setText("Open");
                        approvedt.setVisibility(View.VISIBLE);
                        approvedt.setImageResource(R.drawable.open);
                        datet.setText(TechnicalDate);

                    } else if (TechnicalStatus.equals("1")) {
                        statust.setVisibility(View.VISIBLE);
                        statust.setText("Approved");
                        approvedt.setVisibility(View.VISIBLE);
                        approvedt.setImageResource(R.drawable.approved);
                        datet.setText(TechnicalDate);
                    } else if (TechnicalStatus.equals("0")) {
                        statust.setVisibility(View.VISIBLE);
                        statust.setText("Reject");
                        approvedt.setVisibility(View.VISIBLE);
                        approvedt.setImageResource(R.drawable.rejected);
                        datet.setText(TechnicalDate);

                    }
                }





            }
        }
    }
}
