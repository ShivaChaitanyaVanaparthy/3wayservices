package com.igrand.ThreeWaySolutions.Activities.AGENT;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.AddUserAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.CityList;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCityList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterUserList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminCityList;
import com.igrand.ThreeWaySolutions.Response.AgentUserList;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class UsersListAgent extends BaseActivity {

String MobileNumber,AgentId;
Button add;
ApiInterface apiInterface;
    AgentUserList.StatusBean statusBean;
    RecyclerView recyclerView;
    RecyclerAdapterUserList recyclerUser;
    ImageView back;
    PrefManagerAgent prefManagerAgent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list_agent);


        add=findViewById(R.id.add);
        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);

        if(getIntent()!=null)
        {
            MobileNumber=getIntent().getStringExtra("MobileNumber");
            //AgentId=getIntent().getStringExtra("AgentId");

        }


        prefManagerAgent=new PrefManagerAgent(UsersListAgent.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        AgentId=profile.get("agentid");


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(UsersListAgent.this, DashBoardAgent.class);
                intent.putExtra("MobileNumber",MobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(UsersListAgent.this,AddUserAgent.class);
                intent.putExtra("MobileNumber",MobileNumber);
                intent.putExtra("AgentId",AgentId);
                startActivity(intent);
            }
        });




        final ProgressDialog progressDialog = new ProgressDialog(UsersListAgent.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AgentUserList> call = apiInterface.agentUserList(AgentId);
        call.enqueue(new Callback<AgentUserList>() {
            @Override
            public void onResponse(Call<AgentUserList> call, Response<AgentUserList> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(UsersListAgent.this, "Users List......", Toast.LENGTH_SHORT).show();
                    List<AgentUserList.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(UsersListAgent.this));
                    recyclerUser = new RecyclerAdapterUserList(UsersListAgent.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(UsersListAgent.this, "No User's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<AgentUserList> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(UsersListAgent.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


    }
}
