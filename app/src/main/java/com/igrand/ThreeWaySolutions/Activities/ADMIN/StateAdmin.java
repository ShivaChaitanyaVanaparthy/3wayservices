package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminAddCity;
import com.igrand.ThreeWaySolutions.Response.AdminAddState;

public class StateAdmin extends BaseActivity {

    ImageView back;
    ApiInterface apiInterface;
    EditText projectname,villagename;
    RadioButton active,inactive;
    RadioGroup rg;
    AdminAddState.StatusBean statusBean;
    String mobileNumber,Project,Village,Status;
    Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_admin);

        back=findViewById(R.id.back);

        projectname=findViewById(R.id.cityname);
        add=findViewById(R.id.add);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Project=projectname.getText().toString();


                if(Project.isEmpty() || Project.equals("")){

                    Toast.makeText(StateAdmin.this, "Please enter State", Toast.LENGTH_SHORT).show();

                } else {

                    final ProgressDialog progressDialog = new ProgressDialog(StateAdmin.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AdminAddState> call = apiInterface.adminAddState(Project);
                    call.enqueue(new Callback<AdminAddState>() {
                        @Override
                        public void onResponse(Call<AdminAddState> call, Response<AdminAddState> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(StateAdmin.this, "State Added Successfully......", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(StateAdmin.this, StateList.class);
                                intent.putExtra("MobileNumber",mobileNumber);
                                startActivity(intent);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(StateAdmin.this, "Error while adding...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<AdminAddState> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(StateAdmin.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });

                }



            }
        });

    }
}
