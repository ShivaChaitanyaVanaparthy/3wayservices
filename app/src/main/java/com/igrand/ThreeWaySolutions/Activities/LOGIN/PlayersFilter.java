package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.StateAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterPlayerList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.PlayersListResponse;

import java.util.List;

public class PlayersFilter extends BaseActivity {

    String StateId,DistrictId,MandalId,VillageId;
    ApiInterface apiInterface;
    RecyclerAdapterPlayerList recyclerAdapter;
    RecyclerView recyclerView;
    PlayersListResponse.StatusBean statusBean;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_filter);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);


        if(getIntent()!=null){

            StateId=getIntent().getStringExtra("StateId");
            DistrictId=getIntent().getStringExtra("DistrictId");
            MandalId=getIntent().getStringExtra("MandalId");
            VillageId=getIntent().getStringExtra("VillageId");
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(StateId!=null){

            final ProgressDialog progressDialog1 = new ProgressDialog(PlayersFilter.this);
            progressDialog1.setMessage("Loading.....");
            progressDialog1.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PlayersListResponse> call1 = apiInterface.adminPlayersFilterState(StateId);
            call1.enqueue(new Callback<PlayersListResponse>() {
                @Override
                public void onResponse(Call<PlayersListResponse> call, Response<PlayersListResponse> response) {

                    if (response.code() == 200) {
                        progressDialog1.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<PlayersListResponse.DataBean> dataBeans = response.body().getData();

                        //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                        recyclerView.setLayoutManager(new LinearLayoutManager(PlayersFilter.this));
                        recyclerAdapter = new RecyclerAdapterPlayerList(PlayersFilter.this, dataBeans);
                        recyclerView.setAdapter(recyclerAdapter);
                    } else if (response.code() != 200) {
                        progressDialog1.dismiss();
                       // Toast.makeText(PlayersFilter.this, "No Players...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<PlayersListResponse> call, Throwable t) {
                    progressDialog1.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(PlayersFilter.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });

        }

        if(DistrictId!=null){


            final ProgressDialog progressDialog1 = new ProgressDialog(PlayersFilter.this);
            progressDialog1.setMessage("Loading.....");
            progressDialog1.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PlayersListResponse> call1 = apiInterface.adminPlayersFilterDst(DistrictId);
            call1.enqueue(new Callback<PlayersListResponse>() {
                @Override
                public void onResponse(Call<PlayersListResponse> call, Response<PlayersListResponse> response) {

                    if (response.code() == 200) {
                        progressDialog1.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<PlayersListResponse.DataBean> dataBeans = response.body().getData();

                        //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                        recyclerView.setLayoutManager(new LinearLayoutManager(PlayersFilter.this));
                        recyclerAdapter = new RecyclerAdapterPlayerList(PlayersFilter.this, dataBeans);
                        recyclerView.setAdapter(recyclerAdapter);
                    } else if (response.code() != 200) {
                        progressDialog1.dismiss();
                        //Toast.makeText(PlayersFilter.this, "No Players...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<PlayersListResponse> call, Throwable t) {
                    progressDialog1.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(PlayersFilter.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });

        }

        if(MandalId!=null){

            final ProgressDialog progressDialog1 = new ProgressDialog(PlayersFilter.this);
            progressDialog1.setMessage("Loading.....");
            progressDialog1.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PlayersListResponse> call1 = apiInterface.adminPlayersFilterMandal(MandalId);
            call1.enqueue(new Callback<PlayersListResponse>() {
                @Override
                public void onResponse(Call<PlayersListResponse> call, Response<PlayersListResponse> response) {

                    if (response.code() == 200) {
                        progressDialog1.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<PlayersListResponse.DataBean> dataBeans = response.body().getData();

                        //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                        recyclerView.setLayoutManager(new LinearLayoutManager(PlayersFilter.this));
                        recyclerAdapter = new RecyclerAdapterPlayerList(PlayersFilter.this, dataBeans);
                        recyclerView.setAdapter(recyclerAdapter);
                    } else if (response.code() != 200) {
                        progressDialog1.dismiss();
                        //Toast.makeText(PlayersFilter.this, "No Players...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<PlayersListResponse> call, Throwable t) {
                    progressDialog1.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(PlayersFilter.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });


        }

        if(VillageId!=null){

            final ProgressDialog progressDialog1 = new ProgressDialog(PlayersFilter.this);
            progressDialog1.setMessage("Loading.....");
            progressDialog1.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PlayersListResponse> call1 = apiInterface.adminPlayersFilterVillage(VillageId);
            call1.enqueue(new Callback<PlayersListResponse>() {
                @Override
                public void onResponse(Call<PlayersListResponse> call, Response<PlayersListResponse> response) {

                    if (response.code() == 200) {
                        progressDialog1.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<PlayersListResponse.DataBean> dataBeans = response.body().getData();

                        //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                        recyclerView.setLayoutManager(new LinearLayoutManager(PlayersFilter.this));
                        recyclerAdapter = new RecyclerAdapterPlayerList(PlayersFilter.this, dataBeans);
                        recyclerView.setAdapter(recyclerAdapter);
                    } else if (response.code() != 200) {
                        progressDialog1.dismiss();
                        //Toast.makeText(PlayersFilter.this, "No Players...", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<PlayersListResponse> call, Throwable t) {
                    progressDialog1.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(PlayersFilter.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });


        }



    }
}
