package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.ChangePassword;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.OTP;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AddUserResponse;
import com.igrand.ThreeWaySolutions.Response.EditUserResponse;
import com.igrand.ThreeWaySolutions.Response.UpdateUserResponse;
/*import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;*/
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditUserAdmin extends BaseActivity {
    ImageView back,imageupload;
    ApiInterface apiInterface;
    EditUserResponse.StatusBean statusBean;
    String MobileNumber;
    EditText email,numberofleads;
    TextView name,number;
    ImageView circleImageView;
    FrameLayout userprofile;
    Button edit;
    String Email,Mobile,Profile,Status,Active,imagePic,picturePath,MobileNumber1;
    RadioGroup rg;
    RadioButton active,inactive;

    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    Bitmap converetdImage;
    //private ArrayList<Image> images;
    String filepath;
    MultipartBody.Part body;
    String Image;
    File file= new File("");
    String emptydocument="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        back=findViewById(R.id.back);

        name=findViewById(R.id.name);
        number=findViewById(R.id.number);
        email=findViewById(R.id.email);
       // numberofleads=findViewById(R.id.numberofleads);
        circleImageView=findViewById(R.id.image);
        userprofile=findViewById(R.id.userprofile);
        edit=findViewById(R.id.edit);
        rg=findViewById(R.id.rg);
        active=findViewById(R.id.active);
        inactive=findViewById(R.id.inactive);


        rg.check(R.id.active);


        if(getIntent()!=null){

            MobileNumber=getIntent().getStringExtra("MobileNumber");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);




               /* ImagePicker.with(EditUserAdmin.this)                         //  Initialize ImagePicker with activity or fragment context
                        .setToolbarColor("#212121")         //  Toolbar color
                        .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
                        .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                        .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                        .setProgressBarColor("#4CAF50")     //  ProgressBar color
                        .setBackgroundColor("#212121")      //  Background color
                        .setCameraOnly(false)               //  Camera mode
                        .setMultipleMode(false)              //  Select multiple images or single image
                        .setFolderMode(true)                //  Folder mode
                        .setShowCamera(true)                //  Show camera button
                        .setFolderTitle("Albums")           //  Folder title (works with FolderMode = true)
                        .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
                        .setDoneTitle("Done")               //  Done button title
                        .setLimitMessage("You have reached selection limit")    // Selection limit message
                        .setMaxSize(10)                     //  Max images can be selected
                        .setSavePath("ImagePicker")         //  Image capture folder name
                        .setSelectedImages(images)          //  Selected images
                        .setAlwaysShowDoneButton(true)      //  Set always show done button in multiple mode
                        .setKeepScreenOn(true)              //  Keep screen on when selecting images
                        .start();*/
            }

        });





        final ProgressDialog progressDialog = new ProgressDialog(EditUserAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditUserResponse> call = apiInterface.adminEditUser(MobileNumber);
        call.enqueue(new Callback<EditUserResponse>() {
            @Override
            public void onResponse(Call<EditUserResponse> call, Response<EditUserResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    userprofile.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<EditUserResponse.DataBean> dataBeans=response.body().getData();

                    name.setText(dataBeans.get(0).getUsername());
                    number.setText(dataBeans.get(0).getMobile());
                    email.setText(dataBeans.get(0).getEmail());
                    //ImageFile=dataBeans.get(0).getProfile();
                    Picasso.get().load(dataBeans.get(0).getProfile()).error(R.drawable.profilepic).into(circleImageView);

                    Image=dataBeans.get(0).getProfile();


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(EditUserAdmin.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<EditUserResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(EditUserAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Email =email.getText().toString();
                Mobile=number.getText().toString();



                if (active.isChecked()) {

                    Active="1";


                } else if (inactive.isChecked()) {

                    Active="0";

                }

                updateUser(Email,Mobile,Active);

            }
        });

        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(circleImageView);

            bitmap = ((BitmapDrawable) circleImageView.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      /*  if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);

            try {
                if (images != null) {
                    for (int i = 0; i < images.size(); i++) {
                        Uri uri = Uri.fromFile(new File(images.get(0).getPath()));
                        Picasso.get().load(uri).into(circleImageView);
//                        Picasso.with(getApplicationContext()).load(uri).into(profile_image);
                        image = null;
                        String filepath = images.get(0).getPath();
                        if (filepath != null && !filepath.equals("")) {
                            image = new File(filepath);
                        }
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }*/
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                circleImageView.setImageBitmap(converetdImage);



            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
//            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
//
//            try {
//                if (images != null) {
//                    for (int i = 0; i < images.size(); i++) {
//                        Uri uri = Uri.fromFile(new File(images.get(0).getPath()));
//                        Picasso.get().load(uri).into(circleImageView);
////                        Picasso.with(getApplicationContext()).load(uri).into(profile_image);
//
//                        filepath = images.get(0).getPath();
//
//                        body = null;
//
//                        if (image != null) {
//                            try {
//                                File file = new File(filepath);
//                                if (file.exists()) {
//                                    RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
//                                    body = MultipartBody.Part.createFormData("profile", file.getName(), requestBody);
//                                }
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//                        }
//
//
//                    }
//                }
//            } catch (NullPointerException e) {
//                e.printStackTrace();
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    private void updateUser(String email, String mobile, String active) {


        final ProgressDialog progressDialog=new ProgressDialog(EditUserAdmin.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();



        MultipartBody.Part body = null;

        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("profile", image.getName(), requestFile);

        }else {

            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
            body = MultipartBody.Part.createFormData("profile", emptydocument, requestFile);
        }
        RequestBody emailid = RequestBody.create(MediaType.parse("multipart/form-data"), email);
        final RequestBody MobileNumber1 = RequestBody.create(MediaType.parse("multipart/form-data"), mobile);
        RequestBody StatusActive = RequestBody.create(MediaType.parse("multipart/form-data"), active);



        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UpdateUserResponse> call = apiInterface.adminUpdateUser(emailid,MobileNumber1,StatusActive,body);
        call.enqueue(new Callback<UpdateUserResponse>() {
            @Override
            public void onResponse(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    UpdateUserResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(EditUserAdmin.this, "User Edited Successfully...", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(EditUserAdmin.this, UsersListAdmin.class);
                    intent.putExtra("MobileNumber",MobileNumber);
                    startActivity(intent);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(EditUserAdmin.this, "Error...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<UpdateUserResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(EditUserAdmin.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });
    }
}
