package com.igrand.ThreeWaySolutions.Activities.AGENT;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.LeadDetailsSubAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentComment;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAgentImages;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterSubAgentDocuments;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AgentLeadsCommentsResponse;
import com.igrand.ThreeWaySolutions.Response.AgentUpdateSubAgentLeadResponse;
import com.igrand.ThreeWaySolutions.Response.AgentUpdateSubAgentRejectLeadResponse;

import java.util.Arrays;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsAgent extends BaseActivity {

    ImageView back, img1, img2, img3, img4, document, approved, approved1,approvedp,approveds,approvedl,approvedle,approvedt;
    String Id, Property, Date, Village, Checking, Status, Comments, GoogleLocation, Address, MarketingStatus, MarketingDate, CheckingDate,mobileNumber,UserType,ProcurementStatus, ProcurementDate;
    TextView id, property, date, village, checking, status, google, address, remarks, date1, status11, date11,statusp, datep,statusl,statuss,datel,dates,statusle,datele,acres,survey,mandal,district,statust,datet;
    Dialog dialog;
    LinearLayout remarks1;
    ApiInterface apiInterface;
    AgentLeadsCommentsResponse.StatusBean statusBean;
    RecyclerAdapterAgentComment recyclerAdapter;
    Button update,reject;
    AgentUpdateSubAgentLeadResponse.StatusBean statusBean1;
    AgentUpdateSubAgentRejectLeadResponse.StatusBean statusBean2;
    LinearLayout linear;

    String Document,Image,LesionDate,LesionStatus,Survey,Acres,District,Mandal,TechnicalStatus,TechnicalDate;
            String[] document2,imagedoc;
    RecyclerView recyclerView,recyclerVieww;
    RecyclerAdapterAgentDocuments recyclerAdapter1;
    String LegalStatus,SurveyStatus,LegalDate,SurveyDate,Latitude,Longitude,pdf;
    LinearLayout marketing,procurement;
    List<String> strings,stringsimg;
    String[] document3;
    RecyclerAdapterSubAgentDocuments recyclerAdapter2;
    RecyclerAdapterAgentImages recyclerAdapter3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details_agent);

        back = findViewById(R.id.back);
        id = findViewById(R.id.Id);
        property = findViewById(R.id.Property);
        date = findViewById(R.id.date);
        village = findViewById(R.id.Village);
        //checking=findViewById(R.id.Checking);
        //document = findViewById(R.id.document);
        google = findViewById(R.id.google);
        address = findViewById(R.id.address);
        //remarks=findViewById(R.id.remarks);
        status = findViewById(R.id.status1);
        date1 = findViewById(R.id.date1);
       // remarks1 = findViewById(R.id.remarks1);
        approved = findViewById(R.id.approved);

        status11 = findViewById(R.id.status11);
        approved1 = findViewById(R.id.approved1);
        //remarks11 = findViewById(R.id.remarks11);
        date11 = findViewById(R.id.date11);
        update = findViewById(R.id.update);

        datep = findViewById(R.id.datep);
        statusp = findViewById(R.id.statusp);
        approvedp = findViewById(R.id.approvedp);
        recyclerView = findViewById(R.id.recyclerView);

        statuss = findViewById(R.id.statuss);
        dates = findViewById(R.id.dates);
        approveds = findViewById(R.id.approveds);
        statusl = findViewById(R.id.statusl);
        datel = findViewById(R.id.datel);
        approvedl = findViewById(R.id.approvedl);
        statusle = findViewById(R.id.statusle);
        datele = findViewById(R.id.datele);
        approvedle = findViewById(R.id.approvedle);
       /* marketing = findViewById(R.id.marketing);
        procurement = findViewById(R.id.procurement);*/
        linear = findViewById(R.id.linear);
        reject = findViewById(R.id.reject);
        acres = findViewById(R.id.acres);
        survey = findViewById(R.id.survey);
        mandal = findViewById(R.id.mandal);
        district = findViewById(R.id.district);
        statust = findViewById(R.id.statust);
        datet = findViewById(R.id.datet);
        approvedt = findViewById(R.id.approvedt);
        recyclerVieww = findViewById(R.id.recyclerVieww);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                /*Intent intent=new Intent(LeadDetailsAgent.this, DashBoardAgent.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/
            }
        });


        if (getIntent() != null) {
            Id = getIntent().getStringExtra("ID");
            Property = getIntent().getStringExtra("Property");
            Date = getIntent().getStringExtra("Date");
            Village = getIntent().getStringExtra("Village");
            Checking = getIntent().getStringExtra("Checking");
            Document = getIntent().getStringExtra("Document");
            Image = getIntent().getStringExtra("Image");
            Comments = getIntent().getStringExtra("Comments");
            Latitude = getIntent().getStringExtra("Latitude");
            Longitude = getIntent().getStringExtra("Longitude");
            Address = getIntent().getStringExtra("Address");
            Status = getIntent().getStringExtra("Status");

            MarketingStatus = getIntent().getStringExtra("MarketingStatus");
            MarketingDate = getIntent().getStringExtra("MarketingDate");
            CheckingDate = getIntent().getStringExtra("CheckingDate");
            mobileNumber = getIntent().getStringExtra("MobileNumber");
            UserType = getIntent().getStringExtra("UserType");

            ProcurementDate = getIntent().getStringExtra("ProcurementDate");
            ProcurementStatus = getIntent().getStringExtra("ProcurementStatus");

            LegalStatus = getIntent().getStringExtra("LegalStatus");
            SurveyStatus = getIntent().getStringExtra("SurveyStatus");
            LegalDate = getIntent().getStringExtra("LegalDate");
            SurveyDate = getIntent().getStringExtra("SurveyDate");
            LesionStatus = getIntent().getStringExtra("LesionStatus");
            LesionDate = getIntent().getStringExtra("LesionDate");

            Acres = getIntent().getStringExtra("Acres");
            Survey = getIntent().getStringExtra("Survey");
            Mandal = getIntent().getStringExtra("Mandal");
            District = getIntent().getStringExtra("District");

            TechnicalDate = getIntent().getStringExtra("TechnicalDate");
            TechnicalStatus = getIntent().getStringExtra("TechnicalStatus");
        }

        acres.setText(Acres);
        survey.setText(Survey);
        mandal.setText(Mandal);
        district.setText(District);


       /* marketing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent intent=new Intent(LeadDetailsAgent.this, NearByProjectsList1.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });


        procurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LeadDetailsAgent.this, ProposalDocuments.class);
                intent.putExtra("ID",Id);
                startActivity(intent);

            }
        });*/





       if(Document.equals("")){

       }else {

           document2 = Document.split(",");

           strings = Arrays.asList(Document.split(","));

           for (int i = 0; i < strings.size(); i++) {


               document3 = strings.get(i).split("\\.");
               for (int j = 0; j < document2.length; j++) {

                   pdf = document2[j].substring(document2[j].length() - 3);
                   if (pdf.equals("pdf")) {

                       recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                       recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsAgent.this, document2, Document, strings, "key");
                       recyclerView.setAdapter(recyclerAdapter2);

                   } else {
                       recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                       recyclerAdapter1 = new RecyclerAdapterAgentDocuments(LeadDetailsAgent.this, document2, strings, Document);
                       recyclerView.setAdapter(recyclerAdapter1);

                   }

               }

           }

       }
                imagedoc=Image.split(",");

                stringsimg = Arrays.asList(Image.split(","));

                recyclerVieww.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter3 = new RecyclerAdapterAgentImages(LeadDetailsAgent.this,imagedoc,stringsimg,Image,"keyagent");
                recyclerVieww.setAdapter(recyclerAdapter3);



          /*  pdf=  document3[1].substring(document3[1].length() - 3);

            if (pdf.equals("pdf")) {

                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter2 = new RecyclerAdapterSubAgentDocuments(LeadDetailsAgent.this,document2,Document,  strings,"key");
                recyclerView.setAdapter(recyclerAdapter2);

            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerAdapter1 = new RecyclerAdapterAgentDocuments(LeadDetailsAgent.this,document2,strings,Document);
                recyclerView.setAdapter(recyclerAdapter1);

            }
*/



        if(Status.equals("0")){
            linear.setVisibility(View.VISIBLE);


            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsAgent.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AgentUpdateSubAgentLeadResponse> call = apiInterface.agentupdatelead(Id);
                    call.enqueue(new Callback<AgentUpdateSubAgentLeadResponse>() {
                        @Override
                        public void onResponse(Call<AgentUpdateSubAgentLeadResponse> call, Response<AgentUpdateSubAgentLeadResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(LeadDetailsAgent.this, "Lead Activated successfully...", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(LeadDetailsAgent.this, DashBoardAgent.class);
                                intent.putExtra("MobileNumber",mobileNumber);
                                startActivity(intent);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(LeadDetailsAgent.this, "Error while updating lead...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<AgentUpdateSubAgentLeadResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(LeadDetailsAgent.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });

                }
            });


            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsAgent.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AgentUpdateSubAgentRejectLeadResponse> call = apiInterface.agentupdateleadreject(Id);
                    call.enqueue(new Callback<AgentUpdateSubAgentRejectLeadResponse>() {
                        @Override
                        public void onResponse(Call<AgentUpdateSubAgentRejectLeadResponse> call, Response<AgentUpdateSubAgentRejectLeadResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean2 = response.body() != null ? response.body().getStatus() : null;
                                Toast.makeText(LeadDetailsAgent.this, "Lead Deactivated successfully...", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(LeadDetailsAgent.this, DashBoardAgent.class);
                                intent.putExtra("MobileNumber",mobileNumber);
                                startActivity(intent);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(LeadDetailsAgent.this, "Error while updating lead...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<AgentUpdateSubAgentRejectLeadResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(LeadDetailsAgent.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });




                }
            });



        }


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Latitude + "," + Longitude + "";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });

        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //checking.setText(Checking);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        //remarks1.setText(Comments);

       /* remarks1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final RecyclerView recyclerView;

                dialog = new Dialog(LeadDetailsAgent.this);
                dialog.setContentView(R.layout.dialogboxremarks);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                recyclerView = dialog.findViewById(R.id.text);





                final ProgressDialog progressDialog = new ProgressDialog(LeadDetailsAgent.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AgentLeadsCommentsResponse> call = apiInterface.agentLeadsComments(Id);
                call.enqueue(new Callback<AgentLeadsCommentsResponse>() {
                    @Override
                    public void onResponse(Call<AgentLeadsCommentsResponse> call, Response<AgentLeadsCommentsResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            List<AgentLeadsCommentsResponse.DataBean> dataBeans=response.body().getData();
                            //Toast.makeText(LeadDetailsAgent.this, "Leads List...", Toast.LENGTH_SHORT).show();
                            recyclerView.setLayoutManager(new LinearLayoutManager(LeadDetailsAgent.this));
                            recyclerAdapter = new RecyclerAdapterAgentComment(LeadDetailsAgent.this,dataBeans);
                            recyclerView.setAdapter(recyclerAdapter);
                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(LeadDetailsAgent.this, "No Comments...", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AgentLeadsCommentsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(LeadDetailsAgent.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();
                    }
                });




            }
        });*/

        id.setText(Id);
        property.setText(Property);
        date.setText(Date);
        village.setText(Village);
        //google.setText(GoogleLocation);
        address.setText(Address);
        //remarks.setText(Comments);
        date1.setText(Date);
        // remarks1.setText(Comments);


        if (Checking != null) {

            if (Checking.equals("0")) {

                status.setVisibility(View.VISIBLE);
                status.setText("Rejected");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.rejected);
                date1.setText(CheckingDate);

            } else if (Checking.equals("1")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Approved");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.approved);
                date1.setText(CheckingDate);
            } else if (Checking.equals("2")) {
                status.setVisibility(View.VISIBLE);
                status.setText("Pending");
                approved.setVisibility(View.VISIBLE);
                approved.setImageResource(R.drawable.question);
                date1.setText(CheckingDate);
            }


            if (MarketingStatus != null) {

                if (MarketingStatus.equals("0")) {

                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Rejected");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.rejected);
                    date11.setText(MarketingDate);

                } else if (MarketingStatus.equals("1")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Approved");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.approved);
                    date11.setText(MarketingDate);
                } else if (MarketingStatus.equals("2")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Processing");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.question);
                    date11.setText(MarketingDate);
                }else if (MarketingStatus.equals("3")) {
                    status11.setVisibility(View.VISIBLE);
                    status11.setText("Open");
                    approved1.setVisibility(View.VISIBLE);
                    approved1.setImageResource(R.drawable.open);
                    date11.setText(MarketingDate);
                }


                if (ProcurementStatus != null) {

                    if (ProcurementStatus.equals("4")) {

                        statusp.setText("Rejected");
                        approvedp.setImageResource(R.drawable.rejected);
                        datep.setText(ProcurementDate);

                    } else if (ProcurementStatus.equals("5")) {
                        statusp.setText("Approved");
                        approvedp.setImageResource(R.drawable.approved);
                        datep.setText(ProcurementDate);
                    } else if (ProcurementStatus.equals("3")) {
                        statusp.setText("Processing");
                        approvedp.setImageResource(R.drawable.processing);
                        datep.setText(ProcurementDate);
                    } else if (ProcurementStatus.equals("2")) {
                        statusp.setText("Open");
                        approvedp.setImageResource(R.drawable.open);
                        datep.setText(ProcurementDate);
                    }

                }

                if (TechnicalStatus != null) {

                    if (TechnicalStatus.equals("2")) {

                        statust.setVisibility(View.VISIBLE);
                        statust.setText("Open");
                        approvedt.setVisibility(View.VISIBLE);
                        approvedt.setImageResource(R.drawable.open);
                        datet.setText(TechnicalDate);

                    } else if (TechnicalStatus.equals("1")) {
                        statust.setVisibility(View.VISIBLE);
                        statust.setText("Approved");
                        approvedt.setVisibility(View.VISIBLE);
                        approvedt.setImageResource(R.drawable.approved);
                        datet.setText(TechnicalDate);
                    } else if (TechnicalStatus.equals("0")) {
                        statust.setVisibility(View.VISIBLE);
                        statust.setText("Reject");
                        approvedt.setVisibility(View.VISIBLE);
                        approvedt.setImageResource(R.drawable.rejected);
                        datet.setText(TechnicalDate);

                    }
                }
                }

            }

        }
    }
