package com.igrand.ThreeWaySolutions.Activities.CHECKING;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.NotificationsAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PrefManagerAgent;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerChecking;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterNotificationsAgent;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterNotificationsChecking;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.NotificationsAgentListResponse;

import java.util.HashMap;
import java.util.List;

public class NotificationsChecking extends BaseActivity {

    ImageView back;
    RecyclerView recyclerView;
    RecyclerAdapterNotificationsChecking recyclerAdapterNotificationsChecking;
    NotificationsAgentListResponse.StatusBean statusBean;
    LinearLayout add;
    ApiInterface apiInterface;
    String MobileNumber;
    PrefManagerChecking prefManagerAgent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications_checking);

        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(NotificationsChecking.this, DashBoardChecking.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        prefManagerAgent=new PrefManagerChecking(NotificationsChecking.this);
        HashMap<String, String> profile=prefManagerAgent.getUserDetails();
        MobileNumber=profile.get("mobilenumber");
        // UserName=profile.get("username");

        final ProgressDialog progressDialog = new ProgressDialog(NotificationsChecking.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationsAgentListResponse> call = apiInterface.notificationListChecking(MobileNumber);
        call.enqueue(new Callback<NotificationsAgentListResponse>() {
            @Override
            public void onResponse(Call<NotificationsAgentListResponse> call, Response<NotificationsAgentListResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;


                    List<NotificationsAgentListResponse.DataBean> dataBeans1 = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(NotificationsChecking.this));
                    recyclerAdapterNotificationsChecking = new RecyclerAdapterNotificationsChecking(NotificationsChecking.this,dataBeans1);
                    recyclerView.setAdapter(recyclerAdapterNotificationsChecking);
                    Toast.makeText(NotificationsChecking.this, "Notification's List...", Toast.LENGTH_SHORT).show();


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    //Toast.makeText(AddNotificationsAdmin.this, "Please Check the Password and try again...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationsAgentListResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(NotificationsChecking.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });
    }
}
