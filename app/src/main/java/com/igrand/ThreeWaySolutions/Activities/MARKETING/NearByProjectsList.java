package com.igrand.ThreeWaySolutions.Activities.MARKETING;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.LeadDetailsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.ProjectsList;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterCheckingDocuments;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterNearByProjectList;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterProjectList;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.AdminProjectsList;
import com.igrand.ThreeWaySolutions.Response.NearByProjectResponse;

import java.util.List;

public class NearByProjectsList extends BaseActivity {

    RecyclerView recyclerView;
    ImageView back;
    RecyclerAdapterNearByProjectList recyclerUser;
    ApiInterface apiInterface;
    NearByProjectResponse.StatusBean statusBean;
    Button add;
    String mobileNumber,ID,Key;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by_projects_list);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);


        if(getIntent()!=null){

            mobileNumber=getIntent().getStringExtra("MobileNumber");
            ID=getIntent().getStringExtra("ID");
            Key=getIntent().getStringExtra("Key");
        }

        if(Key!=null){
            if(Key.equals("Key")){
                add.setVisibility(View.GONE);
            }
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent=new Intent(NearByProjectsList.this, DashBoardMarketing.class);
                intent.putExtra("MobileNumber",mobileNumber);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
*/
                finish();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(NearByProjectsList.this, NearbyProjects1.class);
                intent.putExtra("MobileNumber",mobileNumber);
                intent.putExtra("ID",ID);
                startActivity(intent);
            }
        });





        final ProgressDialog progressDialog = new ProgressDialog(NearByProjectsList.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NearByProjectResponse> call = apiInterface.nearByProjectList(ID);
        call.enqueue(new Callback<NearByProjectResponse>() {
            @Override
            public void onResponse(Call<NearByProjectResponse> call, Response<NearByProjectResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    Toast.makeText(NearByProjectsList.this, "Project List......", Toast.LENGTH_SHORT).show();
                    List<NearByProjectResponse.DataBean> dataBeans=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(NearByProjectsList.this));
                    recyclerUser = new RecyclerAdapterNearByProjectList(NearByProjectsList.this,dataBeans);
                    recyclerView.setAdapter(recyclerUser);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(NearByProjectsList.this, "No project's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<NearByProjectResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(NearByProjectsList.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });





    }
}
