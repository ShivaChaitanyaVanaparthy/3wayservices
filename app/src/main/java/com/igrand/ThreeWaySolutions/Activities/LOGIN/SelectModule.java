package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.igrand.ThreeWaySolutions.R;

public class SelectModule extends BaseActivity {

    CardView residential,rental,manavillage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_module);

        residential=findViewById(R.id.residential);
        rental=findViewById(R.id.rental);
        manavillage=findViewById(R.id.manavillage);


        residential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SelectModule.this,Login.class);
                startActivity(intent);
            }
        });

        rental.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SelectModule.this, "Coming Soon..!!", Toast.LENGTH_SHORT).show();
            }
        });

        manavillage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SelectModule.this,ManaVillageRegister.class);
                startActivity(intent);
            }
        });

    }
}
