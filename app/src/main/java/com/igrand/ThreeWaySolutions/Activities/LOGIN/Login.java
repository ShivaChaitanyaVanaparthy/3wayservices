package com.igrand.ThreeWaySolutions.Activities.LOGIN;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.igrand.ThreeWaySolutions.Activities.ADMIN.DashBoardAdmin;
import com.igrand.ThreeWaySolutions.Activities.ADMIN1.DashBoardAdmin1;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin;
import com.igrand.ThreeWaySolutions.Activities.AGENT.DashBoardAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.PrefManagerAgent;
import com.igrand.ThreeWaySolutions.Activities.AGENT.SUBAGENT.DashBoardSubAgent;
import com.igrand.ThreeWaySolutions.Activities.CHECKING.DashBoardChecking;
import com.igrand.ThreeWaySolutions.Activities.INVESTOR.DashBoardInvestor;
import com.igrand.ThreeWaySolutions.Activities.LEGAL.DashBoardLegal;
import com.igrand.ThreeWaySolutions.Activities.LESION.DashBoardLesion;
import com.igrand.ThreeWaySolutions.Activities.LABOUR.DashBoardLabour;
import com.igrand.ThreeWaySolutions.Activities.LABOURCONTRACTOR.DashBoardLabourContract;
import com.igrand.ThreeWaySolutions.Activities.MARKETING.DashBoardMarketing;
import com.igrand.ThreeWaySolutions.Activities.MARKETING_AGENT.DashBoardMarketingAgent;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerAdmin1;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerChecking;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerLegal;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerLesion;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerMarketing;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerProcurement;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerSubAgent;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerSurvey;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical;
import com.igrand.ThreeWaySolutions.Activities.PREFMANAGER.PrefManagerTechnical1;
import com.igrand.ThreeWaySolutions.Activities.PROCUREMENT.DashBoardProcurement;
import com.igrand.ThreeWaySolutions.Activities.PROJECT_CONTRACTOR.DashBoardProject;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.DashBoardSiteEngineer;
import com.igrand.ThreeWaySolutions.Activities.SITEENGINEER.PrefManagerSiteEngineer;
import com.igrand.ThreeWaySolutions.Activities.SURVEY.DashBoardSurvey;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL.DashBoardTechnical;
import com.igrand.ThreeWaySolutions.Activities.TECHNICAL1.DashBoardTechnical1;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.LoginAdminResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Login extends BaseActivity {

    TextView forgetpassword;
    Button loginbutton;
    EditText mobile,password;
    String MobileNumber,Password,UserType,AgentId,UserName,agent_id,checking,user_id;
    ApiInterface apiInterface;
    LoginAdminResponse.StatusBean statusBean;
    private Boolean exit = false;
    PrefManagerAgent prefManagerAgent;
    PrefManagerAdmin prefManagerAdmin;
    PrefManagerAdmin1 prefManagerAdmin1;
    PrefManagerChecking prefManagerChecking;
    PrefManagerMarketing prefManagerMarketing;
    PrefManagerTechnical prefManagerTechnical;
    PrefManagerTechnical1 prefManagerTechnical1;
    PrefManagerProcurement prefManagerProcurement;
    PrefManagerSubAgent prefManagerSubAgent;
    PrefManagerSiteEngineer prefManagerSiteEngineer;
    PrefManagerLegal prefManagerLegal;
    PrefManagerSurvey prefManagerSurvey;
    PrefManagerLesion prefManagerLesion;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_admin);

        forgetpassword=findViewById(R.id.forgetpassword);
        loginbutton=findViewById(R.id.loginbutton);
        mobile=findViewById(R.id.mobile);
        password=findViewById(R.id.password);

//        checking=getIntent().getStringExtra("Home");

        prefManagerAgent=new PrefManagerAgent(Login.this);
        prefManagerAdmin=new PrefManagerAdmin(Login.this);
        prefManagerAdmin1=new PrefManagerAdmin1(Login.this);
        prefManagerChecking=new PrefManagerChecking(Login.this);
        prefManagerMarketing=new PrefManagerMarketing(Login.this);
        prefManagerTechnical=new PrefManagerTechnical(Login.this);
        prefManagerTechnical1=new PrefManagerTechnical1(Login.this);
        prefManagerProcurement=new PrefManagerProcurement(Login.this);
        prefManagerSubAgent=new PrefManagerSubAgent(Login.this);
        prefManagerLegal=new PrefManagerLegal(Login.this);
        prefManagerSurvey=new PrefManagerSurvey(Login.this);
        prefManagerLesion=new PrefManagerLesion(Login.this);
        prefManagerSiteEngineer=new PrefManagerSiteEngineer(Login.this);


        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do something, e.g. set your TextView here via .setText()
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value = String.valueOf(s.length());

                if (value.equals("6")) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);

                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(Login.this,ForgetPassword.class);
                startActivity(intent);
            }
        });

        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MobileNumber=mobile.getText().toString();
                Password=password.getText().toString();
                Integer length = mobile.getText().length();


                if(MobileNumber.equals("")|| length != 10) {

                    Toast.makeText(Login.this, "Please Enter Valid Mobile Number...", Toast.LENGTH_SHORT).show();
                } else if(Password.equals("")) {

                    Toast.makeText(Login.this, "Please Enter Password...", Toast.LENGTH_SHORT).show();
                }

                else if (!MobileNumber.isEmpty() && !Password.isEmpty()) {
                    {

                        final ProgressDialog progressDialog = new ProgressDialog(Login.this);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();
                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<LoginAdminResponse> call = apiInterface.adminLogin(MobileNumber,Password);
                        call.enqueue(new Callback<LoginAdminResponse>() {
                            @Override
                            public void onResponse(Call<LoginAdminResponse> call, Response<LoginAdminResponse> response) {



                             //   if(response.body()!=null) {


                                   // statusBean = response.body() != null ? response.body().getStatus() : null;




                                    if (response.code() == 200)

                                    {

                                        statusBean = response.body() != null ? response.body().getStatus() : null;
                                        progressDialog.dismiss();
                                        List<LoginAdminResponse.DataBean> dataBean = response.body().getData();


                                        UserType = dataBean.get(0).getUsertype();
                                        AgentId = dataBean.get(0).getUser_id();
                                        UserName = dataBean.get(0).getUsername();
                                        agent_id = dataBean.get(0).getAgent_id();
                                        user_id = dataBean.get(0).getUser_id();

                                        statusBean = response.body() != null ? response.body().getStatus() : null;


                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();

                                        if (UserType.equals("agent")) {
                                            Intent intent = new Intent(Login.this, DashBoardAgent.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            intent.putExtra("AgentId", AgentId);
                                            intent.putExtra("Name", UserName);
                                            startActivity(intent);
                                            prefManagerAgent.createLogin(MobileNumber, AgentId, UserName);

                                        } else if (UserType.equals("admin")) {


                                            dialog = new Dialog(Login.this);
                                            dialog.setContentView(R.layout.dialogbox);
                                            dialog.show();
                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                            Window window = dialog.getWindow();
                                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                                            RadioButton phase1,phase2;
                                            RadioGroup rg;
                                            Button submit;

                                            rg=dialog.findViewById(R.id.rg);
                                            phase1=dialog.findViewById(R.id.phase1);
                                            phase2=dialog.findViewById(R.id.phase2);
                                            submit=dialog.findViewById(R.id.submit);

                                            submit.setOnClickListener(view1 -> {
                                                if (phase1.isChecked()) {

                                                    dialog.dismiss();

                                                    Intent intent = new Intent(Login.this, DashBoardAdmin.class);
                                                    intent.putExtra("MobileNumber", MobileNumber);
                                                    startActivity(intent);
                                                    prefManagerAdmin.createLogin(MobileNumber, UserName);

                                                } else if (phase2.isChecked()) {

                                                    dialog.dismiss();
                                                    Intent intent = new Intent(Login.this, DashBoardAdmin1.class);
                                                    intent.putExtra("MobileNumber", MobileNumber);
                                                    startActivity(intent);
                                                    prefManagerAdmin1.createLogin(MobileNumber, UserName);
                                                } else {

                                                    Toast.makeText(Login.this, "Please select PHASE-1/PHASE-2...", Toast.LENGTH_SHORT).show();
                                                }

                                            });







                                        } else if (UserType.equals("checking_team")) {

                                            Intent intent = new Intent(Login.this, DashBoardChecking.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                            prefManagerChecking.createLogin(MobileNumber, UserName);
                                        } else if (UserType.equals("marketing_team")) {

                                            Intent intent = new Intent(Login.this, DashBoardMarketing.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                            prefManagerMarketing.createLogin(MobileNumber, UserName);
                                        }

                                        else if (UserType.equals("techenical_team")) {


                                            dialog = new Dialog(Login.this);
                                            dialog.setContentView(R.layout.dialogbox);
                                            dialog.show();
                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                            Window window = dialog.getWindow();
                                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                                            RadioButton phase1,phase2;
                                            RadioGroup rg;
                                            Button submit;

                                            rg=dialog.findViewById(R.id.rg);
                                            phase1=dialog.findViewById(R.id.phase1);
                                            phase2=dialog.findViewById(R.id.phase2);
                                            submit=dialog.findViewById(R.id.submit);

                                            submit.setOnClickListener(view1 -> {
                                                if (phase1.isChecked()) {

                                                    dialog.dismiss();

                                                    Intent intent = new Intent(Login.this, DashBoardTechnical.class);
                                                    intent.putExtra("MobileNumber", MobileNumber);
                                                    startActivity(intent);
                                                    prefManagerTechnical.createLogin(MobileNumber, UserName);

                                                } else if (phase2.isChecked()) {

                                                    dialog.dismiss();
                                                    Intent intent = new Intent(Login.this, DashBoardTechnical1.class);
                                                    intent.putExtra("MobileNumber", MobileNumber);
                                                    startActivity(intent);
                                                    prefManagerTechnical1.createLogin(MobileNumber, UserName);
                                                } else {

                                                    Toast.makeText(Login.this, "Please select PHASE-1/PHASE-2...", Toast.LENGTH_SHORT).show();
                                                }

                                            });




                                        }

                                        else if (UserType.equals("procurement_team")) {

                                            Intent intent = new Intent(Login.this, DashBoardProcurement.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                            prefManagerProcurement.createLogin(MobileNumber, UserName, AgentId);
                                        } else if (UserType.equals("legal_team")) {
                                            Intent intent = new Intent(Login.this, DashBoardLegal.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            prefManagerLegal.createLogin(MobileNumber, UserName);

                                            startActivity(intent);
                                        } else if (UserType.equals("survey_team")) {
                                            Intent intent = new Intent(Login.this, DashBoardSurvey.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            prefManagerSurvey.createLogin(MobileNumber, UserName);
                                            startActivity(intent);
                                        } else if (UserType.equals("lesion_team")) {
                                            Intent intent = new Intent(Login.this, DashBoardLesion.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                            prefManagerLesion.createLogin(MobileNumber, UserName);
                                        } else if (UserType.equals("project_contractor")) {
                                            Intent intent = new Intent(Login.this, DashBoardProject.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                        } else if (UserType.equals("labour_contractor")) {
                                            Intent intent = new Intent(Login.this, DashBoardLabourContract.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                        } else if (UserType.equals("labour")) {
                                            Intent intent = new Intent(Login.this, DashBoardLabour.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                        } else if (UserType.equals("marketing_agent")) {
                                            Intent intent = new Intent(Login.this, DashBoardMarketingAgent.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                        } else if (UserType.equals("investor")) {
                                            Intent intent = new Intent(Login.this, DashBoardInvestor.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            startActivity(intent);
                                        } else if (UserType.equals("sub_agent")) {
                                            Intent intent = new Intent(Login.this, DashBoardSubAgent.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            intent.putExtra("agent_id", agent_id);
                                            startActivity(intent);
                                            prefManagerSubAgent.createLogin(MobileNumber, UserName, agent_id);
                                        }else if (UserType.equals("site_engineer")) {
                                            Intent intent = new Intent(Login.this, DashBoardSiteEngineer.class);
                                            intent.putExtra("MobileNumber", MobileNumber);
                                            intent.putExtra("agent_id", agent_id);
                                            intent.putExtra("name", dataBean.get(0).getUsername());
                                            startActivity(intent);
                                            prefManagerSiteEngineer.createLogin(MobileNumber, user_id,UserName);
                                        }

                                    }

                                    else if(response.code()!=200)
                                    {

                                        progressDialog.dismiss();
                                        Converter<ResponseBody, APIError> converter =
                                                ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                        APIError error;
                                        try {
                                            error = converter.convert(response.errorBody());
                                            APIError.StatusBean status=error.getStatus();
                                            Toast.makeText(Login.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                                        } catch (IOException e) { e.printStackTrace(); }

                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                                    }

                                }

                                /*else
                                    {
                                        progressDialog.dismiss();
                                        Toast.makeText(Login.this,  response.message(), Toast.LENGTH_SHORT).show();

                                    }*/

                         // }


                            @Override
                            public void onFailure(Call<LoginAdminResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                Toast toast= Toast.makeText(Login.this,t.getMessage() , Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();


                            }
                        });

                    }


                } else {

                    Toast.makeText(Login.this, "Please Enter Credentials...", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }
}
