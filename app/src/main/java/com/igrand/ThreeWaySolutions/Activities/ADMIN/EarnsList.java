package com.igrand.ThreeWaySolutions.Activities.ADMIN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.igrand.ThreeWaySolutions.Activities.LOGIN.BaseActivity;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterAdmin;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEarns;
import com.igrand.ThreeWaySolutions.Adapters.RecyclerAdapterEarnsAdmin;
import com.igrand.ThreeWaySolutions.Client.ApiClient;
import com.igrand.ThreeWaySolutions.Interface.ApiInterface;
import com.igrand.ThreeWaySolutions.R;
import com.igrand.ThreeWaySolutions.Response.EarnsListResponse;
import com.igrand.ThreeWaySolutions.Response.LeadsListResponse;

import java.util.List;

public class EarnsList extends BaseActivity {

    ImageView back;
    RecyclerView recyclerView;
    Button add;
    RecyclerAdapterEarnsAdmin recyclerAdapter;
    ApiInterface apiInterface;
    ShimmerFrameLayout mShimmerViewContainer;
    EarnsListResponse.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earns_list);

        back=findViewById(R.id.back);
        recyclerView=findViewById(R.id.recyclerView);
        add=findViewById(R.id.add);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(EarnsList.this,AddEarn.class);
                startActivity(intent);
            }
        });

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EarnsListResponse> call = apiInterface.adminEarnList();
        call.enqueue(new Callback<EarnsListResponse>() {
            @Override
            public void onResponse(Call<EarnsListResponse> call, Response<EarnsListResponse> response) {

                if (response.code() == 200) {
                    // progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<EarnsListResponse.DataBean> dataBeans = response.body().getData();
                    //Toast.makeText(DashBoardAdmin.this, "Leads List...", Toast.LENGTH_SHORT).show();
                    recyclerView.setLayoutManager(new LinearLayoutManager(EarnsList.this));
                    recyclerAdapter=new RecyclerAdapterEarnsAdmin(EarnsList.this,dataBeans);
                    recyclerView.setAdapter(recyclerAdapter);
                } else if (response.code() != 200) {
                    //progressDialog1.dismiss();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    Toast.makeText(EarnsList.this, "No Earn's...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<EarnsListResponse> call, Throwable t) {
                // progressDialog1.dismiss();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(EarnsList.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

}
